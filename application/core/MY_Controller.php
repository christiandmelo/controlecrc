<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	
	public function __construct() {
		parent::__construct ();
	    /* verifica se o usuário está logado */
	    usuarioLogado (true);
		
	    $this->load->model("global/Macesso","Macesso");
		if(!$this->Macesso->hasAcesso()){
			die(" Você não tem permissão a essa página!");
		}
    }
	
	public $data;
	public $strTabela;
	
	public function index(){
		// Inicia os modulos necessarios
		$this->load->model("global/Mcrud","Mcrud");
		

		// Bacalhau
		$local = '#/'.$this->data['lista']['precaminho'].$this->data['lista']['classe'];
		$this->data['breadcrumb'] = $this->Macesso->getBreadcrumb( $local );
		
		// Verifica o tipo de controlador
		if( $this->data['tipo'] == 'Parametro' ){
			$this->load->view('global/crud/Vparametro',$this->data);
			return;
		}
		
		if(!isset($this->data['lista']['autolista']) || $this->data['lista']['autolista'] == true){
			$this->Mcrud->setStrTable($this->strTabela);
			
			// Verifica os campos necessarios para buscar somente os dados do cliente
			if(isset($this->data["chave"]) && is_array( $this->data["chave"] ) && count($this->data["chave"]) >= 1){
				$arrFiltroLista = array();
				foreach( $this->data['chave'] as $key => $value ){
						$arrFiltroLista['where'][$key] = $_SESSION[strtolower($value)];
				}
				$arrDataLista = array_merge($arrFiltroLista,$this->data["tabela"]);
			}else{
				$arrDataLista = $this->data["tabela"];
			}
			$this->data['lista']['dados'] = $this->Mcrud->getDados($arrDataLista);
		}
		
		$this->load->view("global/crud/Vfiltro",$this->data);
		
	}
	
	public function lista(){
		
		$arrFiltroLista = array();
		foreach( $this->data['form'] as $keyTab => $rsTab ):
			foreach( $rsTab as $keyForm => $rsForm ):
				if(isset($rsForm['filtro']) and $rsForm['filtro']){
					if($_REQUEST['filtro'.$rsForm['idname']] != ''){
						$arrFiltroLista['like'][$keyForm] = $_REQUEST['filtro'.$rsForm['idname']];
					}
					
				}
			endforeach;
		endforeach; 
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable($this->strTabela);
		// Verifica os campos necessarios para buscar somente os dados do cliente
		if( isset($this->data["chave"] ) && is_array( $this->data["chave"] ) && count($this->data["chave"]) >= 1 ){
			foreach( $this->data['chave'] as $key => $value ){
					$arrFiltroLista['where'][$key] = $_SESSION[strtolower($value)];
			}
		}else{
			$arrDataLista = $this->data["tabela"];
		}
		$arrDataLista = array_merge($arrFiltroLista,$this->data["tabela"]);

		$this->data['lista']['dados'] = $this->Mcrud->getDados($arrDataLista);
		$this->load->view('global/crud/Vlista',$this->data);
		
	}
	
	//função para novo ou editar um produto
	public function novo($codigoTabela = null){
		$this->load->model("global/Mcrud","Mcrud");
		
		//if que controla o editar no metodo
		if(isset($codigoTabela) && $codigoTabela != null){
			
			/*adiciona o cabeçalho de pesquisa no array do edita*/
			foreach($this->data['form'] as $key => $arrCampos){
				foreach($arrCampos as $key => $values){
					if(!isset($values["ativo_salvar"]) || $values["ativo_salvar"] == true){
						$arrKey = explode(".",$key);
						$this->data['edita']['cabecalho'][$key] = $arrKey[1];
					}		
				}
			}
						
			//Adiciona todas as chaves da tabela no where da consulta
			if(isset($this->data['chave'])){
				foreach( $this->data['chave'] as $key => $values){
						$arrChave = explode('.',$key);
						$this->data['edita']['where'] = array($key => $_SESSION[strtolower($arrChave[1])]);
				}
			}
			//Adiciona o codigo da tabela no where da consulta
			$this->data['edita']['where'][$this->data['lista']['codigo']] = $codigoTabela;
			
			$this->Mcrud->setStrTable($this->strTabela);
			$this->data['lista']['dados'] = $this->Mcrud->getDados($this->data["edita"]);
			$this->data['lista']['existeID'] = 1;
		}
		
		$this->load->view('global/crud/Vnovo',$this->data);
		
	}
	
	public function salvaEdicao($tipoRetorno = 1){
		
		// CRUD
		$this->load->model("global/Mcrud","Mcrud");
		
		// Contorno para as abas no novo/editar funcionar sem grandes alterações
		$arrInformacoesFormulario = array();
		foreach( $this->data['form'] as $arrValue) {
			$arrInformacoesFormulario = array_merge( $arrInformacoesFormulario , $arrValue );
		}
		
		if (isset($this->data['camposCompForm'])){
			foreach( $this->data['camposCompForm'] as $keyCompForm => $valueCompForm):
				$arrKeyDadoCompForm = explode("_",$keyCompForm);
				if($arrKeyDadoCompForm[1] == $_POST[$arrKeyDadoCompForm[0]]){
						$arrInformacoesFormulario = array_merge($arrInformacoesFormulario, $valueCompForm);
				}
			endforeach;
		}
		
		$arrValoresDaChave = null;
		$arrFiltroEdicao = null;
		$validacao = true;
		
		//se a tabela necessita da autoinc ele preenche essa opção forçando ao crud pesquisar na tabela GLAUTOINDICE	
		if($this->data['lista']['autoinc'] == "sim"){
			foreach ($this->data['chave'] as $key => $value){
				$arrValoresDaChave[$key] = $_SESSION[strtolower($value)];
				$arrDadosEdicao[$key] = $_SESSION[strtolower($value)];
			}
		}
		
		// Passa por todos os valores informardos no CRUD
		foreach( $arrInformacoesFormulario as $key => $values){
			
			if(!isset($values["ativo_salvar"]) || $values["ativo_salvar"] == true){
			//$this->data['cabe']['where'] = array($values => $chave);
				if(isset($values["idname"])){
				
					$auxIdName = $values["idname"];
					$auxTypePost = $values["tipo"];
					
					// valida os campos
					if( isset( $values["required"] ) && $values["required"] === true && $_REQUEST[$auxIdName] == '' ){
						$data['texto'] = ' requrido , gentileza tentar novamente ou procurar o administrador do sistema!';
						$data['action'] = '';
						$validacao = false;
						break;
					}
					if( isset( $values["mask"] ) && $values["mask"] !== '' && $_REQUEST[$auxIdName] !== '' ){
						$regex = $values["mask"];
						$regex = str_replace('9', '[0-9]', $regex);
						$regex = str_replace('a', '[A-Za-z]', $regex);
						$regex = str_replace('*', '[A-Za-z0-9]', $regex);
						if( !preg_match( '|'.$regex.'|' , $_REQUEST[$auxIdName] ) ){
							$data['texto'] = ' Máscara com valor '.$_REQUEST[$auxIdName].' está incorreta, gentileza tentar novamente ou procurar o administrador do sistema!';
							$data['action'] = '';
							$validacao = false;
							break;
						}
					}
					if( isset( $values["required-type"] ) && $values["required-type"] !== '' and $_REQUEST[$auxIdName] !== '' ){
						switch( $values["required-type"] ){		
							case 'email':
								if( !preg_match( '/.+@.+\./' , $_REQUEST[$auxIdName] ) ){
									$data['texto'] = 'EMAIL , gentileza tentar novamente ou procurar o administrador do sistema!';
									$data['action'] = '';
									$validacao = false;
									break 2;
								}
					            break;
							case 'number':
								if( !preg_match( '@^-?(?:\\d+|\\d{1,3}(?:,\\d{3})+)?(?:\\.\\d+)?$@' , $_REQUEST[$auxIdName] ) ){
									$data['texto'] = 'NUMBER , gentileza tentar novamente ou procurar o administrador do sistema!';
									$data['action'] = '';
									$validacao = false;
									break 2;
								}
					            break;
							case 'integer':
								if( !preg_match( '|^-?\\d+$|' , $_REQUEST[$auxIdName] ) ){
									$data['texto'] = 'Somente números , gentileza tentar novamente ou procurar o administrador do sistema!';
									$data['action'] = '';
									$validacao = false;
									break 2;
								}
								break;
							case 'digits':
								if( !preg_match( '|^\\d+$|' , $_REQUEST[$auxIdName] ) ){
									$data['texto'] = 'TESTE , gentileza tentar novamente ou procurar o administrador do sistema!';
									$data['action'] = '';
									$validacao = false;
									break 2;
								}
								break;
							case 'alphanum':
								if( !preg_match( '|^\\w+$|i' , $_REQUEST[$auxIdName] ) ){
									$data['texto'] = 'ALPHANUM , gentileza tentar novamente ou procurar o administrador do sistema!';
									$data['action'] = '';
									$validacao = false;
									break 2;
								}
								break;
							case 'url':
								if( !preg_match( '|(https?:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,4}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)|i' , $_REQUEST[$auxIdName] ) ){
									$data['texto'] = 'URL , gentileza tentar novamente ou procurar o administrador do sistema!';
									$data['action'] = '';
									$validacao = false;
									break 2;
								}
								break;
							default:
								$data['texto'] = 'Erro na validação do registro, gentileza tentar novamente ou procurar o administrador do sistema!';
								$data['action'] = '';
								$validacao = false;
					            break 2;
						}
					}
					
					//preeche a variavel com os dados a serem editados/inseridos
					if(isset($values['datepicker']) && $values['datepicker'] == true){
						$arrDadosEdicao[$key] = convertData($_REQUEST[$auxIdName]);
					}else if(isset($values['money']) && $values['money'] == true){
						$arrDadosEdicao[$key] = (implode("",explode(".", $_REQUEST[$auxIdName])));
					}else{
						$arrDadosEdicao[$key] = $_REQUEST[$auxIdName];
					}
					
					
					
					//Caso o código esteja preenchido, preenche o array de edição com o código da tabela
					if( $this->data['lista']['codigo'] == $key){
						if($_REQUEST[$auxIdName] != "")
							$arrFiltroEdicao[$key] = $_REQUEST[$auxIdName];
					}
				}
			}
		}
		
		if($validacao){
			/*inseri os registro Fk dependentes*/
			if(isset($this->data['dependencia_fk']) && !isset($arrFiltroEdicao)){
				$continue = true;
				if(isset($this->data['valida_dependencia_fk']) && $this->data['valida_dependencia_fk']["valida"] == true){
					$arrCampoValidarFK = explode("_",$this->data['valida_dependencia_fk']['campovalidar']);
					if($_POST[$arrCampoValidarFK[0]] != $arrCampoValidarFK[1]){
						$continue = false;
					}
				}
				if($continue){
					$tabelaMaster = $this->strTabela;
					$dadosEdicaoDep = array();
					$arrValoresDaChaveFK = array();				
					foreach($this->data['dependencia_fk'] as $keyDep => $valueDep):
						//se a tabela necessita da autoinc ele preenche essa opção forçando ao crud pesquisar na tabela GLAUTOINDICE	
						if(isset($valueDep['chave'])){
							foreach ($valueDep['chave'] as $keyChave => $valueChave){
								$arrValoresDaChaveFK[$keyChave] = $_SESSION[strtolower($valueChave)];
								$dadosEdicaoDep[$keyChave] = $_SESSION[strtolower($value)];
							}
						}
						//prepara os dados de salvar no banco
						foreach($valueDep['dadosEditar'] as $keyDadosDep => $valueDadosDep):
							$arrayKeyDadosDep = explode(".", $keyDadosDep);
							$dadosEdicaoDep[$keyDadosDep] = $arrDadosEdicao[$tabelaMaster.".".$arrayKeyDadosDep[1]];
						endforeach;
						
						
						$this->Mcrud->setStrTable($valueDep['tabela']);
						$retorno = $this->Mcrud->setDados($dadosEdicaoDep,(isset($valueDep['chave'])? $valueDep['chave'] : null),$arrValoresDaChaveFK,null,$keyDep);
						//salva o retorno do campos inserido para a inserção do próximo dado
						$arrKeyDep = explode(".",$keyDep);
						$arrDadosEdicao[$tabelaMaster.".".$arrKeyDep[1]] = $retorno;
						
					endforeach;
				}
			}
			
			/*inseri os registros da tabela do formulario*/
			$this->Mcrud->setStrTable($this->strTabela);
			$retorno = $this->Mcrud->setDados($arrDadosEdicao,(isset($this->data['chave'])? $this->data['chave'] : null),$arrValoresDaChave,$arrFiltroEdicao,$this->data['lista']['codigo']);
		}
			
		if($tipoRetorno == 1){	
			if(isset($retorno) and $retorno !== false){
				if( !isset( $data['texto'] ) )
					$data['texto'] = 'Inserção/Atualização com Sucesso!';
				if( !isset( $data['action'] ) )
					$data['action'] = 'atualizar';
				if( !isset( $data['class'] ) )
					$data['class'] = 'gritter-success';
				if( !isset( $data['id_dest'] ) )
					$data['id_dest'] = 'corpo'.str_replace(' ','_',$this->data['lista']['Titulo']);
			}else{
				if( !isset( $data['texto'] ) )
					$data['texto'] = 'Erro para Inserir/Atualizar o registro, gentileza tentar novamente ou procurar o administrador do sistema!';
				if( !isset( $data['action'] ) )
					$data['action'] = '';
				if( !isset( $data['class'] ) )
					$data['class'] = 'gritter-danger';
			}
			
			$this->load->view("global/crud/Valert",$data);
		}else{
			if(!isset($retorno) or $retorno == false){
				return 0;
			}else{
				return $retorno;	
			}	
			
		}
		
	}
	
	/**
	 * excluir.
	 * Exclui o registro selecionado da tabela;
	 * 
	 */
	public function excluir($chave = null){
		$this->load->model("global/Mcrud","Mcrud");
		
		$arrChave = array();
		$retorno = false;
		
		if(isset($chave) && $chave != null){
			/*adiciona o código da tabela na condição*/
			$arrChave[$this->data['lista']['codigo']] = $chave;
			
			/*adiciona as chaves da tabela na condição*/
			foreach( $this->data['chave'] as $key => $values){
				if( $this->data['lista']['codigo'] == $key )
					$arrChave[$key] = $chave;
				else
					$arrChave[$key] = $_SESSION[strtolower($values)];			
			}
			
			$this->Mcrud->setStrTable($this->strTabela);
			$retorno = $this->Mcrud->removeDados($arrChave);
		}
		if($retorno){
			$data['texto'] = 'Dados excluidos com sucesso!';
			$data['class'] = 'gritter-success';
			$data['action'] = 'excluir';
			$data['id_data'] = $chave;
		}else{
			$data['texto'] = 'Erro para excluir o registro, gentileza tentar novamente ou procurar o administrador do sistema!';
			$data['class'] = 'gritter-danger';
			$data['action'] = '';
		}
		
		$this->load->view("global/crud/Valert",$data);
		
	}


	/**
	 * excluir.
	 * Exclui o registro selecionado da tabela;
	 * 
	 */
	public function excluirInserErrado($arrchave = null, $tabela = null){
		$this->load->model("global/Mcrud","Mcrud");
		
		if(isset($chave) && $chave != null){
			
			$this->Mcrud->setStrTable($tabela);
			$this->Mcrud->removeDados($arrChave);
		}
		
	}
	/*function buscaComponentes(){
		$this->load->view('global/crud/VComponentesFormulario',$this->data);
	}*/
	
	public function converteData($data){
		$data1=substr($data,0,10);
		$hora=substr($data,11,5);
		return implode(!strstr($data1, '/') ? "/" : "-", array_reverse(explode(!strstr($data1, '/') ? "-" : "/", $data1)))." ".$hora;
	}
	
}


?>