<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Classe criada para controlar os acessos a tabela de Filiais
 * @author Christian Melo <christiandmelo@gmail.com>
 * @access public
 */
class Mfilial extends CI_Model{
	
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
	} 

	public function getFiliaisAtivas(){
		$this->Mcrud->setStrTable("glfilial");
		$arrData = array("cabecalho" => array("glfilial.CODFILIAL" => "CODFILIAL",
											  "glfilial.NOMEFANTASIA" => "NOMEFANTASIA"),
							   "where" => array("glfilial.CODCLIENTE" => $_SESSION['codcliente'],
							   					"glfilial.CODEMPRESA" => $_SESSION['codempresa']),
							   "order" => array("glfilial.NOMEFANTASIA" => "ASC")
								);
		return $this->Mcrud->getdados($arrData)->result();
	}
	
}