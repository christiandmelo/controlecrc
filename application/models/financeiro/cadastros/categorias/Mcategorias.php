<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Classe criada para controlar os acessos a tabela de categorias
 * @author Christian Melo <christiandmelo@gmail.com>
 * @access public
 */
class Mcategorias extends CI_Model{ 

	public function getMaxOrdemPorPai($ordemPai){
		$sql = "SELECT MAX(ORDEM) AS 'ORDEM'	
								FROM ficategoria FI
								WHERE FI.CODCLIENTE = ".$_SESSION['codcliente']."
									AND FI.ORDEM LIKE '".$ordemPai.".%'";
		$dados = $this->db->query($sql);
		$retorno = $dados->row();		
		if($retorno->ORDEM != null){
			return $retorno->ORDEM;
		}else{
			return 0;
		}
		
	}
	
	public function getMaxOrdem(){
		$sql = "SELECT MAX(ORDEM) AS 'ORDEM'	
								FROM ficategoria FC
								WHERE FC.CODCLIENTE = ".$_SESSION['codcliente']."
									AND FC.ORDEM NOT LIKE '%.%'";
		$dados = $this->db->query($sql);
		$retorno = $dados->row();
		return ($retorno->ORDEM != NULL)? $retorno->ORDEM : "01" ;
	}

	public function getTodasCategorias(){
		$sql = "SELECT FC.CODCATEGORIA,
									FC.NOME,
									FC.ORDEM	
								FROM ficategoria FC
								WHERE FC.CODCLIENTE = ".$_SESSION['codcliente']."
								ORDER BY FC.ORDEM";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
}