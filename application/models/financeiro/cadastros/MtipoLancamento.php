<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Classe criada para controlar os acessos a tabela de Tipo de Lancamento
 * @author Christian Melo <christiandmelo@gmail.com>
 * @access public
 */
class MtipoLancamento extends CI_Model{ 

	public function getTodosTipos(){
		$sql = "SELECT 	FT.CODTIPOLANCAMENTO,
						FT.DESCRICAO,
						FT.SINAL	
				FROM fitipolancamento FT";
		$dados = $this->db->query($sql);
		return $dados->result_array();
	}
	
}