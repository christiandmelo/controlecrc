<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Classe criada para controlar os acessos a tabela de Fornecedor
 * @author Christian Melo <christiandmelo@gmail.com>
 * @access public
 */
class Mfornecedor extends CI_Model{
	
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
	} 

	public function getFornecedoresClientesAtivos(){
		$this->Mcrud->setStrTable("fifornecedor");
		$arrDataFornecedor = array("cabecalho" => array("fifornecedor.CODFORNECEDOR" => "CODFORNECEDOR",
													 "fifornecedor.NOMEFANTASIA" => "NOMEFANTASIA"),
							   "where" => array("fifornecedor.CODCLIENTE" => $_SESSION['codcliente'],
							   					"fifornecedor.CLIENTE" => 1),
							   "order" => array("fifornecedor.NOMEFANTASIA" => "ASC")
								);
		return $this->Mcrud->getdados($arrDataFornecedor)->result();
	}
	
}