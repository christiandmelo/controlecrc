<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Classe criada para controlar os acessos a tabela de formas de pagamento
 * @author Christian Melo <christiandmelo@gmail.com>
 * @access public
 */
class MformaPagamento extends CI_Model{ 

	public function getTodasFormasPagamentoAtivas(){
		$sql = "SELECT 	FG.CODPAGAMENTO,
						FG.DESCRICAO
				FROM fiformadepagamento FG
				WHERE FG.CODCLIENTE = ".$_SESSION['codcliente']."
					AND SITUACAO = 1";
		$dados = $this->db->query($sql);
		return $dados->result_array();		
		
	}
}