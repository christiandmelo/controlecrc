<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlancamentos extends CI_Model{ 

	/*pesquisa o valor do saldo anterior*/
	public function getSaldoAnterior($mes,$ano){
		$sqlSaldoNeg = "SELECT 	SUM(FI.VALORORIGINAL) AS 'SALDOANTERIOR'
								FROM filancamento FI
								WHERE FI.CODCLIENTE = ".$_SESSION['codcliente']."
									AND FI.CODEMPRESA = ".$_SESSION['codempresa']."
									AND FI.CODFILIAL = ".$_SESSION['codfilial']."
								    AND FI.CODTIPOLANCAMENTO = 1
								    AND DATE_FORMAT( FI.DATAVENCIMENTO , '%Y-%m' ) = DATE_FORMAT( ADDDATE( '".$ano."-".$mes."-01' , INTERVAL - 1 MONTH ) , '%Y-%m' )
								GROUP BY FI.CODTIPOLANCAMENTO";
		$dadosSqlSaldoNeg = $this->db->query($sqlSaldoNeg);
		$retSaldoNeg1 = $dadosSqlSaldoNeg->row();
		if($retSaldoNeg1 != null){
			$retSaldoNeg = $retSaldoNeg1->SALDOANTERIOR;
		}else{
			$retSaldoNeg = 0;
		}
		
		
		$sqlSaldoPos = "SELECT 	SUM(FI.VALORORIGINAL) AS 'SALDOANTERIOR'
											FROM filancamento FI
											WHERE FI.CODCLIENTE = ".$_SESSION['codcliente']."
												AND FI.CODEMPRESA = ".$_SESSION['codempresa']."
												AND FI.CODFILIAL = ".$_SESSION['codfilial']."
												AND FI.CODTIPOLANCAMENTO = 2
												AND DATE_FORMAT( FI.DATAVENCIMENTO , '%Y-%m' ) = DATE_FORMAT( ADDDATE( '".$ano."-".$mes."-01' , INTERVAL - 1 MONTH ) , '%Y-%m' )
											GROUP BY FI.CODTIPOLANCAMENTO";
		$dadosSqlSaldoPos = $this->db->query($sqlSaldoPos);
		$retSaldoPos1 = $dadosSqlSaldoPos->row();
		if($retSaldoPos1 != null){
			$retSaldoPos = $retSaldoPos1->SALDOANTERIOR;
		}else{
			$retSaldoPos = 0;
		}
		
		return $retSaldoPos - $retSaldoNeg;
		
	}

	public function getLancamentosAgrupadosPorMes($datainicial, $datafinal){
		$dti = implode("-",array_reverse(explode("/", $datainicial)))."-01";
		$arrdtf = explode("/",$datafinal);
		$dtf = date("".$arrdtf[1]."-".$arrdtf[0]."-t");
		$sql = "SELECT 	DATE_FORMAT( FI.DATAVENCIMENTO , '%m/%Y' ) AS 'VENCIMENTO',
						SUM(FI.VALORORIGINAL) AS 'VALOR',
						FI.CODTIPOLANCAMENTO
				FROM filancamento FI
				INNER JOIN ficategoria FC
					ON FC.CODCLIENTE = FI.CODCLIENTE
					AND FC.CODCATEGORIA = FI.CODCATEGORIA
				INNER JOIN fiformadepagamento FFP
					ON FFP.CODPAGAMENTO = FI.CODPAGAMENTO
				INNER JOIN fitipolancamento FTP
					ON FTP.CODTIPOLANCAMENTO = FI.CODTIPOLANCAMENTO
				LEFT JOIN ficontacorrente FCCR
					ON FCCR.CODCLIENTE = FI.CODCLIENTE
					AND FCCR.CODEMPRESA = FI.CODEMPRESA
					AND FCCR.CODFILIAL = FI.CODFILIAL
					AND FCCR.CODCONTACORRENTE = FI.CODCONTACORRENTE
				WHERE FI.CODCLIENTE = ".$_SESSION['codcliente']."
					AND FI.CODEMPRESA = ".$_SESSION['codempresa']."
					AND FI.CODFILIAL = ".$_SESSION['codfilial']."
				    AND FI.DATAVENCIMENTO BETWEEN '".$dti."' AND '".$dtf."'
				GROUP BY FI.CODTIPOLANCAMENTO,
					DATE_FORMAT( FI.DATAVENCIMENTO , '%m/%Y' )
				ORDER BY DATE_FORMAT( FI.DATAVENCIMENTO , '%m/%Y' ),
					FI.CODTIPOLANCAMENTO DESC
					";
		$dadosSql = $this->db->query($sql);
		return $dadosSql->result_array();
	}
	
	
	
	/*pesquisa os lançamentos do mês informado*/
	public function getLancamentosDoMes($mes,$ano){
		$sqlDadosListaLancamentos = "SELECT 	FI.DESCRICAO,
												FI.DATAVENCIMENTO AS 'VENCIMENTO',
												FC.NOME AS 'CATEGORIA',
												FFP.DESCRICAO AS 'FORMADEPAGAMENTO',
												FI.VALORORIGINAL AS 'VALOR',
												FI.CODTIPOLANCAMENTO,
												FCCR.DESCRICAO AS 'CONTA',
												FI.CODFISTATUS
										FROM filancamento FI
										INNER JOIN ficategoria FC
											ON FC.CODCLIENTE = FI.CODCLIENTE
										    AND FC.CODCATEGORIA = FI.CODCATEGORIA
										INNER JOIN fiformadepagamento FFP
											ON FFP.CODPAGAMENTO = FI.CODPAGAMENTO
										INNER JOIN fitipolancamento FTP
											ON FTP.CODTIPOLANCAMENTO = FI.CODTIPOLANCAMENTO
										LEFT JOIN ficontacorrente FCCR
											ON FCCR.CODCLIENTE = FI.CODCLIENTE
											AND FCCR.CODEMPRESA = FI.CODEMPRESA
											AND FCCR.CODFILIAL = FI.CODFILIAL
											AND FCCR.CODCONTACORRENTE = FI.CODCONTACORRENTE
										WHERE FI.CODCLIENTE = ?
											AND FI.CODEMPRESA = ?
											AND FI.CODFILIAL = ?
										    AND MONTH(FI.DATAVENCIMENTO) = ?
										    AND YEAR(FI.DATAVENCIMENTO) = ?
										ORDER BY FI.DATAVENCIMENTO";
										
		$arrDados = array(
					$_SESSION['codcliente'],
					$_SESSION['codempresa'],
					$_SESSION['codfilial'],
					$mes,
					$ano
				);
		$dadosSql = $this->db->query($sqlDadosListaLancamentos,$arrDados);
		return $dadosSql->result_array();
	}
	
}