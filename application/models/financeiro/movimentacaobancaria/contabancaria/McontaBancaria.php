<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Classe criada para controlar os acessos a tabela de Conta Bancaria
 * @author Christian Melo <christiandmelo@gmail.com>
 * @access public
 */
class McontaBancaria extends CI_Model{
	public $codcliente;
	public $codempresa;
	public $codfilial;
	/**
    * Variável que contém o código da conta bancaria
    * @access public
    * @name $codcontacorrente
    */
	private $codcontacorrente;
	
	/*
	 * Metodo criado para adicionar valor a variavel com o código da conta corrente
	 * @author Christian Melo <christiandmelo@gmail.com>
	 * @access public
	 * @package ModelContaBancaria
	 * @version 1.0.1
	 * 
	 * @param int $codContaCorrente, váriavel contendo o valor da conta corrente.
	 * 
	 * @return void
	 */
	public function setCodContaCorrente($codContaCorrente){
		$this->codcontacorrente = $codContaCorrente;
	}
	
	
	/*
	 * Metodo criado para atualizar o saldo da conta corrente
	 * @author Christian Melo <christiandmelo@gmail.com>
	 * @access public
	 * @package ModelContaBancaria
	 * @version 1.0.1
	 * 
	 * @param int $valor, Váriavel contendo o valor que será atualizado no saldo.
	 * @param string $operacao, Várivel contendo a operação que será realizada 2 = soma ou 1 = subtracao.
	 * 
	 * @return bool retornando atualizado com sucesso ou não
	 */
	public function setSaldo($valor,$operacao){
		/*busca o valor do saldo*/
		$saldo = $this->getSaldo();
		
		$valorInput = $valor;
		
		switch($operacao){
			case 1:
				$valorInput = $saldo - $valor;
				break;
			case 2:
				$valorInput = $saldo + $valor;
				break;
		}
				
		$this->db->where( "CODCLIENTE" , $this->codcliente );
		$this->db->where( "CODEMPRESA" , $this->codempresa );
		$this->db->where( "CODFILIAL" , $this->codfilial );
		$this->db->where( "CODCONTACORRENTE" , $this->codcontacorrente );
		
		$data = array("SALDO" => $valorInput);
		
		if( $this->db->update( "ficontacorrente" , $data) ){
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * Metodo criado para buscar o saldo da conta corrente
	 * @author Christian Melo <christiandmelo@gmail.com>
	 * @access public
	 * @package ModelContaBancaria
	 * @version 1.0.1
	 * 
	 * @return int valor do saldo
	 */
	public function getSaldo(){
		$this->db->select("SALDO");
		$this->db->where( "CODCLIENTE" , $this->codcliente );
		$this->db->where( "CODEMPRESA" , $this->codempresa );
		$this->db->where( "CODFILIAL" , $this->codfilial );
		$this->db->where( "CODCONTACORRENTE" , $this->codcontacorrente );
		$result = $this->db->get("ficontacorrente");
		$row = $result->row();
		
		return $row->SALDO;
		
	}
	
	/*
	 * Metodo criado para buscar as contas correntes
	 * @author Christian Melo <christiandmelo@gmail.com>
	 * @access public
	 * @package McontaBancaria
	 * @version 1.0.1
	 * 
	 * @param array $arraySelect, Array que contém os campos do select.
	 * 
	 * @return array de conta bancária
	 */
	public function getTodasContas(){
		$this->db->select(array("ficontacorrente.CODCONTACORRENTE" => "CODCONTACORRENTE",
							 	"ficontacorrente.DESCRICAO" => "DESCRICAO",
								"GL.NOMEREDUZIDO" => "NOMEREDUZIDO"));
		$this->db->join("glbanco AS GL", "GL.CODCLIENTE = ficontacorrente.CODCLIENTE AND GL.CODBANCO = ficontacorrente.CODBANCO");		
		$this->db->where( "ficontacorrente.CODCLIENTE" , $this->codcliente );
		$this->db->where( "ficontacorrente.CODEMPRESA" , $this->codempresa );
		$this->db->where( "ficontacorrente.CODFILIAL" , $this->codfilial );
		$this->db->where( "ficontacorrente.SITUACAO" , 1 );
		$result = $this->db->get("ficontacorrente");
		
		return $result->result_array();
		
	}
	
} 