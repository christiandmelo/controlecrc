<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

/**
 * Mparametro.
 * Classe para gerencias os parametros do sistema.
 *
 * @package global
 * @version 0.1
 */
class Mparametro extends CI_Model {
	
	// Atributos
	
	// Metodos
	
	/**
	 * __construct.
	 */
	public function __construct() {
		parent::__construct ();
	}
	
	private function validaSistemas( $strSistema = null ){
		// Verifica de qual sistema é para criar os parametros.
		$boolTudoOk = true;
		switch ($strSistema) {
			case null :
				$arrTabela = array (
				'fsparametro'
						);
				break;
			case 'fsparametro' :
			case 'fs' :
				$arrTabela = array ( 'fsparametro' );
				break;
			default :
				$boolTudoOk = false;
				break;
		}
		if(!$boolTudoOk)
			return false;
		else
			return $arrTabela;
	}
	
	/**
	 * validaEstruturaParametros.
	 */
	public function validaEstruturaParametros($strSistema = null) {
	}
	
	/**
	 * getParametro.
	 */
	public function getParametro($strSistema = null) {
		
		// Constroi as condições e faz a consulta
		$arrTabela = $this->validaSistemas( $strSistema );
		$retorno = array ();
		$nTabelas = count ( $arrTabela );
		foreach ( $arrTabela as $indice => $strTabela ) {
			if ($nTabelas == 1)
				$strRTabela = $strTabela;
			$strCondicao = "$strTabela.CODCLIENTE = " . (( int ) $this->session->userdata ( 'codcliente' ));
			$this->db->select ( " 
					$strTabela.CODCLIENTE, 
					IFNULL($strTabela.CODEMPRESA,0) AS CODEMPRESA,
					IFNULL($strTabela.CODFILIAL,0) AS CODFILIAL,
					$strTabela.NCERT,
					$strTabela.PCERT
			" );
			$this->db->from ( $strTabela );
			$this->db->where ( $strCondicao );
			foreach ( $this->db->get ()->result_array () as $key => $arrRow ) {
				$retorno [$strTabela] [$arrRow ['CODCLIENTE']] [$arrRow ['CODEMPRESA']] [$arrRow ['CODFILIAL']] ['NCERT'] = $arrRow ['NCERT'];
				$retorno [$strTabela] [$arrRow ['CODCLIENTE']] [$arrRow ['CODEMPRESA']] [$arrRow ['CODFILIAL']] ['PCERT'] = $arrRow ['PCERT'];
			}
		}
		if ($nTabelas == 1)
			return $retorno [$strRTabela];
		return $retorno;
	}
	
	/**
	 * criaEstruturaParametros.
	 * 
	 * @param boolean $boolForcaCriacao
	 * @param mixed $strSistema
	 */
	public function criaParametros( $codcliente, $codempresa = null, $codfilial = null, $strSistema = null) {

		$arrTabela = $this->validaSistemas( $strSistema );
		foreach ( $arrTabela as $indice => $strTabela ) {
			$this->db->insert( $strTabela , array(
					"$strTabela.CODCLIENTE" => $codcliente,
					"$strTabela.CODEMPRESA" => $codempresa,
					"$strTabela.CODFILIAL" => $codfilial,
			) );
		}
		
	}
	
}

?>	