<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Macesso.
 * Classe que gerencia todo o acesso ao sistema.
 * 
 * @package global
 * @version 0.1
 */
class Macesso extends CI_Model{
	
	/**
	 * getMenu.
	 * Metodo que busca o menu, returnando um array bem definido do menu.
	 * 
	 */
	public function getMenu($codusuario = 0,$strCondicao = null,$mixRetorno = 'lateral'){
		
		// Consulta para usuario especifico
		$strConsulta = "
			SELECT 	DISTINCT
					GLL.CODLINK,
					GLL.NOME,
					GLL.ORDEM,
					GLL.CAMINHO,
					GLL.ICONEFA,
					GLL.DESCRICAO
			FROM gllink AS GLL
			INNER JOIN glgrupolink AS GLGL
				ON GLGL.CODLINK = GLL.CODLINK
			INNER JOIN glgrupousuario AS GLGU
				ON GLGU.CODCLIENTE = GLGL.CODCLIENTE
				AND GLGU.CODEMPRESA = GLGL.CODEMPRESA 
				AND GLGU.CODFILIAL = GLGL.CODFILIAL
				AND GLGU.CODGRUPO = GLGL.CODGRUPO
			WHERE GLGU.CODUSUARIO = ?
				AND GLGU.CODCLIENTE = ?
                AND GLGU.CODEMPRESA = ?
                AND GLGU.CODFILIAL = ?
				AND GLL.ATIVO = 1";
			
			
			
		// Busca de um nivel especifico. Só para atender ao menu central.
		if( is_string( $strCondicao ) && ( preg_match('~^[0-9]{2}\.[0-9]{2}\.[0-9]{2}$~', $strCondicao) == true ) ){
			// Da uma leve tratada na String
			$intControleCondicao = true;
			$strTratamento = explode('.', $strCondicao);
			foreach($strTratamento as $key => $value) {
				if( $value === '00' && $intControleCondicao){
					$intControleCondicao = false;
					$strTratamento[$key] = "__";
					$intControleCondicao++;
				}
			}
			$strNovoCondicao = $strTratamento[0].".".$strTratamento[1].".".$strTratamento[2];
			// Consulta
			$strConsulta .= "
				AND GLL.ORDEM LIKE '".$strNovoCondicao."'";
		}
		$strConsulta .= "
				ORDER BY 
					GLL.ORDEM
			";
			
		/*Informa os parametros da consulta*/
		$arrDados = array(
				$codusuario,
				$_SESSION['codcliente'],
				$_SESSION['codempresa'],
				$_SESSION['codfilial']
			);
		
		// Busca os registros referentes ao menu e retorna os mesmos
		$menu = $this->db->query($strConsulta,$arrDados);
		switch( $mixRetorno ){
			case 'busca':
			case 'lateral':
				// Cria o Menu Lateral/Busca
				// Mesma estrutura para os dois menus, só leve modificação para saber as peculiaridades :D
				if( $mixRetorno == 'busca' ){
					$booBusca = true;
				}else{
					$booBusca = false;
				}
				$strClassHS = ' class="has-sub" ';
				$strClassSM = ' class="sub-menu" ';
				$strStyleUl = ' style="display: none;" ';
				// Variaveis para controle de script
				$intReg = 0;
				$arrNvControl = null;
				$arrNivel = null;
				$strMenu = '';
				// passa por todos os registros do menu
				foreach( $menu->result() as $idLink => $rsLink ){
					// Separa os nivels ( congelado para 3, mas com possibilidade de inteligencia para calcular sozinho )
					$intReg++;
					$arrNivel = explode('.',$rsLink->ORDEM);
					// Fecha as <ul> caso necessario
					if( !is_null( $arrNvControl ) ){
						if( ( $arrNvControl[1] != '00' && $arrNvControl[1] != $arrNivel[1] ) ){
							$strMenu .= '	</ul>';
							$strMenu .= '</li>';
						}
						if( ( $arrNvControl[0] != $arrNivel[0] ) ){
							$strMenu .= '	</ul>';
							$strMenu .= '</li>';
						}
					}
					// Cria a linha no menu
					if( $arrNivel[1] == '00' || $arrNivel[2] == '00' ){
						$strMenu .= '<li '.$strClassHS.' data-menu-busca="'.str_replace('.', '', $rsLink->ORDEM).'" >';
						$strMenu .= '	<a href="'.( $rsLink->CAMINHO == '#' ? 'javascript:;' : $rsLink->CAMINHO );
						if( $rsLink->CAMINHO == '#' ){
							$strMenu .= '" >';
							if( !$booBusca )
								$strMenu .= '	    <b class="caret pull-right"></b>';
						}else{
							$strMenu .= '">';
						}
						if( !is_null($rsLink->ICONEFA) )
							$strMenu .= '	    <i class="'.$rsLink->ICONEFA.'"></i>';
						
						if( $rsLink->CAMINHO != '#' )
							$strMenu .= '	    <span data-menu-busca="'.str_replace('.', '', $rsLink->ORDEM).'" >';
						else
							$strMenu .= '	    <span>';
						
						$strMenu .= $rsLink->NOME;
						$strMenu .= '</span>';
						
						$strMenu .= '	</a>';
						if( $rsLink->CAMINHO != '#' )
							$strMenu .= '	<ul '.$strStyleUl.' >';
						else
							$strMenu .= '	<ul '.$strClassSM.'>';
					}else{
						$strMenu .= '<li data-menu-busca="'.str_replace('.', '', $rsLink->ORDEM).'" >';
						$strMenu .= '	<a href="'.($rsLink->CAMINHO == '#' ? 'javascript:;' : $rsLink->CAMINHO ).'" >';
						if( !is_null($rsLink->ICONEFA) )
							$strMenu .= '	    <i class="'.$rsLink->ICONEFA.'"></i>';
						
						if( $rsLink->CAMINHO != '#' )
							$strMenu .= '	    <span data-menu-busca="'.str_replace('.', '', $rsLink->ORDEM).'" >';
						else
							$strMenu .= '	    <span>';
						
						$strMenu .= $rsLink->NOME;
						$strMenu .= '</span>';
						
						$strMenu .= '	</a>';
						$strMenu .= '</li>';
					}
					$arrNvControl = $arrNivel;
				}
				// Fecha os ultimos que ficaram abertos
				if( $arrNvControl[2] != '00' ){
					$strMenu .= '	</ul>';
					$strMenu .= '</li>';
				}
				if( $arrNvControl[1] != '00' ){
					$strMenu .= '	</ul>';
					$strMenu .= '</li>';
				}
				if( $arrNvControl[1] == '00' && $arrNvControl[2] == '00' ){
					$strMenu .= '	</ul>';
					$strMenu .= '</li>';
				}
				break;
			
			case 'central' :
				// Cria o Menu Lateral
				// Variaveis para controle de script
				$strMenu = '';
				// passa por todos os registros do menu
				foreach( $menu->result() as $idLink => $rsLink ){
					if( $strCondicao == $rsLink->ORDEM )
						continue;
					$strMenu .= '<!-- begin col-3 -->';
				    $strMenu .= '<div class="col-md-3 col-sm-6" data-help-system-target="" >';
				    $strMenu .= '	<div class="widget widget-stats bg-black-menu" >';
					$strMenu .= '	<a href="'.( $rsLink->CAMINHO != '#' ? $rsLink->CAMINHO : '#/inicio/menu/'.$rsLink->ORDEM ).'" >';
					$strMenu .= '		<div class="stats-icon stats-icon-lg"><i class="'.$rsLink->ICONEFA.' fa-fw"></i></div>';
					$strMenu .= '		<div class="stats-title"> '.$rsLink->NOME.' </div>';
					$strMenu .= '		<br/>';
					$strMenu .= '		<div class="stats-desc"> '.$rsLink->DESCRICAO.' </div>';
					$strMenu .= '	</a>';
					$strMenu .= '	</div>';
					$strMenu .= '</div>';
				}
				break;
				
			default:
				$strMenu = $menu;
				break;
		}
		
		//echo nl2br(html_escape( $strMenu ));
		//die();
		
		return $strMenu;
		
	}
	
	/**
	 * hasAcesso.
	 * Metodo que valida o acesso do usuario a determinada pagina/conteudo.
	 * 
	 */
	public function hasAcesso(){
		
		// Consulta para verificar se determinado usuario pode acessar determinada pagina
		$strConsulta = "SELECT '1' AS 'temAcesso'
						FROM glusuario GLU
						INNER JOIN glgrupousuario GLG
							ON GLG.CODCLIENTE = GLU.CODCLIENTE
						    AND GLG.CODEMPRESA = GLU.CODEMPRESA
						    AND GLG.CODFILIAL = GLU.CODFILIAL
						    AND GLG.CODUSUARIO = GLU.CODUSUARIO
						INNER JOIN glgrupolink GLGL
							ON GLGL.CODCLIENTE = GLU.CODCLIENTE
						    AND GLGL.CODEMPRESA = GLU.CODEMPRESA
						    AND GLGL.CODFILIAL = GLU.CODFILIAL
						    AND GLGL.CODGRUPO = GLG.CODGRUPO
						INNER JOIN gllink GL
							ON GL.CODLINK = GLGL.CODLINK
						WHERE GLU.CODUSUARIO = ?
							AND GL.CAMINHO = ?
						LIMIT 1";
		//$url = str_replace('/novo', '', $_SERVER['REQUEST_URI']);
		//$url = str_replace('/salvaEdicao', '', $url);
		//$url = str_replace('/excluir', '', $url);
		$arrUrl = explode("/novo",$_SERVER['REQUEST_URI']);
		$arrUrl = explode("/salvaEdicao",$arrUrl[0]);
		$arrUrl = explode("/excluir",$arrUrl[0]);
		$arrUrl = explode("/lista",$arrUrl[0]);
		$arrUrl = explode("/buscaCamposEntradaDoacao",$arrUrl[0]);
		$arrUrl = explode("/salvaEntradaDoacao",$arrUrl[0]);
		$arrUrl = explode("/CtriagemComponente",$arrUrl[0]);
		$arrUrl = explode("/Cestoque",$arrUrl[0]);
		$arrUrl = explode("/get",$arrUrl[0]);
		$arrDados = array(
				$_SESSION['codusuario'],
				"#".$arrUrl[0]
			);
		$objQuery = $this->db->query($strConsulta,$arrDados);
		$numRows = $objQuery->num_rows();
		if($numRows > 0){
			return true;
		}else if($arrUrl[0] == '/estoque/estoque/triagem'){
			return true;
		}else if($arrUrl[0] == '/estoque/estoque'){
			return true;
		}else{
			return false;
		}
		
	}
	
	/**
	 * getBreadcrumb
	 * Metodo que cria e retornar o Navegador do sistema (breadcrumb)
	 * 
	 */
	public function getBreadcrumb( $strOrdem = null ){
		
		$strBreadcrumb = '';
		
		if( is_null($strOrdem) ){
			// Caso seja null, constroi um problema inicial e manda
			$strBreadcrumb .= '<!-- begin breadcrumb -->';
			$strBreadcrumb .= '<ol class="breadcrumb pull-right" data-help-system-target="global.breadcrumb" >';
			$strBreadcrumb .= '	<li class="active"> Inicio </li>';
			$strBreadcrumb .= '</ol>';
			$strBreadcrumb .= '<!-- end breadcrumb -->';
			$strBreadcrumb .= '<!-- begin page-header -->';
			$strBreadcrumb .= '<h1 class="page-header" data-help-system-target="global.pageHeader" > Inicio </h1>';
			$strBreadcrumb .= '<!-- end page-header -->';
		}else{
			// Consulta para encontrar as informações necessarias da navegação
			$strConsulta = "
				SELECT
					gll.CODLINK
					, gll.NOME
					, gll.ORDEM
					, CASE 
						WHEN gll.CAMINHO LIKE '#' THEN CONCAT('#/inicio/menu/',gll.ORDEM)
				        ELSE gll.CAMINHO
					END AS 'CAMINHO'
					, gll.ICONEFA
				FROM gllink AS gll
				INNER JOIN gllink AS gll0
					ON (
						gll0.ORDEM LIKE ?
						OR gll0.CAMINHO LIKE ?
					)
				    AND (
						gll.ORDEM LIKE gll0.ORDEM
				        OR gll.ORDEM LIKE CONCAT(LEFT(gll0.ORDEM,6),'00')
				        OR gll.ORDEM LIKE CONCAT(LEFT(gll0.ORDEM,3),'00.00')
					)
				ORDER BY gll.ORDEM
			";
			$arrDados = array(
				$strOrdem,
				$strOrdem
			);
			
			// Busca os registros referentes ao menu e retorna os mesmos
			$breadcrumb = $this->db->query($strConsulta,$arrDados);
			// Cria o Breadcrumb
			$strBreadcrumb .= '<!-- begin breadcrumb -->';
			$strBreadcrumb .= '<ol class="breadcrumb pull-right" data-help-system-target="global.breadcrumb" >';
			$strBreadcrumb .= '	<li><a href="/app">Inicio</a></li>';
			// passa por todos os registros do menu (Na teorioa ja vem na ordem, por conta do ORDER BY)
			foreach( $breadcrumb->result() as $idLink => $rsLink ){
				if( $rsLink->ORDEM == $strOrdem || $rsLink->CAMINHO == $strOrdem )
					$strBreadcrumb .= '	<li class="active">'.$rsLink->NOME.'</li>';
				else
					$strBreadcrumb .= '	<li><a href="'.$rsLink->CAMINHO.'">'.$rsLink->NOME.'</a></li>';
			}
			$strBreadcrumb .= '</ol>';
			$strBreadcrumb .= '<!-- end breadcrumb -->';
			$strBreadcrumb .= '<!-- begin page-header -->';
			$strBreadcrumb .= '<h1 class="page-header" data-help-system-target="global.pageHeader" >'.$rsLink->NOME.'</h1>';
			$strBreadcrumb .= '<!-- end page-header -->';
		}
		// Retorna a navegação
		return $strBreadcrumb;
		
	}
	
	/**
	 * createPrimeiroAcesso.
	 * Cria todos os registros do primeiro acesso do cliente.
	 * 
	 * @param string $strRazaoSocial
	 * @param string $strEmail
	 * @param int $intCNPJ
	 * @throws Exception
	 */
	public function createPrimeiroAcesso( $strRazaoSocial, $strEmail , $intCNPJ ){
		
		// Basico para funcionar
		$this->load->model("global/Mcrud","Mcrud");
		$this->load->model("global/Mlogin","Mlogin");
		$this->load->model("global/Mparametro","Mparametro");
		
		// Valida se jã existe uma empresa com esse CNPJ.
		$this->Mcrud->setStrTable('glempresa');
		$mixEmpresa =  $this->Mcrud->getDados( array(
				'where' => array(
						'glempresa.CGC' => $intCNPJ,
				),
		))->num_rows();
		$isTudoOk = ( is_null( $mixEmpresa ) || $mixEmpresa === 0 ) ? true : false;
		if( !$isTudoOk ){
			// Implementar classe de exeções
			throw new \Exception('CNPJ Já utilizado. Gentileza entrar em contato com o suporte para mais informações. Codigo do Erro: CP.NA.0001.');
			return false;
		}
		
		// Valida se jã existe um usuario com esse email.
		$this->Mcrud->setStrTable('glusuario');
		$mixEmail =  $this->Mcrud->getDados( array(
				'where' => array(
						'glusuario.EMAIL' => $strEmail,
				),
		))->num_rows();
		$isTudoOk = ( is_null( $mixEmail ) || $mixEmail === 0 ) ? true : false;
		if( !$isTudoOk ){
			// Implementar classe de exeções
			throw new \Exception('Email já utilizado. Gentileza entrar em contato com o suporte para mais informações. Codigo do Erro: CP.NA.0002.');
			return false;
		}
		
		// Cadastra o novo cliente;
		$this->Mcrud->setStrTable('glcliente');
		$idCliente = $this->Mcrud->setDados(
				array(
						"glcliente.QTDUSUARIOS" => 1,
						"glcliente.LIBERADO" => 1,
						"glcliente.NOME" => $strRazaoSocial,
						"glcliente.NOMEFANTASIA" => $strRazaoSocial,
				),
				array(
						"glcliente.CODCLIENTE" => "CODCLIENTE",
				)
				);
		$isTudoOk = ( $idCliente === false ? false : true );
		if( !$isTudoOk ){
			// Implementar classe de exeções
			throw new Exception('Não foi possivel criar seu cadastro. Entre em contato com o suporte para mais informações! Codigo do Erro: CP.NA.0003.');
			return false;
		}
		
		// Cadastra o nova empresa;
		$this->Mcrud->setStrTable('glempresa');
		$idEmpresa = $this->Mcrud->setDados(
				array(
						"glempresa.CODCLIENTE" => $idCliente,
						"glempresa.QTDFILIAL" => 5,
						"glempresa.NOME" => $strRazaoSocial,
						"glempresa.NOMEFANTASIA" => $strRazaoSocial,
						"glempresa.CGC" => $intCNPJ,
				),
				array(
						"glempresa.CODCLIENTE" => "CODCLIENTE",
				),
				array(
						"glempresa.CODCLIENTE" => $idCliente
				),
				null,
				'glempresa.CODEMPRESA'
				);
		$isTudoOk = ( $idEmpresa === false ? false : true );
		if( !$isTudoOk ){
			// Implementar classe de exeções
			throw new Exception('Não foi possivel criar seu cadastro. Entre em contato com o suporte para mais informações! Codigo do Erro: CP.NA.0004.');
			return false;
		}
		
		
		// Cadastra o nova filial;
		$this->Mcrud->setStrTable('glfilial');
		$idFilial = $this->Mcrud->setDados(
				array(
						"glfilial.CODCLIENTE" => $idCliente,
						"glfilial.CODEMPRESA" => $idEmpresa,
						"glfilial.NOMEFANTASIA" => $strRazaoSocial,
						"glfilial.CNPJ" => $intCNPJ,
				),
				array(
						"glfilial.CODCLIENTE" => "CODCLIENTE",
						"glfilial.CODEMPRESA" => "CODEMPRESA",
				),
				array(
						"glfilial.CODEMPRESA" => $idEmpresa,
						"glfilial.CODCLIENTE" => $idCliente
				),
				null,
				'glfilial.CODFILIAL'
				);
		$isTudoOk = ( $idFilial === false ? false : true );
		if( !$isTudoOk ){
			// Implementar classe de exeções
			throw new Exception('Não foi possivel criar seu cadastro. Entre em contato com o suporte para mais informações! Codigo do Erro: CP.NA.0005.');
			return false;
		}
		
		// Cadastra o novo Grupo de acesso;
		$this->Mcrud->setStrTable('glgrupo');
		$idGrupo = $this->Mcrud->setDados(
				array(
						"glgrupo.CODCLIENTE" => $idCliente,
						"glgrupo.CODEMPRESA" => $idEmpresa,
						"glgrupo.CODFILIAL" => $idFilial,
						"glgrupo.NOME" => 'Acesso Geral',
				),
				array(
						"glgrupo.CODCLIENTE" => "CODCLIENTE",
						"glgrupo.CODEMPRESA" => "CODEMPRESA",
				),
				array(
						"glgrupo.CODFILIAL" => $idFilial,
						"glgrupo.CODEMPRESA" => $idEmpresa,
						"glgrupo.CODCLIENTE" => $idCliente
				),
				null,
				"glgrupo.CODGRUPO"
				);
		$isTudoOk = ( $idGrupo === false ? false : true );
		if( !$isTudoOk ){
			// Implementar classe de exeções
			throw new Exception('Não foi possivel criar seu cadastro. Entre em contato com o suporte para mais informações! Codigo do Erro: CP.NA.0006.');
			return false;
		}
		
		// Vincula os links ( Que não são de Administraçõo ) ao novo grupo
		$strInsert = "
				INSERT INTO glgrupolink
				SELECT ?, ?, ?, ?, CODLINK, 0, now(), 0, now() FROM gllink WHERE ADMIN = 0
		";
		$arrDadosInsert = array(
				$idCliente,
				$idFilial,
				$idEmpresa,
				$idGrupo
		);
		
		var_dump( $arrDadosInsert );
		echo 'asdasd';
		
		$this->db->query($strInsert,$arrDadosInsert);
		$this->db->affected_rows();
		
		// Cadastra o novo usuario;
		$this->Mcrud->setStrTable('glusuario');
		$idUsuario = $this->Mcrud->setDados(
				array(
						"glusuario.CODCLIENTE" => $idCliente,
						"glusuario.CODEMPRESA" => $idEmpresa,
						"glusuario.CODFILIAL" => $idFilial,
						"glusuario.NOME" => $strRazaoSocial,
						"glusuario.ATIVO" => 1,
						"glusuario.EMAIL" => $strEmail,
						"glusuario.SENHA" => md5('123456'),
				)
				);
		$isTudoOk = ( $idUsuario === false ? false : true );
		if( !$isTudoOk ){
			// Implementar classe de exeções
			throw new Exception('Não foi possivel criar seu cadastro. Entre em contato com o suporte para mais informações! Codigo do Erro: CP.NA.0008.');
			return false;
		}
		
		// Vincula usuario ao grupo;
		$this->Mcrud->setStrTable('glgrupousuario');
		$idVinculoAcesso = $this->Mcrud->setDados(
				array(
						"glgrupousuario.CODCLIENTE" => $idCliente,
						"glgrupousuario.CODEMPRESA" => $idEmpresa,
						"glgrupousuario.CODFILIAL" => $idFilial,
						"glgrupousuario.CODGRUPO" => $idGrupo,
						"glgrupousuario.CODUSUARIO" => $idUsuario,
				)
				);
		$isTudoOk = ( $idVinculoAcesso === false ? false : true );
		if( !$isTudoOk ){
			// Implementar classe de exeções
			throw new Exception('Não foi possivel criar seu cadastro. Entre em contato com o suporte para mais informações! Codigo do Erro: CP.NA.0009.');
			return false;
		}
		
		// Cria os parametros iniciais do sistema
		$this->Mparametro->criaParametros($idCliente,$idEmpresa);
		
	}
	
}

?>