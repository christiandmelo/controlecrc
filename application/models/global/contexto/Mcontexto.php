<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Mcontexto.
 * 
 * 
 * @package global
 * @version 0.1
 */
class Mcontexto extends CI_Model{
	
	public $arrDados;
	
	public function setContexto($getSelect = null,$getTipo = null){
		
		$arrPost = explode("-Z", $getSelect);
		
		switch($getTipo){
			case "conta":
				$this->arrDados['consulta1'] = array("CODCLIENTE" => $arrPost[0],
								  "CODEMPRESA" => 1,
								  "CODFILIAL" => 1);
				break;
			case "empresa":
				$this->arrDados['consulta1'] = array("CODEMPRESA" => $arrPost[0],
								  "CODFILIAL" => 1);
				break;
			case "filial":
				$this->arrDados['consulta1'] = array("CODFILIAL" => $arrPost[0]);
				break;
		}
		
		$arrFiltro = array("CODUSUARIO" => $_SESSION['codusuario']);							   
		$this->Mcrud->setDados($this->arrDados['consulta1'],null,null,$arrFiltro);
		
		$this->Mcrud->setStrTable("glusuario");
		$this->arrDados['consulta2']['cabecalho'] = array("glusuario.CODCLIENTE" => "codcliente",
														  "glcliente.NOMEFANTASIA" => "nomecliente",
													  	  "glusuario.CODEMPRESA" => "codempresa",
														  "glempresa.NOMEFANTASIA" => "nomeempresa",
														  "glusuario.CODFILIAL" => "codfilial",
														  "glfilial.NOMEFANTASIA" => "nomefilial");
		$this->arrDados['consulta2']['left-join'] = array("glcliente" => "glcliente.codcliente = glusuario.codcliente",
													 "glempresa" => "glempresa.codcliente = glusuario.codcliente and glempresa.codempresa = glusuario.codempresa",
													 "glfilial" => "glfilial.codcliente = glusuario.codcliente and glfilial.codempresa = glusuario.codempresa and glfilial.codfilial = glusuario.codfilial");
		$this->arrDados['consulta2']['where'] = array("glusuario.CODUSUARIO" => $_SESSION['codusuario']);		
		$arrResult = $this->Mcrud->getDados($this->arrDados['consulta2']);
		$rowResult = $arrResult->row();
		
		if(is_object( $rowResult )){
			$this->session->set_userdata(array("codcliente" => $rowResult -> codcliente ,
								   	   "nomecliente" => $rowResult -> nomecliente,
									   "codempresa" => $rowResult -> codempresa,
									   "nomeempresa" => $rowResult -> nomeempresa,
									   "codfilial" => $rowResult -> codfilial,
									   "nomefilial" => $rowResult -> nomefilial));
		}

		?>
		<script type="text/javascript">
			$("#btModal-Message-Close").click();
			window.location.href = "app";
		</script>
		<?php
	}
	
}
	