<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Mparametro.
 * Classe para gerencias os parametros do sistema.
 * 
 * @package global
 * @version 0.1
 */
class Marmazenamento extends CI_Model{
	
	// Atributos
	
	/**
	 * $raiz.
	 * Local de armazenamento default do sistema ( Já não esta como eu queria.... Mas ta sussa ).
	 * 
	 */
	private $raiz;
	
	/**
	 * $pastasCliente.
	 * Mapeamento das pastas mais importantes do cliente.
	 * 
	 */
	private $pastasCliente;
	private $pastasEmpresa;
	private $pastasFiliais;
	
	// Metodos
	
	/**
	 * __construct.
	 * Inicia o necessario para a classe funcionar :D.
	 * 
	 */
	public function __construct() {
        parent::__construct ();
		
		$this->load->model("global/Mparametro","Mparametro");
		$this->load->model("global/Mcrud","Mcrud");
		$this->montaEstrutura();
		$this->criaEstrutura();
    }
	
	/**
	 * getMapeamentoDiretorios.
	 * Retorna todo o mapeamento de pastas local do cliente.
	 * 
	 */
	public function getMapeamentoDiretorios( $tipo = 'array' ){
		switch( $tipo ){
			case 'jstree':
				/** Da para fazer uma recursividade aqui em jovem :D **/
				$return = '';
				// Navega pela Estrutura do Cliente
				foreach( $this->pastasFiliais as $codCliente => $arrEmpresa ){
					//Aqui não pretendo listar nada..
					foreach( $arrEmpresa as $codEmpresa => $arrFiliais ){
						// Empresa
						$return .= '	    	<ul>';
						$return .= '	            <li>';
						$return .= '	                Nome da Empresa - '.$codEmpresa;
						foreach( $arrFiliais as $codFilial => $naoSei ){
							$return .= '	                <ul>';
							$return .= '	                    <li>';
							$return .= '	                    	Filial - '.$codFilial;
							// Aqui eu preciso listar o que tem realmente dentro da pasta. Na vdd da para fazer uma recursividade em função de listagem
							$return .= '	                    	<ul>';
							$return .= '	                            <li>';
							$return .= '	                            	2015';
							// Aqui os meses
							for( $m = 1; $m <= 12 ; $m++ ){
								$return .= '	                            	<ul>';
								$return .= '	                            		<li>';
								$return .= '	                            			Mês - '.$m;
								for( $d = 1; $d <= 30 ; $d++ ){
									$return .= '	                            			<ul>';
									$return .= '	                            				<li> Dia '.$d;
									// Algumas Notas
									$return .= '	                            				<ul>';
									$return .= '	                            					<li data-jstree='."'".'{"icon":"glyphicon glyphicon-note"}'."'".' > Nota Teste 1 </li>';
									$return .= '	                            					<li data-jstree='."'".'{"icon":"glyphicon glyphicon-note"}'."'".' > Nota Teste 2 </li>';
									$return .= '	                            					<li data-jstree='."'".'{"icon":"glyphicon glyphicon-note"}'."'".' > Nota Teste 3 </li>';
									$return .= '	                            				</ul>';
									// Algumas Notas
									$return .= ' </li>';	
									$return .= '	                            			</ul>';
								}
								$return .= '	                            		</li>';
								$return .= '	                            	</ul>';
							}
							$return .= '	                		    </li>';
							$return .= '	                        </ul>';
							// Filial =P
							$return .= '	       				 </li>';
							$return .= '	               	</ul>';	
						}
						$return .= '   				 </li>';
						$return .= '			</ul>';	
					}
				}
				$return .= '	           			</li>';
				$return .= '	                </u>';
				break;
			case 'array':
			default:
				$return = array(
					'raiz' => $this->raiz,
					'cliente' => $this->pastasCliente,
					'empresa' => $this->pastasEmpresa,
					'filial' => $this->pastasFiliais,
				);
				break;
		}
		
		return $return;
	}
	
	/**
	 * montaEstrutura.
	 * Controis a estrutura necessario para funcionar o armazenamento Local
	 * 
	 * @param int $codcliente
	 */
	public function montaEstrutura($codcliente = null){
		// Mapea as pastas do sistema
		$this->raiz = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'armazenamentos'.DIRECTORY_SEPARATOR;
		$arrBusca['cabecalho'] = array(	
										'glfilial.CODCLIENTE' => 'codcliente',
										'glfilial.CODEMPRESA' => 'codempresa',
										'glfilial.CODFILIAL' => 'codfilial',
									  );
		$arrBusca['where'] = array(	
									'glfilial.CODCLIENTE' => $this->session->userdata('codcliente'),
								  );
		$this->Mcrud->setStrTable('glfilial');
		$rsFiliais = $this->Mcrud->getDados( $arrBusca );
		$arrFilial = $rsFiliais->result_array();
		foreach( $arrFilial as $key => $arrValues ){
			
			$pastaCliente = $arrValues['codcliente'].DIRECTORY_SEPARATOR;
			$pastaEmpresa = $arrValues['codempresa'].DIRECTORY_SEPARATOR;
			$pastaCertificado = 'cert'.DIRECTORY_SEPARATOR;
			$pastaFilial = $arrValues['codfilial'].DIRECTORY_SEPARATOR;
			
			$this->pastasCliente[$arrValues['codcliente']]['raiz'] = $pastaCliente;
			$this->pastasEmpresa[$arrValues['codcliente']][$arrValues['codempresa']]['raiz'] = $pastaEmpresa;
			$this->pastasEmpresa[$arrValues['codcliente']][$arrValues['codempresa']]['certificado'] = $pastaCertificado;
			$this->pastasFiliais[$arrValues['codcliente']][$arrValues['codempresa']][$arrValues['codfilial']]['raiz'] = $pastaFilial;
			
		}
		
	}
	
	/**
	 * criaEstrutura.
	 * Cria a estrutura inicial de pasta no sistema, de acordo com o cliente logado no sistema.
	 * 
	 */
	public function criaEstrutura(){
		
		// Variaveis
		$strPastaCliente = '';
		$strPastaEmpresa = '';
		$strPastaFilial = '';
		$strPastaCertificado = '';
		// Valida e Cria os diretorios necessarios
		foreach( $this->pastasFiliais as $codcliente => $arrEmpresas ){
			// Pasta do Cliente
			if( !file_exists( $strPastaCliente = $this->raiz ) )
				mkdir( $strPastaCliente = $this->raiz );
			$strPastaCliente = $this->raiz.$this->pastasCliente[$codcliente]['raiz'];
			if( !file_exists( $strPastaCliente ) )
				mkdir( $strPastaCliente );
			foreach( $arrEmpresas as $codempresa => $arrFiliais ){
				// Pastas da Empresa e Certificado
				$strPastaEmpresa = $this->raiz.$this->pastasCliente[$codcliente]['raiz'].$this->pastasEmpresa[$codcliente][$codempresa]['raiz'];
				$strPastaCertificado = $this->raiz.$this->pastasCliente[$codcliente]['raiz'].$this->pastasEmpresa[$codcliente][$codempresa]['raiz'].$this->pastasEmpresa[$codcliente][$codempresa]['certificado'];
				if( !file_exists( $strPastaEmpresa ) ){
					mkdir( $strPastaEmpresa );
					mkdir( $strPastaCertificado );
				}
				foreach( $arrFiliais as $codfilial => $arrPastas ){
					// Pasta Filial
					$strPastaFilial = $this->raiz.$this->pastasCliente[$codcliente]['raiz'].$this->pastasEmpresa[$codcliente][$codempresa]['raiz'].$arrPastas['raiz'];
					if( !file_exists( $strPastaFilial ) )
						mkdir( $strPastaFilial );
				}
			}
		}
		
	}
	
}

?>	