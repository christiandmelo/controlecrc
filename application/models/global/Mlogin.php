<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlogin extends CI_Model{ 
	
	/**
	 * Mtodo para fazer a pesquisa do usuário
	 */
	public function getAutentica($condicao){
		$this->db->select("glusuario.NOME, glusuario.EMAIL, glusuario.CAMINHOFOTO, glusuario.TIPOUSUARIO,glusuario.CODCLIENTE,glcliente.NOMEFANTASIA AS 'NOMECLIENTE', glusuario.CODEMPRESA, glempresa.NOMEFANTASIA AS 'NOMEEMPRESA', glusuario.CODFILIAL, glfilial.NOMEFANTASIA AS 'NOMEFILIAL', glusuario.CODUSUARIO");
		$this->db->from('glusuario');
		$this->db->join('glcliente', 'glcliente.CODCLIENTE = glusuario.CODCLIENTE');
		$this->db->join('glempresa', 'glempresa.CODCLIENTE = glusuario.CODCLIENTE and glempresa.CODEMPRESA = glusuario.CODEMPRESA');
		$this->db->join('glfilial', 'glfilial.CODCLIENTE = glusuario.CODCLIENTE and glfilial.CODEMPRESA = glusuario.CODEMPRESA and glfilial.CODFILIAL = glusuario.CODFILIAL');
		$this->db->where('glusuario.EMAIL',$condicao['email']);
		$this->db->where('glusuario.SENHA',$condicao['senha']);
		$this->db->where('glcliente.LIBERADO',1,false);
		$this->db->where('glusuario.ATIVO',1,false);
		
		$usuario = $this->db->get()->row();
		
		if(isset($usuario->EMAIL) == $condicao['email'] && $condicao['email'] !== '' ){
			$this->session->set_userdata(array("logado" => true,
										   	   "nomeusuario" => (isset($usuario->NOME))?$usuario->NOME:'',
									   	  	   "tipousuario" => (isset($usuario->TIPOUSUARIO))?$usuario->TIPOUSUARIO:'',
										   	   "caminhoFoto" => (isset($usuario->CAMINHOFOTO))?$usuario->CAMINHOFOTO:'',
										   	   "codcliente" => (isset($usuario->CODCLIENTE))?$usuario->CODCLIENTE:'' ,
										   	   "nomecliente" => (isset($usuario->NOMECLIENTE))?$usuario->NOMECLIENTE:'',
										   	   "codempresa" => (isset($usuario->CODEMPRESA))?$usuario->CODEMPRESA:'',
										   	   "nomeempresa" => (isset($usuario->NOMEEMPRESA))?$usuario->NOMEEMPRESA:'',
										   	   "codfilial" => (isset($usuario->CODFILIAL))?$usuario->CODFILIAL:'',
										   	   "nomefilial" => (isset($usuario->NOMEFILIAL))?$usuario->NOMEFILIAL:'',
										   	   "codusuario" => (isset($usuario->CODUSUARIO ))?$usuario->CODUSUARIO:''));	
		}		
	}
}

?>