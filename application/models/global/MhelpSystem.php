<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MHelpSystem.
 * Classe que o sistema de ajuda do portal.
 * 
 * @package global
 * @version 0.1
 */
class MhelpSystem extends CI_Model{
	
	/**
	 * __construct.
	 * Fun��o de intancia da classe. Carrega os modulos necessarios para funcionamento.
	 * 
	 */
	public function __construct(){
		parent::__construct();
	}
	
	/**
	 * $array_result.
	 * Tipo de retorno do 'getSystemHelps'. Retorna um array result();
	 * 
	 * @var integer
	 */
	public static $array_result = 0;
	
	/**
	 * $json_help_system.
	 * Tipo de retorno do 'getSystemHelps'. Trata o resultado, e retorna um array onde a chave � o ID do helper e o valor e o TEXTO dele;
	 * 
	 * @var integer
	 */
	public static $json_help_system = 1;
	
	/**
	 * getSystemHelps.
	 * Busca os helps da tela cadastrados no sistema
	 * 
	 * @param number $returnTipe
	 * @param string $caminhoOrdem
	 * @param string $user
	 */
	public function getSystemHelps($returnTipe = 0,$caminhoOrdem = null,$user = null) {
		// Busca todos os helpers cadastrados no banco de dados
		$strConsulta = "
				SELECT *
				FROM glhelpsystem
			";
		$arrDados = array();
		$helpers = $this->db->query($strConsulta,$arrDados);
		// Verifica o tipo de retorno
		switch( $returnTipe ){
			case 0:
				$return = $helpers->result();
				break;
			case 1:
				foreach( $helpers->result() as $orderHelper => $objHelper ){
					$return[$objHelper->ID] = $objHelper->TEXTO;
				}
				break;
			default:
				$return = false;
		}
		return $return;
	}
	
	/**
	 * saveHelp.
	 * Trata o array e salva o helper no banco de dados.
	 * 
	 * @todo A ideia e fazer a fun��o salvar... Mas ela n�o faz nada... :P
	 * 
	 * @param array $arrParans
	 */
	public function saveHelp($arrParans){
		return false;
	}
	
}

?>