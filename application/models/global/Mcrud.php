<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Mcrud.
 * Classe para facilizar as ações de CRUD do sistema.
 * 
 * @package global
 * @version 0.1
 */
class Mcrud extends CI_Model{
	
	// Atributos
	
	private $strTable;
	
	// Metodos
	
	/**
	 * getStrTable.
	 * Recupera o nome da Table que esta sendo feita o CRUD.
	 * 
	 * @return nome da Tabela
	 */
	public function getStrTable(){
		return $this->strTable;
	}
	
	/**
	 * setStrTable.
	 * Adiciona o nome da Table que esta sendo feita o CRUD.
	 * 
	 */
	public function setStrTable( $strTable ){
		return $this->strTable = $strTable;
	}
	
	/**
	 * getDados.
	 * Recupera o(s) registro(s) do db.
	 * 
	 * @param $arrData - Se indicado, retorna somente um registro, caso contário, todos os registros.
	 * @param $tipo - Se indicado, busca os parametros para montar a consulta de acordo com o informado, caso não informado busca do lista.
	 * @return objeto da banco de dados da tabela cadastros
	 */
	public function getDados($arrData = null){
		
		/*carrega os cabecalhos da consulta*/
		if( isset($arrData['cabecalho']) && is_array($arrData['cabecalho'])){
			foreach( $arrData['cabecalho'] as $key => $values){
				$this->db->select($key." as ".$values);
			}
		}
		
		/*carregas os joins da consulta*/
		if( isset($arrData['join']) && is_array($arrData['join'])){
			foreach( $arrData['join'] as $key => $values){
				$this->db->join($key, $values);		
			}
		}
		
		/*carregas os left joins da consulta*/
		if( isset($arrData['left-join']) && is_array($arrData['left-join'])){
			foreach( $arrData['left-join'] as $key => $values){
				$this->db->join($key, $values,'left');		
			}
		}
		
		/*carrega os where da consulta*/
		if( isset( $arrData['where'] ) && is_array( $arrData['where'] ) ){
			foreach( $arrData['where'] as $key => $value ){
				$this->db->where( $key , $value );
			}
		}
		
		
		/*carrega os where com like da consulta*/
		if( isset( $arrData['like'] ) && is_array( $arrData['like'] ) ){
			foreach( $arrData['like'] as $key => $value ){
				$this->db->like( $key , $value );
			}
		}
		
		/*carrega o order by da consulta*/
		if (isset( $arrData['order']) && is_array( $arrData['order'])){
			foreach ( $arrData['order'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}
		
		return $this->db->get( $this->strTable );
	}
	
	/**
	 * removeDados.
	 * Deleta um registro do db.
	 * 
	 * @param $arrFiltroEdicao do registro a ser deletado
	 * @return boolean;
	 */
	public function removeDados($arrFiltroEdicao = null){
		if( isset($arrFiltroEdicao) ){
			foreach( $arrFiltroEdicao as $key => $value ){
				$this->db->where( $key , $value );
			}
			if ($this->db->delete( $this->strTable )){
				return true;
			}else{
				return false;
			}
		}
	}
	
	/**
	 * setDados.
	 * Grava o(s) dado(s) na tabela.
	 * 
	 * @param $arrDados. Array que contém os campos a serem inseridos
	 * @param $arrFiltroEdicao. Se for passado o $arrFiltroEdicao via parâmetro, então atualizo o registro em vez de inseri-lo.
	 * @return boolean
	 */
	public function setDados($arrDados = null, $arrChave = null, $arrValoresDaChave = null, $arrFiltroEdicao = null, $codigoTabela = null) {
		
		//Se $arrDados não for um Array ele não faz porra nenhuma
		if( is_array($arrDados) ){
			//Preenche no Array arrDados os campos de MODIF das tabelas
			$arrDados['REGMODIFPOR'] = ( !isset( $_SESSION['codusuario'] ) ? 0 : $_SESSION['codusuario'] );
			$arrDados['REGMODIFEM'] = date("Y-m-d H:i:s");
			
			// caso a variavel $arrFiltroEdicao esteja preenchida, atualiza o registro.
			if( isset($arrFiltroEdicao) ){
				
				//preeche os dados do where
				foreach( $arrFiltroEdicao as $key => $value ){
					$this->db->where( $key , $value );
				}
				
				if(isset($arrChave) && $arrChave != null){
					foreach($arrChave as $keyAuto => $valueAuto){
						//$arrAutoInc['autoinc']['where'][$valueAuto] = $arrValoresDaChave[$keyAuto];
						$this->db->where( $this->strTable.'.'.$valueAuto , $arrValoresDaChave[$keyAuto] );
					}
				}
				
				//Realiza o update
				if( $this->db->update( $this->strTable , $arrDados) ){
					return true;
				} else {
					return false;
				}
				
			//caso a variavel $arrFiltroEdicao não esteja preenchida prepara para inserir um novo registro
			}else{
				//Preenche no Array arrDados os campos de CRIADO das tabelas
				$arrDados['REGCRIADOPOR'] = ( !isset( $_SESSION['codusuario'] ) ? 0 : $_SESSION['codusuario'] );
				$arrDados['REGCRIADOEM'] = date("Y-m-d H:i:s");
				
				$updAutoInc = false;
				$valorAutoInc = false;
				
				if(isset($arrValoresDaChave) && $arrValoresDaChave != null){
					// Aqui é para buscar e atualizar na glautoindice
					$tabelaPrincipal = $this->getStrTable();
					$arrAutoInc = array();
					$arrAutoInc['autoinc']['cabecalho']['(VALOR+1)'] = 'VALOR';
					
					// Navega pelo array para montar a condição
					foreach($arrChave as $keyAuto => $valueAuto){
						$arrAutoInc['autoinc']['where'][$valueAuto] = $arrValoresDaChave[$keyAuto];
					}
					
					// Busca na GLAUTOINDICE
					$arrAutoInc['autoinc']['where']['TABELA'] = $this->getStrTable();
					$this->setStrTable('glautoindice');
					$arrAutoIndiceDados = $this->getDados($arrAutoInc['autoinc']);
					$arrValorAutoInc = $arrAutoIndiceDados -> first_row();
					
					// Caso não encontre valor, inseri um primeiro com valor 1
					if( !isset( $arrValorAutoInc ) ){
						$arrDadosAutoIndiceInsert = $arrAutoInc['autoinc']['where'];
						$arrDadosAutoIndiceInsert['TABELA'] = $tabelaPrincipal;
						$arrDadosAutoIndiceInsert['VALOR'] = 1;
						$this->db->insert( $this->strTable , $arrDadosAutoIndiceInsert);
						$valorAutoInc = 1;
					}else{
						$updAutoInc = true;
						$valorAutoInc = $arrValorAutoInc->VALOR;
					}
					//Adiciona o valor encontrado no insert
					$arrDados[$codigoTabela] = $valorAutoInc;
					
					// Volta para a tabela que estava
					$this->setStrTable($tabelaPrincipal);
				}

				if( $this->db->insert( $this->strTable , $arrDados) ){
					if( $valorAutoInc === false ){
						$insertId = $this->db->insert_id($this->strTable);
					}else{
						$insertId = $valorAutoInc;
					}
					if( $updAutoInc ){
						$this->setStrTable('glautoindice');
						foreach( $arrAutoInc['autoinc']['where'] as $key => $value ){
							$this->db->where( $key , $value );
						}
						$this->db->update( $this->strTable , array('VALOR'=>($valorAutoInc==1?2:$valorAutoInc)));
						$this->setStrTable($tabelaPrincipal);
					}
					return $insertId;
				} else {
					return false;
				}
				
			}
		}
		
	}
	
}

?>