<div class="p-20">
	<!-- begin row -->
	<div class="row">
	    <!-- begin col-2 -->
	    <div class="col-md-2">
	        <form>
	            <div class="input-group m-b-15">
                    <input type="text" class="form-control input-sm input-white" placeholder="Pesquisar Email">
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-inverse" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
	        </form>
	        <div class="hidden-sm hidden-xs">
                <h5 class="m-t-20">Email</h5>
                <ul class="nav nav-pills nav-stacked nav-inbox">
                    <li class="active">
                        <a href="#">
                            <i class="fa fa-inbox fa-fw m-r-5"></i> Entrada (10)
                        </a>
                    </li>
                    <li><a href="#"><i class="fa fa-inbox fa-fw m-r-5"></i> Enviados</a></li>
                    <li><a href="#"><i class="fa fa-pencil fa-fw m-r-5"></i> Rascunho</a></li>
                    <li><a href="#"><i class="fa fa-trash-o fa-fw m-r-5"></i> Lixeira</a></li>
                    <li><a href="#"><i class="fa fa-star fa-fw m-r-5"></i> Favoritos</a></li>
                </ul>
            </div>
        </div>
	    <!-- end col-2 -->
	    <!-- begin col-10 -->
	    <div class="col-md-10">
            <div class="email-btn-row hidden-xs">
                <a href="#" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> Novo</a>
                <a href="#" class="btn btn-sm btn-default disabled">Resposta</a>
                <a href="#" class="btn btn-sm btn-default disabled">Apagar</a>
                <a href="#" class="btn btn-sm btn-default disabled">Arquivar</a>
                <a href="#" class="btn btn-sm btn-default disabled">Lixo</a>
                <a href="#" class="btn btn-sm btn-default disabled">Mover para</a>
                <a href="#" class="btn btn-sm btn-default disabled">Categorias</a>
            </div>
	        <div class="email-content">
                <table class="table table-email">
                    <thead>
                        <tr class="">
                            <th class="email-select"><a href="#" data-click="email-select-all"><i class="fa fa-square-o fa-fw"></i></a></th>
                            <th colspan="2">
                                <div class="dropdown">
                                    <a href="#" class="email-header-link" data-toggle="dropdown" aria-expanded="false">Mostrar Todos <i class="fa fa-angle-down m-l-5"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="active"><a href="#">Todos</a></li>
                                        <li><a href="#">Não Lido</a></li>
                                        <li><a href="#">Contatos</a></li>
                                        <li><a href="#">Grupos</a></li>
                                    </ul>
                                </div>
                            </th>
                            <th>
                                <div class="dropdown">
                                    <a href="#" class="email-header-link" data-toggle="dropdown" aria-expanded="false">Ordenar por <i class="fa fa-angle-down m-l-5"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="active"><a href="#">Data</a></li>
                                        <li><a href="#">Assunto</a></li>
                                        <li><a href="#">Tamanho</a></li>
                                    </ul>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Leap Motion
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Maecenas ultrices interdum leo, eu aliquam diam mattis sed.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Leap Motion
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Ut tristique dapibus nibh, sed scelerisque magna vehicula a.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Stefie Chin
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Etiam pretium neque vitae vulputate fermentum.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Alan Tan
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Duis et justo in nisl tristique lobortis id at ligula.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Yu Ming Tan
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Sed dapibus nec velit eget pulvinar. Etiam id erat in eros imperdiet tempus.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Harvinder Signh
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Aliquam diam risus, condimentum ut diam in, fermentum euismod sem.
                            </td>
                            <td class="email-date">12/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Fiona Loh
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Praesent dapibus ultricies magna, ac laoreet ante pellentesque nec. 
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Derrick Tew
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Nullam eget nibh et dui vestibulum aliquam vitae a lacus.
                            </td>
                            <td class="email-date">10/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Terry Dew
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Nulla eget placerat ante, sed hendrerit felis. Praesent vitae convallis erat.
                            </td>
                            <td class="email-date">09/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                John Doe
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Sed dapibus nec velit eget pulvinar. Etiam id erat in eros imperdiet tempus.
                            </td>
                            <td class="email-date">08/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Leap Motion
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Ut tristique dapibus nibh, sed scelerisque magna vehicula a.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Stefie Chin
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Etiam pretium neque vitae vulputate fermentum.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Alan Tan
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Duis et justo in nisl tristique lobortis id at ligula.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Yu Ming Tan
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Sed dapibus nec velit eget pulvinar. Etiam id erat in eros imperdiet tempus.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Fiona Loh
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Praesent dapibus ultricies magna, ac laoreet ante pellentesque nec. 
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Derrick Tew
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Nullam eget nibh et dui vestibulum aliquam vitae a lacus.
                            </td>
                            <td class="email-date">10/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Terry Dew
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Nulla eget placerat ante, sed hendrerit felis. Praesent vitae convallis erat.
                            </td>
                            <td class="email-date">09/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                John Doe
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Sed dapibus nec velit eget pulvinar. Etiam id erat in eros imperdiet tempus.
                            </td>
                            <td class="email-date">08/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Leap Motion
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Ut tristique dapibus nibh, sed scelerisque magna vehicula a.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                        <tr class="">
                            <td class="email-select"><a href="#" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                            <td class="email-sender">
                                Stefie Chin
                            </td>
                            <td class="email-subject">
                                <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                Etiam pretium neque vitae vulputate fermentum.
                            </td>
                            <td class="email-date">11/4/2015</td>
                        </tr>
                    </tbody>
                </table>
                <div class="email-footer clearfix">
                    737 messages
                    <ul class="pagination pagination-sm m-t-0 m-b-0 pull-right">
                        <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-double-left"></i></a></li>
                        <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-left"></i></a></li>
                        <li><a href="javascript:;"><i class="fa fa-angle-right"></i></a></li>
                        <li><a href="javascript:;"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div>
	        </div>
	    </div>
	    <!-- end col-10 -->
	</div>
	<!-- end row -->
	</div>
</div>