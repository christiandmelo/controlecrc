		<?php
			include APPPATH . "/views/funcoes/funcoes.php";
		?>
		<!-- begin #content -->
		<div class="content" id="content" >
			
			<!-- <form class="form-horizontal">
				<div class="col-lg-5">
					<div class="form-group">
						<label class="control-label col-md-4">Theme White</label>
						<div class="col-md-8">
						    <select class="form-control selectpicker" data-live-search="true" data-style="btn-white" style="display: none;">
                                <option value="" selected>Select a Country</option>
                                <option value="AF">Afghanistan</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andorra</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AQ">Antarctica</option>
                                <option value="AG">Antigua and Barbuda</option>
                            </select>
						</div>
					</div>
				</div>
			</form> -->
			
			
			<?php
				// Mostra o Breadcrumb caso ele seja enviado
				if( isset($breadcrumb) )
					echo $breadcrumb;
			?>
			<!-- begin row -->
			<div class="row" >
				<?php
					if( !is_string( $menu ) || $menu == '' ):
				?>
				
			    <!-- begin col-3 -->
			    <div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-black">
			            <div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
			            <div class="stats-title">Falha ao Carregar Aplicativos</div>
			            <!--
			            <div class="stats-number">7,842,900</div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 70.1%;"></div>
                        </div>
                        -->
                        <div class="stats-desc">Você não possui acesso aos aplicativos!</div>
			        </div>
			    </div>
			    
				<?php
					else:
						//echo $menu;
					endif;
				?>
			</div>
			
			<div class="row">
			    <!-- begin col-4 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
			        <div class="panel panel-inverse" data-sortable-id="index-2">
			            <div class="panel-heading">
			                <h4 class="panel-title">Receitas e Despesas</h4>
			            </div>
			            <div class="panel-body">
                            <div id="chart_div"></div>
                            <?php
								$dt_inicial=fconvertDataAmericana($data_inicial); 
								$dt_final=fconvertDataAmericana($data_final); 
								
								$di = strtotime($dt_inicial); 
								$df=strtotime($dt_final);
								$compChart = ""; 
								while($di<$df){
									$compChart .= "['".date('m/Y',$di)."',"; 
									$achou = 0;
									$achouTipo1 = 0;
									$achouTipo2 = 0;
									foreach($LancamentosPorMes as $key => $value){
										if($value['VENCIMENTO'] == date('m/Y',$di)){
											$achou = 1;
											if($value['CODTIPOLANCAMENTO'] == 2){
												$achouTipo2 = 1;
												$compChart .= $value['VALOR'].",";
											}
											
											if($value['CODTIPOLANCAMENTO'] == 1){
												$achouTipo1 = 1;
												if($achouTipo2 == 0){
													$compChart .= "0,";
												}
												$compChart .= $value['VALOR']."],";
											}
										}
									}
									if($achou == 1 && $achouTipo1 == 0){$compChart .= "0],";}
									if($achou == 0){ $compChart .= "0,0],";} 
									$di = strtotime('@'.$di.'+ 1 month'); 
								} 
					        ?>
                            <script type="text/javascript">
                                  google.charts.load('current', {'packages':['bar']});
							      google.charts.setOnLoadCallback(drawChart);
							
							      function drawChart() {
							        var data = google.visualization.arrayToDataTable([
							          ['', 'Receita', 'Despesa'],
							          <?=$compChart;?>
							        ]);
							        var options = {
							          chart: {
							            title: 'Gráfico com o Total Mensal de Receitas e Despesas',
							          },
							          bars: 'vertical',
							          vAxis: {format: 'decimal'},
							          height: 300
							        };
							        var chart = new google.charts.Bar(document.getElementById('chart_div'));
							        chart.draw(data, google.charts.Bar.convertOptions(options));
							      }
								</script>
                        </div>
			        </div>
			    </div>
			</div>
			<!-- end row -->
			
			<?php /*
			
			<!-- begin row -->
			<div class="row">
			    <div class="col-md-8">
			        <div class="widget-chart with-sidebar bg-black">
			            <div class="widget-chart-content">
			                <h4 class="chart-title">
			                    Visitors Analytics
			                    <small>Where do our visitors come from</small>
			                </h4>
			                <div id="visitors-line-chart" class="morris-inverse" style="height: 260px;"></div>
			            </div>
			            <div class="widget-chart-sidebar bg-black-darker">
			                <div class="chart-number">
			                    1,225,729
			                    <small>visitors</small>
			                </div>
			                <div id="visitors-donut-chart" style="height: 160px"></div>
			                <ul class="chart-legend">
			                    <li><i class="fa fa-circle-o fa-fw text-success m-r-5"></i> 34.0% <span>New Visitors</span></li>
			                    <li><i class="fa fa-circle-o fa-fw text-primary m-r-5"></i> 56.0% <span>Return Visitors</span></li>
			                </ul>
			            </div>
			        </div>
			    </div>
			    <div class="col-md-4">
			        <div class="panel panel-inverse" data-sortable-id="index-1">
			            <div class="panel-heading">
			                <h4 class="panel-title">
			                    Visitors Origin
			                </h4>
			            </div>
			            <div id="visitors-map" class="bg-black" style="height: 181px;"></div>
			            <div class="list-group">
                            <a href="#" class="list-group-item list-group-item-inverse text-ellipsis">
                                <span class="badge badge-success">20.95%</span>
                                1. United State 
                            </a> 
                            <a href="#" class="list-group-item list-group-item-inverse text-ellipsis">
                                <span class="badge badge-primary">16.12%</span>
                                2. India
                            </a>
                            <a href="#" class="list-group-item list-group-item-inverse text-ellipsis">
                                <span class="badge badge-inverse">14.99%</span>
                                3. South Korea
                            </a>
                        </div>
			        </div>
			    </div>
			</div>
			<!-- end row -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-4 -->
			    <div class="col-md-4">
			        <!-- begin panel -->
			        <div class="panel panel-inverse" data-sortable-id="index-2">
			            <div class="panel-heading">
			                <h4 class="panel-title">Chat History <span class="label label-success pull-right">4 message</span></h4>
			            </div>
			            <div class="panel-body bg-silver">
                            <div data-scrollbar="true" data-height="225px">
                                <ul class="chats">
                                    <li class="left">
                                        <span class="date-time">yesterday 11:23pm</span>
                                        <a href="javascript:;" class="name">Sowse Bawdy</a>
                                        <a href="javascript:;" class="image"><img alt="" src="/assets/img/user-12.jpg" /></a>
                                        <div class="message">
                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit volutpat. Praesent mattis interdum arcu eu feugiat.
                                        </div>
                                    </li>
                                    <li class="right">
                                        <span class="date-time">08:12am</span>
                                        <a href="#" class="name"><span class="label label-primary">ADMIN</span> Me</a>
                                        <a href="javascript:;" class="image"><img alt="" src="/assets/img/user-13.jpg" /></a>
                                        <div class="message">
                                            Nullam posuere, nisl a varius rhoncus, risus tellus hendrerit neque.
                                        </div>
                                    </li>
                                    <li class="left">
                                        <span class="date-time">09:20am</span>
                                        <a href="#" class="name">Neck Jolly</a>
                                        <a href="javascript:;" class="image"><img alt="" src="/assets/img/user-10.jpg" /></a>
                                        <div class="message">
                                            Euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                        </div>
                                    </li>
                                    <li class="left">
                                        <span class="date-time">11:15am</span>
                                        <a href="#" class="name">Shag Strap</a>
                                        <a href="javascript:;" class="image"><img alt="" src="/assets/img/user-14.jpg" /></a>
                                        <div class="message">
                                            Nullam iaculis pharetra pharetra. Proin sodales tristique sapien mattis placerat.
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <form name="send_message_form" data-id="message-form">
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" name="message" placeholder="Enter your message here.">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-sm" type="button">Send</button>
                                    </span>
                                </div>
                            </form>
                        </div>
			        </div>
			        <!-- end panel -->
			    </div>
			    <!-- end col-4 -->
			    <!-- begin col-4 -->
			    <div class="col-md-4">
			        <!-- begin panel -->
			        <div class="panel panel-inverse" data-sortable-id="index-3">
			            <div class="panel-heading">
			                <h4 class="panel-title">Today's Schedule</h4>
			            </div>
			            <div id="schedule-calendar" class="bootstrap-calendar"></div>
			            <div class="list-group">
                            <a href="#" class="list-group-item text-ellipsis">
                                <span class="badge badge-success">9:00 am</span> Sales Reporting
                            </a> 
                            <a href="#" class="list-group-item text-ellipsis">
                                <span class="badge badge-primary">2:45 pm</span> Have a meeting with sales team
                            </a>
                        </div>
			        </div>
			        <!-- end panel -->
			    </div>
			    <!-- end col-4 -->
			    <!-- begin col-4 -->
			    <div class="col-md-4">
			        <!-- begin panel -->
			        <div class="panel panel-inverse" data-sortable-id="index-4">
			            <div class="panel-heading">
			                <h4 class="panel-title">New Registered Users <span class="pull-right label label-success">24 new users</span></h4>
			            </div>
                        <ul class="registered-users-list clearfix">
                            <li>
                                <a href="javascript:;"><img src="/assets/img/user-5.jpg" alt="" /></a>
                                <h4 class="username text-ellipsis">
                                    Savory Posh
                                    <small>Algerian</small>
                                </h4>
                            </li>
                            <li>
                                <a href="javascript:;"><img src="/assets/img/user-3.jpg" alt="" /></a>
                                <h4 class="username text-ellipsis">
                                    Ancient Caviar
                                    <small>Korean</small>
                                </h4>
                            </li>
                            <li>
                                <a href="javascript:;"><img src="/assets/img/user-1.jpg" alt="" /></a>
                                <h4 class="username text-ellipsis">
                                    Marble Lungs
                                    <small>Indian</small>
                                </h4>
                            </li>
                            <li>
                                <a href="javascript:;"><img src="/assets/img/user-8.jpg" alt="" /></a>
                                <h4 class="username text-ellipsis">
                                    Blank Bloke
                                    <small>Japanese</small>
                                </h4>
                            </li>
                            <li>
                                <a href="javascript:;"><img src="/assets/img/user-2.jpg" alt="" /></a>
                                <h4 class="username text-ellipsis">
                                    Hip Sculling
                                    <small>Cuban</small>
                                </h4>
                            </li>
                            <li>
                                <a href="javascript:;"><img src="/assets/img/user-6.jpg" alt="" /></a>
                                <h4 class="username text-ellipsis">
                                    Flat Moon
                                    <small>Nepalese</small>
                                </h4>
                            </li>
                            <li>
                                <a href="javascript:;"><img src="/assets/img/user-4.jpg" alt="" /></a>
                                <h4 class="username text-ellipsis">
                                    Packed Puffs
                                    <small>Malaysian></small>
                                </h4>
                            </li>
                            <li>
                                <a href="javascript:;"><img src="/assets/img/user-9.jpg" alt="" /></a>
                                <h4 class="username text-ellipsis">
                                    Clay Hike
                                    <small>Swedish</small>
                                </h4>
                            </li>
                        </ul>
			            <div class="panel-footer text-center">
			                <a href="javascript:;" class="text-inverse">View All</a>
			            </div>
			        </div>
			        <!-- end panel -->
			    </div>
			    <!-- end col-4 -->
			</div>
			<!-- end row -->*/
  			 ?>
		</div>
		<!-- end #content -->