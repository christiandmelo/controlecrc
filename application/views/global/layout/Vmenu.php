		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar nav -->
				<ul class="nav">
					<?php
						if( !is_string( $menu ) || $menu == '' ):
					?>
					<li class="has-sub">
						<a href="javascript:;" >
						    <b class="caret pull-right"></b>
						    <i class="fa fa-laptop"></i>
						    <span> Error </span>
					    </a>
						<ul class="sub-menu">
						    <li><a href="/error" data-ajajax="true" > Problema ao carregar o menu </a></li>
						</ul>
					</li>
					<?php
						else:
							?>
							<li class="bg-silver" data-help-system-target="global.buscaMenu" >
								<a data-click="sidebar-minify" href="javascript:;" class="form-horizontal" >
								    <i class="fa fa-search"></i>
								    <span> <input type="text" style="/*display: none; */width: 150px;" class="form-control" placeholder="Buscar" id="busca-sistema-input" autocomplete="off" /> </span>
							    </a>
							</li>
							<li data-help-system-target="global.menuLateral" ></li>
							<?php
							echo $menu;
						endif;
					?>
			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->