		<!-- begin #header -->
		<div id="header" class="header navbar navbar-default navbar-fixed-top">
			<!-- begin container-fluid -->
			<div class="container-fluid">
				<!-- begin mobile sidebar expand / collapse button -->
				<div class="navbar-header">
					<a href="/app" data-help-system-target="global.oriscenter" class="navbar-brand"><img class="logo_ini" src='/assets/img/teste1.png'></img></a>
					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- end mobile sidebar expand / collapse button -->
				
				<!-- Parte que contém o Menu do Contexto -->
				<div class="collapse navbar-collapse pull-left" id="top-navbar">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-help-system-target="global.contexto" >
                                <i class="fa fa-database fa-fw"></i> Tele Centro <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <!--<li>
                                	<a id="divContextoCliente" data-toggle="modal" data-href="global/contexto/Ccontexto?tipo=conta" data-ajajax="#modal-message-body" href="#modal-message">
                                		<i class="fa fa-file-o"></i>
                                		Conta => <?=$_SESSION["nomecliente"];?></a>
                                	</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                	<a id="divContextoEmpresa" data-toggle="modal" data-href="global/contexto/Ccontexto?tipo=empresa" data-ajajax="#modal-message-body" href="#modal-message">
                                		<i class="fa fa-file-o"></i>
                                		Empresa => <?=$_SESSION["nomeempresa"];?></a>
                                	</a>
                                </li>
                                <li class="divider"></li>-->
                                <li>
                                	<a id="divContextoFilial" data-toggle="modal" data-href="global/contexto/Ccontexto?tipo=filial" data-ajajax="#modal-message-body" href="#modal-message">
                                		<i class="fa fa-institution alias"></i>
                                		<?=$_SESSION["nomefilial"];?></a>
                                	</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- Fim Contexto -->
				
				<!-- begin header navigation right -->
				<ul class="nav navbar-nav navbar-right">
					<?php
					/*
					<li class="dropdown" id="busca-sistema" >
						<!-- Busca Marotinha -->
						<form class="navbar-form full-width dropdown-toggle" data-toggle="dropdown" autocomplete="off" >
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Digite sua busca de menu" id="busca-sistema-input" />
								<!-- <span class="btn btn-search dropdown-toggle" ><i class="fa fa-search"></i></span> -->
							</div>
						</form>
                        <ul class="dropdown-menu scrollable-menu-search" data-menu-busca="conteiner" >
                            <?php
                            	// Aqui tem aquele bacalhau
                            	echo $menu;
                            ?>
                        </ul>
					</li>
					*/
					?>
					<!--<li class="dropdown" id="notificacoes-principal" >
						<a onclick='setTimeout(notificacoes,3000);' data-toggle="dropdown" class="dropdown-toggle f-s-14" data-help-system-target="global.alertas" >
							<i class="fa fa-bell-o"></i>
							<span data-notificacoes="quantidade" style="display: none;" class="label">0</span>
						</a>
						<ul class="dropdown-menu media-list pull-right animated fadeInDown">
                            <li class="dropdown-header">Notificações (<span data-notificacoes="quantidade">0</span>)</li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><i class="fa fa-bug media-object bg-red"></i></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Relatório de erros em notas</h6>
                                        <div class="text-muted f-s-11">3 minutos atrás</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><img src="/assets/img/user-1.jpg" class="media-object" alt="" /></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Sergio</h6>
                                        <p>Bom dia! você verificou todas as notas enviadas pelo cliente.</p>
                                        <div class="text-muted f-s-11">25 minutos atrás</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><img src="/assets/img/user-2.jpg" class="media-object" alt="" /></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Alan</h6>
                                        <p>Listo abaixo os erros encontrados.</p>
                                        <div class="text-muted f-s-11">35 minutos atrás</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><i class="fa fa-plus media-object bg-green"></i></div>
                                    <div class="media-body">
                                        <h6 class="media-heading"> Novo usuário registrado</h6>
                                        <div class="text-muted f-s-11">1 hora atrás</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><i class="fa fa-envelope media-object bg-blue"></i></div>
                                    <div class="media-body">
                                        <h6 class="media-heading"> Novo Email de João</h6>
                                        <div class="text-muted f-s-11">2 horas atrás</div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-footer text-center">
                                <a href="#/notificacoes" >Veja Mais</a>
                            </li>
						</ul>
					</li>
					<li id="configuracoes-principal" >
						<a data-help-system-target="global.configuraoesGlobais" >
							<i class="fa fa-cog"></i>
						</a>
					</li>-->
					<li id="ajuda-principal" >
						<a data-help-system="open" >
							<i class="fa fa-question"></i>
						</a>
					</li>
					<li class="dropdown navbar-user" data-help-system-target="global.infoUsuario" >
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?=$_SESSION['caminhoFoto'];?>" alt="" /> 
							<span class="hidden-xs" id="VcabecalhoNomeUsuario" ><?=$_SESSION['nomeusuario'];?></span> <b class="caret"></b>
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li class="arrow"></li>
							<li><a href="#/global/seguranca/usuarios/cperfil">Editar Perfil</a></li> 
							<!--<li><a data-href="#/global/email/cemail" data-ajajax='#content' ><span class="badge badge-danger pull-right">2</span> Email</a></li>
							<li><a data-href="#/">Configurações</a></li>-->
							<li class="divider"></li>
							<li><a href="global/clogin/setSair">Sair</a></li>
						</ul>
					</li>
				</ul>
				<!-- end header navigation right -->
			</div>
			<!-- end container-fluid -->
		</div>
		<!-- end #header -->
		
		
		