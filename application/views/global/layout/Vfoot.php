			
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	
	<!-- #modal-message -->
	<div class="modal modal-message fade" id="modal-message">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="btModal-Message-Close" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Troca de Tele Centro</h4>
				</div>
				<div class="modal-body" id="modal-message-body">
					<p>Carregando...</p>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Fechar</a>
				</div>
			</div>
		</div>
	</div>
	
	<!-- modal de edicao de componente -->
	<div class="modal fade" id="modal-edicao-componente">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Edição de Componente</h4>
						</div>
					</div>	
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="form-group">
									<label class="col-md-2 control-label" for="iptEtiqueta">Etiqueta:</label>
									<div class="col-md-4">
										<input type="text" id="iptEtiqueta" class="form-control" disabled="disabled"/>
									</div>
									<label class="col-md-2 control-label" for="iptLote">Lote:</label>
									<div class="col-md-4">
										<input type="text" id="iptLote" class="form-control" disabled="disabled"/>
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-2 control-label" for="ddlFornecedor">Fornecedor:<label style="color: red;">*</label></label>
									<div class="col-md-4">
										<select class="form-control selectpicker" data-live-search="true" data-style="btn-white" id="ddlFornecedor" disabled="disabled">
											<option value="" selected>Selecione...</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-2 control-label" for="iptDescricao">Descrição:<label style="color: red;">*</label></label>
									<div class="col-md-4">
										<input type="text" id="iptDescricao" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="row" style="background-color: yellow; height: 250px;">
								<div class="form-group">
									
								</div>
							</div>
						</form>
					</div>		
					
				</div>
				<div class="modal-footer">
					<button id="btnEnviarVenda" class="btn btn-sm btn-inverse">Enviar para Venda</button>
					<button id="btnEnviarSucata" class="btn btn-sm btn-inverse">Enviar para Sucata</button>
					<button id="btnEnviarEstoque" class="btn btn-sm btn-inverse">Enviar para Estoque</button>
					<button id="btnSalvar" class="btn btn-sm btn-success">Salvar</button>
					<button id="btnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Cancelar</button>
				</div>
			</div>
		</div>
	</div>			
	
	
	
	<!-- modal de edicao de doacao -->
	<div class="modal fade" id="modal-edicao-doacao">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Edição de Doação</h4>
						</div>
					</div>	
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="form-group">
									<label class="col-md-2 control-label" for="ddlFornecedor">Fornecedor:<label style="color: red;">*</label></label>
									<div class="col-md-4">
										<select class="form-control selectpicker" data-live-search="true" data-style="btn-white" id="ddlFornecedor">
											<option value="" selected>Selecione...</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-2 control-label" for="iptDescricao">Descrição:<label style="color: red;">*</label></label>
									<div class="col-md-4">
										<input type="text" id="iptDescricao" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-2 control-label" for="ddlTipoEntrada">Tipo Entrada:<label style="color: red;">*</label></label>
									<div class="col-md-4">
			                            <div class="radio">
			                                <label id="radio">
			                                    <input type="radio" id="rdComponente" value="option1" checked />
			                                    Componente
			                                </label>
			                                <label id="radio">
			                                    <input type="radio" id="rdEquipamento" value="option2" />
			                                    Equipamento
			                                </label>
			                            </div>
			                        </div>
								</div>
							</div>
							<div class="row" style="background-color: yellow; height: 250px;">
								<div class="form-group">
									
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-lg-4">
										<div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="ckAdicionar" value="" />
                                                Continuar Adicionando
                                            </label>
                                        </div>
									</div>
								</div>
							</div>
						</form>		
					</div>
					
				</div>
				<div class="modal-footer">
					<button id="btnSalvar" class="btn btn-sm btn-success">Salvar</button>
					<button id="btnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
	
	
	
	

	
	<?php /*
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
	*/ ?>
	
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="/assets/crossbrowserjs/html5shiv.js"></script>
		<script src="/assets/crossbrowserjs/respond.min.js"></script>
		<script src="/assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="/assets/plugins/gritter/js/jquery.gritter.js"></script>
	<script src="/assets/plugins/jstree/dist/jstree.min.js"></script>
	<script src="assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="assets/plugins/jquery-mask/jquery.mask.js"></script>
	<script src="assets/plugins/parsley/dist/parsley.js"></script>
	<script src="/assets/js/ui-tree.demo.min.js"></script>
	<script src="/assets/js/apps.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/bootstrap-select.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->

	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
</body>
</html>