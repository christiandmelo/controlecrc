<div class="content" id="content">
	
	<?php
		// Mostra o Breadcrumb caso ele seja enviado
		if( isset($breadcrumb) )
			echo $breadcrumb;
	?>
	
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Filtro</h4>
		</div>
		<div class="panel-body">
			<div class="form-horizontal">
				<div class="row">
					<div class="form-group">
						<!-- campo fornecedor -->
						<label class="col-md-2 control-label" for="ddlFornecedor">Fornecedor:<label style="color: red;">*</label></label>
						<div class="col-md-4">
							<select class="form-control" id="ddlFornecedor">
								<option value="">Selecione...</option>
								<option value="1">Dell Computadores</option>
								<option value="2">Banco do Brasil</option>
								<option value="3">Positivo</option>
								<option value="4">San Marco</option>
							</select>
						</div>
						<!-- campo status -->
						<label class="col-md-2 control-label" for="ddlStatus">Status:<label style="color: red;">*</label></label>
						<div class="col-md-4">
							<select class="form-control" id="ddlStatus">
								<option value="">Selecione...</option>
								<option value="1">Em triagem</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<!-- campo tipo entrada -->
						<label class="col-md-2 control-label" for="ddlTipoEntrada">Tipo Entrada:<label style="color: red;">*</label></label>
						<div class="col-md-4">
                            <div class="radio">
                                <label id="radio">
                                    <input type="radio" id="rdComponente" value="option1" checked />
                                    Componente
                                </label>
                                <label id="radio">
                                    <input type="radio" id="rdEquipamento" value="option2" />
                                    Equipamento
                                </label>
                            </div>
                        </div>
                        <!-- campo tipo doação -->
                        <label class="col-md-2 control-label" for="ddlTipoDoacao">Tipo Doação:</label>
                        <div class="col-md-4">
							<select class="form-control" id="ddlTipoDoacao">
								<option value="">Selecione...</option>
								<option value="1">Tipo 1</option>
								<option value="2">Tipo 2</option>
								<option value="3">Tipo 3</option>
								<option value="4">Tipo 4</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<button id="btnNovaEntrada" class="btn btn-sm btn-success m-r-5">Nova Entrada</button>
				<button id="btnEditar" class="btn btn-sm btn-primary m-r-5">Editar</button>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>		
			</div>
			<h4 class="panel-title">Lista de Doações</h4>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
                <table id="tblListaDoacao" class="table table-striped table-bordered">
                	<thead>
                		<tr>
                			<th> </th>
                			<th>Lote / Etiqueta</th>
                			<th>Descrição</th>
                			<th>Tipo Doação</th>
                			<th>Status</th>
                			<th>Quant. Triar</th>
                			<th>Quant. Triada</th>
                		</tr>
                	</thead>
                	<tbody>
                		<tr class="odd gradeX">
                            <td><input type="checkbox" value="" /></td>
                            <td>999</td>
                            <td>bbbb</td>
                            <td>Mouse</td>
                            <td>Em triagem</td>
                            <td>5</td>
                            <td>0</td>
                        </tr>
                        <tr class="even gradeC">
                            <td><input type="checkbox" value="" /></td>
                            <td>888</td>
                            <td>bbbb</td>
                            <td>HD</td>
                            <td>Em triagem</td>
                            <td>3</td>
                            <td>0</td>
                        </tr>
                	</tbody>
                </table>	
			</div>
		</div>	
	</div>
</div>