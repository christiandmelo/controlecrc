<?php
// Mostra o Breadcrumb caso ele seja enviado
if (isset ( $breadcrumb ))
	echo $breadcrumb;
?>

<div id="divRetornoParam"></div>

<div class="panel panel-default">
	<div class="panel-body">
		<form class="form-horizontal"
			data-ajajax="<?=(isset($cabecalhoform['data-ajajax']))? $cabecalhoform['data-ajajax'] : '' ;?>"
			method="<?=(isset($cabecalhoform['method']))? $cabecalhoform['method'] : 'POST' ;?>"
			action="<?=(isset($cabecalhoform['action']))? $cabecalhoform['action'] : 'app/#/' ;?>"
			enctype="<?=(isset($cabecalhoform['enctype']))? $cabecalhoform['enctype'] : 'application/x-www-form-urlencoded' ;?>"
			<?=(isset($cabecalhoform['validate-form']))? $cabecalhoform['validate-form'] : '' ;?>
		>
		<?php
		// verifica se é um editar ou novo
		// if(isset($lista['existeID']) and $lista['existeID'] == 1){
		// $arrDados = $lista['dados']->result_array();
		// }
		
		// Cria uma váriavel para controlar quantidade de Tabs criadas e acrescentar no ID
		$contadorTab = 1;
		// Carrega o Cabeçalho dos Tabs
		?>
			<ul class="nav nav-tabs">
				<?php
				foreach ( $form as $keyTabCabecalho => $rsTabCabecalho ) :
					// Retira os espaços do nome do Tab, para poder criar o seu ID
					$idTabCabecalho = $lista ['classe'] . implode ( "", explode ( " ", $keyTabCabecalho ) );
					?>
					
						<li class="<?=($contadorTab == 1)? 'active' : '' ;?>"><a
					href="#tab<?=$idTabCabecalho.$contadorTab;?>" data-toggle="tab"><?=$keyTabCabecalho;?></a></li>
					<?php
					// incrementa o contador Tab
					$contadorTab ++;
				endforeach
				;
				?>
			</ul>
			<?php
			
			// Reseta a várivel do contador de Tab para compor o Id do tab de forma correta.
			$contadorTab = 1;
			// Variavel para controlar os campos que possui mascara
			$mask = "";
			// Carrega o corpo dos Tabs
			?>
			<div class="tab-content">
			<?php
			foreach ( $form as $keyTab => $rsTab ) :
				// Retira os espaços do nome do Tab, para poder criar o seu ID
				$idTab = $lista ['classe'] . implode ( "", explode ( " ", $keyTab ) );
				?>
					<div
					class="tab-pane fade <?=($contadorTab == 1)? 'active in' : '' ;?>"
					id="tab<?=$idTab.$contadorTab;?>">
					<?php
				foreach ( $rsTab as $keyForm => $rsForm ) :
					
					$arrCampoTabela = explode ( ".", $keyForm );
					
					/*
					 * echo $arrCampoTabela[1]."<br />";
					 *
					 * var_dump($arrDados[0]);
					 */
					
					switch ($rsForm ['tipo']) {
						case "label" :
							if (isset ( $rsForm ['mask'] )) {
								$mask .= '$("#' . $rsForm ['idname'] . '").mask("' . $rsForm ['mask'] . '");';
							}
							
							?>
					        		<div class="form-group">
										<label class="col-md-3 control-label"> 
					                    	<?=$rsForm['label'];?>
					                    	<?=(isset($rsForm['required']))? '*' : ''?>
					                    </label>
										<div class="col-md-9">
											<input type="text"
												value="<?=(isset($arrDados[0][$arrCampoTabela[1]]))? $arrDados[0][$arrCampoTabela[1]] : '';?>"
												<?=(isset($rsForm['required']) and $rsForm['required'] == true)? 'data-parsley-required="true"' : '';?>
												<?=(isset($rsForm['required-type']))? 'data-parsley-type="'.$rsForm["required-type"].'"' : ''?>
												<?=(isset($rsForm['datepicker']) and $rsForm['datepicker'] == true)? 'datepicker=true' : '' ;?>
												id="<?=$rsForm['idname'];?>" name="<?=$rsForm['idname'];?>"
												class="form-control" placeholder="<?=$rsForm['placeholder'];?>"
												<?=(isset($rsForm['disabled']) and $rsForm['disabled'])? 'disabled' : '';?> />
										</div>
									</div>
					        <?php
							break;
						
						case "select" :
							?>
									<div class="form-group">
										<label class="col-md-3 control-label" for="selectbasic">
											<?=$rsForm['label'];?>
											<?=(isset($rsForm['required']))? '*' : ''?>
										</label>
										<div class="col-md-9">
											<select id="<?=$rsForm['idname'];?>"
								name="<?=$rsForm['idname'];?>"
								<?=(isset($rsForm['required']) and $rsForm['required'] == true)? 'data-parsley-required="true"' : '';?>
								class="default-select2 form-control">
												<?php
							foreach ( $rsForm ['values'] as $keySelect => $valuesSelect ) :
								?>
														<option value="<?=$keySelect;?>"
									<?=(isset($arrDados[0][$arrCampoTabela[1]]) and $arrDados[0][$arrCampoTabela[1]] == $keySelect)? 'selected=selected' : '' ;?>><?=$valuesSelect;?></option>		
														<?php
							endforeach
							;
							?>
											</select>
										</div>
									</div>
									<?php
							break;
						
						case "radio" :
							?>
									<!-- Multiple Radios (inline) -->
									<div class="form-group">
										<label class="col-md-4 control-label" for="radios"><?=$rsForm['label'];?></label>
										<div class="col-md-4">
											<label class="radio-inline" for="radios-0"> <input type="radio"
												name="radios" id="radios-0" value="1" checked="checked"> Falta
												configurar
											</label>
										</div>
									</div>
									<?php
							break;
						
						case "check" :
							?>
									<div class="form-group">
										<label class="col-md-4 control-label" for="checkboxes"><?=$rsForm['label'];?></label>
										<div class="col-md-4">
											<label class="checkbox-inline" for="checkboxes-0"> <input
												type="checkbox" name="checkboxes" id="checkboxes-0" value="1">
												Falta configurar
											</label>
										</div>
									</div>
									<?php
							break;
						
						case "file" :
							?>
									<div class="form-group">
										<label class="col-md-3 control-label" for="checkboxes">
					                    	<?=$rsForm['label'];?>
					                    	<?=(isset($rsForm['required']))? '*' : ''?>
										</label> 
										<div class="col-md-9">
											<input
												type="file" 
												id="<?=$rsForm['idname'];?>"
												name="<?=$rsForm['idname'];?>"
												<?=(isset($rsForm['required']) and $rsForm['required'] == true)? 'data-parsley-required="true"' : '';?>
											>
											<p class="help-block">...</p>
										</div>
									</div>
									<?php
							break;
					}
				endforeach
				;
				?>
					</div>
					<?php
				// Incrementar o contador Tab
				$contadorTab ++;
			endforeach
			;
			?>
			</div>

			<!-- Button -->
			<div class="form-group">
				<div class="col-md-4">
					<button type="submit" id="singlebutton" name="singlebutton"
						class="btn btn-primary">Salvar</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		/*$(".default-select2").select2(), $(".multiple-select2").select2({
			placeholder : "Selecione uma opção"
		});*/
		
		$("[datepicker=true]").datepicker({
			todayHighlight : !0,
			autoclose : !0
		})
		
		<?=$mask;?>
		
		$('[data-parsley-validate]').parsley()
			
	});
</script>

