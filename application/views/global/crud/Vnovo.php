
<form class="form-horizontal" data-ajajax="<?=$cabecalhoform['data-ajajax'];?>" method="<?=$cabecalhoform['method'];?>" action="<?=$cabecalhoform['action'];?>" <?=(isset($cabecalhoform['validate-form']))? $cabecalhoform['validate-form'] : '' ;?> >
	<div class="modal-body" >
		<div class="panel panel-inverse">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title"><?=$lista['Titulo']; ?></h4>
			</div>
			<div class="panel-body" style="padding:0px;">
	            
				<!--Div de retorno-->
				<div id="divRetornoNovo<?=$lista['classe'];?>"></div>
					<?php 
					//verifica se é um editar ou novo
					if(isset($lista['existeID']) and  $lista['existeID'] == 1){
						$arrDados = $lista['dados']->result_array();
					}
					//Cria uma váriavel para controlar quantidade de Tabs criadas e acrescentar no ID
					$contadorTab = 1;
					//Carrega o Cabeçalho dos Tabs
					?>
					<ul class="nav nav-tabs">
						<?php
						foreach( $form as $keyTabCabecalho => $rsTabCabecalho ):
							//Retira os espaços do nome do Tab, para poder criar o seu ID
							$idTabCabecalho = $lista['classe'].implode("",explode(" ",$keyTabCabecalho));
							?>
								<li class="<?=($contadorTab == 1)? 'active' : '' ;?>">
									<a href="#tab<?=$idTabCabecalho.$contadorTab;?>" data-toggle="tab"><?=$keyTabCabecalho;?></a>
								</li>
							<?php
							//incrementa o contador Tab
							$contadorTab ++;
						endforeach;
						?>
					</ul>
					<?php
					//Reseta a várivel do contador de Tab para compor o Id do tab de forma correta.
					$contadorTab = 1;
					//Variavel para controlar os campos que possui mascara
					$mask = "";
					//Carrega o corpo dos Tabs
					?>
					<div class="tab-content">
					<?php
						foreach( $form as $keyTab => $rsTab ):
							//Retira os espaços do nome do Tab, para poder criar o seu ID
							$idTab = $lista['classe'].implode("",explode(" ",$keyTab));
							?>
							<div class="tab-pane fade <?=($contadorTab == 1)? 'active in' : '' ;?>" id="tab<?=$idTab.$contadorTab;?>">
							<?php
								$campos = $rsTab;
								include APPPATH."/views/global/crud/VComponentesFormulario.php";	
							?>
							</div>
							<?php
							//Incrementar o contador Tab
							$contadorTab ++;
						endforeach; 
						?>
					</div>
	        </div>
		</div>  
	</div>
	<div class="modal-footer">
		<button type="submit" id="singlebutton" name="singlebutton" class="btn btn-sm btn-success">Salvar</button>
	    <button id="ModalbtnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Cancelar</button>
	</div>
</form>

<script type="text/javascript">
	$(function(){
		
		/*$(".default-select2").select2(), $(".multiple-select2").select2({
			placeholder : "Selecione uma opção"
		});*/
		
		$("[datepicker=true]").datepicker({
			todayHighlight : !0,
			autoclose : !0,
			dateFormat: "dd/mm/yy"
		})
		
		<?=$mask;?>
		
		$('[data-parsley-validate]').parsley()
	});
</script>

