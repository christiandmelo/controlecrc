<?php // Mostra o Breadcrumb caso ele seja enviado
if (isset($breadcrumb))
	echo $breadcrumb;

//Variavel para controlar os campos que possui mascara
$mask = "";
?>

<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <!-- <a data-click="panel-collapse" class="btn btn-icon btn-default" href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a> -->
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Filtro</h4>
            </div>
            <div class="panel-body">
                <!--Inicio formulario do filtro-->
                <form 	method="post" 
                		data-ajajax="#corpo<?= str_replace(' ', '_', $lista['Titulo']); ?>" 
                		data-parsley-validate='true'
                		action="<?= (isset($lista['novo-filtro']))? $lista['novo-filtro'] : $lista['precaminho'].$lista['classe']."/lista"; ?>" 
                		class="form-horizontal" 
                		role="form"
                		>
                <?php
                $existeFiltro = false;
                foreach( $form as $keyTab => $rsTab ):
                	foreach( $rsTab as $keyForm => $rsForm ):
                		if(isset($rsForm['filtro']) and $rsForm['filtro']){
                			$existeFiltro = true;
                			$arrCampoTabela = explode(".",$keyForm);
                			switch ($rsForm['tipo']){
                				case "label":
                					if(isset($rsForm['mask'])){
                						$mask .= '$("#filtro'.$rsForm['idname'].'").mask("'.$rsForm['mask'].'");';
                					}
                					?>
					                <div class="form-group">
					                    <label class="col-md-2 control-label" for="<?= $rsForm['idname']; ?>"> <?= $rsForm['label']; ?></label>
					                    <div class="col-md-5">
					                        <input type="text" <?= (isset($rsForm['datepicker']) and $rsForm['datepicker'] == true) ? 'datepicker=true' : ''; ?> id="filtro<?= $rsForm['idname']; ?>" name="filtro<?= $rsForm['idname']; ?>" class="form-control" placeholder="<?= (isset($rsForm['placeholder'])) ? $rsForm['placeholder'] : ''; ?>" <?= isset($rsForm['disabled']) ? 'disabled' : ''; ?>/>
					                    </div>
					                </div>
					                <?php
					                break;

                				case "select":
					                ?>
					
					                <div class="form-group"
					                	 <?=(isset($rsForm['filtro-visible']) && $rsForm['filtro-visible'] == false)? "style='display:none'" : "" ;?> 
					                >
					                    <label class="col-md-2 control-label" for="<?= $rsForm['idname']; ?>"> 
					                    	<?= $rsForm['label']; ?>
					                    	<?=(isset($rsForm['filtro-required']))? '*' : ''?>
					                    </label>
					                    <div class="col-md-5">
					                        <select id="filtro<?= $rsForm['idname']; ?>" 
				                        			name="filtro<?= $rsForm['idname']; ?>" 
				                        			class="default-select2 form-control" "
				                        			<?=(isset($rsForm['filtro-required']) and $rsForm['filtro-required'] == true)? 'data-parsley-required="true"' : '';?> 
				                        		>
					                            <?php
					                            foreach($rsForm['values'] as $keySelect => $valuesSelect):
													var_dump($rsForm['values']);
						                            ?>
						                            <option value="<?=$keySelect;?>"><?=$valuesSelect;?></option>
					                            <?php endforeach; ?>
					                        </select>
					                    </div>
					                </div>
					                <?php
					                break;

                				case "radio":
					                ?>
					                <!-- Multiple Radios (inline) -->
					                <div class="form-group">
					                    <label class="col-md-2 control-label" for="radios">
					                        <?= $rsForm['label']; ?>
					                    </label>
					                    <div class="col-md-5">
					                        <label class="radio-inline" for="radios-0">
					                            <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
					                            Falta configurar
					                        </label>
					                    </div>
					                </div>
					                <?php
					                break;

                				case "check":
					                ?>
					                <div class="form-group">
					                    <label class="col-md-4 control-label" for="checkboxes"><?= $rsForm['label']; ?></label>
					                    <div class="col-md-4">
					                        <label class="checkbox-inline" for="checkboxes-0">
					                            <input type="checkbox" name="checkboxes" id="checkboxes-0" value="1">
					                            Falta configurar
					                        </label>
					                    </div>
					                </div>
					                <?php
					                break;
                				}
  				  			}
                		endforeach;
                endforeach;

                if(!$existeFiltro){
                ?>
                <div class="form-group">
                    <label class="col-md-3 control-label"> Não existe filtro para essa tela!</label>
                </div>
                <?php } ?>

                <!--Botão que executa o filtro e busca os dados-->
                <button id="btnFiltrar" type="submit" style="float:right" class="btn btn-sm btn-inverse">
                    <!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
                    <i class="glyphicon glyphicon-search"></i> Filtrar
                </button>
                </form>
                <!--Fim formulario do filtro-->
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 ui-sortable">
        <div data-sortable-id="ui-general-5" class="panel panel-inverse" style="margin-bottom: 0px;">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <div class="content-btn-icon">
                    	<?php
                    	if (!isset($botoes['novo']) || $botoes['novo'] != 'inativo'){
                    		?>
	                        <a id="btnNovo"
	                           data-toggle="modal"
	                           class="btn btn-sm btn-success"
	                           data-target="#modal<?=$lista['classe']; ?>"
	                           data-ajajax="#corpoModal<?=$lista['classe']; ?>"
	                           data-href="<?=$lista['precaminho'].$lista['classe'];?>/novo"
	                           data-original-title="Novo"
	                           title="Novo">
	                            <i class="fa fa-file-o"></i> Novo
	                        </a>
	                        <?php }	?>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!--Inicio do conteúdo do filtro-->
                <h4 class="panel-title"> <?= $lista['Titulo']; ?> </h4>
            </div>
            <!-- Div que carrega o filtro-->
            <div id="corpo<?= str_replace(' ', '_', $lista['Titulo']); ?>" class="panel-body">
                <?php
                if (isset($lista['autolista']) && $lista['autolista'] == false) {
                echo "Gentileza realizar o filtro para buscar as informações";
                } else {
                include APPPATH . "/views/global/crud/Vlista.php";
                }
                ?>
            </div>
        </div>
        <!-- end panel -->
        <script type="text/javascript">
            jQuery('.panel-body').css('max-height', ( window.screen.availHeight - ( 54 + 43 + ( 30 * 2 ) + ( 15 * 2 ) ) ) );
            jQuery('.panel-body').css('overflow','auto');

            $(function(){
                $("[datepicker=true]").datepicker({
                    todayHighlight : !0,
                    autoclose : !0
                })

                <?= $mask; ?>
                    $('[data-parsley-validate]').parsley()

                });
        </script>
    </div>
</div>

<?php
if(isset($lista['divcomplementar'])){
?>
<div id="<?=$lista['divcomplementar']; ?>"></div>
	<?php } ?>

<!-- Carrega a função js da página se existir-->
<?php
if(isset($funcao_js)){
	?>
<script src="<?=$funcao_js;?>"></script>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="modal<?=(isset($lista['novo-modal']))? $lista['novo-modal'] : $lista['classe']; ?>">
    <div class="modal-dialog">
        <div class="modal-content" id="corpoModal<?=(isset($lista['novo-modal']))? $lista['novo-modal'] : $lista['classe']; ?>">    	
            <div class="modal-body" >
            	<div class="panel panel-inverse">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
						</div>
						<h4 class="panel-title"><?=$lista['Titulo']; ?></h4>
					</div>
					<div class="panel-body">
                        <p>Carregando suas informações...</p>
                    </div>
				</div>  
            </div>
            <div class="modal-footer">
                <button id="btnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Cancelar</button>
            </div>
        </div>
    </div>
</div>

