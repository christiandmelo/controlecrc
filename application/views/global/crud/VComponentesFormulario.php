<?php
foreach( $campos as $keyForm => $rsForm ):
					
	$arrCampoTabela = explode(".",$keyForm);
	
	switch ($rsForm['tipo']){
		case "div":
			?>
			<div id="<?=$rsForm['idname'];?>"></div>
			<?php
			break;
			
		case "label":
			if(isset($rsForm['mask'])){
				$reverse = (isset($rsForm['mask-reverse']) && $rsForm['mask-reverse'] == true) ? ', {reverse: true}' : "";
				$mask .= '$("#'.$rsForm['idname'].'").mask("'.$rsForm['mask'].'" '.$reverse.');';
			}
			?>
    		<div class="form-group" <?=(isset($rsForm['visible']) and $rsForm['visible'] === false)? 'style="display: none;"' : '';?> >
                <label class="col-md-3 control-label"> 
                	<?=$rsForm['label'];?>
                	<?=(isset($rsForm['required']))? '*' : ''?>
                </label>
                <div class="col-md-9">
                    <input 	type="text" 
                    		value="<?php
                    				if (isset($arrDados[0][$arrCampoTabela[1]])){
                    					echo $arrDados[0][$arrCampoTabela[1]];
									}else if(isset($rsForm['value'])){
										echo $rsForm['value'];
									}
									?>" 
                    		<?=(isset($rsForm['required']) and $rsForm['required'] == true)? 'data-parsley-required="true"' : '';?> 
                    		<?=(isset($rsForm['required-type']))? 'data-parsley-type="'.$rsForm["required-type"].'"' : ''?> 
                    		<?=(isset($rsForm['datepicker']) and $rsForm['datepicker'] == true)? 'datepicker=true' : '' ;?> 
                    		id="<?=$rsForm['idname'];?>" 
                    		name="<?=$rsForm['idname'];?>" 
                    		class="form-control" 
                    		placeholder="<?=isset($rsForm['placeholder'])?$rsForm['placeholder']:'';?>" 
                    		<?=(isset($rsForm['disabled']) and $rsForm['disabled'])? 'disabled' : '';?>
                    />
                </div>
            </div>
        	<?php
			break;
			
		case "select":
			?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="selectbasic">
					<?=$rsForm['label'];?>
					<?=(isset($rsForm['required']))? '*' : ''?>
				</label>
				<div class="col-md-9">
					<select id="<?=$rsForm['idname'];?>" 
							name="<?=$rsForm['idname'];?>" 
							<?=(isset($rsForm['required']) and $rsForm['required'] == true)? 'data-parsley-required="true"' : '';?> 
							class="default-select2 form-control"
							<?=(isset($rsForm['onchange']) and $rsForm['onchange'] == true)? 'onchange="'.$rsForm['onchange'].'"' : '';?>
						>
						<?php
							foreach($rsForm['values'] as $keySelect => $valuesSelect):
								?>
								<option value="<?=$keySelect;?>" 
									<?=(isset($arrDados[0][$arrCampoTabela[1]]) && is_array($arrDados) && $arrDados[0][$arrCampoTabela[1]] == $keySelect)? 'selected=selected' : '' ;?> 
								><?=$valuesSelect;?></option>		
								<?php
							endforeach;
						?>
					</select>
				</div>
			</div>
			<?php
			break;
			
		case "radio-inline":
			?>					
			<div class="form-group">
				<label class="col-md-3 control-label ui-sortable">
					<?=$rsForm['label'];?>
					<?=(isset($rsForm['required']))? '*' : ''?>
				</label>
				<div class="col-md-9 ui-sortable"> 
					<?php
					foreach($rsForm['values'] as $keySelect => $valuesSelect):
						?>
						<label class="radio-inline">
							<input 	type="radio"  
							   		name="<?=$rsForm['idname'];?>"
							   		id="<?=$rsForm['idname'];?>" 
							   		value="<?=$keySelect;?>"
							   		<?=(isset($rsForm['required']) and $rsForm['required'] == true)? 'data-parsley-required="true"' : '';?>  
							   		<?=(isset($arrDados[0][$arrCampoTabela[1]]) and $arrDados[0][$arrCampoTabela[1]] == $keySelect)? 'checked="checked"' : '' ;?>
							   		<?=isset($rsForm['onclick'])? 'onclick="'.$rsForm['onclick'].'()"' : '';?>
							/>
							<?=$valuesSelect;?> 
						</label>		
						<?php
					endforeach;
					?> 
				</div>
			</div>
			<?php
			break;
			
		case "check":
			?>
			<div class="form-group">
				<label class="col-md-4 control-label" for="checkboxes">
					<?=$rsForm['label'];?></label>
				<div class="col-md-4">
					<label class="checkbox-inline" for="checkboxes-0">
						<input type="checkbox" name="checkboxes" id="checkboxes-0" value="1">
						Falta configurar
					</label>
				</div>
			</div>
			<?php
			break;
	}
endforeach;