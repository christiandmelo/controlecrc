<div id="tableLista<?=str_replace(' ','_',$lista['Titulo']);?>" class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<?php 
					foreach( $tabela['cabecalho'] as $rsCabecalho ): 
						?>
						<th><?=$rsCabecalho; ?></th>
					<?php 
					endforeach; 
				?>
			</tr>
		</thead>
		<tbody>
			<?php 
				if( count( $lista['dados']->result_array() ) > 0 ):
					foreach( $lista['dados']->result_array() as $nDado => $arrDados ): 
							$cont = 0;
							foreach( $arrDados as $nomeColuna => $valor ):
								if($cont == 0){
									?>
									<tr style="width: 40px;" class="odd gradeX" data-tr-reg="<?=$valor;?>" >
									<?php
									$cont = $valor;
								}
								?>
								<td id="<?=str_replace(' ', '_', $nomeColuna).$cont?>" ><?=$valor;?></td>
							<?php endforeach; ?>
							<td>
								<!-- botão editar -->
								<a 	id="btnEditar<?=$cont;?>"
									<?= (!isset($lista['data-toggle']) || $lista['data-toggle'] == true) ? 'data-toggle="modal"' : ''; ?>
									<?= (!isset($lista['data-toggle']) || $lista['data-toggle'] == true) ? 'data-target="#modal' . $lista['classe'] . '"' : ''; ?>
									<?= (!isset($lista['divcomplementar'])) ? 'data-ajajax="#corpoModal' . $lista['classe'] . '"' : 'data-ajajax="#' . $lista['divcomplementar'] . '"'; ?>
									<?= (!isset($lista['novoeditar'])) ? 'data-href="' . $lista['precaminho'] . $lista['classe'] . '/novo"' : 'data-href="' . $lista['novoeditar'] . '"'; ?>
									class="btn btn-sm btn-warning"
									data-original-title="Editar"
									data-value="<?=$cont;?>"
									title="Editar"
									data-ajajax-tipo="editar"
									data-ajajax-idtabela="tableLista<?= str_replace(' ', '_', $lista['Titulo']); ?>"
									>
								<i class="fa fa-edit (alias)"></i> Editar
								</a>
								<?php
									if (!isset($botoes['excluir']) || $botoes['excluir'] != 'inativo'){
										?>
										<a id="btnExcluir"
											data-ajajax="#corpoModal<?= $lista['classe']; ?>"
											data-href="<?=$lista['precaminho'].$lista['classe']; ?>/excluir"
											class="btn btn-sm btn-danger"
											data-original-title="Excluir"
											data-value="<?=$cont;?>"
											title="Excluir"
											data-ajajax-tipo="editar"
											data-ajajax-idtabela="tableLista<?= str_replace(' ', '_', $lista['Titulo']); ?>">
											<i class="fa fa-trash-o"></i> Excluir
										</a>
								<?php } ?>
							</td>
						</tr>
						<?php 
					endforeach;
				else:
			?>
			<tr>
				<td colspan="<?php echo count($tabela['cabecalho'])+1; ?>" > Sem Registro </td>
			</tr>
			<?php
				endif; 
			?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	<?php
	if(isset($lista['divcomplementar'])){
		?>
		$(function(){
			$("#<?=$lista['divcomplementar'];?>").html("");
		});
	<?php } ?>
</script>

