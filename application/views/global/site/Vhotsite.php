<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v1.9/frontend/one-page-parallax/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Dec 2015 18:50:50 GMT -->
<head>
	<meta charset="utf-8" />
	<title>PortalDID</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link rel="icon" href="/assets/img/favicon.png" />
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="site/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="site/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link href="site/assets/css/animate.min.css" rel="stylesheet" />
	<link href="site/assets/css/style.min.css" rel="stylesheet" />
	<link href="site/assets/css/style-responsive.min.css" rel="stylesheet" />
	<link href="site/assets/css/theme/default.css" id="theme" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="site/assets/plugins/pace/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body data-spy="scroll" data-target="#header-navbar" data-offset="51">
	
	
	
	
	
	
	
	
    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin #header -->
        <div class="fade in" id="page-container">
        <!-- begin #header -->
        <div class="header navbar navbar-transparent navbar-fixed-top navbar-small" id="header">
            <!-- begin container -->
            <div class="container">
                <!-- begin navbar-header -->
                <div class="navbar-header">
                    <button data-target="#header-navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="/assets/img/teste1.png" style="height: 40px" class="logo_ini"></a>
                    <!--
	                    <a href="index.html" class="navbar-brand">
	                        <span class="brand-logo"></span>
	                        <span class="brand-text">
	                            <span class="text-theme">Color</span> Admin
	                        </span>
	                    </a>
					-->
                </div>
                <!-- end navbar-header -->
                <!-- begin navbar-collapse -->
                <div id="header-navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a data-toggle="dropdown" data-click="scroll-to-target" href="#home">INICIO<b class="caret"></b></a>
                           <!-- <ul class="dropdown-menu dropdown-menu-left animated fadeInDown">
                                <li><a href="index.html">Page with Transparent Header</a></li>
                                <li><a href="index_inverse_header.html">Page with Inverse Header</a></li>
                                <li><a href="index_default_header.html">Page with White Header</a></li>
                                <li><a href="extra_element.html">Extra Element</a></li>
                           </ul>-->
                        </li>
                        <li class=""><a data-click="scroll-to-target" href="#about">SOBRE</a></li>
                        <li class="active"><a data-click="scroll-to-target" href="#team">EQUIPE</a></li>
                        <li class=""><a data-click="scroll-to-target" href="#service">SERVIÇOS</a></li>
                        <li class=""><a data-click="scroll-to-target" href="#work">TELE CENTROS</a></li>
                        <li class=""><a data-click="scroll-to-target" href="#contact">CONTATO</a></li>
                        <li><a href="login">LOGIN</a></li>
                    </ul>
                </div>
                <!-- end navbar-collapse -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #header -->
        
        <!-- begin #home -->
        <!--<div id="home" class="content has-bg home">
            <!-- begin content-bg 
            <div class="content-bg">
                <img src="site/assets/img/home-bg.jpg" alt="Home" />
            </div>
            <!-- end content-bg 
            <!-- begin container 
            <div class="container home-content">
                <h1>Bem vindo!</h1>
                <!-- <h3>Multipurpose One Page Theme</h3> 
						
				
                <!--<a href="#" class="btn btn-theme">Nossas Solu&ccedil;&otilde;es</a> <a href="#" class="btn btn-outline">Assinar Agora</a><br />
                <br />
                <!-- ou <a href="#">subscribe</a> newsletter 
            </div>
            <!-- end container 
        </div>-->
       <div class="content has-bg home" id="home" style="height: 669px;">
            <!-- begin content-bg -->
            <div class="content-bg">
                <img alt="Home" src="assets/img/home-bg.jpg">
            </div>
            <!-- end content-bg -->
            <!-- begin container -->
            <div class="container home-content">
                <h1>Bem Vindo ao Portal DID</h1>
                <h3>Sua central de recondicionamento de computadores de Belo Horizonte</h3>
                <p>
                    Destinamos os locais certos todas as peças, computadores e dispositivos aos seus locais corretos.<br>
                    
                </p>
                <a class="btn btn-theme" href="#">Explore Mais</a> 
            </div>
            <!-- end container -->
        </div>
        <!-- end #home -->
        
        <!-- begin #about -->
        <div id="about" class="content" data-scrollview="true">
            <!-- begin container -->
            <div class="container fadeInDown contentAnimated finishAnimated" data-animation="true" data-animation-type="fadeInDown">
                <h2 class="content-title">Sobre</h2>
                <p class="content-desc">
                    Trabalhamos com todos os tipos de equipamentos
                </p>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-6">
                        <!-- begin about -->
                        <div class="about">
                            <h3>Nossa História</h3>
                            <p>
                                Breve Texto.
                                Breve Texto.
                                Breve Texto.
                                Breve Texto.
                                Breve Texto.
                            </p>
                            <p>
                                Breve Texto.
                                Breve Texto.
                                Breve Texto.
                                Breve Texto.
                            </p>
                        </div>
                        <!-- end about -->
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-6">
                        <h3>Nossa Filósofia</h3>
                        <!-- begin about-author -->
                        <div class="about-author">
                            <div class="quote bg-silver">
                                <i class="fa fa-quote-left"></i>
                                <h3>Breve Texto.,<br><span>Breve Texto.</span></h3>
                                <i class="fa fa-quote-right"></i>
                            </div>
                            <div class="author">
                                <div class="image">
                                    <img src="assets/img/user-1.jpg" alt="Sean Ngu">
                                </div>
                                <div class="info">
                                    Sean Ngu
                                    <small>Triagem</small>
                                </div>
                            </div>
                        </div>
                        <!-- end about-author -->
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-12">
                        <h3>Nossa Experiênia</h3>
                        <!-- begin skills -->
                        <div class="skills">
                            <div class="skills-name">Computadores</div>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" style="width: 95%">
                                    <span class="progress-number">95%</span>
                                </div>
                            </div>
                            <div class="skills-name">Notebooks</div>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" style="width: 90%">
                                    <span class="progress-number">90%</span>
                                </div>
                            </div>
                            <div class="skills-name">Impressoras</div>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" style="width: 85%">
                                    <span class="progress-number">85%</span>
                                </div>
                            </div>
                            <div class="skills-name">Periféricos</div>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" style="width: 80%">
                                    <span class="progress-number">80%</span>
                                </div>
                            </div>
                        </div>
                        <!-- end skills -->
                    </div>
                    <!-- end col-4 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #about -->
    
        <!-- begin #milestone -->
        <div id="milestone" class="content bg-black-darker has-bg" data-scrollview="true">
            <div class="content-bg">
                <img src="assets/img/milestone-bg.jpg" alt="Milestone" />
            </div>
            <div class="container">
                <div class="row">
                </div>
            </div>
        </div>
        <!-- end #milestone -->
        
        <!-- begin #team -->
        <div id="team" class="content" data-scrollview="true">
            <!-- begin container -->
            <div class="container">
                <h2 class="content-title">Nosso time</h2>
                <p class="content-desc">
                    Este é o nosso time responsável por toda a idéia<br> 
                </p>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <!-- begin team -->
                        <div class="team">             
                            <div class="image flipInX contentAnimated finishAnimated" data-animation="true" data-animation-type="flipInX">
                                <img src="assets/img/user-1.jpg" alt="Maria Silveira">
                            </div>
                            <div class="info">
                                <h3 class="name">Maria</h3>
                                <div class="title text-theme">Diretora</div>
                                <p>Breve Texto.</p>
                                <div class="social">
                                    <a href="#"><i class="fa fa-facebook fa-lg fa-fw"></i></a>
                                    <a href="#"><i class="fa fa-twitter fa-lg fa-fw"></i></a>
                                    <a href="#"><i class="fa fa-google-plus fa-lg fa-fw"></i></a>
                                </div>
                            </div>                     
                        </div>
                        <!-- end team -->
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <!-- begin team -->
                        <div class="team">             
                            <div class="image flipInX contentAnimated finishAnimated" data-animation="true" data-animation-type="flipInX">
                                <img src="assets/img/user-2.jpg" alt="João Batista">
                            </div>
                            <div class="info">
                                <h3 class="name">João</h3>
                                <div class="title text-theme">Gerente de Infra</div>
                                <p>Breve Texto.</p>
                                <div class="social">
                                    <a href="#"><i class="fa fa-facebook fa-lg fa-fw"></i></a>
                                    <a href="#"><i class="fa fa-twitter fa-lg fa-fw"></i></a>
                                    <a href="#"><i class="fa fa-google-plus fa-lg fa-fw"></i></a>
                                </div>
                            </div>                     
                        </div>
                        <!-- end team -->
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <!-- begin team -->
                        <div class="team">             
                            <div class="image flipInX contentAnimated finishAnimated" data-animation="true" data-animation-type="flipInX">
                                <img src="assets/img/user-3.jpg" alt="Pedro silva">
                            </div>
                            <div class="info">
                                <h3 class="name">Pedro</h3>
                                <div class="title text-theme">Coordenador de Triagem</div>
                                <p>Breve Texto. </p>
                                <div class="social">
                                    <a href="#"><i class="fa fa-facebook fa-lg fa-fw"></i></a>
                                    <a href="#"><i class="fa fa-twitter fa-lg fa-fw"></i></a>
                                    <a href="#"><i class="fa fa-google-plus fa-lg fa-fw"></i></a>
                                </div>
                            </div>                     
                        </div>
                        <!-- end team -->
                    </div>
                    <!-- end col-4 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #team -->
        
        <!-- begin #quote -->
        <!-- begin #milestone -->
        <div id="milestone" class="content bg-black-darker has-bg" data-scrollview="true">
            <div class="content-bg">
                <img src="assets/img/milestone-bg.jpg" alt="Milestone" />
            </div>
            <div class="container">
                <div class="row">
                </div>
            </div>
        </div>
        <!-- end #milestone -->
        <!-- end #quote -->
        
        <!-- beign #service -->
        <div id="service" class="content" data-scrollview="true">
            <!-- begin container -->
            <div class="container">
                <h2 class="content-title">Nossos serviços ofertados</h2>
                <p class="content-desc">
                    Breve Texto,<br>
                    Breve Texto.
                </p>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    
                    <!-- end col-4 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #about -->
        
        <!-- beign #action-box -->
        <!-- begin #milestone -->
        <div id="milestone" class="content bg-black-darker has-bg" data-scrollview="true">
            <div class="content-bg">
                <img src="assets/img/milestone-bg.jpg" alt="Milestone" />
            </div>
            <div class="container">
                <div class="row">
                </div>
            </div>
        </div>
        <!-- end #milestone -->
        <!-- end #action-box -->
        
        <!-- begin #work -->
        <div data-scrollview="true" class="content" id="work">
            <!-- begin container -->
            <div data-animation-type="fadeInDown" data-animation="true" class="container fadeInDown contentAnimated finishAnimated">
                <h2 class="content-title">Nossos Tele Centros</h2>
                <p class="content-desc">
                    Breve Texto,<br>
                    Breve Texto.
                </p>
                <!-- begin row -->
                <div class="row row-space-10">
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-6">
                        <!-- begin work -->
                        <div class="work">
                            <div class="image">
                                <a href="#"><img alt="Work 1" src="assets/img/work-1.jpg"></a>
                            </div>
                            <div class="desc">
                                <span class="desc-title">Breve Texto</span>
                                <span class="desc-text">Breve Texto.</span>
                            </div>
                        </div>
                        <!-- end work -->
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-6">
                        <!-- begin work -->
                        <div class="work">
                            <div class="image">
                                <a href="#"><img alt="Work 3" src="assets/img/work-3.jpg"></a>
                            </div>
                            <div class="desc">
                                <span class="desc-title">Breve Texto.</span>
                                <span class="desc-text">Breve Texto.</span>
                            </div>
                        </div>
                        <!-- end work -->
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-6">
                        <!-- begin work -->
                        <div class="work">
                            <div class="image">
                                <a href="#"><img alt="Work 5" src="assets/img/work-5.jpg"></a>
                            </div>
                            <div class="desc">
                                <span class="desc-title">Breve Texto.</span>
                                <span class="desc-text">Breve Texto.</span>
                            </div>
                        </div>
                        <!-- end work -->
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-6">
                        <!-- begin work -->
                        <div class="work">
                            <div class="image">
                                <a href="#"><img alt="Work 7" src="assets/img/work-7.jpg"></a>
                            </div>
                            <div class="desc">
                                <span class="desc-title">Breve Texto.</span>
                                <span class="desc-text">Breve Texto.</span>
                            </div>
                        </div>
                        <!-- end work -->
                    </div>
                    <!-- end col-3 -->
                </div>
                <!-- end row -->
                <!-- begin row -->
                <div class="row row-space-10">
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-6">
                        <!-- begin work -->
                        <div class="work">
                            <div class="image">
                                <a href="#"><img alt="Work 2" src="assets/img/work-2.jpg"></a>
                            </div>
                            <div class="desc">
                                <span class="desc-title">Breve Texto.</span>
                                <span class="desc-text">Breve Texto.</span>
                            </div>
                        </div>
                        <!-- end work -->
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-6">
                        <!-- begin work -->
                        <div class="work">
                            <div class="image">
                                <a href="#"><img alt="Work 4" src="assets/img/work-4.jpg"></a>
                            </div>
                            <div class="desc">
                                <span class="desc-title">Breve Texto.</span>
                                <span class="desc-text">Breve Texto.</span>
                            </div>
                        </div>
                        <!-- end work -->
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-6">
                        <!-- begin work -->
                        <div class="work">
                            <div class="image">
                                <a href="#"><img alt="Work 6" src="assets/img/work-6.jpg"></a>
                            </div>
                            <div class="desc">
                                <span class="desc-title">Breve Texto.</span>
                                <span class="desc-text">Breve Texto.</span>
                            </div>
                        </div>
                        <!-- end work -->
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-6">
                        <!-- begin work -->
                        <div class="work">
                            <div class="image">
                                <a href="#"><img alt="Work 8" src="assets/img/work-8.jpg"></a>
                            </div>
                            <div class="desc">
                                <span class="desc-title">Breve Texto.</span>
                                <span class="desc-text">Breve Texto.</span>
                            </div>
                        </div>
                        <!-- end work -->
                    </div>
                    <!-- end col-3 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #work -->
        
        
        
        <!-- begin #contact -->
        <div data-scrollview="true" class="content bg-silver-lighter" id="contact">
            <!-- begin container -->
            <div class="container">
                <h2 class="content-title">Entre em Contatos</h2>
                <p class="content-desc">
                    Breve Texto.,<br>
                    Breve Texto.
                </p>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-6 -->
                    <div data-animation-type="fadeInLeft" data-animation="true" class="col-md-6 fadeInLeft contentAnimated finishAnimated">
                        <h3>Breve Texto..</h3>
                        <p>
                            Breve Texto.
                        </p>
                        <p>
                            <strong>Breve Texto.</strong><br>
                            Ave Brasil<br>
                            Belo Horizonte Francisco, 94107<br>
                            CEP: 9999-999<br>
                        </p>
                        <p>
                            <span class="phone">Tel: (99) 9999-9999</span><br>
                            <a href="mailto:hello@emailaddress.com">email@email.com</a>
                        </p>
                    </div>
                    <!-- end col-6 -->
                    <!-- begin col-6 -->
                    <div data-animation-type="fadeInRight" data-animation="true" class="col-md-6 form-col fadeInRight contentAnimated finishAnimated">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3">Nome <span class="text-theme">*</span></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Email <span class="text-theme">*</span></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Mensagem <span class="text-theme">*</span></label>
                                <div class="col-md-9">
                                    <textarea rows="10" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9 text-left">
                                    <button class="btn btn-theme btn-block" type="submit">Enviar Messagem</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end col-6 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
    </div>
    <!-- end #page-container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="site/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="site/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="site/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="site/assets/crossbrowserjs/html5shiv.js"></script>
		<script src="site/assets/crossbrowserjs/respond.min.js"></script>
		<script src="site/assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="site/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<script src="site/assets/plugins/scrollMonitor/scrollMonitor.js"></script>
	<script src="site/assets/js/apps.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<script>    
	    $(document).ready(function() {
	        App.init();
	    });
	</script>
</body>

<!-- Mirrored from seantheme.com/color-admin-v1.9/frontend/one-page-parallax/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Dec 2015 18:52:10 GMT -->

