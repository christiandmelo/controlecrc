<!DOCTYPE html>
<html lang="en">

<head>

	<title>ORISCenter</title>
	<meta name="author" content="CHRISTIAN MELO">
	<meta name="description" content="ORISCenter - Um local para auxiliar as pessoas em suas vidas financeiras e buscando entregar um sitema financeiro online para as suas finanças pessoais">
	<meta name="keywords" content="blog, financeiro, portal">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    
	
	<link rel="icon" href="/assets/img/favicon.png" />
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="site/assets/css/blog-home.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>


	<?php 
		if($_SERVER['SERVER_NAME'] == "oriscenter.com.br" || $_SERVER['SERVER_NAME'] == "www.oriscenter.com.br"){
		 	?>
			<!-- Analitycs -->
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			  ga('create', 'UA-74261782-1', 'auto');
			  ga('send', 'pageview');
			
			</script>
			<?php
		}
	?>



    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img class="logo_ini" style="height: 40px"  src='/assets/img/logo_oriscenter_branco.png'></img></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                    	<?php
                    		if($_SERVER['SERVER_NAME'] == "oriscenter.com.br" || $_SERVER['SERVER_NAME'] == "www.oriscenter.com.br"){
                    			?>
                    			<!--<a href="http://p.pw/bajljK">Login - Portal</a>-->
                    			<a href="login">Login - Portal</a>
                    			<?php
							}else{
								?>
								<a href="login">Login - Portal</a>
								<?php
							}
                    	?>
                        
                        
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="well">
					<p>A ORISCenter desenvolve um portal de finanças pessoais não deixe de acessar!</p>
					<a href="login">Clique aqui para acessar o portal</a>
				</div>
			</div>
		</div>
	</div>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
        	<!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">
                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Pesquisar</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Categorias</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li>
                                	<a href="#">Investimentos</a>
                                </li>
                                <li>
                                	<a href="#">Vida Financeira</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Portal Financeiro</a>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
                <div class="well" style="height:300px">
                	<a href="http://api.hostinger.com.br/redir/7174983" target="_blank">
                		<img src="http://hostinger.com.br/banners/br/hostinger-300x250-2.gif" alt="Hospedagem" border="0" width="300" height="250" />
                	</a>
                    <?php 
						if($_SERVER['SERVER_NAME'] == "oriscenter.com.br" || $_SERVER['SERVER_NAME'] == "www.oriscenter.com.br"){
						 	?>
							
							(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
							<?php
						}
					?>
                </div>
            </div>








            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <!--<h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1>->

                <!-- First Blog Post -->
                <h2>
                    <a href="#">O que é o Tesouro Direto?</a>
                </h2>
                <p class="lead">
                    Por Christian Melo | <a href="#">Investimentos</a>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Postado 25 de Fevereiro, 2016 às 16:00 Horas</p>
                <hr>
                <img class="img-responsive" src="http://placehold.it/900x300" alt="">
                <hr>
                <p>Tesouro Direto é uma das melhores alternativas de investimentos existentes do mercado brasileiro. Confirmo isso pois não existe investimento tão acessível
                	e com uma alta rentabilidade, baixo risco e pequenos custos de manutenção. Sua rentabilidade [..]</p>
                <a class="btn btn-primary" href="#">Continuar Lendo <span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>

                <!-- Pager -->
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Anterior</a>
                    </li>
                    <li class="next">
                        <a href="#">Próximo &rarr;</a>
                    </li>
                </ul>

            </div>

        </div>
        <!-- /.row -->
        
        
        
        <div class="row">
        	<div class="col-md-12">
        		<div class="well" style="height:100px">
	        		<?php 
						if($_SERVER['SERVER_NAME'] == "oriscenter.com.br" || $_SERVER['SERVER_NAME'] == "www.oriscenter.com.br"){
						 	?>
			        		
							<?php
						}
					?>
				</div>
        	</div>
        </div>
        

        <hr>








        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; ORISCenter 2016 - <?=date('Y');?></p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>






    </div>
    <!-- /.container -->

	<script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
