<div class="content" id="content">
	
	<?php
		// Mostra o Breadcrumb caso ele seja enviado
		if( isset($breadcrumb) )
			echo $breadcrumb;
	?>
	
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<h4 class="panel-title">Triagem Equipamento</h4>
		</div>
		<div class="panel-body">
			<form class="form-horizontal">
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptEtiqueta">Etiqueta:</label>
						<div class="col-md-4">
							<input type="text" id="iptEtiqueta" class="form-control" disabled="disabled"/>
						</div>
						<label class="col-md-2 control-label" for="iptQuantidade">Quantidade:</label>
						<div class="col-md-4">
							<input type="text" id="iptQuantidade" class="form-control" disabled="disabled"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptFornecedor">Fornecedor:</label>
						<div class="col-md-4">
							<input type="text" id="iptFornecedor" class="form-control" disabled="disabled"/>
						</div>
						<label class="col-md-2 control-label" for="iptTipoDoacao">Tipo Doação:</label>
						<div class="col-md-4">
							<input type="text" id="iptTipoDoacao" class="form-control" disabled="disabled"/>
						</div>	
					</div>
				</div>
				
				<br />
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="">Memória:<label style="color: red;">*</label></label>
						<div class="col-md-2">
							<input type="text" id="" class="form-control" disabled="disabled"/>
						</div>
						<div class="col-lg-3">
							<select class="form-control" id="" >
								<option value="">Selecione...</option>
								<option value="1">aaa</option>
								<option value="2">bbb</option>
								<option value="3">ccc</option>
								<option value="4">ddd</option>
							</select>
						</div>
						<div class="col-md-5">
							<button id="btnInserir" class="btn btn-sm btn-success m-r-5">Inserir</button>
							<button id="btnTrocar" class="btn btn-sm btn-primary m-r-5">Trocar</button>
							<button id="btnEstoque" class="btn btn-sm btn-warning m-r-5">Estoque</button>
							<button id="btnSucata" class="btn btn-sm btn-danger m-r-5">Sucata</button>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="">HD:<label style="color: red;">*</label></label>
						<div class="col-md-2">
							<input type="text" id="" class="form-control" disabled="disabled"/>
						</div>
						<div class="col-lg-3">
							<select class="form-control" id="" >
								<option value="">Selecione...</option>
								<option value="1">aaa</option>
								<option value="2">bbb</option>
								<option value="3">ccc</option>
								<option value="4">ddd</option>
							</select>
						</div>
						<div class="col-md-5">
							<button id="btnInserir" class="btn btn-sm btn-success m-r-5">Inserir</button>
							<button id="btnTrocar" class="btn btn-sm btn-primary m-r-5">Trocar</button>
							<button id="btnEstoque" class="btn btn-sm btn-warning m-r-5">Estoque</button>
							<button id="btnSucata" class="btn btn-sm btn-danger m-r-5">Sucata</button>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="">Fonte:<label style="color: red;">*</label></label>
						<div class="col-md-2">
							<input type="text" id="" class="form-control" disabled="disabled"/>
						</div>
						<div class="col-lg-3">
							<select class="form-control" id="" >
								<option value="">Selecione...</option>
								<option value="1">aaa</option>
								<option value="2">bbb</option>
								<option value="3">ccc</option>
								<option value="4">ddd</option>
							</select>
						</div>
						<div class="col-md-5">
							<button id="btnInserir" class="btn btn-sm btn-success m-r-5">Inserir</button>
							<button id="btnTrocar" class="btn btn-sm btn-primary m-r-5">Trocar</button>
							<button id="btnEstoque" class="btn btn-sm btn-warning m-r-5">Estoque</button>
							<button id="btnSucata" class="btn btn-sm btn-danger m-r-5">Sucata</button>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="">Placa de vídeo:<label style="color: red;">*</label></label>
						<div class="col-md-2">
							<input type="text" id="" class="form-control" disabled="disabled"/>
						</div>
						<div class="col-lg-3">
							<select class="form-control" id="" >
								<option value="">Selecione...</option>
								<option value="1">aaa</option>
								<option value="2">bbb</option>
								<option value="3">ccc</option>
								<option value="4">ddd</option>
							</select>
						</div>
						<div class="col-md-5">
							<button id="btnInserir" class="btn btn-sm btn-success m-r-5">Inserir</button>
							<button id="btnTrocar" class="btn btn-sm btn-primary m-r-5">Trocar</button>
							<button id="btnEstoque" class="btn btn-sm btn-warning m-r-5">Estoque</button>
							<button id="btnSucata" class="btn btn-sm btn-danger m-r-5">Sucata</button>
						</div>
					</div>
				</div>
				
				<br />
				<br />
				<div class="col-lg-6"></div>
				<div class="col-lg-6">
					<button id="btnSucataTotal" class="btn btn-sm btn-default">Gerar Sucata Total</button>
					<button id="btnDoacaoInterna" class="btn btn-sm btn-default">Doação Interna</button>
					<button id="btnDoacaoExterna" class="btn btn-sm btn-default">Doação Externa</button>
					<button id="btnEnviarEstoque" class="btn btn-sm btn-default">Enviar Para Estoque</button>
				</div>
			</form>			
		</div>
	</div>
</div>		