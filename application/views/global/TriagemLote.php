<div class="content" id="content">
	
	<?php
		// Mostra o Breadcrumb caso ele seja enviado
		if( isset($breadcrumb) )
			echo $breadcrumb;
	?>
	
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<h4 class="panel-title">Triagem Componentes</h4>
		</div>
		<div class="panel-body">
			<form class="form-horizontal">
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptLote">Lote:</label>
						<div class="col-md-4">
							<input type="text" id="iptLote" class="form-control" disabled="disabled"/>
						</div>
						<label class="col-md-2 control-label" for="iptQuantidade">Quantidade:</label>
						<div class="col-md-4">
							<input type="text" id="iptQuantidade" class="form-control" disabled="disabled"/>
						</div>	
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptFornecedor">Fornecedor:</label>
						<div class="col-md-4">
							<input type="text" id="iptFornecedor" class="form-control" disabled="disabled"/>
						</div>
						<label class="col-md-2 control-label" for="iptTipoDoacao">Tipo Doação:</label>
						<div class="col-md-4">
							<input type="text" id="iptTipoDoacao" class="form-control" disabled="disabled"/>
						</div>	
					</div>
				</div>
			</form>
			
			<br />
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<button id="btnEditar" class="btn btn-sm btn-primary m-r-5">Editar</button>
						<button id="btnGerarSucata" class="btn btn-sm btn-success m-r-5">Gerar Sucata</button>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>		
					</div>
					<h4 class="panel-title">Componentes</h4>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
	                    <table id="tblComponentes" class="table table-striped table-bordered">
	                    	<thead>
	                    		<tr>
	                    			<th> </th>
	                    			<th>Etiqueta</th>
	                    			<th>Descrição</th>
	                    			<th>Status</th>
	                    		</tr>
	                    	</thead>
	                    	<tbody>
	                    		<tr class="odd gradeX">
	                                <td><input type="checkbox" value="" /></td>
	                                <td>999.1</td>
	                                <td>bbbb</td>
	                                <td>Estoque</td>
	                            </tr>
	                            <tr class="even gradeC">
	                                <td><input type="checkbox" value="" /></td>
	                                <td>999.1</td>
	                                <td>bbbb</td>
	                                <td>Sucata</td>
	                            </tr>
	                            <tr class="odd gradeX">
	                                <td><input type="checkbox" value="" /></td>
	                                <td>999.1</td>
	                                <td>bbbb</td>
	                                <td>Em triagem</td>
	                            </tr>
	                            <tr class="even gradeC">
	                                <td><input type="checkbox" value="" /></td>
	                                <td>999.1</td>
	                                <td>bbbb</td>
	                                <td>Em triagem</td>
	                            </tr>
	                            <tr class="odd gradeX">
	                                <td><input type="checkbox" value="" /></td>
	                                <td>999.1</td>
	                                <td>bbbb</td>
	                                <td>Em triagem</td>
	                            </tr>
	                    	</tbody>
	                    </table>	
					</div>
				</div>
			</div>			
		</div>
	</div>	
	
		
</div>
