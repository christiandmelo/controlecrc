<div class="content" id="content">
	
	<?php
		// Mostra o Breadcrumb caso ele seja enviado
		if( isset($breadcrumb) )
			echo $breadcrumb;
	?>
	
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<h4 class="panel-title">Campos Componentes</h4>
		</div>
		<div class="panel-body">
			<form class="form-horizontal">
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="ddlTipoDoacao">Tipo Doação:<label style="color: red;">*</label></label>
						<div class="col-md-4">
							<select class="form-control" id="ddlTipoDoacao">
								<option value="">Selecione...</option>
								<option value="1">Mouse</option>
								<option value="2">Teclado</option>
								<option value="3">Caixas de Som</option>
								<option value="4">Adaptador USB</option>
							</select>
						</div>	
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptQuantidade">Quantidade:<label style="color: red;">*</label></label>
						<div class="col-md-4">
							<input type="text" id="iptQuantidade" class="form-control"/>
						</div>	
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptLote">Lote:</label>
						<div class="col-md-4">
							<input type="text" id="iptLote" class="form-control" disabled="disabled"/>
						</div>
					</div>
				</div>
			</form>
		</div>	
	</div>	
	
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<h4 class="panel-title">Campos Equipamentos</h4>
		</div>
		<div class="panel-body">
			<form class="form-horizontal">
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="ddlTipoDoacaoEquip">Tipo Doação:<label style="color: red;">*</label></label>
						<div class="col-md-4">
							<select class="form-control" id="ddlTipoDoacaoEquip">
								<option value="">Selecione...</option>
								<option value="1">Monitor</option>
								<option value="2">Impressora</option>
								<option value="3">Gabinete</option>
							</select>
						</div>	
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptQuantidadeEquip">Quantidade:</label>
						<div class="col-md-4">
							<input type="text" id="iptQuantidadeEquip" class="form-control" disabled="disabled"/>
						</div>	
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptEtiqueta">Etiqueta:<label style="color: red;">*</label></label>
						<div class="col-md-4">
							<input type="text" id="iptEtiqueta" class="form-control"/>
						</div>
					</div>
				</div>
			</form>
		</div>	
	</div>	
	
</div>