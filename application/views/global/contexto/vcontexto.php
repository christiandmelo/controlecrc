<form class="form-horizontal" data-ajajax="#retornoContexto" method="POST" action="global/contexto/Ccontexto/Trocacontexto">
	<div class="form-group">
		<label class="col-md-3 control-label" for="selectbasic">Selecione um Tele Centro</label>
		<div class="col-md-9">
			<input type="hidden" id="txtContextoTipo" name="txtContextoTipo" value="<?=$contexto['tipo'];?>" />
			<select id="selTrocaContexto" name="selTrocaContexto" class="default-select2 form-control">
				<?php
	
				if( count( $lista['dados']->result_array() ) > 0 ):
					foreach( $lista['dados']->result() as $row): 
							?>
							<option value="<?=$row->CODIGO;?>-Z<?=$row->NOMEFANTASIA;?>" <?=($row->CODIGO == $_SESSION[$contexto['CODIGO']])? 'selected=selected' : '' ;?> ><?=$row->NOMEFANTASIA;?></option>
							<?php
					endforeach;
				endif;
				?>
			</select>
		</div>
	</div>
	
	
	<!-- Button -->
	<div class="form-group">
	  <div class="col-md-4">
	    <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Salvar</button>
	  </div>
	</div>
</form>

<div id="retornoContexto"></div>
