<ol class="breadcrumb pull-right">
	<li><a href="javascript:;">Inicio</a></li>
	<li class="active">Página de perfil</li>
</ol>

<h1 class="page-header">Página de perfil</h1>

<div id="divRetornoNovoCperfil"></div>

<?php
foreach( $lista['dados']->result_array() as $nDado => $arrDados ): 
	?>
	<div class="profile-container">
		<div class="profile-section">
			<div class="profile-left">    
				<div class="profile-image">
					<img src="<?=$arrDados['CAMINHOFOTO'];?>">
					<i class="fa fa-user hide"></i>
				</div>
				<div class="m-b-10">
					<a href="#" class="btn btn-warning btn-block btn-sm">Trocar foto</a>
				</div>
			</div>
			<div class="profile-right">
				<div class="profile-info">
					<div class="table-responsive">
						<form data-parsley-validate="true" action="global/seguranca/usuarios/cperfil/salvaEdicao" method="post" data-ajajax="#divRetornoNovoCperfil" class="form-horizontal" novalidate="">
							<table class="table table-profile">
								<tbody>
									<tr>
										<td class="field">Nome*:</td>
										<td>
											<input type="text" id="txtnomeCperfil" name="txtnomeCperfil" value="<?=$arrDados['NOME'];?>" data-parsley-required="true" class="form-control input-inline input-xs" placeholder="Adicionar Nome" />
										</td>
									</tr>								
									<tr>
										<td class="field">Email*:</td>
										<td>
											<input type="text" id="txtemailCperfil" name="txtemailCperfil" value="<?=$arrDados['EMAIL'];?>" data-parsley-required="true" class="form-control input-inline input-xs" placeholder="Adicionar Email" />
										</td>
									</tr>
									<tr class="divider">
										<td colspan="2"></td>
									</tr>
									<tr class="highlight">
										<td class="field">Senha</td>
										<td><a href="app/#/">Trocar senha</a></td>
									</tr>
									<tr>
										<td></td>
										<td>
											<div class="form-group">
											  <div class="col-md-4">
											    <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Salvar</button>
											  </div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
endforeach;
?>

<script type="text/javascript">
	$(function(){
		/*$(".default-select2").select2(), $(".multiple-select2").select2({
			placeholder : "Selecione uma opção"
		});*/
		
		$("[datepicker=true]").datepicker({
			todayHighlight : !0,
			autoclose : !0
		})
		
		<?=$mask;?>
		
		$('[data-parsley-validate]').parsley()
			
	});
</script>

