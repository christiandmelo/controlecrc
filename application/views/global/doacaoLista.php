
<div class="content" id="content">
	
	<?php
		// Mostra o Breadcrumb caso ele seja enviado
		if( isset($breadcrumb) )
			echo $breadcrumb;
	?>
	
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Filtro</h4>
		</div>
		<div class="panel-body">
			<form class="form-horizontal">
				<div class="form-group">
					<label class="col-md-2 control-label" for="ddlFornecedor">Fornecedor:<label style="color: red;">*</label></label>
					<div class="col-md-4">
						<select class="form-control" id="ddlFornecedor">
							<option value="">Selecione...</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
						</select>
					</div>
					<button id="btnFiltrar" type="submit" class="btn btn-sm btn-default">Filtrar</button>
				</div>
			</form>
		</div>
	</div>
	
	<div class="row">
		<div class="panel panel-inverse">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<button id="btnNovo" class="btn btn-sm btn-success m-r-5">Novo</button>
					<button id="btnEditar" class="btn btn-sm btn-primary m-r-5">Editar</button>
					<button id="btnExcluir" class="btn btn-sm btn-danger m-r-5">Excluir</button>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>		
				</div>
				<h4 class="panel-title">Lista de Doações</h4>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
                    <table id="tblListaDoacao" class="table table-striped table-bordered">
                    	<thead>
                    		<tr>
                    			<th> </th>
                    			<th>Lote / Etiqueta</th>
                    			<th>Fornecedor</th>
                    			<th>Tipo Entrada</th>
                    			<th>Tipo Doação</th>
                    			<th>Quantidade</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    		<tr class="odd gradeX">
                                <td><input type="checkbox" value="" /></td>
                                <td>999</td>
                                <td>Dell Computadores</td>
                                <td>Componente</td>
                                <td>Mouse</td>
                                <td>5</td>
                            </tr>
                            <tr class="even gradeC">
                                <td><input type="checkbox" value="" /></td>
                                <td>777</td>
                                <td>Dell Computadores</td>
                                <td>Produto</td>
                                <td>Computador</td>
                                <td>1</td>
                            </tr>
                            <tr class="odd gradeX">
                                <td><input type="checkbox" value="" /></td>
                                <td>555</td>
                                <td>Dell Computadores</td>
                                <td>Produto</td>
                                <td>Impressora</td>
                                <td>1</td>
                            </tr>
                            <tr class="even gradeC">
                                <td><input type="checkbox" value="" /></td>
                                <td>999</td>
                                <td>Dell Computadores</td>
                                <td>Produto</td>
                                <td>Monitor</td>
                                <td>1</td>
                            </tr>
                            <tr class="odd gradeX">
                                <td><input type="checkbox" value="" /></td>
                                <td>888</td>
                                <td>Dell Computadores</td>
                                <td>Componente</td>
                                <td>HD</td>
                                <td>3</td>
                            </tr>
                    	</tbody>
                    </table>	
				</div>
			</div>	
		</div>
	</div>
</div>

