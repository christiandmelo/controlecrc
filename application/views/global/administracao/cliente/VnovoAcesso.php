<div class="modal-dialog">
	<div class="modal-content">
		<form data-ajajax="#divRetornoNovoUsuario" method="post" action="publico/novoAcesso" data-parsley-validate="true" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title"> Novo Usuario </h4>
			</div>
			<div class="modal-body" id="corpoModalNovoUsuario">
				<div id="divRetornoNovoUsuario"></div>
				<div class="panel-body" >
					<div class="form-group">
	                    <label class="control-label"> CPF / CNPJ </label>
                        <input type="text" value="" id="intPFPJ" name="intPFPJ" class="form-control" data-parsley-required="true" data-parsley-type="number" placeholder="" />
	                </div>
	        		<div class="form-group">
	                    <label class="control-label"> Nome / Raz&atilde;o Social </label>
                        <input type="text" value="" id="txtNRS" name="txtNRS" class="form-control" data-parsley-required="true" placeholder="" />
	                </div>
	        		<div class="form-group">
	                    <label class="control-label"> Email </label>
                        <input type="text" value="" id="txtEmail" name="txtEmail" class="form-control" data-parsley-required="true" data-parsley-type="email" placeholder="" />
	                </div>
				</div>
	        </div>
			<div class="modal-footer">
				<button type="submit" id="submit" name="submit" class="btn btn-success btn-sm">Salvar</button>
				<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Fechar</a>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		
		$('[data-parsley-validate]').parsley()
			
	});
</script>