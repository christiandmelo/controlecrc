<?php // Mostra o Breadcrumb caso ele seja enviado
if (isset($breadcrumb))
	echo $breadcrumb;

//Variavel para controlar os campos que possui mascara
$mask = "";
?>

<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <!-- <a data-click="panel-collapse" class="btn btn-icon btn-default" href="javascript:;" data-original-title="" title=""><i class="fa fa-minus"></i></a> -->
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Filtro</h4>
            </div>
            <div class="panel-body">
                <!--Inicio formulario do filtro-->
                <form 	method="post" 
                		data-ajajax="#corpo<?= str_replace(' ', '_', $lista['Titulo']); ?>" 
                		action="<?=$lista['lista_filtro'];?>"
                		data-parsley-validate="true"
                		class="form-horizontal" 
                		role="form">
                <?php
                $existeFiltro = false;
                foreach( $filtro as $keyTab => $rsTab ):
                	foreach( $rsTab as $keyForm => $rsForm ):
                		if(isset($rsForm['filtro']) and $rsForm['filtro']){
                			$existeFiltro = true;
                			$arrCampoTabela = explode(".",$keyForm);
                			switch ($rsForm['tipo']){
                				case "label":
                					if(isset($rsForm['mask'])){
                						$mask .= '$("#filtro'.$rsForm['idname'].'").mask("'.$rsForm['mask'].'");';
                					}
                					?>
                					<div class="form-group">
                    					<label class="col-md-2 control-label" for="<?= $rsForm['idname']; ?>"> <?= $rsForm['label']; ?></label>
                    					<div class="col-md-5">
                        					<input type="text" <?= (isset($rsForm['datepicker']) and $rsForm['datepicker'] == true) ? 'datepicker=true' : ''; ?> id="filtro<?= $rsForm['idname']; ?>" name="filtro<?= $rsForm['idname']; ?>" class="form-control" placeholder="<?= (isset($rsForm['placeholder'])) ? $rsForm['placeholder'] : ''; ?>" <?= isset($rsForm['disabled']) ? 'disabled' : ''; ?>/>
                    					</div>
                					</div>
                					<?php
                					break;

                				case "select":
                				?>

                					<div class="form-group">
                    					<label class="col-md-2 control-label" for="<?= $rsForm['idname']; ?>"> 
                    						<?= $rsForm['label']; ?>
                    						<?=(isset($rsForm['required']))? '*' : ''?>
                    					</label>
                    					<div class="col-md-5">
                        					<select class="form-control default-select2 form-control" 
                        							id="filtro<?= $rsForm['idname']; ?>" 
                        							name="filtro<?= $rsForm['idname']; ?>" 
                        							<?=(isset($rsForm['required']) and $rsForm['required'] == true)? 'data-parsley-required="true"' : '';?>>
                            				<?php
                            					foreach($rsForm['values'] as $keySelect => $valuesSelect):
                            						?>
                            						<option value="<?= $keySelect; ?>"><?= $valuesSelect; ?></option>
                            					<?php endforeach; ?>
                        					</select>
                    					</div>
                					</div>
                					<?php
                					break;

                				}
                			}
                		endforeach;
                endforeach;

                if(!$existeFiltro){
                	?>
                	<div class="form-group">
                    	<label class="col-md-3 control-label"> Não existe filtro para essa tela!</label>
                	</div>
                <?php } ?>

                <!--Botão que executa o filtro e busca os dados-->
                <button id="btnFiltrar" type="submit" style="float:right" class="btn btn-sm btn-inverse">
                    <!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
                    <i class="glyphicon glyphicon-search"></i> Filtrar
                </button>
                </form>
                <!--Fim formulario do filtro-->
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 ui-sortable">
        <div data-sortable-id="ui-general-5" class="panel panel-inverse" style="margin-bottom: 0px;">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <div class="content-btn-icon">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!--Inicio do conteúdo do filtro-->
                <h4 class="panel-title"> <?= $lista['Titulo']; ?> </h4>
            </div>
            <!-- Div que carrega o filtro-->
            <div id="corpo<?= str_replace(' ', '_', $lista['Titulo']); ?>" class="panel-body">
            </div>
        </div>
        <!-- end panel -->
        <script type="text/javascript">
            jQuery('.panel-body').css('max-height', ( window.screen.availHeight - ( 54 + 43 + ( 30 * 2 ) + ( 15 * 2 ) ) ) );
            jQuery('.panel-body').css('overflow','auto');

            $(function(){

                $("[datepicker=true]").datepicker({
                    todayHighlight : !0,
                    autoclose : !0
                })

                <?= $mask; ?>
                    $('[data-parsley-validate]').parsley()

                });
        </script>
    </div>
</div>


<br />
<br />
<div id="corpoDivCestoque"></div>


<?php
if(isset($lista['divcomplementar'])){
?>
<div id="<?= $lista['divcomplementar']; ?>"></div>
	<?php } ?>

<!-- Carrega a função js da página se existir-->
<?php
if(isset($funcao_js)){
	?>
<script src="<?= $funcao_js; ?>"></script>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="modal<?= $lista['classe']; ?>">
    <div class="modal-dialog">
        <div class="modal-content" id="corpoModal<?=$lista['classe']; ?>">    	
            <div class="modal-body" >
            	<div class="panel panel-inverse">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
						</div>
						<h4 class="panel-title"><?=$lista['Titulo']; ?></h4>
					</div>
					<div class="panel-body">
                        <p>Carregando suas informações...</p>
                    </div>
				</div>  
            </div>
            <div class="modal-footer">
                <button id="ModalbtnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Cancelar</button>
            </div>
        </div>
    </div>
</div>

