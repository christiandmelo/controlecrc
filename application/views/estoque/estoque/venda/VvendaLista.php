<?php
	include APPPATH . "/views/funcoes/funcoes.php";
?>


<table class="table table-striped table-bordered">
<thead>
	<tr>
		<th>Etiqueta</th>
		<th>Tipo Doação</th>
		<th>Status Lançamento</th>
		<th>Id na Loja</th>
		<th>Loja</th>
		<th>Data Emissão</th>
		<th>Valor</th>		
		<th style="width: 20px"></th>
	</tr>
</thead>
<tbody>
	<?php 
	
		if( count( $registros ) > 0 ){
			foreach( $registros as $key => $value ):
					echo "<tr class='odd gradeX'>
							<td>".$value['ETIQUETA']."</td>
							<td>".$value['TIPODOACAO']."</td>
							<td>".$value['STATUSLANCAMENTO']."</td>
							<td>".$value['IDLOJA']."</td>
							<td>".$value['LOJA']."</td>
							<td>".converteData($value['DATAEMISSAO'])."</td>
							<td>".$value['VALORORIGINAL']."</td>
							<td></td>
						 <tr>";
			endforeach;
		}else{
			?>
			<tr>
				<td colspan="20" > Sem Registro </td>
			</tr>
		<?php
			};
	?>
</tbody>
</table>