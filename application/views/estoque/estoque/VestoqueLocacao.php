<br />

<div id="divRetornoComp"></div>

<div class="row">
	<div class="col-md-12 ui-sortable">
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<h4 class="panel-title">Montagem e alocação do produto</h4>
		</div>
		<div class="panel-body">
			<form class="form-horizontal" id="formTriagemEquipamento" data-ajajax="#divRetornoComp" method="POST" action="estoque/estoque/Cestoque/salvarLocacaoEquipamento" data-parsley-validate-loc='true'>
				<input 	type="text" value="<?=$_POST['codproduto'];?>" id="txtCodProdutoEstoqueLocacao" style="display: none" class="form-control" disabled="disabled"/>
				<div class='form-group'>
					<div class="form-group">
						<label class="col-md-2 control-label" > 
							Tipo Doacao*:
						</label>
						<div class="col-md-5">
	    					<select class="form-control default-select2" 
	    							id="txtTipoDoacaoCtriagemComponenteLocacao" 
	    							name="txtTipoDoacaoCtriagemComponenteLocacao" 
	    							data-parsley-required="true"
	    							onchange="listarFornecedores()">
	        						<option value="1">Interna</option>
	        						<option value="0" <?=($compLocacao[0]->INTERNA == 0 && $compLocacao[0]->INTERNA != 1 && $compLocacao[0]->INTERNA != null)? 'selected=selected' : "" ;?>>Externa</option>
	    					</select>
						</div>
					</div>
				</div>
				
				<div class='form-group' id="divTeleCentroCtriagemComponenteLocacao">
					<div class="form-group">
						<label class="col-md-2 control-label" > 
							Tele Centro*:
						</label>
						<div class="col-md-5">
	    					<select class="form-control default-select2" 
	    							id="txtTeleCentroCtriagemComponenteLocacao" 
	    							name="txtTeleCentroCtriagemComponenteLocacao"
	    							data-parsley-required="true" >
	    							<option value="">Escolha um valor!</option>
	    							<?php
	    							foreach ($filiais as $row){
	    								?>
										<option value="<?=$row->CODFILIAL;?>"
												<?=($compLocacao[0]->CODFILIALLOCADO != 0)? 'selected=selected' : "" ;?>
											>
											<?=$row->NOMEFANTASIA;?>
										</option>
										<?php
									}
	    							?>
	    					</select>
						</div>
					</div>
				</div>
				
				<div class='form-group' id="divClienteCtriagemComponenteLocacao" style="display: none">
					<div class="form-group">
						<label class="col-md-2 control-label" > 
							Cliente*:
						</label>
						<div class="col-md-5">
	    					<select class="form-control default-select2" 
	    							id="txtClienteCtriagemComponenteLocacao" 
	    							name="txtClienteCtriagemComponenteLocacao" >
	    					</select>
						</div>
					</div>
				</div>
				<?php
				$codstatusLocacao = "";
				foreach($compLocacao as $row){
					$codstatusLocacao = $_POST['codtrstatus'];
					?>
	    			<div class="row">
						<div class="form-group">
							<label class="col-md-2 control-label" for="">
								<?=$row->NOMETIPOFILHO;?>
								<label style="color: red;">*</label>
							</label>
							<div class="col-md-2">
								<input 	type="text" 
										id="txtIdEtiqueta<?=$row->CODCOMPONENTESLOCACAO;?>" 
										value="<?=$row->CODPRODUTOFILHO;?>"
										data-parsley-required="true"
                    					data-parsley-type="number"
                    					data-componente-equipamento="true"
										class="form-control" 
										disabled="disabled"/>
							</div>
							<div id="divComponentesDoProduto<?=$row->CODCOMPONENTESLOCACAO;?>" class="col-md-5">
								<?php
								if($row->CODPRODUTOFILHO == "" && $row->CODCOMPONENTESDALOCACAO == ""){
									if($row->CODTRSTATUS == 3){
										?>
										<a 	data-toggle="modal"
		                            		data-target="#modalCestoque"
											data-original-title="Editar"  
											data-ajajax-tipo="editar" 
											title="Inserir"
											class="btn btn-sm btn-success m-r-5"
											onclick="modalComponenteExternoEquipamento(<?=$row->CODTIPOFILHO;?>,<?=$row->CODPRODUTO;?>,<?=$row->CODCOMPONENTESLOCACAO;?>)"
											>
											 
											<i class="fa fa-edit (alias)"></i> Inserir
										</a>
										<?php
									}
								}else{
									if($row->CODTRSTATUS == 3 || $row->CODTRSTATUS == 6){
										?>
										<a 	data-toggle="modal"
		                            		data-target="#modalCestoque"
											data-original-title="Editar"  
											data-ajajax-tipo="editar" 
											title="Trocar"
											class="btn btn-sm btn-primary m-r-5"
											onclick="modalComponenteExternoEquipamento(<?=$row->CODTIPOFILHO;?>,<?=$row->CODPRODUTO;?>,<?=$row->CODCOMPONENTESLOCACAO;?>,<?=$row->CODPRODUTOFILHO;?>,<?=$row->CODCOMPONENTESDALOCACAO;?>)"
											 >
											<i class="fa fa-exchange"></i> Trocar
										</a>
										<?php
									}
								}
								?>
							</div>
						</div>
					</div>
	    			<?php
				}
        		?>
				<br />
				<br />
				<div class="col-lg-7"></div>
				<div class="col-lg-7">
					<?php
					if($_POST['codtrstatus'] == 3){
					?>
						<button type="submit"
						 	title="Realizar alocação do equipamento"
							class="btn btn-sm btn-default"> 
							Cadastrar locação do produto
						</button>
					<?php
					}
					?>				
				</div>				
			</form>	
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		window.scrollTo(0, 980);
		
		$('[data-parsley-validate-loc]').parsley()
	});
</script>

