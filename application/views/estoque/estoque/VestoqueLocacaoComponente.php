<div class="modal-body" >
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title">Trocar Componente do produto</h4>
		</div>
		<div class="panel-body" >
			<div id="divRetSalvCompEquipExt"></div>
			
           <table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Código</th>
						<th>Etiqueta</th>
						<?php
						$exiteCamposExtra = false;
						foreach ($cabecalho as $key => $value){
							echo "<th>".$value['CAMPOS']."</th>";
							$exiteCamposExtra = true;
						}
						?>
						<th style="width: 20px">Ação</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					
						if( count( $registros ) > 0 ){
							$controletr = 0;
							$coddoacao = 0;
							foreach( $registros as $key => $value ):
								if($controletr != $value['CODPRODUTO']){
									if($controletr != 0) echo "<td style='width: 20px'><button onclick='salvarComponenteLocacaoExterna(".$_POST['codprodutopai'].",".$value['CODPRODUTO'].",".$_POST['codcomponenteslocacao'].",".$_POST['codantigoprodutofilho'].",".$_POST['codcomponentesdalocacao'].")' class='btn btn-sm btn-warning m-r-5'>Selecionar</button></td></tr>"; 
									echo "<tr>
											<td>".$value['CODPRODUTO']."</td>
											<td>".$value['ETIQUETA']."</td>";
								} 
								if($exiteCamposExtra){
									?>
										<td><?=$value['VALOR'];?></td>
									<?php
								}
								$controletr = $value['CODPRODUTO']; 
								$coddoacao = $value['CODDOACAO'];
							endforeach;
							echo "<td style='width: 20px'><button onclick='salvarComponenteLocacaoExterna(".$_POST['codprodutopai'].",".$value['CODPRODUTO'].",".$_POST['codcomponenteslocacao'].",".$_POST['codantigoprodutofilho'].",".$_POST['codcomponentesdalocacao'].")' class='btn btn-sm btn-warning m-r-5'>Selecionar</button></td></tr>";
						}else{
							?>
							<tr>
								<td colspan="5" > Sem Registro </td>
							</tr>
						<?php
							};
					?>
				</tbody>
			</table>
        </div>
	</div>  
</div>
<div class="modal-footer">
    <button id="ModalbtnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Fechar</button>
</div>


