<br />

<div id="divRetornoComp"></div>

<div class="row">
	<div class="col-md-12 ui-sortable">
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<h4 class="panel-title">Triagem Equipamento</h4>
		</div>
		<div class="panel-body">
			<form class="form-horizontal" id="formTriagemEquipamento" data-ajajax="#divRetornoComp" method="POST" action="estoque/estoque/triagem/CtriagemComponente/enviarEstoqueEquipamento" data-parsley-validate='true'>
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptLote">Etiqueta:</label>
						<div class="col-md-3">
							<input type="text" id="iptEtiqueta" value="<?=$cabecalho->ETIQUETA;?>" class="form-control" disabled="disabled"/>
							<input type="text" id="iptCodTipoEntrada" value="<?=$cabecalho->CODTIPOENTRADA;?>" class="form-control" disabled="disabled" style="display:none"/>
							<input type="text" id="iptCodCodprodutoPai" value="<?=$produto->CODPRODUTO;?>" class="form-control" disabled="disabled" style="display:none"/>
							<input type="text" id="iptCodDoacaoprodutoPai" value="<?=$cabecalho->CODDOACAO;?>" class="form-control" disabled="disabled" style="display:none"/>
						</div>
						<label class="col-md-2 control-label" for="iptQuantidade">Quantidade:</label>
						<div class="col-md-3">
							<input type="text" id="iptQuantidade" value="<?=$cabecalho->QUANTIDADE;?>" class="form-control" disabled="disabled"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="iptTipoDoacao">Tipo Doação:</label>
						<div class="col-md-3">
							<input type="text" id="iptTipoDoacao" value="<?=$cabecalho->TIPO;?>" class="form-control" disabled="disabled"/>
						</div>	
						<label class="col-md-2 control-label" for="iptQuantidadeTriada">Qtd. Triado:</label>
						<div class="col-md-3">
							<input type="text" id="iptQuantidadeTriadaVTriagemComponente" value="<?=$cabecalho->QUANTIDADETRIADA;?>" class="form-control" disabled="disabled"/>
						</div>		
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label">Status Equipamento:</label>
						<div class="col-md-3">
							<input type="text" value="<?=$produto->STATUS;?>" class="form-control" disabled="disabled" />
						</div>
						<label class="col-md-2 control-label" for="iptFornecedor">Fornecedor:</label>
						<div class="col-md-3">
							<input type="text" id="iptFornecedor" value="<?=$cabecalho->FORNECEDOR;?>" class="form-control" disabled="disabled"/>
						</div>
					</div>
				</div>
			
				<br />
				<?php
				$codstatusProduto = "";
				foreach($compProduto as $row){
					$codstatusProduto = $row->CODTRSTATUS;
					?>
	    			<div class="row">
						<div class="form-group">
							<label class="col-md-2 control-label" for="">
								<?=$row->NOMETIPOFILHO;?>
								<?=($row->OBRIGATORIO == 1)? '<label style="color: red;">*</label>' : '';?>
							</label>
							<div class="col-md-2">
								<input 	type="text" 
										id="txtIdEtiqueta<?=$row->CODCOMPONENTESTIPO;?>" 
										value="<?=$row->CODPRODUTOFILHO;?>"
										<?=($row->OBRIGATORIO == 1)? 'data-parsley-required="true"' : ''?>
                    					data-parsley-type="number"
                    					data-componente-equipamento="true"
										class="form-control" 
										disabled="disabled"/>
							</div>
							<div id="divComponentesDoProduto<?=$row->CODCOMPONENTESTIPO;?>" class="col-md-5">
								<?php
								if($row->CODPRODUTOFILHO == "" && $row->CODCOMPONENTESPRODUTO == ""){
									if($row->CODTRSTATUS != 4 && $row->CODTRSTATUS != 3 && $row->CODTRSTATUS != 8){
										?>
										<a 	data-toggle="modal"
		                            		data-target="#modalCtriagem"
											data-original-title="Editar"  
											data-ajajax-tipo="editar" 
											title="Inserir"
											class="btn btn-sm btn-success m-r-5"
											onclick="novoEditarComponente(<?=$row->CODDOACAO;?>,<?=$row->CODTIPOFILHO;?>,'undefined','undefined',<?=$row->CODCOMPONENTESTIPO;?>)" > 
											<i class="fa fa-edit (alias)"></i> Inserir
										</a>
										<?php
									}
								}else{
									if($row->CODTRSTATUS != 4 && $row->CODTRSTATUS != 3 && $row->CODTRSTATUS != 8){
										?>
										<a 	data-toggle="modal"
		                            		data-target="#modalCtriagem"
											data-original-title="Editar"  
											data-ajajax-tipo="editar" 
											title="Trocar"
											class="btn btn-sm btn-primary m-r-5"
											onclick="modalTrocaComponenteEquipamento(<?=$row->CODCOMPONENTESPRODUTO;?>,<?=$row->CODDOACAO;?>,<?=$row->CODTIPOFILHO;?>,<?=($row->CODPRODUTOFILHO != '')? $row->CODPRODUTOFILHO : 0 ;?>,<?=($row->ETIQUETA != "")? $row->ETIQUETA : "0" ;?>)" > 
											<i class="fa fa-exchange"></i> Trocar
										</a>
										<?php
									}
									if($row->CODPRODUTOFILHO != ""){
										if($row->CODTRSTATUS != 4 && $row->CODTRSTATUS != 3 && $row->CODTRSTATUS != 8){
											?>
											<a 	data-toggle="modal"
		                                		data-target="#modalCtriagem"
												data-original-title="Editar"  
												data-ajajax-tipo="editar" 
												title="Estoque"
												class="btn btn-sm btn-warning m-r-5"
												onclick="modalEstoqueComponentePorEquipamento(<?=$row->CODPRODUTOFILHO;?>,<?=$row->CODCOMPONENTESPRODUTO;?>,<?=($row->ETIQUETA != "")? $row->ETIQUETA : "0" ;?>)" > 
												<i class="fa fa-archive"></i> Estoque
											</a>
											<a 	title="Estoque"
												class="btn btn-sm btn-danger m-r-5"
												onclick="sucataComponenteEquipamento(<?=$row->CODPRODUTOFILHO;?>,<?=$row->CODTIPOFILHO;?>,<?=$row->CODDOACAO;?>,<?=$row->CODCOMPONENTESPRODUTO;?>)"> 
												<i class="fa fa-trash-o"></i>
												Sucata
											</a>
											<?php
										}
										?>
										<a 	data-toggle="modal"
	                                		data-target="#modalCtriagem"
											data-original-title="Editar"  
											data-ajajax-tipo="editar" 
											title="Log"
											class="btn btn-sm btn-default m-r-5"
											onclick="verlogComponentesEquipamento(<?=$row->CODPRODUTOFILHO;?>,<?=$row->CODCOMPONENTESPRODUTO;?>)" > 
											<i class="fa fa-history"></i> Histórico Componente
										</a>
										<?php
									}
								}
								?>
							</div>
						</div>
					</div>
	    			<?php
				}
        		?>
				<br />
				<br />
				<div class="col-lg-7"></div>
				<div class="col-lg-7">
					<?php
					if($produto->CODTRSTATUS == 1 || $produto->CODTRSTATUS == 2 || $produto->CODTRSTATUS == 3){
					?>
						<a 	title="Enviar equipamento para sucata"
							class="btn btn-sm btn-default"
							onclick="sucataGeralEquipamento(<?=$produto->CODPRODUTO;?>,<?=$cabecalho->CODDOACAO;?>)"> 
							Enviar Equipamento para Sucata
						</a>
					<?php
					}
					if($produto->CODTRSTATUS == 1 || $produto->CODTRSTATUS == 2){
					?>
						<button id="btnEnviarEstoque" type="submit" class="btn btn-sm btn-default">Enviar Para Estoque</button>
					<?php
					}
					?>
					<a 	data-toggle='modal'
                		data-target='#modalCtriagem'
						data-original-title='Editar'  
						data-ajajax-tipo='editar' 
						title='Log'
						class='btn btn-sm btn-default m-r-5'
						onclick='verlogComponente(<?=$produto->CODPRODUTO;?>)' > 
						<i class='fa fa-history'></i> Histórico
					</a>					
				</div>
			</form>	
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		window.scrollTo(0, 980);
		
		$('[data-parsley-validate]').parsley()
	});
</script>		