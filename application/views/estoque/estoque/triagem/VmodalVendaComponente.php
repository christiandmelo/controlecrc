<form class="form-horizontal" data-ajajax="#divRetornoComp" method="POST" action="estoque/estoque/triagem/CtriagemComponente/vendaComponente" data-parsley-validate='true'>
	<div class="modal-body" >
		<div class="panel panel-inverse">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title">Geração de venda do componente</h4>
			</div>
			<div class="panel-body" >
				<div id="divRetSalvEstoqueCompEquip"></div>
				<div class='form-group'>
					<label class='col-md-3 control-label'>
						ID Produto da loja*:
					</label>
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtidlojaVendaCtriagemComponente' id='txtidlojaVendaCtriagemComponente' data-parsley-required='true' >
						<input type='text' class='form-control' disabled="disabled" style="display:none" value="<?=$_POST['codproduto'];?>" name='txtcodprodutoVendaCtriagemComponente' id='txtcodprodutoVendaCtriagemComponente' data-parsley-required='true' >
						<input type='text' class='form-control' disabled="disabled" style="display:none" value="<?=$_POST['coddoacao'];?>" name='txtcoddoacaoVendaCtriagemComponente' id='txtcoddoacaoVendaCtriagemComponente' >
						<?php
							if(isset($_POST['tipoentrada'])){
								?>
								<input type='text' class='form-control' disabled="disabled" style="display:none" value="<?=$_POST['tipoentrada'];?>" name='txttipoentradaVendaCtriagemComponente' id='txttipoentradaVendaCtriagemComponente' >
								<?php
							}
						?>
					</div>
				</div>
				<div class='form-group'>
					<label class='col-md-3 control-label'>
						Loja*:
					</label>
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtlojaVendaCtriagemComponente' id='txtlojaVendaCtriagemComponente' data-parsley-required='true' >
					</div>
				</div>
				<div class='form-group'>
					<label class='col-md-3 control-label'>
						Valor*:
					</label>
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtvalorVendaCtriagemComponente' id='txtvalorVendaCtriagemComponente' data-parsley-required='true' >
					</div>
				</div>
	        </div>
		</div>  
	</div>
	<div class="modal-footer">
		<button onclick="salvarVendaComponente(<?=$_POST['codproduto'];?>)" class="btn btn-sm btn-success">Salvar</button>
	    <button id="ModalbtnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Fechar</button>
	</div>
</form>

<script type="text/javascript">
	$(function(){		
		$('[data-parsley-validate]').parsley()
		
		$("#txtvalorVendaCtriagemComponente").mask("#.##0,00", {reverse: true});
		
	});
</script>