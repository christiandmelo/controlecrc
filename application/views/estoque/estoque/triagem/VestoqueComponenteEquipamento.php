<div class="modal-body" >
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title">Trocar Componente do produto</h4>
		</div>
		<div class="panel-body" >
			<div id="divRetSalvEstoqueCompEquip"></div>
			<div class='form-group'>
				<label class='col-md-3 control-label'>
					Etiqueta*:
				</label>
				<div class='col-md-9'>
					<input type='text' class='form-control' value="<?=($_POST['etiqueta'] != 0)? $_POST['etiqueta'] : "" ;?>" name='txtEtiquetaEstoqueCtriagemComponente' id='txtEtiquetaEstoqueCtriagemComponente' data-parsley-type='number' data-parsley-required='true' >
				</div>
			</div>
        </div>
	</div>  
</div>
<div class="modal-footer">
	<button onclick="trocarEstoqueComponentePorEquipamento(<?=$_POST['codproduto'];?>,<?=$_POST['codcomponentesproduto'];?>)" class="btn btn-sm btn-success">Salvar</button>
    <button id="ModalbtnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Fechar</button>
</div>


