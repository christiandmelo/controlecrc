<div class="modal-body" >
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title">Trocar Componente do produto</h4>
		</div>
		<div class="panel-body" >
			<div id="divRetSalvCompEquip"></div>
			
			<?php
			if($_POST['codprodutofilho'] != 0){
			?>
				<div class="form-group">
					<label class="col-md-3 control-label" for="selectbasic">
						Destino do componente antigo*
						<input type="text" id="iptCodAntigoEtiqueta" value="<?=($_POST['codantigoetiqueta']);?>" class="form-control" disabled="disabled" style="display:none"/>
					</label>
					<div class="col-md-9">
						<select id="selComponenteAntigoCtriagemComponente" 
								name="selComponenteAntigoCtriagemComponente" 
								data-parsley-required="true" 
								onchange="trocaComponenteEquipamentoEtiqueta()"
								class="default-select2 form-control" >
									<option value="" >Escolha um valor!</option>
									<option value="2" >Enviar para sucata</option>
									<option value="1" >Enviar para estoque</option>		
						</select>
					</div>
				</div>
				
				<div id="divRetornoTrocaComponenteEquipamento"></div>
				
				<br />
				<br />
			<?php
			}
			?>
           <table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Código</th>
						<th>Etiqueta</th>
						<?php
						foreach ($cabecalho as $key => $value){
							echo "<th>".$value['CAMPOS']."</th>";
						}
						?>
						<th style="width: 20px"></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					
						if( count( $registros ) > 0 ){
							$controletr = 0;
							$coddoacao = 0;
							foreach( $registros as $key => $value ):
								if($controletr != $value['CODPRODUTO']){
									if($controletr != 0) echo "<td style='width: 20px'><button onclick='trocarComponenteEquipamento(".$controletr.",".$_POST['codprodutofilho'].",".$_POST['codcomponentesproduto'].",".$coddoacao.",".$_POST['codtipo'].")' class='btn btn-sm btn-warning m-r-5'>Trocar</button></td></tr>"; 
									echo "<tr>
											<td>".$value['CODPRODUTO']."</td>
											<td>".$value['ETIQUETA']."</td>";
								} 
								?>
									<td><?=$value['VALOR'];?></td>
								<?php
								$controletr = $value['CODPRODUTO']; 
								$coddoacao = $value['CODDOACAO'];
							endforeach;
							echo "<td style='width: 20px'><button onclick='trocarComponenteEquipamento(".$controletr.",".$_POST['codprodutofilho'].",".$_POST['codcomponentesproduto'].",".$coddoacao.",".$_POST['codtipo'].")' class='btn btn-sm btn-warning m-r-5'>Selecionar</button></td></tr>";
						}else{
							?>
							<tr>
								<td colspan="5" > Sem Registro </td>
							</tr>
						<?php
							};
					?>
				</tbody>
			</table>
        </div>
	</div>  
</div>
<div class="modal-footer">
    <button id="ModalbtnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Fechar</button>
</div>


