<?php
	include APPPATH . "/views/funcoes/funcoes.php";
?>

<br />

<div id="divRetornoComp"></div>

<div class="row">
	<div class="col-md-12 ui-sortable">	
		<div class="panel panel-inverse">
			<div class="panel-heading">
				<h4 class="panel-title">Triagem Componente</h4>
			</div>
			<div class="panel-body">
				<form class="form-horizontal">
					<div class="row">
						<div class="form-group">
							<label class="col-md-2 control-label" for="iptLote">Lote:</label>
							<div class="col-md-3">
								<input type="text" id="iptLote" value="<?=$cabecalho->CODLOTE;?>" class="form-control" disabled="disabled"/>
								<input type="text" id="iptCodTipoEntrada" value="<?=$cabecalho->CODTIPOENTRADA;?>" class="form-control" disabled="disabled" style="display:none"/>
							</div>
							<label class="col-md-2 control-label" for="iptQuantidade">Quantidade:</label>
							<div class="col-md-1">
								<input type="text" id="iptQuantidade" value="<?=$cabecalho->QUANTIDADE;?>" class="form-control" disabled="disabled"/>
							</div>
							<label class="col-md-2 control-label" for="iptQuantidadeTriada">Qtd. Triado:</label>
							<div class="col-md-1">
								<input type="text" id="iptQuantidadeTriadaVTriagemComponente" value="<?=$cabecalho->QUANTIDADETRIADA;?>" class="form-control" disabled="disabled"/>
							</div>		
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<label class="col-md-2 control-label" for="iptTipoDoacao">Tipo Doação:</label>
							<div class="col-md-3">
								<input type="text" id="iptTipoDoacao" value="<?=$cabecalho->TIPO;?>" class="form-control" disabled="disabled"/>
							</div>	
							<label class="col-md-2 control-label" for="iptFornecedor">Fornecedor:</label>
							<div class="col-md-4">
								<input type="text" id="iptFornecedor" value="<?=$cabecalho->FORNECEDOR;?>" class="form-control" disabled="disabled"/>
							</div>
						</div>
					</div>
				</form>
				
				<br />
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>		
						</div>
						<h4 class="panel-title">Componentes</h4>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
		                    <table class="table table-striped table-bordered">
		                    	<thead>
		                    		<tr>
		                    			<th>Etiqueta</th>
		                    			<th>Status</th>
		                    			<th>Triado em</th>
		                    			<td style="width: 280px;">Ações</td>
		                    		</tr>
		                    	</thead>
		                    	<tbody id="tblComponentes">
		                    		<?php
		                    		$contador = $cabecalho->QUANTIDADE;
		                    		while ($contador > $cabecalho->QUANTIDADETRIADA){
		                    			?>
		                    			<tr class="odd gradeX" id="trVtriagemComponente<?=$contador;?>">
			                                <td> - </td>
			                                <td><?=$cabecalho->STATUS;?></td>
			                                <td> - </td>
			                                <td style="width: 250px;">
			                                	<a 	data-toggle="modal"
			                                		data-target="#modalCtriagem"
													data-original-title="Editar"  
													data-ajajax-tipo="editar" 
													title="Editar"
													class="btn btn-sm btn-success m-r-5"
													onclick="novoEditarComponente(<?=$cabecalho->CODDOACAO;?>,<?=$cabecalho->CODTIPO;?>)" > 
													<i class="fa fa-edit (alias)"></i>Inserir
												</a>
												<button id="btnGerarSucata" 
														class="btn btn-sm btn-danger m-r-5" 
														onclick="sucataComponente(<?=$cabecalho->CODDOACAO;?>,<?=$cabecalho->CODTIPO;?>)">
													<i class="fa fa-trash-o"></i> Sucata
												</button>
											</td>
		                            	</tr>
		                    			<?php
		                    			$contador--;
		                    		}
		                    		
									foreach($produto as $row){
										?>
		                    			<tr  class="odd gradeX">
			                                <td><?=$row->ETIQUETA;?></td>
			                                <td id="tdstatuscomponente<?=$row->CODPRODUTO;?>" ><?=$row->STATUS;?></td>
			                                <td><?=fconverteData($row->REGCRIADOEM);?></td>
			                                <td style="width: 280px;">
			                                	<a 	data-toggle="modal"
			                                		data-target="#modalCtriagem"
													data-original-title="Editar"  
													data-ajajax-tipo="editar" 
													title="Editar"
													class="btn btn-sm btn-warning m-r-5"
													onclick="novoEditarComponente(<?=$cabecalho->CODDOACAO;?>,<?=$cabecalho->CODTIPO;?>,<?=$row->ETIQUETA;?>,<?=$row->CODPRODUTO;?>)" > 
													<i class="fa fa-edit (alias)"></i> Editar
												</a>
												<?php if($row->CODTRSTATUS == 3){
													?>
													<a 	data-toggle="modal"
				                                		data-target="#modalCtriagem"
														data-original-title="Editar"  
														data-ajajax-tipo="editar" 
														title="Editar"
														class="btn btn-sm btn-success m-r-5"
														id="btVendaVtriagemComponente<?=$row->CODPRODUTO;?>";
														onclick="modalvendaComponente(<?=$row->CODPRODUTO;?>,<?=$cabecalho->CODDOACAO;?>)" > 
														<i class="fa fa-money"></i> Venda
													</a>
												<?php
												}
												?>
												<a 	data-toggle="modal"
			                                		data-target="#modalCtriagem"
													data-original-title="Editar"  
													data-ajajax-tipo="editar" 
													title="Log"
													class="btn btn-sm btn-default m-r-5"
													onclick="verlogComponente(<?=$row->CODPRODUTO;?>)" > 
													<i class="fa fa-history"></i> Histórico
												</a>
											</td>
		                            	</tr>
		                    			<?php
									}
		                    		
		                    		
		                    		foreach($sucata as $row){
										?>
		                    			<tr  class="odd gradeX">
			                                <td> - </td>
			                                <td><?=$row->STATUS;?></td>
			                                <td><?=fconverteData($row->REGCRIADOEM);?></td>
			                                <td style="width: 250px;"></td>
		                            	</tr>
		                    			<?php
									}
		                    		?>
		                    	</tbody>
		                    </table>	
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>	
</div>

<script type="text/javascript">
	window.scrollTo(0, 980);
</script>


