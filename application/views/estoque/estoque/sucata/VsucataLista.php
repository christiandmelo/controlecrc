<?php
	include APPPATH . "/views/funcoes/funcoes.php";
?>

<table class="table table-striped table-bordered">
<thead>
	<tr>
		<th>Tipo Doação</th>
		<th>Fornecedor</th>
		<th>Data Entrada</th>
		<th>Quantidade</th>
	</tr>
</thead>
<tbody>
	<?php 
	
		if( count( $registros ) > 0 ){
			foreach( $registros as $key => $value ):
				?>
				<tr>
					<td><?=$value['TIPO'];?></td>
					<td><?=$value['FORNECEDOR'];?></td>
					<td><?=converteData($value['DATAENTRADA']);?></td>
					<td><?=$value['QUANTIDADE'];?></td>
				</tr>
				<?php
			endforeach;
		}else{
			?>
			<tr>
				<td colspan="5" > Sem Registro </td>
			</tr>
		<?php
			};
	?>
</tbody>
</table>