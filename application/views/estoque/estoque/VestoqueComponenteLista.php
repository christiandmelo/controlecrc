<div id="divRetornoComp"></div>

<table class="table table-striped table-bordered">
<thead>
	<tr>
		<th>Código</th>
		<th>Etiqueta</th>
		<th>Fornecedor</th>
		<?php
		foreach ($cabecalho as $key => $value){
			echo "<th>".$value['CAMPOS']."</th>";
		}
		?>
		<th style="width: 350px">Ações</th>
	</tr>
</thead>
<tbody>
	<?php 
	
		if( count( $registros ) > 0 ){
			$codproduto = 0;
			$codstatus = 0;
			$coddoacao = 0;
			foreach( $registros as $key => $value ):
				if($codproduto != $value['CODPRODUTO']){
					if($codproduto != 0){
						?>
						<td style='width: 300px'>
							<a 	data-toggle="modal"
                        		data-target="#modalCestoque"
								data-original-title="Editar"  
								data-ajajax-tipo="editar" 
								title="Editar"
								class="btn btn-sm btn-success m-r-5"
								id="btVendaVestoqueComponenteLista<?=$codproduto;?>";
								onclick="modalvendaComponente(<?=$codproduto?>,<?=$coddoacao;?>)" > 
								<i class="fa fa-money"></i> Venda
							</a>
							<button id="btnGerarSucata" 
									class="btn btn-sm btn-danger m-r-5" 
									onclick="sucataComponente(<?=$codproduto;?>,<?=$codtipo;?>,<?=$coddoacao;?>)">
								<i class="fa fa-trash-o"></i> Sucata
							</button>
							<a data-toggle='modal'
                        		data-target='#modalCestoque'
								data-original-title='Editar'  
								data-ajajax-tipo='editar' 
								title='Log'
								class='btn btn-sm btn-default m-r-5'
								onclick='verlogComponente(<?=$codproduto;?>)' > 
								<i class='fa fa-history'></i> Histórico
							</a>
						</td>
					</tr> 
					<?php
					}
					echo "<tr class='odd gradeX' id='trVestoqueComponenteLista".$value['CODPRODUTO']."'>
							<td>".$value['CODPRODUTO']."</td>
							<td>".$value['ETIQUETA']."</td>
							<td>".$value['FORNECEDOR']."</td>";
				}
				?>
					<td><?=$value['VALOR'];?></td>
				<?php
				$codproduto = $value['CODPRODUTO'];
				$coddoacao = $value['CODDOACAO'];
				$codstatus = $value['CODTRSTATUS']; 
				$codtipo = $value['CODTIPO'];
			endforeach;
			?>
				<td style='width: 300px'>
					<a 	data-toggle="modal"
                		data-target="#modalCestoque"
						data-original-title="Editar"  
						data-ajajax-tipo="editar" 
						title="Editar"
						class="btn btn-sm btn-success m-r-5"
						id="btVendaVestoqueComponenteLista<?=$codproduto;?>";
						onclick="modalvendaComponente(<?=$codproduto?>,<?=$coddoacao;?>)" > 
						<i class="fa fa-money"></i> Venda
					</a>
					<button id="btnGerarSucata" 
							class="btn btn-sm btn-danger m-r-5" 
							onclick="sucataComponente(<?=$codproduto;?>,<?=$codtipo;?>,<?=$coddoacao;?>)">
						<i class="fa fa-trash-o"></i> Sucata
					</button>
					<a 	data-toggle='modal'
                		data-target='#modalCestoque'
						data-original-title='Editar'  
						data-ajajax-tipo='editar' 
						title='Log'
						class='btn btn-sm btn-default m-r-5'
						onclick='verlogComponente(<?=$codproduto;?>)' > 
						<i class='fa fa-history'></i> Histórico
					</a>
				</td>
			</tr>
		<?php
		}else{
			?>
			<tr>
				<td colspan="5" > Sem Registro </td>
			</tr>
		<?php
			};
	?>
</tbody>
</table>