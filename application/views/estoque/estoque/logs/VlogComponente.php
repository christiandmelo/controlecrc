<?php
	include APPPATH . "/views/funcoes/funcoes.php";
?>


<div class="modal-body" >
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title">Histórico do Componente</h4>
		</div>
		<div class="panel-body" >
           <table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Etiqueta</th>
						<th>Tipo Componente</th>
						<th>Status</th>
						<th>Usuário Inclusão</th>
						<th>Data Inclusão</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					
						if( count( $logsComponente ) > 0 ){
							foreach( $logsComponente as $key => $value ): 
								?>
								<tr>
									<td><?=$value['ETIQUETA'];?></td>
									<td><?=$value['TIPO'];?></td>
									<td><?=$value['STATUS'];?></td>
									<td><?=$value['USUARIO'];?></td>
									<td><?=fconverteData($value['REGCRIADOEM']);?></td>
								</tr>
								<?php
							endforeach;
						}else{
							?>
							<tr>
								<td colspan="5" > Sem Registro </td>
							</tr>
						<?php
							};
					?>
				</tbody>
			</table>
        </div>
	</div>  
</div>
<div class="modal-footer">
    <button id="btnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Fechar</button>
</div>


