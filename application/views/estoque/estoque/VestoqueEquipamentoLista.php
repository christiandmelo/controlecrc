<table class="table table-striped table-bordered">
<thead>
	<tr>
		<th>Código</th>
		<th>Etiqueta</th>
		<th>Fornecedor</th>
		<th>Tipo Doação</th>
		<th style="width: 200px">Ações</th>
	</tr>
</thead>
<tbody>
	<?php 
	
		if( count( $registros ) > 0 ){
			foreach( $registros as $key => $value ):
					echo "<tr class='odd gradeX'>
							<td>".$value['CODPRODUTO']."</td>
							<td>".$value['ETIQUETA']."</td>
							<td>".$value['FORNECEDOR']."</td>
							<td>".$value['TIPO']."</td>
							<td><a 	data-toggle='modal'
                            		data-target='#modalCestoque'
									data-original-title='Editar'  
									data-ajajax-tipo='editar' 
									title='Log'
									class='btn btn-sm btn-default m-r-5'
									onclick='verlogEquipamento(".$value['CODPRODUTO'].")' > 
									<i class='fa fa-history'></i> Histórico
								</a>
								<a 	title='Locação'
									class='btn btn-sm btn-success m-r-5'
									id='bntVerProdutoParaAlocar".$value['CODPRODUTO']."'
									onclick='modalLocacaoEquipamento(".$value['CODPRODUTO'].",".$value['CODTRSTATUS'].")' > 
									Alocação
								</a>
							</td>
						 <tr>";
			endforeach;
		}else{
			?>
			<tr>
				<td colspan="5" > Sem Registro </td>
			</tr>
		<?php
			};
	?>
</tbody>
</table>