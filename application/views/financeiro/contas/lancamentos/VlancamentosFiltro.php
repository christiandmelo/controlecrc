<?php // Mostra o Breadcrumb caso ele seja enviado
if (isset($breadcrumb))
	echo $breadcrumb;

//Variavel para controlar os campos que possui mascara
$mask = "";
?>

<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Filtro</h4>
            </div>
            <div class="panel-body">
                <!--Inicio formulario do filtro-->
                <form method="post" data-ajajax="#corpoLancamentos" action="financeiro/contas/lancamentos/Clancamentos" class="form-horizontal" role="form">
                	<input type='text' class='form-control' name='tipoListagem' id='tipoListagem' value="2" style="display: none" disabled="disabled" >

                <div class="form-group">
                	<label class="col-md-1 control-label">
						Ano:
					</label>
                    <div class="col-md-2">
                        <input type='text' class='form-control' name='ano' id='ano' value="<?=date("Y");?>" >
                    </div>
                    <label class="col-md-1 control-label">
						Mês:
					</label>
                    <div class="col-md-5">
						<select id="mes" name="mes" class="default-select2 form-control">
							<option value="01" <?=(date("m") == "01")? "selected=selected" : "" ;?> >Janeiro</option>
							<option value="02" <?=(date("m") == "02")? "selected=selected" : "" ;?> >Fevereiro</option>
							<option value="03" <?=(date("m") == "03")? "selected=selected" : "" ;?> >Março</option>
							<option value="04" <?=(date("m") == "04")? "selected=selected" : "" ;?> >Abril</option>
							<option value="05" <?=(date("m") == "05")? "selected=selected" : "" ;?> >Maio</option>
							<option value="06" <?=(date("m") == "06")? "selected=selected" : "" ;?> >Junho</option>
							<option value="07" <?=(date("m") == "07")? "selected=selected" : "" ;?> >Julho</option>
							<option value="08" <?=(date("m") == "08")? "selected=selected" : "" ;?> >Agosto</option>
							<option value="09" <?=(date("m") == "09")? "selected=selected" : "" ;?> >Setembro</option>
							<option value="10" <?=(date("m") == "10")? "selected=selected" : "" ;?> >Outubro</option>
							<option value="11" <?=(date("m") == "11")? "selected=selected" : "" ;?> >Novembro</option>
							<option value="12" <?=(date("m") == "12")? "selected=selected" : "" ;?> >Dezembro</option>
						</select>
					</div>
					<!--Botão que executa o filtro e busca os dados-->
	                <button id="btnFiltrar" type="submit" class="btn btn-sm btn-inverse">
	                    <!-- <span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
	                    <i class="glyphicon glyphicon-search"></i> Filtrar
	                </button>
                </div>
                

                
                </form>
                <!--Fim formulario do filtro-->
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 ui-sortable">
        <div data-sortable-id="ui-general-5" class="panel panel-inverse" style="margin-bottom: 0px;">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <div class="content-btn-icon">
	                        <a id="btnNovo"
	                           data-toggle="modal"
	                           class="btn btn-sm btn-success"
	                           data-target="#modalClancamentos"
	                           data-ajajax="#corpoModalClancamentos"
	                           data-href="financeiro/contas/lancamentos/Clancamentos/novoLancamento"
	                           data-original-title="Novo"
	                           title="Novo">
	                            <i class="fa fa-file-o"></i> Novo
	                        </a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <!--Inicio do conteúdo do filtro-->
                <h4 class="panel-title"> Lançamentos</h4>
            </div>
            <!-- Div que carrega o filtro-->
            <div id="corpoLancamentos" class="panel-body">
                <?php
                include APPPATH . "/views/financeiro/contas/lancamentos/VlancamentosLista.php";
                ?>
            </div>
        </div>
        <!-- end panel -->
        <script type="text/javascript">
            jQuery('.panel-body').css('max-height', ( window.screen.availHeight - ( 54 + 43 + ( 30 * 2 ) + ( 15 * 2 ) ) ) );
            jQuery('.panel-body').css('overflow','auto');

            $(function(){

                <?= $mask; ?>
                    $('[data-parsley-validate]').parsley()

                });
        </script>
    </div>
</div>

<!-- Carrega a função js da página se existir-->

<!-- Modal -->
<div class="modal fade" id="modalClancamentos">
    <div class="modal-dialog">
        <div class="modal-content" id="corpoModalClancamentos">    	
            <div class="modal-body" >
            	<div class="panel panel-inverse">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
						</div>
						<h4 class="panel-title"></h4>
					</div>
					<div class="panel-body">
                        <p>Carregando suas informações...</p>
                    </div>
				</div>  
            </div>
            <div class="modal-footer">
                <button id="btnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Cancelar</button>
            </div>
        </div>
    </div>
</div>



<style type="text/css">
	.valor{
		color: #06d;
	}
	
	.valor-neg{
		color: #ff4b00
	}
</style>