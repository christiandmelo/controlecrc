<?php
	include APPPATH . "/views/funcoes/funcoes.php";
	
	
	/*inicia o saldo do mês anterior ao pesquisado*/
	$saldo = $saldoAnterior;
	
?>


<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Vencimento</th>
			<th>Conta ou Cartão</th>
			<th>Descrição</th>
			<th>Categoria</th>
			<th>Forma de Pagamento</th>
			<th>Valor</th>
			<th>Saldo</th>
			<th></th>
			<th></th>
			<th style="width: 20px">Ações</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="6">Saldo Anterior</td>
			<td><?=$saldo;?></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<?php 
			$totalDividas = 0;
			$totalRecebimentos = 0;
			if( count( $registros ) > 0 ){
				foreach( $registros as $key => $value ):
					switch ($value['CODTIPOLANCAMENTO']){
						case 1:
							$totalDividas += $value['VALOR'];
							$saldo -= $value['VALOR'];
							break;
						case 2:
							$totalRecebimentos += $value['VALOR'];
							$saldo += $value['VALOR'];
							break;
						
					}
					?>
					<tr>
						<td><?=fconverteData($value['VENCIMENTO']);?></td>
						<td><?=$value['CONTA'];?></td>
						<td><?=$value['DESCRICAO'];?></td>
						<td><?=$value['CATEGORIA'];?></td>
						<td><?=$value['FORMADEPAGAMENTO'];?></td>
						<td <?=($value['CODTIPOLANCAMENTO'] == 1 )? 'class="valor-neg"' : 'class="valor"';?> ><?=converteMoeda($value['VALOR']);?></td>
						<td <?=($saldo > 0 )? 'class="valor"' : 'class="valor-neg"';?> ><?=converteMoeda($saldo);?></td>
						<td>
							<?php
							if($value['CODFISTATUS'] == 2){
								?><img src="assets/img/financeiro/contas/pago.png"></img><?php
							}else if(date("Y-m-d") > $value['VENCIMENTO']){
								?><img src="assets/img/financeiro/contas/aberto.png"></img><?php
							}else{
								?><img src="assets/img/financeiro/contas/vencido.png"></img><?php
							}
							?>
						</td>
						<td>
							<?php
							if($value['CODFISTATUS'] == 2){
								?><img src="assets/img/financeiro/contas/baixado.png"></img><?php
							}else{
								?><img src="assets/img/financeiro/contas/emaberto.png"></img><?php
							}
							?>
						</td>
						<td>
							<a 	data-toggle="modal"
								data-target="#modalClancamentos"
								data-ajajax="#corpoModalClancamentos"
								data-href="financeiro/contas/lancamentos/Clancamentos/novo"
								class="btn btn-sm btn-warning"
								data-original-title="Editar"
								title="Editar"
								data-ajajax-tipo="editar"
								data-ajajax-idtabela="tableListaLançamentos"
									>
								<i class="fa fa-edit (alias)"></i> Editar
								</a>
								
								
						</td>
					 <tr>
					 <?php
				endforeach;
			}else{
				?>
				<tr>
					<td colspan="20" > Sem Registro </td>
				</tr>
			<?php
				};
		?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">Total dos Lançamentos</td>
			<td colspan="5"> 
				<span class="valor"><?=converteMoeda($totalRecebimentos);?></span> - 
				<span class="valor-neg"><?=converteMoeda($totalDividas);?></span> = 
				<?php
					$total = converteMoeda(($totalRecebimentos-$totalDividas)+$saldoAnterior);
					if($total > 0){
						?><span class="valor"><?=$total;?></span><?php
					}else{
						?><span class="valor-neg"><?=$total;?></span><?php
					}
				?>
			</td>
		</tr>
	</tfoot>
</table>