<div id="divRetornoNovoLancamento"></div>

<form class="form-horizontal" data-ajajax="#divRetornoNovoLancamento" method="POST" action="financeiro/contas/lancamentos/Clancamentos/salvaEdicao" data-parsley-validate='true'>
	<div class="modal-body" >
		<div class="panel panel-inverse">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title">Geração de venda do componente</h4>
			</div>
			<div class="panel-body" >
				<!--Codigo-->
				<div class='form-group' style="display: none;">
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtcodigoClancamentos' id='txtcodigoClancamentos' disabled="disabled" >
					</div>
				</div>
				
				<!--Status-->
				<div class='form-group' style="display: none;">
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtstatusClancamentos' id='txtstatusClancamentos' disabled="disabled" value="1" >
					</div>
				</div>
				
				<!--Categorias -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="selectbasic">
						Categoria*
					</label>
					<div class="col-md-9">
						<select id="selCategoriaClancamentos" name="selCategoriaClancamentos" data-parsley-required="true" class="default-select2 form-control">
							<option value="">Selecione...</option>
							<?php
								foreach($categorias as $keyArray => $valuesSelect){
									?>
									<option value="<?=$valuesSelect['CODCATEGORIA'];?>" >
										<?=$valuesSelect['ORDEM'];?>. <?=$valuesSelect['NOME'];?>
									</option>		
									<?php
								}
							?>
						</select>
					</div>
				</div>
				
				<!--Conta Corrente -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="selectbasic">
						Conta Bancária*
					</label>
					<div class="col-md-9">
						<select id="selContaCorrenteClancamentos" name="selContaCorrenteClancamentos" data-parsley-required="true" class="default-select2 form-control">
							<option value="">Selecione...</option>
							<?php
								foreach($contaBancaria as $keyArray => $valuesSelect){
									?>
									<option value="<?=$valuesSelect['CODCONTACORRENTE'];?>" >
										<?=$valuesSelect['NOMEREDUZIDO']?> - <?=$valuesSelect['DESCRICAO'];?>
									</option>		
									<?php
								}
							?>
						</select>
					</div>
				</div>
				
				<!--Forma de Pagamento -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="selectbasic">
						Forma de Pagamento*
					</label>
					<div class="col-md-9">
						<select id="selFormaDePagamentoClancamentos" name="selFormaDePagamentoClancamentos" data-parsley-required="true" class="default-select2 form-control">
							<option value="">Selecione...</option>
							<?php
								foreach($formapagamento as $keyArray => $valuesSelect){
									?>
									<option value="<?=$valuesSelect['CODPAGAMENTO'];?>" >
										<?=$valuesSelect['DESCRICAO'];?>
									</option>		
									<?php
								}
							?>
						</select>
					</div>
				</div>
				
				<!--Descrição-->
				<div class='form-group'>
					<label class='col-md-3 control-label'>
						Descrição*:
					</label>
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtDescricaoClancamentos' id='txtDescricaoClancamentos' data-parsley-required='true' >
					</div>
				</div>
				
				<!--Tipo de Lançamento -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="selectbasic">
						Tipo de Lançamento*
					</label>
					<div class="col-md-9">
						<select id="selTipoDeLancamentoClancamentos" name="selTipoDeLancamentoClancamentos" data-parsley-required="true" class="default-select2 form-control">
							<?php
								foreach($tipolancamento as $keyArray => $valuesSelect){
									?>
									<option value="<?=$valuesSelect['CODTIPOLANCAMENTO'];?>" >
										<?=$valuesSelect['DESCRICAO'];?>
									</option>		
									<?php
								}
							?>
						</select>
					</div>
				</div>
				
				<!--Data Vencimento-->
				<div class='form-group'>
					<label class='col-md-3 control-label'>
						Data do Vencimento*:
					</label>
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtVencimentoClancamentos' id='txtVencimentoClancamentos' data-parsley-required='true' datepicker='true' >
					</div>
				</div>
				
				<!--Valor original-->
				<div class='form-group'>
					<label class='col-md-3 control-label'>
						Valor*:
					</label>
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtValorOriginalClancamentos' id='txtValorOriginalClancamentos' data-parsley-required='true' >
					</div>
				</div>
				
				<!--Data de Pagamento-->
				<div class='form-group'>
					<label class='col-md-3 control-label'>
						Data do Pagamento:
					</label>
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtPagamentoClancamentos' id='txtPagamentoClancamentos' datepicker='true' >
					</div>
				</div>
				
				<!--Valor original-->
				<div class='form-group'>
					<label class='col-md-3 control-label'>
						Valor Pago:
					</label>
					<div class='col-md-9'>
						<input type='text' class='form-control' name='txtValorPagoClancamentos' id='txtValorPagoClancamentos' >
					</div>
				</div>
	        </div>
		</div>  
	</div>
	<div class="modal-footer">
		<button onclick="" class="btn btn-sm btn-success">Salvar</button>
	    <button id="ModalbtnCancelar" data-dismiss="modal" class="btn btn-sm btn-danger">Fechar</button>
	</div>
</form>

<script type="text/javascript">
	$(function(){		
		$('[data-parsley-validate]').parsley()
		
		$("#txtValorOriginalClancamentos").mask("#.##0,00", {reverse: true});
		$("#txtValorPagoClancamentos").mask("#.##0,00", {reverse: true});
		
		$("[datepicker=true]").datepicker({
			todayHighlight : !0,
			autoclose : !0,
			dateFormat: "dd/mm/yy"
		})
	});
</script>