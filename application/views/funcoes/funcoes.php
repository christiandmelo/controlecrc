<?php

function fconverteData($data){
		$data1=substr($data,0,10);
		$hora=substr($data,11,5);
		return implode(!strstr($data1, '/') ? "/" : "-", array_reverse(explode(!strstr($data1, '/') ? "-" : "/", $data1)))." ".$hora;
	}

function fconvertDataBrasil($data){
	return(implode("/",array_reverse(explode("-", $data))));
}

function fconvertDataAmericana($data){
	return(implode("-",array_reverse(explode("/", $data))));
}

function fconverteDataHora($data) {

$data1=substr($data,0,10);
$hora=substr($data,11,8);
return implode(!strstr($data1, '/') ? "/" : "-", array_reverse(explode(!strstr($data1, '/') ? "-" : "/", $data1)))." ".$hora;

}


/**
 * Retorna a diferença em dias entre duas datas
 * OBS: a data deve estar no formato "YYYY-mm-dd"
 */

function fdiferencaDatas ($data1, $data2){
	return (strtotime(($data2)) - strtotime(($data1)))/  ( 60 * 60 * 24 );
}

function converteMoeda($valor) {
	// SE FOR BRASILEIRO
	if (strrpos($valor, ',') != false)
		return (str_replace(',', '.', (str_replace('.', '', $valor))));
	else
		return (number_format($valor, 2, ',', '.'));
}

function soNumero($str) {

	return preg_replace("/[^0-9]/", "", $str);

}

function mostraCPF($cpf) {
	return $cpf = substr($cpf, 0, 3) . '.' . substr($cpf, 3, 3) . '.' . substr($cpf, 6, 3) . '-' . substr($cpf, 9, 2);
}

//escreveCEP
function mostraCEP($cep) {
	return $cep = substr($cep, 0, 5) . '-' . substr($cep, 5, 3);
}

function escreveTelefone($telefone) {
	return $telefone = '(' . substr($telefone, 0, 2) . ')' . substr($telefone, 2, 4) . '-' . substr($telefone, 6, 4) . '';
}
?>