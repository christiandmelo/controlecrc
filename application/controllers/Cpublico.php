<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cpublico extends CI_Controller{
	
	/**
	 * hotsite.
	 * Pagina Inicial do sistema.
	 *
	 */
	public function hotsite(){
	
		// teste
		$this->load->view('global/site/Vhotsite');
	
	}
	
	/**
	 * Pagina de Erro Default do sistema.
	 */
	public function error(){
	
		$this->load->view('global/layout/Verror_404');
	
	}
	

	/**
	 * novoAcesso.
	 *
	 * Metodo aparte utilizado para criar o primeiro acesso dos usuarios.
	 *
	 */
	public function novoAcesso(){
		
		// Valida se é para mostrar o formulario ou para pegar dados
		if( $this->input->post('submit') === NULL ){
				
			//Devolve a view com o Form para criação de novo usuario
			$this->load->view("global/administracao/cliente/VnovoAcesso");
				
		}else{
				
			// Basico para funcionar
			$this->load->model("global/Macesso","Macesso") ;
			$this->load->model("global/Mlogin","Mlogin") ;
			$isTudoOk = true;
			$strEmail = $this->input->post('txtEmail');
			$strRazaoSocial = $this->input->post('txtNRS');
			$intCNPJ = $this->input->post('intPFPJ');
			$intCNPJ = $this->input->post('intPFPJ');
			
			// Sem campos vazios
			$isTudoOk = ( $intCNPJ == '' || !isset( $intCNPJ ) ) ? false : $isTudoOk;
			$isTudoOk = ( $strRazaoSocial == '' || !isset( $strRazaoSocial ) ) ? false : $isTudoOk;
			$isTudoOk = ( $strEmail == '' || !isset( $strEmail ) ) ? false : $isTudoOk;
			if( !$isTudoOk ){
				$data['texto'] = 'Todos os campos são necessario preenchimento.';
				$data['class'] = 'gritter-danger';
				$data['action'] = '';
				$this->load->view("global/crud/Valert",$data);
				return;
			}
			
			// Cria os registros iniciais
			try {
				$this->Macesso->createPrimeiroAcesso( $strRazaoSocial, $strEmail, $intCNPJ );
			} catch (Exception $e) {
				$data['texto'] = $e->getMessage();
				$data['class'] = 'gritter-danger';
				$data['action'] = '';
				$this->load->view("global/crud/Valert",$data);
				return;
			}
				
			// dados para login
			$dadosForm = array("email" => $strEmail,
					"senha" => md5('123456'));
				
			$this->Mlogin->getAutentica($dadosForm);
				
			if( isset($_SESSION['logado']) ){
				$data['html'] = '<script type="text/javascript" > window.location.replace("/app"); </script>';
				$this->load->view("global/crud/VBlank",$data);
			}else{
				$data['texto'] = 'Usuario ou Senha Invalidos!';
				$data['class'] = 'gritter-danger';
				$data['action'] = '';
				$this->load->view("global/crud/Valert",$data);
			}
				
		}
	
	}
	
}
