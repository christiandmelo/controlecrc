<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Cvenda extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("trtipo");
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.TIPOENTRADA" => "CODTIPOENTRADA",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'],
							   					"trtipo.TIPOENTRADA" => 1)
								);
		$arrDadosTipoEntrada = $this->Mcrud->getdados($arrDataFornecedor);
		$arrDadosTipoEntradaTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratados[$row->CODTIPO."_".$row->CODTIPOENTRADA] = $row->DESCRICAO;
		}
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Estoque",
						"classe" => "Cvenda",
						"precaminho" => "estoque/estoque/venda",
						"lista_filtro" => "estoque/estoque/venda/Cvenda/lista",
						"autoinc" => "sim",
						"divcomplementar" => "divCarregarCorpoEdicaoCvenda",
						"codigo" => "trproduto.CODPRODUTO",
				),
				"chave" => array (
						"trproduto.CODCLIENTE" => "CODCLIENTE",
						"trproduto.CODEMPRESA" => "CODEMPRESA",
						"trproduto.CODFILIAL" => "CODFILIAL",
				),
				"filtro" => array (
						"Geral" => array (
								"trproduto.CODTIPO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Doação:",
										"idname" => "selTipoDoacaoCvenda",
										"required" => true,
										"values" => $arrDadosTipoEntradaTratados
								),
						) 
				),
				"funcao_js" => "assets/js/funcoes/estoque/estoque/venda/funcoesVenda.js",
		);
		
	}
	
	public function index(){
		
		$this->load->view("estoque/VestoqueFiltro",$this->data);
		
	}
	
	public function lista(){
		$arrTipo = explode("_", $_POST['filtroselTipoDoacaoCvenda']);
		
		$sqlDadosListaEstoque = "SELECT 	TR.ETIQUETA,
											TR.CODPRODUTO,
									        TT.DESCRICAO AS 'TIPODOACAO',
									        DATAEMISSAO,
											LOJA,
											IDLOJA,
											VALORORIGINAL,
									        FI.NOME AS 'STATUSLANCAMENTO'
									FROM trproduto TR
									INNER JOIN filancamento FL
										ON FL.CODCLIENTE = TR.CODCLIENTE
									    AND FL.CODEMPRESA = TR.CODEMPRESA
									    AND FL.CODFILIAL = TR.CODFILIAL
									    AND FL.CODPRODUTO = TR.CODPRODUTO
									INNER JOIN fistatus FI
										ON FI.CODFISTATUS = FL.CODFISTATUS
									INNER JOIN trtipo TT
										ON TT.CODCLIENTE = TR.CODCLIENTE
										AND TT.CODTIPO = TR.CODTIPO
									WHERE TT.COMPONENTEPRODUTO = 1
										AND TR.CODCLIENTE = ?
										AND TR.CODEMPRESA = ?
										AND TR.CODFILIAL = ?
										AND TR.CODTIPO = ?";
										
		$arrDados = array(
					$_SESSION['codcliente'],
					$_SESSION['codempresa'],
					$_SESSION['codfilial'],
					$arrTipo[0],
				);
		$dadosSql = $this->db->query($sqlDadosListaEstoque,$arrDados);
		$arrDadosListaEstoque['registros'] = $dadosSql->result_array();
		
		
		$this->load->view("estoque/estoque/venda/VvendaLista",$arrDadosListaEstoque);

		
	}
	
}

?>