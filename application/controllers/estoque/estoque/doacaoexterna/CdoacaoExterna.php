<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class CdoacaoExterna extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("trtipo");
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'])
								);
		$arrDadosTipoEntrada = $this->Mcrud->getdados($arrDataFornecedor);
		$arrDadosTipoEntradaTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratados[$row->CODTIPO] = $row->DESCRICAO;
		}
		
		$this->Mcrud->setStrTable(null);
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Doações Externas",
						"classe" => "trlocacao",
						"precaminho" => "estoque/estoque/doacaoexterna/",
						"lista_filtro" => "estoque/estoque/doacaoexterna/CdoacaoExterna/lista",
						"autoinc" => "sim",
						"divcomplementar" => "divCarregarCorpoEdicaoCdoacaoExterna",
						"codigo" => "trlocacao.CODLOCACAO",
				),
				"chave" => array (
						"trlocacao.CODCLIENTE" => "CODCLIENTE",
						"trlocacao.CODEMPRESA" => "CODEMPRESA",
						"trlocacao.CODFILIAL" => "CODFILIAL",
				),
				"filtro" => array (
						"Geral" => array (
								"trproduto.CODTIPO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Doação:",
										"idname" => "selTipoDoacaoCdoacaoExterna",
										"values" => $arrDadosTipoEntradaTratados
								),
						) 
				),
		);
		
	}
	
	public function index(){
		
		$this->load->view("estoque/VestoqueFiltro",$this->data);
		
	}
	
	public function lista(){
		$sqlDadosListaDoacaoExterna = "SELECT 	TD.REGCRIADOEM AS 'DATAENTRADA',
								TT.DESCRICAO AS 'TIPO',
						        FO.NOMEFANTASIA AS 'FORNECEDOR',
						        COUNT(TP.CODPRODUTO) AS 'QUANTIDADE'
						FROM trlocacao TS
						INNER JOIN trproduto TP
							ON TP.CODCLIENTE = TS.CODCLIENTE
							AND TP.CODEMPRESA = TS.CODEMPRESA
							AND TP.CODFILIAL = TS.CODFILIAL
						INNER JOIN trdoacao TD
							ON TP.CODCLIENTE = TS.CODCLIENTE
						    AND TP.CODEMPRESA = TS.CODEMPRESA
						    AND TP.CODFILIAL = TS.CODFILIAL
						    AND TP.CODDOACAO = TP.CODDOACAO
						INNER JOIN fifornecedor FO
							ON FO.CODCLIENTE = TS.CODCLIENTE
						    AND FO.CODFORNECEDOR = TD.CODFORNECEDOR
						INNER JOIN trtipo TT
							ON TT.CODCLIENTE = TS.CODCLIENTE
						    AND TT.CODTIPO = TD.CODTIPO
						WHERE TS.CODCLIENTE = ".$_SESSION['codcliente']."
						    AND TS.CODEMPRESA = ".$_SESSION['codempresa']."
						    AND TS.CODFILIAL = ".$_SESSION['codfilial']."
						    AND TP.CODTRSTATUS = 5";
							
		if($_POST['filtroselTipoDoacaoCdoacaoExterna'] != ""){
			$sqlDadosListaDoacaoExterna .= " AND TT.CODTIPO = ".$_POST['filtroselTipoDoacaoCdoacaoExterna'];
		}
		$sqlDadosListaDoacaoExterna .=" GROUP BY TD.REGCRIADOEM,
								TT.DESCRICAO,
						        FO.NOMEFANTASIA";
		$dadosSql = $this->db->query($sqlDadosListaDoacaoExterna);
		$arrDadosListaEstoque['registros'] = $dadosSql->result_array();
		
		
		$this->load->view("estoque/estoque/sucata/VsucataLista",$arrDadosListaEstoque);
		
	}
}

?>