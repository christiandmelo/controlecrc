<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Clocacao extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->load->model("fiscal/cadastros/Mfilial","Mfilial");
		$arrDadosBancosTratados = array ("" => "Escolha um valor!");
		foreach ($this->Mfilial->getFiliaisAtivas() as $row){
		    $arrDadosBancosTratados[$row->CODFILIAL] = $row->NOMEFANTASIA;
		}
		
		$this->Mcrud->setStrTable("trtipo");
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.TIPOENTRADA" => "CODTIPOENTRADA",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'])
								);
		$arrDadosTipoEntrada = $this->Mcrud->getdados($arrDataFornecedor);
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratados[$row->CODTIPO."_".$row->CODTIPOENTRADA] = $row->DESCRICAO;
		}
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Alocação Interna",
						"classe" => "Clocacao",
						"precaminho" => "estoque/estoque/locacao/",
						"autoinc" => "sim",
						"autolista" => false,
						"divcomplementar" => "corpoDivCestoque",
						"data-toggle" => false,
						"novo-modal" => "Cestoque",
						"novo-filtro" => "estoque/estoque/Cestoque/lista",
						"codigo" => "trlocacao.CODLOCACAO",
				),
				"botoes" => array(
								"excluir" => "inativo",
								"novo" => "inativo",
							),
				"chave" => array (
						"trlocacao.CODCLIENTE" => "CODCLIENTE",
						"trlocacao.CODEMPRESA" => "CODEMPRESA",
						"trlocacao.CODFILIAL" => "CODFILIAL",
				),
				"tabela" => array (
						"cabecalho" => array (
								"trlocacao.CODLOCACAO" => "Codigo",
								"TP.ETIQUETA" => "Etiqueta"
						),
						"join" => array (
								"trproduto AS TP" => "TP.CODCLIENTE = trlocacao.CODCLIENTE AND TP.CODEMPRESA = trlocacao.CODEMPRESA AND TP.CODFILIAL = trlocacao.CODFILIAL AND TP.CODPRODUTO = trlocacao.CODPRODUTO",
						)
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoClocacao",
						"method" => "post",
						"action" => "estoque/cadastros/tipo/Clocacao/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"TP.ETIQUETA" => array (
										"tipo" => "label",
										"filtro" => true,
										"label" => "Etiqueta:",
										"idname" => "codEtiquetaCestoque",
								),
								"trlocacao.CODFILIALLOCADO" => array(
											  "tipo" => "select",
											  "filtro" => true,
											  "label" => "Tele Centro:",
											  "idname" => "selTeleCentroClocacao",
											  "required" => true,
											  "values" => $arrDadosBancosTratados
								),
								"trproduto.CODTIPO" => array("tipo" => "select",
										"filtro" => true,
										"filtro-required" => true,
										"label" => "Tipo Doação:",
										"idname" => "selTipoDoacaoCestoque",
										"required" => true,
										"values" => $arrDadosTipoEntradaTratados
								),
								"trproduto.CODFORNECEDOR" => array("tipo" => "select",
										"filtro" => true,
										"filtro-visible" => false,
										"label" => "Fornecedor:",
										"idname" => "selFornecedorCestoque",
										"required" => true,
										"values" => array("" => "Escolha uma opção!")
								),
								"trlocacao.CODLOCACAO" => array("tipo" => "select",
										"filtro" => true,
										"filtro-visible" => false,
										"label" => "Fornecedor:",
										"idname" => "selCodLocacaoCestoque",
										"required" => true,
										"values" => array("" => "Escolha uma opção!")
								),
						) 
				),
				"funcao_js" => "assets/js/funcoes/estoque/estoque/funcoesEstoque.js",
		);
		
		$this->strTabela = "trlocacao";
	}
}

?>