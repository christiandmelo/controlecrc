<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Cestoque extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->load->model("fiscal/cadastros/Mfilial","Mfilial");
		
		
		$this->Mcrud->setStrTable("trtipo");
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.TIPOENTRADA" => "CODTIPOENTRADA",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'])
								);
		$arrDadosTipoEntrada = $this->Mcrud->getdados($arrDataFornecedor);
		$arrDadosTipoEntradaTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratados[$row->CODTIPO."_".$row->CODTIPOENTRADA] = $row->DESCRICAO;
		}
		
		$this->Mcrud->setStrTable("fifornecedor");
		$arrDataFornecedor = array("cabecalho" => array("fifornecedor.CODFORNECEDOR" => "CODFORNECEDOR",
													 "fifornecedor.NOMEFANTASIA" => "NOMEFANTASIA"),
							   "where" => array("fifornecedor.CODCLIENTE" => $_SESSION['codcliente']),
							   "order" => array("fifornecedor.NOMEFANTASIA" => "ASC")
								);
		$arrDadosFornecedor = $this->Mcrud->getdados($arrDataFornecedor);
		$arrDadosFornecedorTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosFornecedor->result() as $row){
		    $arrDadosFornecedorTratados[$row->CODFORNECEDOR] = $row->NOMEFANTASIA;
		}
		$this->Mcrud->setStrTable(null);
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Estoque",
						"classe" => "Cestoque",
						"precaminho" => "estoque/estoque/",
						"lista_filtro" => "estoque/estoque/Cestoque/lista",
						"autoinc" => "sim",
						"divcomplementar" => "divCarregarCorpoEdicaoCestoque",
						"codigo" => "trproduto.CODPRODUTO",
				),
				"chave" => array (
						"trproduto.CODCLIENTE" => "CODCLIENTE",
						"trproduto.CODEMPRESA" => "CODEMPRESA",
						"trproduto.CODFILIAL" => "CODFILIAL",
				),
				"filtro" => array (
						"Geral" => array (
								"trproduto.CODFORNECEDOR" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Fornecedor:",
										"idname" => "selFornecedorCestoque",
										"values" => $arrDadosFornecedorTratados
								),
								"trproduto.CODTIPO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Doação:",
										"idname" => "selTipoDoacaoCestoque",
										"required" => true,
										"values" => $arrDadosTipoEntradaTratados
								),
						) 
				),
				"funcao_js" => "assets/js/funcoes/estoque/estoque/funcoesEstoque.js",
		);
		
	}
	
	public function index(){
		
		$this->load->view("estoque/VestoqueFiltro",$this->data);
		
	}
	
	public function lista(){
		$arrTipo = explode("_", $_POST['filtroselTipoDoacaoCestoque']);
		
		if($arrTipo[1] == 1){	
		
			$sqlDadosListaEstoque = "SELECT TR.CODPRODUTO,
											TCP.CODCAMPOSTIPO,
											TCP.CODCAMPOSPRODUTO,
											TR.ETIQUETA,
											TR.CODPRODUTO,
											TR.CODTRSTATUS,
									        TCP.VALOR,
									        TR.CODDOACAO,
									        FO.NOMEFANTASIA AS 'FORNECEDOR',
									        TR.CODTIPO
									FROM trproduto TR
									INNER JOIN trdoacao TD
										ON TD.CODCLIENTE = TR.CODCLIENTE
										AND TD.CODEMPRESA = TR.CODEMPRESA
										AND TD.CODFILIAL = TR.CODFILIAL
										AND TD.CODDOACAO = TR.CODDOACAO
									LEFT JOIN fifornecedor	FO
										ON FO.CODCLIENTE = TR.CODCLIENTE
										AND FO.CODFORNECEDOR = TD.CODFORNECEDOR
									INNER JOIN trtipo TT
										ON TT.CODCLIENTE = TR.CODCLIENTE
									    AND TT.CODTIPO = TR.CODTIPO
									INNER JOIN trcampostipo TRCT
										ON TRCT.CODCLIENTE = TR.CODCLIENTE
									    AND TRCT.CODTIPO = TR.CODTIPO
									    AND TRCT.ATIVO = 1
									LEFT JOIN trcamposproduto TCP
										ON TCP.CODCLIENTE = TR.CODCLIENTE
									    AND TCP.CODEMPRESA = TR.CODEMPRESA
									    AND TCP.CODFILIAL = TR.CODFILIAL
									    AND TCP.CODPRODUTO = TR.CODPRODUTO
									    AND TCP.CODCAMPOSTIPO = TRCT.CODCAMPOSTIPO
									WHERE TT.COMPONENTEPRODUTO = 1
									    AND TR.CODTRSTATUS = 3
									    AND TR.CODCLIENTE = ?
									    AND TR.CODEMPRESA = ?
									    AND TR.CODFILIAL = ?
									    AND TR.CODTIPO = ?
									    AND TR.ETIQUETA IS NOT NULL";
			if($_POST['filtroselFornecedorCestoque'] != ""){
				$sqlDadosListaEstoque .= " AND FO.CODFORNECEDOR = ".$_POST['filtroselFornecedorCestoque'];
			}
			$sqlDadosListaEstoque .= " ORDER BY TR.CODPRODUTO,
										TCP.CODCAMPOSTIPO";
			$arrDados = array(
						$_SESSION['codcliente'],
						$_SESSION['codempresa'],
						$_SESSION['codfilial'],
						$arrTipo[0],
					);
			$dadosSql = $this->db->query($sqlDadosListaEstoque,$arrDados);
			$arrDadosListaEstoque['registros'] = $dadosSql->result_array();
			
			
			$sqlCabecalho = "SELECT  TCT.CODCAMPOSTIPO,
										TCT.NOME AS 'CAMPOS'
								FROM trcampostipo TCT
								INNER JOIN trtipo TT
									ON TT.CODCLIENTE = TCT.CODCLIENTE
									AND TT.CODTIPO = TCT.CODTIPO
								WHERE TT.TIPOENTRADA = 1
									AND TCT.CODCLIENTE = ?
									AND TCT.CODTIPO = ?
								    AND TCT.ATIVO = 1
								ORDER BY TCT.CODCAMPOSTIPO";
			$arrDadosCabe = array(
						$_SESSION['codcliente'],
						$arrTipo[0],
					);
			$dadosCabe = $this->db->query($sqlCabecalho,$arrDadosCabe);
			$arrDadosListaEstoque['cabecalho'] = $dadosCabe->result_array();
			
			
			$this->load->view("estoque/estoque/VestoqueComponenteLista",$arrDadosListaEstoque);
			
			
		}else{
			
			$sqlDadosListaEstoque = "SELECT 	TR.CODPRODUTO AS 'CODPRODUTO',
												TRPAI.DESCRICAO AS 'TIPO',
												TR.ETIQUETA,
												FO.NOMEFANTASIA AS 'FORNECEDOR',
												TR.CODDOACAO,
												TR.CODTRSTATUS
										FROM trproduto TR
										INNER JOIN trtipo TRPAI
											ON TRPAI.CODCLIENTE = TR.CODCLIENTE
											AND TRPAI.CODTIPO = TR.CODTIPO
										INNER JOIN trdoacao TD
											ON TD.CODCLIENTE = TR.CODCLIENTE
											AND TD.CODEMPRESA = TR.CODEMPRESA
											AND TD.CODFILIAL = TR.CODFILIAL
											AND TD.CODDOACAO = TR.CODDOACAO
										LEFT JOIN fifornecedor	FO
											ON FO.CODCLIENTE = TR.CODCLIENTE
											AND FO.CODFORNECEDOR = TD.CODFORNECEDOR
										LEFT JOIN trlocacao TL
											ON TL.CODCLIENTE = TR.CODCLIENTE
											AND TL.CODEMPRESA = TR.CODEMPRESA
											AND TL.CODFILIAL = TR.CODFILIAL
											AND TL.CODPRODUTO = TR.CODPRODUTO
										WHERE TR.CODCLIENTE = ?
											AND TR.CODEMPRESA = ?
											AND TR.CODFILIAL = ?
										    AND TR.CODTRSTATUS IN (3,6)
									    	AND TR.CODTIPO = ?";
			if($_POST['filtroselFornecedorCestoque'] != ""){
				$sqlDadosListaEstoque .= " AND FO.CODFORNECEDOR = ".$_POST['filtroselFornecedorCestoque'];
			}
			if(isset($_POST['filtroselCodLocacaoCestoque'])){
				$sqlDadosListaEstoque .= " AND TL.CODLOCACAO IS NOT NULL";
			}
			if(isset($_POST['filtrocodEtiquetaCestoque']) && $_POST['filtrocodEtiquetaCestoque'] != ""){
				$sqlDadosListaEstoque .= " AND TR.ETIQUETA = ".$_POST['filtrocodEtiquetaCestoque'];
			}
			$arrDados = array(
						$_SESSION['codcliente'],
						$_SESSION['codempresa'],
						$_SESSION['codfilial'],
						$arrTipo[0],
					);
			$dadosSql = $this->db->query($sqlDadosListaEstoque,$arrDados);
			$arrDadosListaEstoque['registros'] = $dadosSql->result_array();
			
			
			$this->load->view("estoque/estoque/VestoqueEquipamentoLista",$arrDadosListaEstoque);
		}
		
	}
	
	

	public function modalLocacao(){
		$arrDadosLocacao['filiais'] = $this->Mfilial->getFiliaisAtivas();
				
		$sqlComponenteslocacao = "SELECT TRTFILHO.CODTIPO AS 'CODTIPOFILHO',
												TRTFILHO.DESCRICAO AS 'NOMETIPOFILHO',
												TRF.CODPRODUTO AS 'CODPRODUTOFILHO',
												TRP.CODCOMPONENTESDALOCACAO AS 'CODCOMPONENTESDALOCACAO',
												TRF.ETIQUETA,
												TR.CODDOACAO,
												TCL.CODCOMPONENTESLOCACAO,
												TR.CODPRODUTO,
												TR.CODTRSTATUS,
												TL.CODFILIALLOCADO,
												TL.INTERNA
										FROM trcomponenteslocacao TCL
										INNER JOIN trtipo TRTFILHO
											ON TRTFILHO.CODCLIENTE = TCL.CODCLIENTE
										    AND TRTFILHO.CODTIPO = TCL.CODTIPOFILHO
										INNER JOIN trproduto TR
											ON TR.CODCLIENTE = TCL.CODCLIENTE
										    AND TR.CODTIPO = TCL.CODTIPOPAI
										LEFT JOIN trlocacao TL
											ON TL.CODCLIENTE = TR.CODCLIENTE
											AND TL.CODEMPRESA = TR.CODEMPRESA
											AND TL.CODFILIAL = TR.CODFILIAL
											AND TL.CODPRODUTO = TR.CODPRODUTO
										LEFT JOIN trcomponentesdalocacao TRP
											ON TRP.CODCLIENTE = TR.CODCLIENTE
											AND TRP.CODEMPRESA = TR.CODEMPRESA
											AND TRP.CODFILIAL = TR.CODFILIAL
											AND TRP.CODPRODUTOPAI = TR.CODPRODUTO
											AND TRP.CODCOMPONENTESLOCACAO = TCL.CODCOMPONENTESLOCACAO
										LEFT JOIN trproduto TRF
											ON TRF.CODCLIENTE = TR.CODCLIENTE
											AND TRF.CODEMPRESA = TR.CODEMPRESA
											AND TRF.CODFILIAL = TR.CODFILIAL
											AND TRF.CODPRODUTO = TRP.CODPRODUTOFILHO
											AND TRF.CODTIPO = TCL.CODTIPOFILHO
										WHERE TCL.ATIVO = 1
											AND TR.CODCLIENTE = ?
											AND TR.CODEMPRESA = ?
											AND TR.CODFILIAL = ?
											AND TR.CODPRODUTO = ?";
			$arrDados = array(
				$_SESSION['codcliente'],
				$_SESSION['codempresa'],
				$_SESSION['codfilial'],
				$_POST['codproduto']
			);
			$dadosSql = $this->db->query($sqlComponenteslocacao,$arrDados);
			$arrDadosLocacao['compLocacao'] = $dadosSql->result();
		
		$this->load->view('estoque/estoque/VestoqueLocacao',$arrDadosLocacao);
	}
	
	
	public function modalComponenteExternoEquipamento(){
		$sqlDadosComponentes = "SELECT 	TD.CODPRODUTO,
										TCP.CODCAMPOSTIPO,
										TCP.CODCAMPOSPRODUTO,
										TD.ETIQUETA,
								        TCP.VALOR,
								        TD.CODDOACAO
								FROM trproduto TD
								INNER JOIN trtipo TT
									ON TT.CODCLIENTE = TD.CODCLIENTE
								    AND TT.CODTIPO = TD.CODTIPO
								LEFT JOIN trcampostipo TRCT
									ON TRCT.CODCLIENTE = TD.CODCLIENTE
								    AND TRCT.CODTIPO = TD.CODTIPO
								    AND TRCT.ATIVO = 1
								LEFT JOIN trcamposproduto TCP
									ON TCP.CODCLIENTE = TD.CODCLIENTE
								    AND TCP.CODEMPRESA = TD.CODEMPRESA
								    AND TCP.CODFILIAL = TD.CODFILIAL
								    AND TCP.CODPRODUTO = TD.CODPRODUTO
								    AND TCP.CODCAMPOSTIPO = TRCT.CODCAMPOSTIPO
								WHERE TT.COMPONENTELOCACAO = 1
									AND TD.CODTRSTATUS = 3
								    AND TD.CODCLIENTE = ?
								    AND TD.CODEMPRESA = ?
								    AND TD.CODFILIAL = ?
								    AND TD.CODTIPO = ?
								    AND TD.ETIQUETA IS NOT NULL
								ORDER BY TD.CODPRODUTO,
									TCP.CODCAMPOSTIPO";
		$arrDados = array(
					$_SESSION['codcliente'],
					$_SESSION['codempresa'],
					$_SESSION['codfilial'],
					$_POST['codtipo'],
				);
		$dadosSql = $this->db->query($sqlDadosComponentes,$arrDados);
		$arrDadosTroca['registros'] = $dadosSql->result_array();
		
		
		$sqlCabecalho = "SELECT  TCT.CODCAMPOSTIPO,
									TCT.NOME AS 'CAMPOS'
							FROM trcampostipo TCT
							INNER JOIN trtipo TT
								ON TT.CODCLIENTE = TCT.CODCLIENTE
								AND TT.CODTIPO = TCT.CODTIPO
							WHERE TT.COMPONENTELOCACAO = 1
								AND TCT.CODCLIENTE = ?
								AND TCT.CODTIPO = ?
							    AND TCT.ATIVO = 1
							ORDER BY TCT.CODCAMPOSTIPO";
		$arrDadosCabe = array(
					$_SESSION['codcliente'],
					$_POST['codtipo'],
				);
		$dadosCabe = $this->db->query($sqlCabecalho,$arrDadosCabe);
		$arrDadosTroca['cabecalho'] = $dadosCabe->result_array();
		
		
		$this->load->view('estoque/estoque/VestoqueLocacaoComponente',$arrDadosTroca);
	}
	
	
	
	
	public function salvarComponenteExternoEquipamento(){
		$this->load->model("global/Mcrud","Mcrud");
		$validacao = false;
		$retornoLocacao = 0;
		
		$chavecom["CODCLIENTE"] = "CODCLIENTE";
		$chavecom["CODEMPRESA"] = "CODEMPRESA";
		$chavecom["CODFILIAL"] = "CODFILIAL";
		$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
		$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
		$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
		
		if($_POST['codantigoprodutofilho'] != "undefined"){
			$dadosEdicaoProdutoAntigo['CODTRSTATUS'] = 3;
			$dadosWhereUpProdutoAntigo['CODPRODUTO'] = $_POST['codantigoprodutofilho'];
			$this->Mcrud->setStrTable('trproduto');
			$retornoUp = $this->Mcrud->setDados($dadosEdicaoProdutoAntigo,$chavecom,$arrValoresDaChave,$dadosWhereUpProdutoAntigo,'CODPRODUTO');
			
			$dadosEdicaoLogCompAntigo = $arrValoresDaChave;
			$dadosEdicaoLogCompAntigo['CODTRSTATUS'] = 3;
			$dadosEdicaoLogCompAntigo['CODPRODUTO'] = $_POST['codantigoprodutofilho'];
			$this->Mcrud->setStrTable('trlogproduto');
			$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoLogCompAntigo,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
			
			$dadosEdicaoLogComp['CODPRODUTOFILHO'] = $_POST['codprodutofilho'];
			$dadosWhereUpLogComp['CODCOMPONENTESDALOCACAO'] = $_POST['codcomponentesdalocacao'];
			$this->Mcrud->setStrTable('trcomponentesdalocacao');
			$retornoLocacao = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,$dadosWhereUpLogComp,'CODCOMPONENTESDALOCACAO');
		}else{

			$dadosEdicaoLogComp = $arrValoresDaChave;
			$dadosEdicaoLogComp['CODCOMPONENTESLOCACAO'] = $_POST['codcomponenteslocacao'];
			$dadosEdicaoLogComp['CODPRODUTOPAI'] = $_POST['codprodutopai'];
			$dadosEdicaoLogComp['CODPRODUTOFILHO'] = $_POST['codprodutofilho'];
			$this->Mcrud->setStrTable('trcomponentesdalocacao');
			$retornoLocacao = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,null,'CODCOMPONENTESDALOCACAO');
			
		}
		
		if($retornoLocacao != 0){
			$dadosEdicaoProduto['CODTRSTATUS'] = 15;
			$dadosWhereUpProduto['CODPRODUTO'] = $_POST['codprodutofilho'];
			$this->Mcrud->setStrTable('trproduto');
			$retornoUp = $this->Mcrud->setDados($dadosEdicaoProduto,$chavecom,$arrValoresDaChave,$dadosWhereUpProduto,'CODPRODUTO');
			
			if($retornoUp != 0){
				$dadosEdicaoLogComp = $arrValoresDaChave;
				$dadosEdicaoLogComp['CODTRSTATUS'] = 15;
				$dadosEdicaoLogComp['CODPRODUTO'] = $_POST['codprodutofilho'];
				$this->Mcrud->setStrTable('trlogproduto');
				$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
				if($retornoLogCompProduto != 0){
					$validacao = true;
				}
			}
		}
		
		if($validacao){
			?>
			<script type="text/javascript" >
				jQuery('#bntVerProdutoParaAlocar<?=$_POST['codprodutopai'];?>').click();
				jQuery(function(){
					$.gritter.add({
						title : "Alerta!",
						text : "Componente vinculado com sucesso!",
						class_name: "gritter-success",
					});
				})
			</script>
			<?php
		}else{	
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Erro vincular, gentileza entrar em contato com o administrador da rede!",
							class_name: "gritter-error",
						});
					})
			</script>
			<?php
		}
	}
	
	
	
	
	public function salvarLocacaoEquipamento(){
		$this->load->model("global/Mcrud","Mcrud");
		$validacao = false;
		$codtrstatus = 5;
		
		$chavecom["CODCLIENTE"] = "CODCLIENTE";
		$chavecom["CODEMPRESA"] = "CODEMPRESA";
		$chavecom["CODFILIAL"] = "CODFILIAL";
		$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
		$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
		$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
		
		$dadosEdicaoComp = $arrValoresDaChave;
		$dadosEdicaoComp['INTERNA'] = $_POST['txtTipoDoacaoCtriagemComponenteLocacao'];
		$dadosEdicaoComp['CODPRODUTO'] = $_POST['txtCodProdutoEstoqueLocacao'];
		if($_POST['txtTipoDoacaoCtriagemComponenteLocacao'] == 1){
			$dadosEdicaoComp['CODFILIALLOCADO'] = $_POST['txtTeleCentroCtriagemComponenteLocacao'];
			$codtrstatus = 6;
		}else{
			$dadosEdicaoComp['CODFORNECEDOR'] = $_POST['txtClienteCtriagemComponenteLocacao'];
		}
		$dadosEdicaoComp['CODCLIENTELOCADO'] = $_SESSION['codcliente'];
		$dadosEdicaoComp['CODEMPRESALOCADO'] = $_SESSION['codempresa'];
		$this->Mcrud->setStrTable('trlocacao');
		$retornoLocacao = $this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,null,'CODLOCACAO');
		
		if($retornoLocacao != 0){
			$dadosEdicaoProduto['CODTRSTATUS'] = $codtrstatus;
			$dadosWhereUpProduto['CODPRODUTO'] = $_POST['txtCodProdutoEstoqueLocacao'];
			$this->Mcrud->setStrTable('trproduto');
			$retornoUp = $this->Mcrud->setDados($dadosEdicaoProduto,$chavecom,$arrValoresDaChave,$dadosWhereUpProduto,'CODPRODUTO');
			
			if($retornoUp != 0){
				$dadosEdicaoLogComp = $arrValoresDaChave;
				$dadosEdicaoLogComp['CODTRSTATUS'] = $codtrstatus;
				$dadosEdicaoLogComp['CODPRODUTO'] = $_POST['txtCodProdutoEstoqueLocacao'];
				$this->Mcrud->setStrTable('trlogproduto');
				$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
				if($retornoLogCompProduto != 0){
					$validacao = true;
				}
			}
		}
		
		if($validacao){
			?>
			<script type="text/javascript" >
				jQuery('#bntVerProdutoParaAlocar<?=$_POST['txtCodProdutoEstoqueLocacao'];?>').click();
				jQuery(function(){
					$.gritter.add({
						title : "Alerta!",
						text : "Equipamento doado com sucesso!",
						class_name: "gritter-success",
					});
				})
			</script>
			<?php
		}else{	
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Erro doar, gentileza entrar em contato com o administrador da rede!",
							class_name: "gritter-error",
						});
					})
			</script>
			<?php
		}
	}
}

?>