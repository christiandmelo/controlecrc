<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Csucata extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("trtipo");
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'])
								);
		$arrDadosTipoEntrada = $this->Mcrud->getdados($arrDataFornecedor);
		$arrDadosTipoEntradaTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratados[$row->CODTIPO] = $row->DESCRICAO;
		}
		
		$this->Mcrud->setStrTable("fifornecedor");
		$arrDataFornecedor = array("cabecalho" => array("fifornecedor.CODFORNECEDOR" => "CODFORNECEDOR",
													 "fifornecedor.NOMEFANTASIA" => "NOMEFANTASIA"),
							   "where" => array("fifornecedor.CODCLIENTE" => $_SESSION['codcliente'],
							   					"fifornecedor.CLIENTE" => 0),
							   "order" => array("fifornecedor.NOMEFANTASIA" => "ASC")
								);
		$arrDadosFornecedor = $this->Mcrud->getdados($arrDataFornecedor);
		$arrDadosFornecedorTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosFornecedor->result() as $row){
		    $arrDadosFornecedorTratados[$row->CODFORNECEDOR] = $row->NOMEFANTASIA;
		}
		$this->Mcrud->setStrTable(null);
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Sucatas",
						"classe" => "Csucata",
						"precaminho" => "estoque/estoque/",
						"lista_filtro" => "estoque/estoque/sucata/Csucata/lista",
						"autoinc" => "sim",
						"divcomplementar" => "divCarregarCorpoEdicaoCsucata",
						"codigo" => "trproduto.CODPRODUTO",
				),
				"chave" => array (
						"trproduto.CODCLIENTE" => "CODCLIENTE",
						"trproduto.CODEMPRESA" => "CODEMPRESA",
						"trproduto.CODFILIAL" => "CODFILIAL",
				),
				"filtro" => array (
						"Geral" => array (
								"trproduto.CODFORNECEDOR" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Fornecedor:",
										"idname" => "selFornecedorCsucata",
										"values" => $arrDadosFornecedorTratados
								),
								"trproduto.CODTIPO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Doação:",
										"idname" => "selTipoDoacaoCsucata",
										"values" => $arrDadosTipoEntradaTratados
								),
						) 
				),
				"funcao_js" => "assets/js/funcoes/estoque/estoque/funcoesEstoque.js",
		);
		
	}
	
	public function index(){
		
		$this->load->view("estoque/VestoqueFiltro",$this->data);
		
	}
	
	public function lista(){
		$sqlDadosListaSucata = "SELECT 	TD.REGCRIADOEM AS 'DATAENTRADA',
								TT.DESCRICAO AS 'TIPO',
						        FO.NOMEFANTASIA AS 'FORNECEDOR',
						        COUNT(TS.CODSUCATA) AS 'QUANTIDADE'
						FROM trsucata TS
						INNER JOIN trdoacao TD
							ON TD.CODCLIENTE = TS.CODCLIENTE
						    AND TD.CODEMPRESA = TS.CODEMPRESA
						    AND TD.CODFILIAL = TS.CODFILIAL
						    AND TD.CODDOACAO = TS.CODDOACAO
						INNER JOIN fifornecedor FO
							ON FO.CODCLIENTE = TS.CODCLIENTE
						    AND FO.CODFORNECEDOR = TD.CODFORNECEDOR
						INNER JOIN trtipo TT
							ON TT.CODCLIENTE = TS.CODCLIENTE
						    AND TT.CODTIPO = TD.CODTIPO
						WHERE TS.CODCLIENTE = 1
						    AND TS.CODEMPRESA = 1
						    AND TS.CODFILIAL = 1
						    AND TS.CODTRSTATUS = 4";
		if($_POST['filtroselFornecedorCsucata'] != ""){
			$sqlDadosListaSucata .= " AND FO.CODFORNECEDOR = ".$_POST['filtroselFornecedorCsucata'];
		}
		if($_POST['filtroselTipoDoacaoCsucata'] != ""){
			$sqlDadosListaSucata .= " AND TT.CODTIPO = ".$_POST['filtroselTipoDoacaoCsucata'];
		}
		$sqlDadosListaSucata .=" GROUP BY TD.REGCRIADOEM,
								TT.DESCRICAO,
						        FO.NOMEFANTASIA";
		$arrDados = array(
					$_SESSION['codcliente'],
					$_SESSION['codempresa'],
					$_SESSION['codfilial'],
					$_POST['filtroselTipoDoacaoCsucata'],
				);
		$dadosSql = $this->db->query($sqlDadosListaSucata,$arrDados);
		$arrDadosListaEstoque['registros'] = $dadosSql->result_array();
		
		
		$this->load->view("estoque/estoque/sucata/VsucataLista",$arrDadosListaEstoque);
		
	}
}

?>