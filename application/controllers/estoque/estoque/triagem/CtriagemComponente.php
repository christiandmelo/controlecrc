<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class CtriagemComponente extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		$this->data = array (
				"lista" => array (
						"Titulo" => "Cadastro/Edição de Componente",
						"classe" => "CtriagemComponente",
						"precaminho" => "estoque/estoque/triagem/",
						"autoinc" => "sim",
						"codigo" => "trproduto.CODPRODUTO",
				),
				"chave" => array (
						"trproduto.CODCLIENTE" => "CODCLIENTE",
						"trproduto.CODEMPRESA" => "CODEMPRESA",
						"trproduto.CODFILIAL" => "CODFILIAL",
						),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCtriagemComponente",
						"method" => "post",
						"action" => "estoque/estoque/triagem/CtriagemComponente/salvaNovoComponente",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"trproduto.CODPRODUTO" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "codprodutoCtriagemComponente",
										"disabled" => true,
								),
								"trproduto.CODDOACAO" => array (
										"tipo" => "label",
										"label" => "coddoacao:",
										"idname" => "coddoacaoCtriagemComponente",
										"visible" => false,
										"disabled" => true,
										"required" => true,
										"required-type" => "number",
								),
								"trproduto.CODTIPO" => array (
										"tipo" => "label",
										"label" => "codtipo:",
										"idname" => "codtipoCtriagemComponente",
										"visible" => false,
										"disabled" => true,
										"required" => true,
										"required-type" => "number",
								),
								"trproduto.CODTRSTATUS" => array (
										"tipo" => "label",
										"label" => "codtrstatus:",
										"idname" => "codtrstatusCtriagemComponente",
										"value" => 3,
										"visible" => false,
										"disabled" => true,
										"required" => true,
										"required-type" => "number",
								),
								"trdoacao.QUANTIDADETRIADA" => array (
										"tipo" => "label",
										"label" => "qtdtriada:",
										"idname" => "QuantidadeTriadaCtriagemComponente",
										"visible" => false,
										"disabled" => true,
										"ativo_salvar" => false,
										"required" => true,
										"required-type" => "number",
								),
								"trdoacao.CODTIPOENTRADA" => array (
										"tipo" => "label",
										"label" => "codtipo:",
										"idname" => "CodTipoEntradaCtriagemComponente",
										"visible" => false,
										"disabled" => true,
										"ativo_salvar" => false,
										"value" => 1,
										"required" => true,
										"required-type" => "number",
								),
								"trdoacao.QUANTIDADE" => array (
										"tipo" => "label",
										"label" => "qtd:",
										"idname" => "QuantidadeCtriagemComponente",
										"visible" => false,
										"disabled" => true,
										"ativo_salvar" => false,
										"required" => true,
										"required-type" => "number",
								),
								"trproduto.ETIQUETA" => array("tipo" => "label",
										"label"=> "Etiqueta:",
										"idname" => "EtiquetaCtriagemComponente",
										"required" => true,
										"required-type" => "number",
										),
						) 
				) 
		);
		
		$this->strTabela = "trproduto";
		
    }
	
	/**
	 * Página inicial
	 */
	public function index($coddoacao = null){
		
		$dados = array();
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("trdoacao AS TRD");
		$arrDataCabecalho = array("cabecalho" => array("TRD.CODLOTE" => "CODLOTE",
													   "TRD.CODDOACAO" => "CODDOACAO",
													   "TRD.CODTIPO" => "CODTIPO",
													   "TRD.ETIQUETA" => "ETIQUETA",
													   "TRD.QUANTIDADE" => "QUANTIDADE",
													   "TRD.QUANTIDADETRIADA" => "QUANTIDADETRIADA",
													   "TRT.DESCRICAO" => "TIPO",
													   "FO.NOMEFANTASIA" => "FORNECEDOR",
													   "TRS.DESCRICAO" => "STATUS",
													   "TRD.CODTIPOENTRADA" => "CODTIPOENTRADA",
													   ),
							   "join" => array(
							   			"fifornecedor AS FO" => "FO.CODCLIENTE = TRD.CODCLIENTE AND FO.CODFORNECEDOR = TRD.CODFORNECEDOR",
							   			"trtipo AS TRT" => "TRT.CODCLIENTE = TRD.CODCLIENTE AND TRT.CODTIPO = TRD.CODTIPO",
							   			"trstatus as TRS" => "TRS.CODTRSTATUS = TRD.CODTRSTATUS",
										),
							   "where" => array(
							   				"TRD.CODCLIENTE" => $_SESSION['codcliente'],
							   				"TRD.CODEMPRESA" => $_SESSION['codempresa'],
							   				"TRD.CODFILIAL" => $_SESSION['codfilial'],
							   				"TRD.CODDOACAO" => $coddoacao
											)
								);
		$arrDadosCabecalho = $this->Mcrud->getDados($arrDataCabecalho);
		$dados['cabecalho'] = $arrDadosCabecalho->row();
		
		if($dados['cabecalho']->CODTIPOENTRADA == 1){
				
			$this->Mcrud->setStrTable("trproduto AS TRP");
			$arrDataProduto = array("cabecalho" => array("TRP.ETIQUETA" => "ETIQUETA",
														 "TRP.REGCRIADOEM" => "REGCRIADOEM",
														 "TRS.DESCRICAO" => "STATUS",
														 "TRS.CODTRSTATUS" => "CODTRSTATUS",
														 "TRP.CODPRODUTO" => "CODPRODUTO",
									),
									"join" => array("trstatus AS TRS" => "TRS.CODTRSTATUS = TRP.CODTRSTATUS",
									),
									"where" => array("TRP.CODCLIENTE" => $_SESSION['codcliente'],
							   				"TRP.CODEMPRESA" => $_SESSION['codempresa'],
							   				"TRP.CODFILIAL" => $_SESSION['codfilial'],
							   				"TRP.CODDOACAO" => $coddoacao
											)
								);
			$arrDadosProduto = $this->Mcrud->getDados($arrDataProduto);
			$dados['produto'] = $arrDadosProduto->result();
			
			$this->Mcrud->setStrTable("trsucata AS TRP");
			$arrDataSucata = array("cabecalho" => array("TRP.REGCRIADOEM" => "REGCRIADOEM",
														 "TRS.DESCRICAO" => "STATUS",
									),
									"join" => array("trstatus AS TRS" => "TRS.CODTRSTATUS = TRP.CODTRSTATUS",
									),
									"where" => array("TRP.CODCLIENTE" => $_SESSION['codcliente'],
							   				"TRP.CODEMPRESA" => $_SESSION['codempresa'],
							   				"TRP.CODFILIAL" => $_SESSION['codfilial'],
							   				"TRP.CODDOACAO" => $coddoacao
											)
								);
			$arrDadosSucata = $this->Mcrud->getDados($arrDataSucata);
			$dados['sucata'] = $arrDadosSucata->result();
			
			$this->load->view("estoque/estoque/triagem/VtriagemComponente",$dados);
		}else{
			
			$this->Mcrud->setStrTable("trproduto AS TRP");
			$arrDataProduto = array("cabecalho" => array("TRP.CODPRODUTO" => "CODPRODUTO",
														"TRP.CODTRSTATUS" => "CODTRSTATUS",
														"TS.DESCRICAO" => "STATUS"
									),
									"join" => array("trstatus AS TS" => "TS.CODTRSTATUS = TRP.CODTRSTATUS"),
									"where" => array(
							   				"TRP.CODCLIENTE" => $_SESSION['codcliente'],
							   				"TRP.CODEMPRESA" => $_SESSION['codempresa'],
							   				"TRP.CODFILIAL" => $_SESSION['codfilial'],
							   				"TRP.CODDOACAO" => $coddoacao
											)
								);
			$arrDadosProduto = $this->Mcrud->getDados($arrDataProduto);
			$dados['produto'] = $arrDadosProduto->row();
			
			$sqlComponentesProduto = "SELECT 	TRTFILHO.CODTIPO AS 'CODTIPOFILHO',
												TRTFILHO.DESCRICAO AS 'NOMETIPOFILHO',
												TRF.CODPRODUTO AS 'CODPRODUTOFILHO',
												TRP.CODCOMPONENTESPRODUTO AS 'CODCOMPONENTESPRODUTO',
												TRF.ETIQUETA,
												TR.CODDOACAO,
												TRT.CODCOMPONENTESTIPO,
												TR.CODPRODUTO,
												TR.CODTRSTATUS,
												TRT.OBRIGATORIO
										FROM trproduto TR
										INNER JOIN trcomponentestipo TRT
											ON TRT.CODCLIENTE = TR.CODCLIENTE
										    AND TRT.CODTIPOPAI = TR.CODTIPO
										    AND TRT.ATIVO = 1
										INNER JOIN trtipo TRTFILHO
											ON TRTFILHO.CODCLIENTE = TR.CODCLIENTE
										    AND TRTFILHO.CODTIPO = TRT.CODTIPOFILHO
										LEFT JOIN trcomponentesproduto TRP
											ON TRP.CODCLIENTE = TR.CODCLIENTE
										    AND TRP.CODEMPRESA = TR.CODEMPRESA
										    AND TRP.CODFILIAL = TR.CODFILIAL
										    AND TRP.CODPRODUTOPAI = TR.CODPRODUTO
										    AND TRP.CODCOMPONENTESTIPO = TRT.CODCOMPONENTESTIPO
										LEFT JOIN trproduto TRF
											ON TRF.CODCLIENTE = TR.CODCLIENTE
										    AND TRF.CODEMPRESA = TR.CODEMPRESA
										    AND TRF.CODFILIAL = TR.CODFILIAL
										    AND TRF.CODPRODUTO = TRP.CODPRODUTOFILHO
										    AND TRF.CODTIPO = TRT.CODTIPOFILHO
										WHERE TR.CODCLIENTE = ?
											AND TR.CODEMPRESA = ?
											AND TR.CODFILIAL = ?
										    AND TR.CODDOACAO = ?";
			$arrDados = array(
				$_SESSION['codcliente'],
				$_SESSION['codempresa'],
				$_SESSION['codfilial'],
				$coddoacao
			);
			$dadosSql = $this->db->query($sqlComponentesProduto,$arrDados);
			$dados['compProduto'] = $dadosSql->result();
			
			$this->load->view("estoque/estoque/triagem/VtriagemEquipamento",$dados);	
		}
		
		
	}
	
	
	public function novoComponente($view = 1,$codproduto = '',$coddoacao = '',$codtipo = '',$quantidadetriada = '',$quantidade = ''){
		if(isset($_POST['coddoacao']) && $_POST['coddoacao'] != '') $coddoacao = $_POST['coddoacao'];
		if(isset($_POST['codtipo']) && $_POST['codtipo'] != '') $codtipo = $_POST['codtipo'];
		if(isset($_POST['quantidadetriada']) && $_POST['quantidadetriada'] != '') $quantidadetriada = $_POST['quantidadetriada'];
		if(isset($_POST['quantidade']) && $_POST['quantidade'] != '') $quantidade = $_POST['quantidade'];
		if(isset($_POST['codproduto']) && $_POST['codproduto'] != 'undefined'){
			$codproduto = $_POST['codproduto'];
			$this->data["cabecalhoform"]["action"] = "estoque/estoque/triagem/CtriagemComponente/salvaEdicaoComponente";
			$this->data['form']['Geral']['trproduto.ETIQUETA']['value'] = $_POST['etiqueta'];
			$this->data['form']['Geral']['trproduto.ETIQUETA']['disabled'] = true;
			$this->data['form']['Geral']['trproduto.ETIQUETA']['ativo_salvar'] = false;	
		}
		
		
		$this->data['form']['Geral']['trproduto.CODPRODUTO']['value'] = $codproduto;
		$this->data['form']['Geral']['trproduto.CODDOACAO']['value'] = $coddoacao;
		$this->data['form']['Geral']['trproduto.CODTIPO']['value'] = $codtipo;
		$this->data['form']['Geral']['trdoacao.QUANTIDADETRIADA']['value'] = $quantidadetriada;
		$this->data['form']['Geral']['trdoacao.QUANTIDADE']['value'] = $quantidade;
		
		$sqlCamposComponentes = "	SELECT TRCT.CODCAMPOSTIPO,
										   TRCT.NOME,
										   TRCP.VALOR,
										   TRCP.CODPRODUTO,
										   TRCP.CODCAMPOSPRODUTO
									FROM trtipo TRT
									INNER JOIN trcampostipo TRCT
										ON TRCT.CODCLIENTE = TRT.CODCLIENTE
									    AND TRCT.CODTIPO = TRT.CODTIPO
									    AND TRCT.ATIVO = 1
									LEFT JOIN trcamposproduto TRCP
										ON TRCP.CODCLIENTE = TRT.CODCLIENTE
									    AND TRCP.CODCAMPOSTIPO = TRCT.CODCAMPOSTIPO
									    AND TRCP.CODCLIENTE = ?
									    AND TRCP.CODEMPRESA = ?
									    AND TRCP.CODFILIAL = ?
									    AND TRCP.CODPRODUTO = ?
									WHERE TRT.TIPOENTRADA = 1
										AND TRT.ATIVO = 1
										AND TRCT.ATIVO = 1
										AND TRT.CODTIPO = ?";	
		$arrDados = array(
				$_SESSION['codcliente'],
				$_SESSION['codempresa'],
				$_SESSION['codfilial'],
				$codproduto,
				$codtipo
			);
		$dadosSql = $this->db->query($sqlCamposComponentes,$arrDados);
		foreach($dadosSql->result() as $rows):
			$nome = str_replace(' ','',$rows->NOME);
			$this->data['form']['Geral'][$nome.'.trcamposproduto.VALOR']['tipo'] = 'label';
			$this->data['form']['Geral'][$nome.'.trcamposproduto.VALOR']['label'] = $rows->NOME;
			$this->data['form']['Geral'][$nome.'.trcamposproduto.VALOR']['idname'] = 'txt'.$nome."CtriagemComponentes";
			$this->data['form']['Geral'][$nome.'.trcamposproduto.VALOR']['required'] = 'true';
			$this->data['form']['Geral'][$nome.'.trcamposproduto.VALOR']['value'] = $rows->VALOR;
			$this->data['form']['Geral'][$nome.'.trcamposproduto.VALOR']["ativo_salvar"] = false;
			$this->data['form']['Geral'][$nome.'.trcamposproduto.VALOR']["codcampostipo"] = $rows->CODCAMPOSTIPO;
			$this->data['form']['Geral'][$nome.'.trcamposproduto.VALOR']["codcamposproduto"] = $rows->CODCAMPOSPRODUTO;
		endforeach;
		
		if($view == 1){
			if($_POST['codtipoentrada'] == 2){
				$this->data["cabecalhoform"]["action"] = "estoque/estoque/triagem/CtriagemComponente/salvaEdicaoComponenteProduto";
				$this->data['form']['Geral']["trproduto.ETIQUETA"]["required"] = false;
				$this->data['form']['Geral']["trproduto.ETIQUETA"]["visible"] = false;
				$this->data['form']['Geral']['trproduto.CODTRSTATUS']["value"] = 14;
				$this->data['form']['Geral']['trdoacao.CODTIPOENTRADA']['value'] = 2;
				
				$this->data['form']['Geral']['trcomponentesproduto.codprodutopai']['tipo'] = 'label';
				$this->data['form']['Geral']['trcomponentesproduto.codprodutopai']['label'] = "codprodutopai";
				$this->data['form']['Geral']['trcomponentesproduto.codprodutopai']['idname'] = "txtCodprodutoPaiCtriagemComponentes";
				$this->data['form']['Geral']['trcomponentesproduto.codprodutopai']['required'] = 'true';
				$this->data['form']['Geral']['trcomponentesproduto.codprodutopai']['value'] = $_POST['codprodutopai'];
				$this->data['form']['Geral']['trcomponentesproduto.codprodutopai']["ativo_salvar"] = false;
				$this->data['form']['Geral']['trcomponentesproduto.codprodutopai']["visible"] = false;
				
				$this->data['form']['Geral']['trcomponentesproduto.codcomponentestipo']['tipo'] = 'label';
				$this->data['form']['Geral']['trcomponentesproduto.codcomponentestipo']['label'] = "codcomponentetipo";
				$this->data['form']['Geral']['trcomponentesproduto.codcomponentestipo']['idname'] = "txtCodComponentesTipoCtriagemComponentes";
				$this->data['form']['Geral']['trcomponentesproduto.codcomponentestipo']['required'] = 'true';
				$this->data['form']['Geral']['trcomponentesproduto.codcomponentestipo']['value'] = $_POST['codcomponentestipo'];
				$this->data['form']['Geral']['trcomponentesproduto.codcomponentestipo']["ativo_salvar"] = false;
				$this->data['form']['Geral']['trcomponentesproduto.codcomponentestipo']["visible"] = false;
				
			}
			$this->load->view('global/crud/Vnovo',$this->data);	
		}else
			return $this->data['form']['Geral'];
		
		
	}
	
	
	
	public function salvaNovoComponente($tipoRetorno = 1){
		if($this->validaEtiqueta($_POST['EtiquetaCtriagemComponente']) == 1){
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Erro enviar componente para estoque, Etiqueta informada já existe na base de dados!",
							class_name: "gritter-error",
						});
					})
			</script>
			<?php
			die();
		}
		if($_POST['CodTipoEntradaCtriagemComponente'] == 2){
			$this->data['form']['Geral']["trproduto.ETIQUETA"]["ativo_salvar"] = false;
		}
		$retornoCodProduto = $this->salvaEdicao(2);
		
		if($retornoCodProduto != 0){
			if($tipoRetorno == 1){
				$quantidadeTriada = $_POST['QuantidadeTriadaCtriagemComponente']+1;
				
				$this->db->where('trdoacao.CODCLIENTE',$_SESSION['codcliente']);
				$this->db->where('trdoacao.CODEMPRESA',$_SESSION['codempresa']);
				$this->db->where('trdoacao.CODFILIAL',$_SESSION['codfilial']);
				$this->db->where('trdoacao.CODDOACAO',$_POST['coddoacaoCtriagemComponente']); 
				$this->db->set('QUANTIDADETRIADA',$quantidadeTriada);
				$this->db->set('REGMODIFPOR',!isset( $_SESSION['codusuario'] ) ? 0 : $_SESSION['codusuario'] );
				$this->db->set('REGMODIFEM', date("Y-m-d H:i:s"));
				if($quantidadeTriada == $_POST['QuantidadeCtriagemComponente'])$this->db->set("CODTRSTATUS",2);
				$this->db->update('trdoacao');
				$this->data['form']['Geral']['trdoacao.QUANTIDADETRIADA']['value'] = $quantidadeTriada;
			}
			
			$formulario = $this->novoComponente(2,$retornoCodProduto,$_POST['coddoacaoCtriagemComponente'],$_POST['codtipoCtriagemComponente']);
			
			$chavecom = array (
						"CODCLIENTE" => "CODCLIENTE",
						"CODEMPRESA" => "CODEMPRESA",
						"CODFILIAL" => "CODFILIAL",
						);
			$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
			$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
			$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
			
			foreach($formulario as $key => $row):
				$arrKey = explode(".",$key);
				if(isset($arrKey[2]) && $arrKey[2] == 'VALOR'){
					$dadosEdicaoComp = $arrValoresDaChave;
					$dadosEdicaoComp['VALOR'] = $_POST[$row['idname']];
					$dadosEdicaoComp['CODPRODUTO'] = $retornoCodProduto;
					$dadosEdicaoComp['CODCAMPOSTIPO'] = $row['codcampostipo'];
					$this->Mcrud->setStrTable('trcamposproduto');
					$this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,null,'CODCAMPOSPRODUTO');
				}
			endforeach;
			
			$dadosEdicaoComp = $arrValoresDaChave;
			$dadosEdicaoComp['CODPRODUTO'] = $retornoCodProduto;
			$dadosEdicaoComp['CODTRSTATUS'] = $_POST['codtrstatusCtriagemComponente'];
			$this->Mcrud->setStrTable('trlogproduto');
			$this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
			
			if($tipoRetorno == 1){
				$sqlDadosRetorno = "SELECT TRP.ETIQUETA AS 'ETIQUETA',
										   TRP.REGCRIADOEM AS 'REGCRIADOEM',
										   TRS.DESCRICAO AS 'STATUS',
										   TRP.CODPRODUTO
									FROM trproduto TRP
									INNER JOIN trstatus TRS
										ON TRS.CODTRSTATUS = TRP.CODTRSTATUS
									WHERE TRP.CODCLIENTE = ?
											AND TRP.CODEMPRESA = ?
											AND TRP.CODFILIAL = ?
											AND TRP.CODPRODUTO = ?";
				$arrDados = array(
					$_SESSION['codcliente'],
					$_SESSION['codempresa'],
					$_SESSION['codfilial'],
					$retornoCodProduto
				);
				$dadosSql = $this->db->query($sqlDadosRetorno,$arrDados);
				$rowRetorno = $dadosSql->row();
				?>
				<script type="text/javascript">
					$("#iptQuantidadeTriadaVTriagemComponente").attr("value",<?=$quantidadeTriada;?>);
					<?php
					if($quantidadeTriada == $_POST['QuantidadeCtriagemComponente']){
						?>
						$("#Status<?=$_POST['coddoacaoCtriagemComponente'];?>").text("Triagem Finalizada");
						<?php
					}
					?>
					$("#Qtd_Triado<?=$_POST['coddoacaoCtriagemComponente'];?>").text("<?=$quantidadeTriada;?>");
					$("#trVtriagemComponente<?=$quantidadeTriada;?>").remove();
					$("#tblComponentes").append("<tr class='odd gradeX'><td><?=$rowRetorno->ETIQUETA;?></td><td id='tdstatuscomponente<?=$rowRetorno->CODPRODUTO;?>' ><?=$rowRetorno->STATUS;?></td><td><?=$this->converteData($rowRetorno->REGCRIADOEM);?></td><td><a data-toggle='modal' data-target='#modalCtriagem' data-original-title='Editar' data-ajajax-tipo='editar' title='Editar' class='btn btn-sm btn-warning m-r-5' onclick='novoEditarComponente(<?=$_POST['coddoacaoCtriagemComponente'];?>,<?=$_POST['codtipoCtriagemComponente'];?>,<?=$rowRetorno->ETIQUETA;?>,<?=$retornoCodProduto;?>)' ><i class='fa fa-edit (alias)'></i> Editar</a><a 	data-toggle='modal' data-target='#modalCtriagem' data-original-title='Editar' data-ajajax-tipo='editar' title='Editar' class='btn btn-sm btn-success m-r-5' id='btVendaVtriagemComponente<?=$retornoCodProduto;?>'; onclick='modalvendaComponente(<?=$retornoCodProduto;?>,<?=$_POST['coddoacaoCtriagemComponente'];?>)' ><i class='fa fa-money'></i> Venda</a><a data-toggle='modal' data-target='#modalCtriagem' data-original-title='Editar' data-ajajax-tipo='editar' title='Log' class='btn btn-sm btn-default m-r-5' onclick='verlogComponente(<?=$rowRetorno->CODPRODUTO;?>)' ><i class='fa fa-history'></i> Histórico</a></td></tr>");
					jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Componente salvo com sucesso. <br /><br /><h4><b>ETIQUETA: <?=$rowRetorno->ETIQUETA;?></b></h4>",
							class_name: "gritter-success",
							time: 200000
						});
					})
					jQuery("#ModalbtnCancelar").click();
				</script>
				<?php
			}else{
				return $retornoCodProduto;
			}
		
		}else{
			return 0;
		}
		
	}



	public function salvaEdicaoComponente($tipoRetorno = 1){
		$this->load->model("global/Mcrud","Mcrud");
		$retornoCodProduto = $_POST['codprodutoCtriagemComponente'];
		
		$formulario = $this->novoComponente(2,$retornoCodProduto,$_POST['coddoacaoCtriagemComponente'],$_POST['codtipoCtriagemComponente'],'');
		
		$chavecom["CODCLIENTE"] = "CODCLIENTE";
		$chavecom["CODEMPRESA"] = "CODEMPRESA";
		$chavecom["CODFILIAL"] = "CODFILIAL";
		$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
		$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
		$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
		
		foreach($formulario as $key => $row):
			$arrKey = explode(".",$key);
			if(isset($arrKey[2]) && $arrKey[2] == 'VALOR'){
				$dadosEdicaoComp['VALOR'] = $_POST[$row['idname']];
				$dadosWhereUp['CODPRODUTO'] = $retornoCodProduto;
				$dadosWhereUp['CODCAMPOSTIPO'] = $row['codcampostipo'];
				$dadosWhereUp['CODCAMPOSPRODUTO'] = $row['codcamposproduto'];
				$this->Mcrud->setStrTable('trcamposproduto');
				$this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,$dadosWhereUp,'CODCAMPOSPRODUTO');
			}
		endforeach;
		
		if($tipoRetorno == 1){
			?>
			<script type="text/javascript" >
				jQuery(function(){
					$.gritter.add({
						title : "Alerta!",
						text : "Dados atualizados com Sucesso!",
						class_name: "gritter-success",
					});
				})
			</script>
			<?php
		}else{
			return $retornoCodProduto;
		}
	}

	
	/*
	 * $tipoRetorno = 1 => Gerar sucata pela tela de componente sem salvar o produto;
	 * $tipoRetorno = 2 => Gerar sucata pela tela de equipamento;
	 * $tipoRetorno = 3 => Gerar sucata pela troca de componente do equipamento;
	 */
	public function SucataComponente($tipoRetorno=1,$coddoacao = 0,$codproduto = 0,$codtipo = 0){
		if(isset($_POST['coddoacao']) && $_POST['coddoacao'] != '') $coddoacao = $_POST['coddoacao'];
		if(isset($_POST['codproduto']) && $_POST['codproduto'] != '') $codproduto = $_POST['codproduto'];
		if(isset($_POST['codtipo']) && $_POST['codtipo'] != '') $codtipo = $_POST['codtipo'];
		
		$this->load->model("global/Mcrud","Mcrud");
		$validacao = false;
		
		$chavecom["CODCLIENTE"] = "CODCLIENTE";
		$chavecom["CODEMPRESA"] = "CODEMPRESA";
		$chavecom["CODFILIAL"] = "CODFILIAL";
		$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
		$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
		$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
		
		$dadosIntoProduto = $arrValoresDaChave;
		$dadosIntoProduto['CODDOACAO'] = $coddoacao;
		if($tipoRetorno == 2 || $tipoRetorno == 3){
			$dadosIntoProduto['CODPRODUTO'] = $codproduto;	
		}
		$dadosIntoProduto['CODTIPO'] = $codtipo;
		$dadosIntoProduto['CODTRSTATUS'] = 4;
		$this->Mcrud->setStrTable('trsucata');
		$retornoSucata = $this->Mcrud->setDados($dadosIntoProduto,$chavecom,$arrValoresDaChave,null,'CODSUCATA');
		
		if($retornoSucata != 0){
			if($tipoRetorno == 1){
					$quantidadeTriada = $_POST['quantidadetriada']+1;
				
					$dadosEdicaoComp['QUANTIDADETRIADA'] = $quantidadeTriada;
					if($quantidadeTriada == $_POST['quantidadetriada'])$dadosEdicaoComp['CODTRSTATUS'] = 2;
					$dadosWhereUp['CODDOACAO'] = $coddoacao;
					$this->Mcrud->setStrTable('trdoacao');
					$retornoUp = $this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,$dadosWhereUp,'CODDOACAO');
					
					if($retornoUp != 0){
						$validacao = true;
					}
			}else if($tipoRetorno == 2 || $tipoRetorno == 3 || $tipoRetorno == 4){
				$dadosEdicaoProduto['CODTRSTATUS'] = 4;
				$dadosWhereUpProduto['CODPRODUTO'] = $codproduto;
				$this->Mcrud->setStrTable('trproduto');
				$retornoUp = $this->Mcrud->setDados($dadosEdicaoProduto,$chavecom,$arrValoresDaChave,$dadosWhereUpProduto,'CODPRODUTO');
				
				if($retornoUp != 0){
					$dadosEdicaoLogComp = $arrValoresDaChave;
					$dadosEdicaoLogComp['CODTRSTATUS'] = 4;
					$dadosEdicaoLogComp['CODSUCATA'] = $retornoSucata;
					$dadosEdicaoLogComp['CODPRODUTO'] = $codproduto;
					$this->Mcrud->setStrTable('trlogproduto');
					$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
					
					if($retornoLogCompProduto != 0 && $tipoRetorno == 2){
						$dadosEdicaoCompProduto['CODPRODUTOFILHO'] = null;
						$dadosWhereUpCompPro['CODCOMPONENTESPRODUTO'] = $_POST['codcomponentesproduto'];
						$this->Mcrud->setStrTable('trcomponentesproduto');
						$retornoCompoProduto = $this->Mcrud->setDados($dadosEdicaoCompProduto,$chavecom,$arrValoresDaChave,$dadosWhereUpCompPro,'CODCOMPONENTESPRODUTO');
					
						if($retornoCompoProduto != 0){
							$validacao = true;
						}
					}else{
						if($retornoLogCompProduto != 0){
							$validacao = true;
						}
					}
				}
			}
		}
		
		if($validacao){
			if($tipoRetorno == 1){
				?>
				<script type="text/javascript">
					$("#iptQuantidadeTriadaVTriagemComponente").attr("value",<?=$quantidadeTriada;?>);
					<?php
					if($quantidadeTriada == $_POST['quantidade']){
						?>
						$("#Status<?=$_POST['coddoacao'];?>").text("Triagem Finalizada");
						<?php
					}
					?>
					$("#Qtd_Triado<?=$_POST['coddoacao'];?>").text("<?=$quantidadeTriada;?>");
					$("#trVtriagemComponente<?=$quantidadeTriada;?>").remove();
					$("#tblComponentes").append("<tr class='odd gradeX'><td></td><td>Sucata</td><td><?=date("d/m/Y H:i");?></td><td></td></tr>");
					jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Sucata gerada com sucesso!",
							class_name: "gritter-success",
						});
					})
				</script>
				<?php
			}else if($tipoRetorno == 2){
				?>
				<script type="text/javascript" >
					jQuery('#btnEditar'+$("#iptCodDoacaoprodutoPai").val()).click();
					jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Sucata gerada com sucesso!",
							class_name: "gritter-success",
						});
					})
				</script>
				<?php
			}elseif($tipoRetorno == 4){
				?>
				<script type="text/javascript" >
					$("#trVestoqueComponenteLista<?=$codproduto;?>").remove();
					jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Sucata gerada com sucesso!",
							class_name: "gritter-success",
						});
					})
				</script>
				<?php
			}else{
				return 1;
			}
		}else if($tipoRetorno == 3){
			return 0;
		}else{	
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Erro gerar sucata, gentileza entrar em contato com o administrador da rede!",
							class_name: "gritter-error",
						});
					})
			</script>
			<?php
		}
	}



	public function verLogComponente($codproduto = 1,$tipoRetorno = 1){
		if($codproduto == 1){ $codproduto = $_POST['codproduto']; }
		
		$sqlDadosLogComponente = "SELECT 	TLP.REGCRIADOEM,
											TP.ETIQUETA,
									        TRT.DESCRICAO AS 'TIPO',
									        GU.NOME AS 'USUARIO',
									        TS.DESCRICAO AS 'STATUS'
									FROM trlogproduto TLP
									INNER JOIN trproduto TP
										ON TP.CODCLIENTE = TLP.CODCLIENTE
									    AND TP.CODEMPRESA = TLP.CODEMPRESA
									    AND TP.CODFILIAL = TLP.CODFILIAL
									    AND TP.CODPRODUTO = TLP.CODPRODUTO
									INNER JOIN trtipo TRT
										ON TRT.CODCLIENTE = TLP.CODCLIENTE
									    AND TRT.CODTIPO = TP.CODTIPO
									LEFT JOIN glusuario GU
										ON GU.CODUSUARIO = TLP.REGCRIADOPOR
									LEFT JOIN trstatus TS
										ON TS.CODTRSTATUS = TLP.CODTRSTATUS
									WHERE TLP.CODCLIENTE = ?
										AND TLP.CODEMPRESA = ?
									    AND TLP.CODFILIAL = ?
									    AND TLP.CODPRODUTO = ?
									ORDER BY TLP.REGCRIADOEM DESC";
				$arrDados = array(
					$_SESSION['codcliente'],
					$_SESSION['codempresa'],
					$_SESSION['codfilial'],
					$codproduto
				);
				
				$dadosSql = $this->db->query($sqlDadosLogComponente,$arrDados);
				$dataSql['logsComponente'] = $dadosSql->result_array();

		if($tipoRetorno == 1){
			if(isset($dataSql['logsComponente'])){
				$this->load->view('estoque/estoque/logs/VlogComponente',$dataSql);
			}else{
				echo "Erro pesquisar registro, gentileza entrar em contato com o administrador da rede!";
			}
		}else{
			if(isset($dataSql['logsComponente'])){
				return $dataSql['logsComponente'];
			}else{
				return 0;
			}
		}					
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*Arquivo da tela de edição do equipamento*/
	
	
	
	public function salvaEdicaoComponenteProduto(){		
		$retornoCodComponente = $this->salvaNovoComponente(2);
		
		$validacao = false;
		if($retornoCodComponente != 0){
			$chavecom["CODCLIENTE"] = "CODCLIENTE";
			$chavecom["CODEMPRESA"] = "CODEMPRESA";
			$chavecom["CODFILIAL"] = "CODFILIAL";
			$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
			$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
			$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
			
			$dadosEdicaoComp = $arrValoresDaChave;
			$dadosEdicaoComp['CODPRODUTOPAI'] = $_POST['txtCodprodutoPaiCtriagemComponentes'];
			$dadosEdicaoComp['CODPRODUTOFILHO'] = $retornoCodComponente;
			$dadosEdicaoComp['CODCOMPONENTESTIPO'] = $_POST['txtCodComponentesTipoCtriagemComponentes'];
			$this->Mcrud->setStrTable('trcomponentesproduto');
			$retornoCompProduto = $this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,null,'CODCOMPONENTESPRODUTO');
			
			if($retornoCompProduto != 0){
				$dadosEdicaoComp = $arrValoresDaChave;
				$dadosEdicaoComp['CODCOMPONENTESPRODUTO'] = $retornoCompProduto;
				$dadosEdicaoComp['CODPRODUTOFILHO'] = $retornoCodComponente;
				$this->Mcrud->setStrTable('trlogcomponentesproduto');
				$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,null,'CODLOGCOMPONENTESPRODUTO');
				
				$validacao = true;
			}
		}
		
		if($validacao){
			?>
			<script type="text/javascript" >
				jQuery('#btnEditar'+$("#iptCodDoacaoprodutoPai").val()).click();
				jQuery(function(){
					$.gritter.add({
						title : "Alerta!",
						text : "Dados inseridos com Sucesso!",
						class_name: "gritter-success",
					});
				})
				jQuery("#ModalbtnCancelar").click();
			</script>
			<?php
		}else{
			?>
			<script type="text/javascript" >
				jQuery(function(){
					$.gritter.add({
						title : "Alerta!",
						text : "Erro inserir Componente para o produto!",
						class_name: "gritter-danger",
					});
				})
			</script>
			<?php
		}
	}


	public function verLogEquipamento($codproduto = 1,$tipoRetorno = 1){
		if($codproduto == 1){ $codproduto = $_POST['codproduto']; }
		
		$sqlDadosLogEquipamento = "SELECT 	TLP.REGCRIADOEM,
											TP.ETIQUETA,
									        TRT.DESCRICAO AS 'TIPO',
									        GU.NOME AS 'USUARIO',
									        TS.DESCRICAO AS 'STATUS'
									FROM trlogproduto TLP
									INNER JOIN trproduto TP
										ON TP.CODCLIENTE = TLP.CODCLIENTE
									    AND TP.CODEMPRESA = TLP.CODEMPRESA
									    AND TP.CODFILIAL = TLP.CODFILIAL
									    AND TP.CODPRODUTO = TLP.CODPRODUTO
									INNER JOIN trtipo TRT
										ON TRT.CODCLIENTE = TLP.CODCLIENTE
									    AND TRT.CODTIPO = TP.CODTIPO
									LEFT JOIN glusuario GU
										ON GU.CODUSUARIO = TLP.REGCRIADOPOR
									LEFT JOIN trstatus TS
										ON TS.CODTRSTATUS = TLP.CODTRSTATUS
									WHERE TLP.CODCLIENTE = ?
										AND TLP.CODEMPRESA = ?
									    AND TLP.CODFILIAL = ?
									    AND TLP.CODPRODUTO = ?
									ORDER BY TLP.REGCRIADOEM DESC";
				$arrDados = array(
					$_SESSION['codcliente'],
					$_SESSION['codempresa'],
					$_SESSION['codfilial'],
					$codproduto
				);
				
				$dadosSql = $this->db->query($sqlDadosLogEquipamento,$arrDados);
				$dataSql['logsEquipamento'] = $dadosSql->result_array();

		if($tipoRetorno == 1){
			if(isset($dataSql['logsEquipamento'])){
				$this->load->view('estoque/estoque/logs/VlogEquipamento',$dataSql);
			}else{
				echo "Erro pesquisar registro, gentileza entrar em contato com o administrador da rede!";
			}
		}else{
			if(isset($dataSql['logsEquipamento'])){
				return $dataSql['logsEquipamento'];
			}else{
				return 0;
			}
		}					
	}

	
	public function verLogComponentesEquipamento(){
		$dadosLog['logsComponente'] = $this->verLogComponente($_POST['codproduto'],2);
		
		$sqlComponentesProduto = "SELECT 	TLP.REGCRIADOEM,
											GU.NOME AS 'USUARIO',
									        TRFILHO.ETIQUETA,
									        TT.DESCRICAO AS 'STATUSATUAL',
									        TTFILHO.DESCRICAO AS 'TIPODOACAO'
									FROM trlogcomponentesproduto TLP
									INNER JOIN trcomponentesproduto TCP
										ON TCP.CODCLIENTE = TLP.CODCLIENTE
									    AND TCP.CODEMPRESA = TLP.CODEMPRESA
									    AND TCP.CODFILIAL = TLP.CODFILIAL
									    AND TCP.CODCOMPONENTESPRODUTO = TLP.CODCOMPONENTESPRODUTO
									INNER JOIN trproduto TRFILHO
										ON TRFILHO.CODCLIENTE = TLP.CODCLIENTE
									    AND TRFILHO.CODEMPRESA = TLP.CODEMPRESA
									    AND TRFILHO.CODFILIAL = TLP.CODFILIAL
									    AND TRFILHO.CODPRODUTO = TLP.CODPRODUTOFILHO
									INNER JOIN trproduto TR
										ON TR.CODCLIENTE = TCP.CODCLIENTE
									    AND TR.CODEMPRESA = TCP.CODEMPRESA
									    AND TR.CODFILIAL = TCP.CODFILIAL
									    AND TR.CODPRODUTO = TCP.CODPRODUTOPAI
									INNER JOIN trtipo TTFILHO
										ON TTFILHO.CODCLIENTE = TLP.CODCLIENTE
									    AND TTFILHO.CODTIPO = TRFILHO.CODTIPO
									LEFT JOIN glusuario GU
										ON GU.CODUSUARIO = TLP.REGCRIADOPOR
									LEFT JOIN trstatus TT
										ON TT.CODTRSTATUS = TRFILHO.CODTRSTATUS
									WHERE TLP.CODCLIENTE = ?
										AND TLP.CODEMPRESA = ?
									    AND TLP.CODFILIAL = ?
									    AND TCP.CODCOMPONENTESPRODUTO = ?
									ORDER BY TLP.REGCRIADOEM DESC";
		$arrDados = array(
					$_SESSION['codcliente'],
					$_SESSION['codempresa'],
					$_SESSION['codfilial'],
					$_POST['codcomponetesproduto'],
				);
		$dadosSql = $this->db->query($sqlComponentesProduto,$arrDados);
		$dadosLog['logsComponenteProduto'] = $dadosSql->result_array();
			
		$this->load->view('estoque/estoque/logs/VlogComponentesEquipamento',$dadosLog);
		
	}
	
	
	public function modalComponenteEquipamento(){
		$sqlDadosComponentes = "SELECT 	TD.CODPRODUTO,
										TCP.CODCAMPOSTIPO,
										TCP.CODCAMPOSPRODUTO,
										TD.ETIQUETA,
										TD.CODPRODUTO,
								        TCP.VALOR,
								        TD.CODDOACAO
								FROM trproduto TD
								INNER JOIN trtipo TT
									ON TT.CODCLIENTE = TD.CODCLIENTE
								    AND TT.CODTIPO = TD.CODTIPO
								INNER JOIN trcampostipo TRCT
									ON TRCT.CODCLIENTE = TD.CODCLIENTE
								    AND TRCT.CODTIPO = TD.CODTIPO
								    AND TRCT.ATIVO = 1
								LEFT JOIN trcamposproduto TCP
									ON TCP.CODCLIENTE = TD.CODCLIENTE
								    AND TCP.CODEMPRESA = TD.CODEMPRESA
								    AND TCP.CODFILIAL = TD.CODFILIAL
								    AND TCP.CODPRODUTO = TD.CODPRODUTO
								    AND TCP.CODCAMPOSTIPO = TRCT.CODCAMPOSTIPO
								WHERE TT.COMPONENTEPRODUTO = 1
									AND TT.TIPOENTRADA = 1
								    AND TD.CODTRSTATUS = 3
								    AND TD.CODCLIENTE = ?
								    AND TD.CODEMPRESA = ?
								    AND TD.CODFILIAL = ?
								    AND TD.CODTIPO = ?
								    AND TD.ETIQUETA IS NOT NULL
								ORDER BY TD.CODPRODUTO,
									TCP.CODCAMPOSTIPO";
		$arrDados = array(
					$_SESSION['codcliente'],
					$_SESSION['codempresa'],
					$_SESSION['codfilial'],
					$_POST['codtipo'],
				);
		$dadosSql = $this->db->query($sqlDadosComponentes,$arrDados);
		$arrDadosTroca['registros'] = $dadosSql->result_array();
		
		
		$sqlCabecalho = "SELECT  TCT.CODCAMPOSTIPO,
									TCT.NOME AS 'CAMPOS'
							FROM trcampostipo TCT
							INNER JOIN trtipo TT
								ON TT.CODCLIENTE = TCT.CODCLIENTE
								AND TT.CODTIPO = TCT.CODTIPO
							WHERE TT.TIPOENTRADA = 1
								AND TCT.CODCLIENTE = ?
								AND TCT.CODTIPO = ?
							    AND TCT.ATIVO = 1
							ORDER BY TCT.CODCAMPOSTIPO";
		$arrDadosCabe = array(
					$_SESSION['codcliente'],
					$_POST['codtipo'],
				);
		$dadosCabe = $this->db->query($sqlCabecalho,$arrDadosCabe);
		$arrDadosTroca['cabecalho'] = $dadosCabe->result_array();
		
		
		$this->load->view('estoque/estoque/triagem/VtrocaComponenteEquipamento',$arrDadosTroca);
	}
	
	
	public function trocarComponenteEquipamento(){
		$continue = true;
		$codstatus = 0;
		$valida = false;
		if($_POST['codprodutofilho'] != 0){
			/*
			 * $_POST['codtipoantigo'] == 1 => ESTOQUE
			 * $_POST['codtipoantigo'] == 2 => SUCATA
			 */
			if($_POST['codexecucaoantigo'] == 1){
				if($this->validaEtiqueta($_POST['txtEtiquetaEstoqueCtriagemComponente']) == 1){
					?>
					<script type="text/javascript">
						jQuery(function(){
								$.gritter.add({
									title : "Alerta!",
									text : "Erro enviar componente para estoque, Etiqueta informada já existe na base de dados!",
									class_name: "gritter-error",
								});
							})
					</script>
					<?php
					die();
				}
				$retornoEstoque = $this->EstoqueComponentePorEquipamento(2,$_POST['codprodutofilho'],$_POST['txtEtiquetaEstoqueCtriagemComponente']);
				$codstatus = 3;
				
				if($retornoEstoque == 0){
					$continue == false;
				} 
			}else{
				$retornoSucata = $this->SucataComponente(3,$_POST['coddoacao'], $_POST['codprodutofilho'], $_POST['codtipo']);
				$codstatus = 4;
				
				if($retornoSucata == 0){
					$continue == false;
				}
			}
		}else{
			$this->load->model("global/Mcrud","Mcrud");
		}
		if($continue){
			$chavecom["CODCLIENTE"] = "CODCLIENTE";
			$chavecom["CODEMPRESA"] = "CODEMPRESA";
			$chavecom["CODFILIAL"] = "CODFILIAL";
			$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
			$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
			$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
			
			$dadosEdicaoNovoComp['CODPRODUTOFILHO'] = $_POST['novocodproduto'];
			$dadosWhereNovoComp['CODCOMPONENTESPRODUTO'] = $_POST['codcomponentesproduto'];
			$this->Mcrud->setStrTable('trcomponentesproduto');
			$retornoNovoComp = $this->Mcrud->setDados($dadosEdicaoNovoComp,$chavecom,$arrValoresDaChave,$dadosWhereNovoComp,'CODCOMPONENTESPRODUTO');
			if($retornoNovoComp != 0){
				$dadosEdicaoLogComp = $arrValoresDaChave;
				$dadosEdicaoLogComp['CODCOMPONENTESPRODUTO'] = $_POST['codcomponentesproduto'];
				$dadosEdicaoLogComp['CODPRODUTOFILHO'] = $_POST['novocodproduto'];
				$this->Mcrud->setStrTable('trlogcomponentesproduto');
				$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,null,'CODLOGCOMPONENTESPRODUTO');
				if($retornoLogCompProduto != 0){
					$dadosEdicaoProd['CODTRSTATUS'] = 15;
					$dadosWhereEdicaoProd['CODPRODUTO'] = $_POST['novocodproduto'];
					$this->Mcrud->setStrTable('trproduto');
					$retornoProduto = $this->Mcrud->setDados($dadosEdicaoProd,$chavecom,$arrValoresDaChave,$dadosWhereEdicaoProd,'CODPRODUTO');
					
					if($retornoProduto != 0){
						$dadosEdicaoLogProd = $arrValoresDaChave;
						$dadosEdicaoLogProd['CODPRODUTO'] = $_POST['novocodproduto'];
						$dadosEdicaoLogProd['CODTRSTATUS'] = 15;
						$this->Mcrud->setStrTable('trlogproduto');
						$retornoLogProduto = $this->Mcrud->setDados($dadosEdicaoLogProd,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
	
						$valida = true;
					}
				}
			}
		}

		if($valida){
			?>
			<script type="text/javascript" >
				jQuery('#btnEditar'+$("#iptCodDoacaoprodutoPai").val()).click();
				jQuery(function(){
					$.gritter.add({
						title : "Alerta!",
						text : "Componente trocado com Sucesso!",
						class_name: "gritter-success",
					});
				})
				jQuery("#ModalbtnCancelar").click();
			</script>
			<?php
		}
	}


	public function modalEstoqueComponentePorEquipamento(){
		$this->load->view('estoque/estoque/triagem/VestoqueComponenteEquipamento');
	}

	

	public function EstoqueComponentePorEquipamento($tipoRetorno=1,$codproduto = 0,$etiqueta = 0){
		if($codproduto == 0){ $codproduto = $_POST['codproduto'];}
		if($etiqueta == 0){ $etiqueta = $_POST['etiqueta'];}
		if($this->validaEtiqueta($etiqueta) == 1){
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Erro enviar componente para estoque, Etiqueta informada já existe na base de dados!",
							class_name: "gritter-error",
						});
					})
			</script>
			<?php
			die();
		}
		$this->load->model("global/Mcrud","Mcrud");
		$validacao = false;
		
		$chavecom["CODCLIENTE"] = "CODCLIENTE";
		$chavecom["CODEMPRESA"] = "CODEMPRESA";
		$chavecom["CODFILIAL"] = "CODFILIAL";
		$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
		$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
		$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
		
		$dadosEdicaoProduto['CODTRSTATUS'] = 3;
		$dadosEdicaoProduto['ETIQUETA'] = $etiqueta;
		$dadosWhereUpProduto['CODPRODUTO'] = $codproduto;
		$this->Mcrud->setStrTable('trproduto');
		$retornoUp = $this->Mcrud->setDados($dadosEdicaoProduto,$chavecom,$arrValoresDaChave,$dadosWhereUpProduto,'CODPRODUTO');
		
		if($retornoUp != 0){
			$dadosEdicaoLogComp = $arrValoresDaChave;
			$dadosEdicaoLogComp['CODTRSTATUS'] = 3;
			$dadosEdicaoLogComp['CODPRODUTO'] = $codproduto;
			$this->Mcrud->setStrTable('trlogproduto');
			$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
			
			if($retornoLogCompProduto != 0){
				$dadosEdicaoCompProduto['CODPRODUTOFILHO'] = null;
				$dadosWhereUpCompPro['CODCOMPONENTESPRODUTO'] = $_POST['codcomponentesproduto'];
				$this->Mcrud->setStrTable('trcomponentesproduto');
				$retornoCompoProduto = $this->Mcrud->setDados($dadosEdicaoCompProduto,$chavecom,$arrValoresDaChave,$dadosWhereUpCompPro,'CODCOMPONENTESPRODUTO');
			
				if($retornoCompoProduto != 0){
					$validacao = true;
				}
			}
		}
		
		if($validacao){
			if($tipoRetorno == 1){
				?>
				<script type="text/javascript" >
					jQuery('#btnEditar'+$("#iptCodDoacaoprodutoPai").val()).click();
					jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Componente enviado para estoque com sucesso!",
							class_name: "gritter-success",
						});
					})
					jQuery("#ModalbtnCancelar").click();
				</script>
				<?php
			}else{
				return 1;
			}
		}else{
			if($tipoRetorno == 1){	
				?>
				<script type="text/javascript">
					jQuery(function(){
							$.gritter.add({
								title : "Alerta!",
								text : "Erro gerar estoque, gentileza entrar em contato com o administrador da rede!",
								class_name: "gritter-error",
							});
						})
					jQuery("#ModalbtnCancelar").click();
				</script>
				<?php
			}else{
				return 0;
			}
		}
	}




	public function enviarEstoqueEquipamento($tipoRetorno=1){	
		$this->load->model("global/Mcrud","Mcrud");
		$validacao = false;
		
		$chavecom["CODCLIENTE"] = "CODCLIENTE";
		$chavecom["CODEMPRESA"] = "CODEMPRESA";
		$chavecom["CODFILIAL"] = "CODFILIAL";
		$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
		$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
		$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
		
		$dadosEdicaoProduto['CODTRSTATUS'] = 3;
		$dadosWhereUpProduto['CODPRODUTO'] = $_POST['iptCodCodprodutoPai'];
		$this->Mcrud->setStrTable('trproduto');
		$retornoUp = $this->Mcrud->setDados($dadosEdicaoProduto,$chavecom,$arrValoresDaChave,$dadosWhereUpProduto,'CODPRODUTO');
		
		if($retornoUp != 0){
			$dadosEdicaoLogComp = $arrValoresDaChave;
			$dadosEdicaoLogComp['CODTRSTATUS'] = 3;
			$dadosEdicaoLogComp['CODPRODUTO'] = $_POST['iptCodCodprodutoPai'];
			$this->Mcrud->setStrTable('trlogproduto');
			$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
			
			if($retornoLogCompProduto != 0){
				$dadosEdicaoComp['QUANTIDADETRIADA'] = 1;
				$dadosEdicaoComp['CODTRSTATUS'] = 2;
				$dadosWhereUp['CODDOACAO'] = $_POST['iptCodDoacaoprodutoPai'];
				$this->Mcrud->setStrTable('trdoacao');
				$retornoDoacao = $this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,$dadosWhereUp,'CODDOACAO');
			
				if($retornoDoacao != 0){
					$validacao = true;
				}
			}
		}
		
		if($validacao){
			?>
			<script type="text/javascript" >
				jQuery('#btnEditar'+$("#iptCodDoacaoprodutoPai").val()).click();
				$("#Status<?=$_POST['iptCodDoacaoprodutoPai'];?>").text("Triagem Finalizada");
				$("#Qtd_Triado<?=$_POST['iptCodDoacaoprodutoPai'];?>").text("1");
				jQuery(function(){
					$.gritter.add({
						title : "Alerta!",
						text : "Equipamento enviado para estoque com sucesso!",
						class_name: "gritter-success",
					});
				})
			</script>
			<?php
		}else{	
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Erro gerar estoque, gentileza entrar em contato com o administrador da rede!",
							class_name: "gritter-error",
						});
					})
			</script>
			<?php
		}
	}


	public function sucataGeralEquipamento($tipoRetorno=1){	
		$this->load->model("global/Mcrud","Mcrud");
		$validacao = false;
		
		$chavecom["CODCLIENTE"] = "CODCLIENTE";
		$chavecom["CODEMPRESA"] = "CODEMPRESA";
		$chavecom["CODFILIAL"] = "CODFILIAL";
		$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
		$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
		$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
		
		$dadosEdicaoProduto['CODTRSTATUS'] = 4;
		$dadosWhereUpProduto['CODPRODUTO'] = $_POST['codproduto'];
		$this->Mcrud->setStrTable('trproduto');
		$retornoUp = $this->Mcrud->setDados($dadosEdicaoProduto,$chavecom,$arrValoresDaChave,$dadosWhereUpProduto,'CODPRODUTO');
		
		if($retornoUp != 0){
			$dadosEdicaoLogComp = $arrValoresDaChave;
			$dadosEdicaoLogComp['CODTRSTATUS'] = 4;
			$dadosEdicaoLogComp['CODPRODUTO'] = $_POST['codproduto'];
			$this->Mcrud->setStrTable('trlogproduto');
			$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
			
			if($retornoLogCompProduto != 0){
				$dadosEdicaoComp['QUANTIDADETRIADA'] = 1;
				$dadosEdicaoComp['CODTRSTATUS'] = 2;
				$dadosWhereUp['CODDOACAO'] = $_POST['coddoacao'];
				$this->Mcrud->setStrTable('trdoacao');
				$retornoDoacao = $this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,$dadosWhereUp,'CODDOACAO');
				if($retornoDoacao != 0){
					$validacao = true;
				}
			}
		}
		
		if($validacao){
			?>
			<script type="text/javascript" >
				jQuery('#btnEditar'+$("#iptCodDoacaoprodutoPai").val()).click();
				$("#Status<?=$_POST['coddoacao'];?>").text("Triagem Finalizada");
				$("#Qtd_Triado<?=$_POST['coddoacao'];?>").text("1");
				jQuery(function(){
					$.gritter.add({
						title : "Alerta!",
						text : "Equipamento enviado para a sucata com sucesso!",
						class_name: "gritter-success",
					});
				})
			</script>
			<?php
		}else{	
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Erro gerar sucata do Equipamento, gentileza entrar em contato com o administrador da rede!",
							class_name: "gritter-error",
						});
					})
			</script>
			<?php
		}
	}

	public function modalvendaComponente(){
		$this->load->view('estoque/estoque/triagem/VmodalVendaComponente');
	}

	public function vendaComponente($tipoRetorno=1){
		if(isset($_POST['txttipoentradaVendaCtriagemComponente'])){ $tipoRetorno = $_POST['txttipoentradaVendaCtriagemComponente']; }	
		$this->load->model("global/Mcrud","Mcrud");
		$validacao = false;
		
		$chavecom["CODCLIENTE"] = "CODCLIENTE";
		$chavecom["CODEMPRESA"] = "CODEMPRESA";
		$chavecom["CODFILIAL"] = "CODFILIAL";
		$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
		$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
		$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
		
		$dadosEdicaoProduto['CODTRSTATUS'] = 9;
		$dadosWhereUpProduto['CODPRODUTO'] = $_POST['txtcodprodutoVendaCtriagemComponente'];
		$this->Mcrud->setStrTable('trproduto');
		$retornoUp = $this->Mcrud->setDados($dadosEdicaoProduto,$chavecom,$arrValoresDaChave,$dadosWhereUpProduto,'CODPRODUTO');
		
		if($retornoUp != 0){
			$dadosEdicaoLogComp = $arrValoresDaChave;
			$dadosEdicaoLogComp['CODTRSTATUS'] = 9;
			$dadosEdicaoLogComp['CODPRODUTO'] = $_POST['txtcodprodutoVendaCtriagemComponente'];
			$this->Mcrud->setStrTable('trlogproduto');
			$retornoLogCompProduto = $this->Mcrud->setDados($dadosEdicaoLogComp,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
			
			if($retornoLogCompProduto != 0){
				$dadosEdicaoLanc = $arrValoresDaChave;
				$dadosEdicaoLanc['CODFISTATUS'] = 1;
				$dadosEdicaoLanc['CODPRODUTO'] = $_POST['txtcodprodutoVendaCtriagemComponente'];
				$dadosEdicaoLanc['CODTIPOLANCAMENTO'] = 2;
				$dadosEdicaoLanc['DATAEMISSAO'] = date("Y-m-d");
				$dadosEdicaoLanc['LOJA'] = $_POST['txtidlojaVendaCtriagemComponente'];
				$dadosEdicaoLanc['IDLOJA'] = $_POST['txtlojaVendaCtriagemComponente'];
				$dadosEdicaoLanc['VALORORIGINAL'] = $_POST['txtvalorVendaCtriagemComponente'];
				$this->Mcrud->setStrTable('filancamento');
				$retornoLanc = $this->Mcrud->setDados($dadosEdicaoLanc,$chavecom,$arrValoresDaChave,null,'CODLANCAMENTO');
				
				if($retornoLanc != 0){
					$validacao = true;	
				}
			}
		}
		
		if($validacao){
			if($tipoRetorno == 1){
				?>
				<script type="text/javascript" >
					$("#tdstatuscomponente<?=$_POST['txtcodprodutoVendaCtriagemComponente'];?>").text("Enviado para Venda");
					$("#btVendaVtriagemComponente<?=$_POST['txtcodprodutoVendaCtriagemComponente'];?>").remove();
					jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Componente enviado para a venda com sucesso!",
							class_name: "gritter-success",
						});
					})
				</script>
				<?php
			}else{
				?>
				<script type="text/javascript" >
					$("#trVestoqueComponenteLista<?=$_POST['txtcodprodutoVendaCtriagemComponente'];?>").remove();
					jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Componente enviado para a venda com sucesso!",
							class_name: "gritter-success",
						});
					})
				</script>
				<?php
			}
		}else{	
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Erro gerar venda do componente, gentileza entrar em contato com o administrador da rede!",
							class_name: "gritter-error",
						});
					})
				jQuery("#ModalbtnCancelar").click();
			</script>
			<?php
		}
	}


	public function validaEtiqueta($etiqueta){
		$validaEtiqueta = "SELECT TP.ETIQUETA
							FROM trproduto	TP
							WHERE TP.CODCLIENTE = ?
								AND TP.CODEMPRESA = ?
							    AND TP.CODFILIAL = ?
							    AND TP.ETIQUETA = ?";
		$arrDados = array(
			$_SESSION['codcliente'],
			$_SESSION['codempresa'],
			$_SESSION['codfilial'],
			$etiqueta
		);
		$dadosSql = $this->db->query($validaEtiqueta,$arrDados);
		$existeEtiqueta = $dadosSql->result_array();
		if (count( $existeEtiqueta ) > 0){
			return 1;
		}else{
			return 2;
		}
	}
}