<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Ctriagem extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("trtipo");
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'])
								);
		$arrDadosTipoEntrada = $this->Mcrud->getDados($arrDataFornecedor);
		$arrDadosTipoEntradaTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratados[$row->CODTIPO] = $row->DESCRICAO;
		}
		
		$this->Mcrud->setStrTable("fifornecedor");
		$arrDataFornecedor = array("cabecalho" => array("fifornecedor.CODFORNECEDOR" => "CODFORNECEDOR",
													 "fifornecedor.NOMEFANTASIA" => "NOMEFANTASIA"),
							   "where" => array("fifornecedor.CODCLIENTE" => $_SESSION['codcliente'],
							   					"fifornecedor.CLIENTE" => 0),
							   "order" => array("fifornecedor.NOMEFANTASIA" => "ASC")
								);
		$arrDadosFornecedor = $this->Mcrud->getDados($arrDataFornecedor);
		$arrDadosFornecedorTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosFornecedor->result() as $row){
		    $arrDadosFornecedorTratados[$row->CODFORNECEDOR] = $row->NOMEFANTASIA;
		}
		
		$this->Mcrud->setStrTable("trstatus");
		$arrDataStatus = array("cabecalho" => array("trstatus.CODTRSTATUS" => "CODTRSTATUS",
													 "trstatus.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trstatus.TIPO" => 1),
							   "order" => array("trstatus.DESCRICAO" => "ASC")
								);
		$arrDadosStatus = $this->Mcrud->getDados($arrDataStatus);
		$arrDadosStatusTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosStatus->result() as $row){
		    $arrDadosStatusTratados[$row->CODTRSTATUS] = $row->DESCRICAO;
		}
		$this->Mcrud->setStrTable(null);	
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Triagem",
						"classe" => "Ctriagem",
						"precaminho" => "estoque/estoque/triagem/",
						"autoinc" => "sim",
						"autolista" => false,
						"divcomplementar" => "divCarregarCorpoEdicaoCtriagem",
						"data-toggle" => false,
						"novoeditar" => "estoque/estoque/triagem/CtriagemComponente/index",
						"codigo" => "trdoacao.CODTIPO",
				),
				"botoes" => array(
								"excluir" => "inativo",
								"novo" => "inativo",
							),
				"chave" => array (
						"trdoacao.CODCLIENTE" => "CODCLIENTE",
						"trdoacao.CODEMPRESA" => "CODEMPRESA",
						"trdoacao.CODFILIAL" => "CODFILIAL",
				),
				"tabela" => array (
						"cabecalho" => array (
								"trdoacao.CODDOACAO" => "Codigo",
								"FO.NOMEFANTASIA" => "Fornecedor",
								"trdoacao.DESCRICAO" => "Descrição",
								"TRT.DESCRICAO" => "Tipo Doação",
								"TRS.DESCRICAO" => "Status",
								"trdoacao.QUANTIDADE" => "Quantidade",
								"trdoacao.QUANTIDADETRIADA" => "Qtd Triado",
						),
						"join" => array (
								"trstatus AS TRS" => "TRS.CODTRSTATUS = trdoacao.CODTRSTATUS",
								"trtipo AS TRT" => "TRT.CODCLIENTE = trdoacao.CODCLIENTE AND TRT.CODTIPO = trdoacao.CODTIPO",
								"fifornecedor AS FO" => "FO.CODCLIENTE = trdoacao.CODCLIENTE AND FO.CODFORNECEDOR = trdoacao.CODFORNECEDOR"
						)
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCtriagem",
						"method" => "post",
						"action" => "estoque/cadastros/tipo/Ctriagem/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"trdoacao.CODTIPO" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "codtipoCtriagem",
										"disabled" => true,
								),
								"trdoacao.CODFORNECEDOR" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Fornecedor:",
										"idname" => "selFornecedorCtriagem",
										"required" => true,
										"values" => $arrDadosFornecedorTratados
								),
								"trdoacao.CODTIPOENTRADA" => array(
											  "tipo" => "select",
											  "filtro" => true,
											  "label" => "Tipo:",
											  "idname" => "selTipoEntradaCtriagem",
											  "required" => true,
											  "values" => array(
															"" => "Escolha um valor!",
														    "1" => "Componente",
														    "2" => "Equipamento"
														   )
								),
								"trdoacao.CODTIPO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Doação:",
										"idname" => "selTipoDoacaoCtriagem",
										"values" => $arrDadosTipoEntradaTratados
								),
								"trdoacao.CODTRSTATUS" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Status:",
										"idname" => "selStatusCtriagem",
										"required" => true,
										"values" => $arrDadosStatusTratados
								),
						) 
				),
				"funcao_js" => "assets/js/funcoes/estoque/estoque/triagem/funcoesTriagem.js",
		);
		
		$this->strTabela = "trdoacao";
	}
}

?>