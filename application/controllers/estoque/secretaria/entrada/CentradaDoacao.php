<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class CentradaDoacao extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("fifornecedor");
		$arrDataFornecedor = array("cabecalho" => array("fifornecedor.CODFORNECEDOR" => "CODFORNECEDOR",
													 "fifornecedor.NOMEFANTASIA" => "NOMEFANTASIA"),
							   "where" => array("fifornecedor.CODCLIENTE" => $_SESSION['codcliente'],
							   					"fifornecedor.CLIENTE" => 0),
							   "order" => array("fifornecedor.NOMEFANTASIA" => "ASC")
								);
		$arrDadosFornecedor = $this->Mcrud->getDados($arrDataFornecedor);
		$arrDadosFornecedorTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosFornecedor->result() as $row){
		    $arrDadosFornecedorTratados[$row->CODFORNECEDOR] = $row->NOMEFANTASIA;
		}
		$this->Mcrud->setStrTable(null);			
		
		
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Entrada de doação na secretaria",
						"classe" => "CentradaDoacao",
						"precaminho" => "estoque/secretaria/entrada/",
						"autoinc" => "sim",
						"autolista" => false,
						"codigo" => "trdoacao.CODDOACAO" 
				),
				"chave" => array (
						"trdoacao.CODCLIENTE" => "CODCLIENTE",
						"trdoacao.CODEMPRESA" => "CODEMPRESA",
						"trdoacao.CODFILIAL" => "CODFILIAL",
				),
				"botoes" => array(
								"excluir" => "inativo",
							),
				"tabela" => array (
						"cabecalho" => array (
								"trdoacao.CODDOACAO" => "Codigo",
								"trtipo.DESCRICAO" => "Tipo",
								"trdoacao.DESCRICAO" => "Descrição",
								"fifornecedor.NOMEFANTASIA" => "Doador",
								"trdoacao.Quantidade" => "Quantidade",
						),
						"join" => array (
								"trtipo" => "trtipo.CODCLIENTE = trdoacao.CODCLIENTE AND trtipo.CODTIPO = trdoacao.CODTIPO",
								"fifornecedor" => "fifornecedor.CODCLIENTE = trdoacao.CODCLIENTE AND fifornecedor.CODFORNECEDOR = trdoacao.CODFORNECEDOR",
						),
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCentradaDoacao",
						"method" => "post",
						"action" => "estoque/secretaria/entrada/CentradaDoacao/salvaEntradaDoacao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"trdoacao.CODDOACAO" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "coddoacaoCentradaDoacao",
										"disabled" => true,
								),
								"trdoacao.CODFORNECEDOR" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Fornecedor:",
										"idname" => "selFornecedorCentradaDoacao",
										"required" => true,
										"values" => $arrDadosFornecedorTratados
								),
								"trdoacao.DESCRICAO" => array (
										"tipo" => "label",
										"label" => "Descrição:",
										"idname" => "descricaoCentradaDoacao",
										"required" => true,
								),
								"trdoacao.CODTIPOENTRADA" => array (
										"tipo" => "radio-inline",
										"label" => "Tipo Entrada:",
										"idname" => "tipoEntradaCentradaDoacao",
										"required" => true,
										"onclick" =>  "buscaCamposCentradaDoacao",
										"values" => array("1" => "Componente",
														  "2" => "Equipamento")
								),
								"div_retorno_campos" => array(
									"tipo" => "div",
									"ativo_salvar" => false,
									"idname" => "divRetornoCamposCentradaDoacao"
								)
						) 
				),
				"funcao_js" => "assets/js/funcoes/estoque/secretaria/entrada/funcoesEntradaDoacao.js",
				"camposCompForm" => array("tipoEntradaCentradaDoacao_1" => array(
													"trdoacao.CODTIPO" => array("tipo" => "select",
																				"label" => "Tipo Doação:",
																				"idname" => "selTipoDoacaoCentradaDoacao",
																				"required" => true,
																				),
													"trdoacao.QUANTIDADE" => array("tipo" => "label",
																					"label" => "Quantidade:",
																					"idname" => "quantidadeCentradaDoacao",
																					"required" => true,
																					"required-type" => "number",
																				),
													"trdoacao.CODLOTE" => array("tipo" => "label",
																				"label"=> "lote:",
																				"idname" => "LoteCentradaDoacao",
																				"disabled" => true,
																				),
										  			),
										  "tipoEntradaCentradaDoacao_2" => array(
													"trdoacao.CODTIPO" => array("tipo" => "select",
																				"label" => "Tipo Doação:",
																				"idname" => "selTipoDoacaoCentradaDoacao",
																				"required" => true,
																				),
													"trdoacao.QUANTIDADE" => array("tipo" => "label",
																					"label" => "Quantidade:",
																					"idname" => "quantidadeCentradaDoacao",
																					"ativo_salvar" => false,
																					"disabled" => true,
																				),
													"trdoacao.ETIQUETA" => array("tipo" => "label",
																				"label"=> "Etiqueta:",
																				"idname" => "EtiquetaCentradaDoacao",
																				"required" => true,
																				"required-type" => "number"
																				),
										  			),
										  	),
				"valida_dependencia_fk" =>array("valida" => true,
												"campovalidar" => "tipoEntradaCentradaDoacao_1",
												),
				"dependencia_fk" => array("trlote.CODLOTE" => array(							
														"chave" => array (
																	"trlote.CODCLIENTE" => "CODCLIENTE",
														),
														"tabela" => "trlote",
														"dadosEditar" => array("trlote.CODLOTE" => "CODLOTE",
																			   "trlote.CODFORNECEDOR" => "CODFORNECEDOR",
																			   "trlote.CODTIPO" => "CODTIPO"
																				)
													),
										  ),
		);
		
		$this->strTabela = "trdoacao";
	}
	
	
	public function buscaCamposEntradaDoacao(){
		//$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("trtipo");
		$arrDataTipoEntrada = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'],
							   					"trtipo.TIPOENTRADA" => $_POST['tipoEntradaCentradaDoacao'])
								);
		$arrDadosTipoEntrada = $this->Mcrud->getDados($arrDataTipoEntrada);
		$arrDadosTipoEntradaTratadosPai = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratadosPai[$row->CODTIPO] = $row->DESCRICAO;
		}

		if(isset($_POST['tipoEntradaCentradaDoacao']) && $_POST['tipoEntradaCentradaDoacao'] == 1){
			$this->data["camposCompForm"]["tipoEntradaCentradaDoacao_1"]["trdoacao.CODTIPO"]["values"] = $arrDadosTipoEntradaTratadosPai;
			$campos1["campos"] = $this->data["camposCompForm"]["tipoEntradaCentradaDoacao_1"];
			
			$this->load->view('global/crud/VComponentesFormulario',$campos1);
			
		}else if (isset($_POST['tipoEntradaCentradaDoacao']) && $_POST['tipoEntradaCentradaDoacao'] == 2){					
			$this->data["camposCompForm"]["tipoEntradaCentradaDoacao_2"]["trdoacao.CODTIPO"]["values"] = $arrDadosTipoEntradaTratadosPai;
			$campos2["campos"] = $this->data["camposCompForm"]["tipoEntradaCentradaDoacao_2"];
			
			$this->load->view('global/crud/VComponentesFormulario',$campos2);
			
		}else{
			return "Erro carregar o restante do formulário!";
		}
	}


	public function salvaEntradaDoacao(){
		if(isset($_POST['tipoEntradaCentradaDoacao']) && $_POST['tipoEntradaCentradaDoacao'] == 2){
			$validaEtiqueta = "SELECT TD.ETIQUETA 
								FROM trdoacao TD
								WHERE TD.CODCLIENTE = ?
									AND TD.CODEMPRESA = ?
								    AND TD.CODFILIAL = ?
									AND TD.ETIQUETA = ?
								UNION ALL
								SELECT TP.ETIQUETA
								FROM trproduto	TP
								WHERE TP.CODCLIENTE = ?
									AND TP.CODEMPRESA = ?
								    AND TP.CODFILIAL = ?
								    AND TP.ETIQUETA = ?";
			$arrDados = array(
				$_SESSION['codcliente'],
				$_SESSION['codempresa'],
				$_SESSION['codfilial'],
				$_POST['EtiquetaCentradaDoacao'],
				$_SESSION['codcliente'],
				$_SESSION['codempresa'],
				$_SESSION['codfilial'],
				$_POST['EtiquetaCentradaDoacao']
			);
			$dadosSql = $this->db->query($validaEtiqueta,$arrDados);
			$existeEtiqueta = $dadosSql->result_array();
			if (count( $existeEtiqueta ) > 0){
				?>
					<script type="text/javascript">
						jQuery(function(){
								$.gritter.add({
									title : "Alerta!",
									text : "Erro cadastrar entrada de doação, Etiqueta informada já existe na base de dados!",
									class_name: "gritter-error",
								});
							})
					</script>
				<?php
				die();
			}
		}
		$validacao = false;
		$coddoacao = $this->salvaEdicao(2);
		
		if($coddoacao != 0){
			if(isset($_POST['tipoEntradaCentradaDoacao']) && $_POST['tipoEntradaCentradaDoacao'] == 2){
				$chavecom["CODCLIENTE"] = "CODCLIENTE";
				$chavecom["CODEMPRESA"] = "CODEMPRESA";
				$chavecom["CODFILIAL"] = "CODFILIAL";
				$arrValoresDaChave['CODCLIENTE'] = $_SESSION['codcliente'];
				$arrValoresDaChave['CODEMPRESA'] = $_SESSION['codempresa'];
				$arrValoresDaChave['CODFILIAL'] = $_SESSION['codfilial'];
				
				$dadosEdicaoProd = $arrValoresDaChave;
				$dadosEdicaoProd['CODTRSTATUS'] = 1;
				$dadosEdicaoProd['CODDOACAO'] = $coddoacao;
				$dadosEdicaoProd['CODTIPO'] = $_POST['selTipoDoacaoCentradaDoacao'];
				$dadosEdicaoProd['ETIQUETA'] = $_POST['EtiquetaCentradaDoacao'];
				$this->Mcrud->setStrTable('trproduto');
				$retornoProduto = $this->Mcrud->setDados($dadosEdicaoProd,$chavecom,$arrValoresDaChave,null,'CODPRODUTO');
				
				if($retornoProduto != 0){
					$validacao = true;
					
					$dadosEdicaoComp = $arrValoresDaChave;
					$dadosEdicaoComp['CODPRODUTO'] = $retornoProduto;
					$dadosEdicaoComp['CODTRSTATUS'] = 1;
					$this->Mcrud->setStrTable('trlogproduto');
					$this->Mcrud->setDados($dadosEdicaoComp,$chavecom,$arrValoresDaChave,null,'CODLOGPRODUTO');
				}else{
					$validacao = false;
				}
			}else{
				$validacao = true;
			}
		}

		if($validacao){
			$mgs = '';
			if(isset($_POST['tipoEntradaCentradaDoacao']) && $_POST['tipoEntradaCentradaDoacao'] == 2){
				$mgs = "Entrada de doação salvo com sucesso. <br /><br /><h4><b>ETIQUETA: ".$_POST['EtiquetaCentradaDoacao']."</b></h4";
			}else{
				$mgs = "Entrada de doação salvo com sucesso. <br /><br /><h4><b>LOTE DOAÇÃO: ".$coddoacao."</b></h4>";
			}
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "<?=$mgs;?>",
							class_name: "gritter-success",
							time: 200000
						});
					})
			</script>
			<?php
		}else{
			//$arrValoresDaChave['CODDOACAO'] = $coddoacao;
			//$this->excluirInserErrado($arrValoresDaChave,"trdoacao");
			?>
			<script type="text/javascript">
				jQuery(function(){
						$.gritter.add({
							title : "Alerta!",
							text : "Erro cadastrar entrada de doação, gentileza entrar em contato com o administrador da rede!",
							class_name: "gritter-error",
						});
					})
			</script>
		<?php
		}
	}

}

?>