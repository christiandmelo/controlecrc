<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Ctipo extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Tipos de Doação",
						"classe" => "Ctipo",
						"precaminho" => "estoque/cadastros/tipo/",
						"autoinc" => "sim",
						"codigo" => "trtipo.CODTIPO" 
				),
				"botoes" => array(
								"excluir" => "inativo",
							),
				"chave" => array (
						"trtipo.CODCLIENTE" => "CODCLIENTE",
				),
				"tabela" => array (
						"cabecalho" => array (
								"trtipo.CODTIPO" => "Codigo",
								"trtipo.DESCRICAO" => "Nome",
								"glativo.DESCRICAO" => "Status",
						),
						"join" => array (
								"glativo" => "glativo.CODATIVO = trtipo.ATIVO"
						)
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCtipo",
						"method" => "post",
						"action" => "estoque/cadastros/tipo/Ctipo/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"trtipo.CODTIPO" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "codtipoCtipo",
										"disabled" => true,
								),
								"trtipo.DESCRICAO" => array (
										"tipo" => "label",
										"label" => "Nome:",
										"idname" => "nomeCtipo",
										"filtro" => true,
										"required" => true,
								),
								"trtipo.TIPOENTRADA" => array(
											  "tipo" => "select",
											  "label" => "Tipo:",
											  "idname" => "selTipoEntradaCtipo",
											  "required" => true,
											  "values" => array(
															"" => "Escolha um valor!",
														    "1" => "Componente",
														    "2" => "Equipamento"
														   )
								),
								"trtipo.PRODUTO" => 
										array(
											  "tipo" => "select",
											  "label" => "Pai Locação:",
											  "idname" => "selProdutoPRDCtipo",
											  "values" => array(
															"0" => "Não",
														    "1" => "Sim",
														   )
								),
								"trtipo.COMPONENTEPRODUTO" => 
										array(
											  "tipo" => "select",
											  "label" => "Compõe produto:",
											  "idname" => "selComponentePRDCtipo",
											  "values" => array(
															"0" => "Não",
														    "1" => "Sim",
														   )
								),
								"trtipo.COMPONENTELOCACAO" => 
										array(
											  "tipo" => "select",
											  "label" => "Compõe locação:",
											  "idname" => "selLocacaoPRDCtipo",
											  "values" => array(
															"0" => "Não",
														    "1" => "Sim",
														   )
								),
								"trtipo.ATIVO" => 
										array(
											  "tipo" => "select",
											  "label" => "Ativo:",
											  "idname" => "selAtivoPRDCtipo",
											  "values" => array(
															"1" => "Ativo",
														    "0" => "Inativo",
														   )
								),
						) 
				) 
		);
		
		$this->strTabela = "trtipo";
	}
}

?>