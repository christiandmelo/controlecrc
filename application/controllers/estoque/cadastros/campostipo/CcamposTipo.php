<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class CcamposTipo extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("trtipo");
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'])
								);
		$arrDadosTipoEntrada = $this->Mcrud->getDados($arrDataFornecedor);
		$arrDadosTipoEntradaTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratados[$row->CODTIPO] = $row->DESCRICAO;
		}

		$this->Mcrud->setStrTable(null);	
		
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Campos para cada tipo de doação",
						"classe" => "CcamposTipo",
						"precaminho" => "estoque/cadastros/campostipo/",
						"autoinc" => "sim",
						"codigo" => "trcampostipo.CODCAMPOSTIPO" 
				),
				"chave" => array (
						"trcampostipo.CODCLIENTE" => "CODCLIENTE",
				),
				"tabela" => array (
						"cabecalho" => array (
								"trcampostipo.CODCAMPOSTIPO" => "Codigo",
								"trtipo.DESCRICAO" => "Tipo",
								"trcampostipo.NOME" => "Nome",
								"glativo.DESCRICAO" => "SITUAÇÃO",
						),
						"join" => array("glativo" => "glativo.CODATIVO = trcampostipo.ATIVO",
										"trtipo" => "trcampostipo.CODCLIENTE = trtipo.CODCLIENTE AND trtipo.CODTIPO= trcampostipo.CODTIPO"
											  				 ), 
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCcamposTipo",
						"method" => "post",
						"action" => "estoque/cadastros/campostipo/CcamposTipo/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"trcampostipo.CODCAMPOSTIPO" => array (
										"tipo" => "label",
										"label" => "Código",
										"idname" => "codcampostipoCcamposTipo",
										"disabled" => true,
								),
								"trcampostipo.CODTIPO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Entrada",
										"idname" => "selTipoCdoacao",
										"required" => true,
										"values" => $arrDadosTipoEntradaTratados
													),
								"trcampostipo.NOME" => array (
										"tipo" => "label",
										"label" => "Nome",
										"idname" => "nomeCcamposTipo",
										"filtro" => true,
										"required" => true,
								),
								"trcampostipo.ATIVO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Situação",
																		"idname" => "selSituacaoCcamposTipo",
																		"required" => true,
																		"values" => array("1" => "Ativo",
																						  "0" => "Inativo"
																						  )
													),
						) 
				) 
		);
		
		$this->strTabela = "trcampostipo";
	}
}

?>