<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class CcomponentesLocacao extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("trtipo");
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'],
							   					"trtipo.PRODUTO" => "1")
								);
		$arrDadosTipoEntrada = $this->Mcrud->getDados($arrDataFornecedor);
		$arrDadosTipoEntradaTratadosPai = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratadosPai[$row->CODTIPO] = $row->DESCRICAO;
		}
		
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'],
							   					"trtipo.COMPONENTELOCACAO" => "1")
								);
		$arrDadosTipoEntrada = $this->Mcrud->getDados($arrDataFornecedor);
		$arrDadosTipoEntradaTratadosFilho = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratadosFilho[$row->CODTIPO] = $row->DESCRICAO;
		}

		$this->Mcrud->setStrTable(null);	
		
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Componentes de um produto para locação",
						"classe" => "CcomponentesLocacao",
						"precaminho" => "estoque/cadastros/componenteslocacao/",
						"autoinc" => "sim",
						"codigo" => "trcomponenteslocacao.CODCOMPONENTESLOCACAO" 
				),
				"chave" => array (
						"trcomponenteslocacao.CODCLIENTE" => "CODCLIENTE",
				),
				"tabela" => array (
						"cabecalho" => array (
								"trcomponenteslocacao.CODCOMPONENTESLOCACAO" => "Codigo",
								"trtipopai.DESCRICAO" => "Tipo Pai",
								"trtipofilho.DESCRICAO" => "Tipo Filho",
								"glativo.DESCRICAO" => "SITUAÇÃO",
						),
						"join" => array("glativo" => "glativo.CODATIVO = trcomponenteslocacao.ATIVO",
										"trtipo as trtipopai" => "trtipopai.CODCLIENTE = trcomponenteslocacao.CODCLIENTE AND trtipopai.CODTIPO = trcomponenteslocacao.CODTIPOPAI",
										"trtipo as trtipofilho" => "trtipofilho.CODCLIENTE = trcomponenteslocacao.CODCLIENTE AND trtipofilho.CODTIPO = trcomponenteslocacao.CODTIPOFILHO"
										), 
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCcomponentesLocacao",
						"method" => "post",
						"action" => "estoque/cadastros/componenteslocacao/CcomponentesLocacao/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"trcomponenteslocacao.CODCOMPONENTESLOCACAO" => array (
										"tipo" => "label",
										"label" => "Código",
										"idname" => "codComponentesLocacaoCcomponentesLocacao",
										"disabled" => true,
								),
								"trcomponenteslocacao.CODTIPOPAI" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Entrada Pai",
										"idname" => "selTipoPaiCdoacao",
										"required" => true,
										"values" => $arrDadosTipoEntradaTratadosPai
								),
								"trcomponenteslocacao.CODTIPOFILHO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Entrada Filho",
										"idname" => "selTipoFilhoCdoacao",
										"required" => true,
										"values" => $arrDadosTipoEntradaTratadosFilho
								),
								"trcomponenteslocacao.ATIVO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Situação",
																		"idname" => "selSituacaoCcomponentesLocacao",
																		"required" => true,
																		"values" => array("1" => "Ativo",
																						  "0" => "Inativo"
																						  )
								),
								"trcomponenteslocacao.OBRIGATORIO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Obrigatorio Cadastro",
																		"idname" => "selObrigatorioCadastroCcomponentesLocacao",
																		"required" => true,
																		"values" => array("0" => "Não",
																						  "1" => "Sim"
																						  )
								),
						) 
				) 
		);
		
		$this->strTabela = "trcomponenteslocacao";
	}
}

?>