<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class CcomponentesTipo extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("trtipo");
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'],
							   					"trtipo.COMPONENTEPRODUTO" => "0")
								);
		$arrDadosTipoEntrada = $this->Mcrud->getDados($arrDataFornecedor);
		$arrDadosTipoEntradaTratadosPai = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratadosPai[$row->CODTIPO] = $row->DESCRICAO;
		}
		
		$arrDataFornecedor = array("cabecalho" => array("trtipo.CODTIPO" => "CODTIPO",
													 "trtipo.DESCRICAO" => "DESCRICAO"),
							   "where" => array("trtipo.CODCLIENTE" => $_SESSION['codcliente'],
							   					"trtipo.COMPONENTEPRODUTO" => "1")
								);
		$arrDadosTipoEntrada = $this->Mcrud->getDados($arrDataFornecedor);
		$arrDadosTipoEntradaTratadosFilho = array ("" => "Escolha um valor!");
		foreach ($arrDadosTipoEntrada->result() as $row){
		    $arrDadosTipoEntradaTratadosFilho[$row->CODTIPO] = $row->DESCRICAO;
		}

		$this->Mcrud->setStrTable(null);	
		
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Componentes que compõem um tipo de doação",
						"classe" => "CcomponentesTipo",
						"precaminho" => "estoque/cadastros/componentestipo/",
						"autoinc" => "sim",
						"codigo" => "trcomponentestipo.CODCOMPONENTESTIPO" 
				),
				"chave" => array (
						"trcomponentestipo.CODCLIENTE" => "CODCLIENTE",
				),
				"tabela" => array (
						"cabecalho" => array (
								"trcomponentestipo.CODCOMPONENTESTIPO" => "Codigo",
								"trtipopai.DESCRICAO" => "Tipo Pai",
								"trtipofilho.DESCRICAO" => "Tipo Filho",
								"glativo.DESCRICAO" => "SITUAÇÃO",
						),
						"join" => array("glativo" => "glativo.CODATIVO = trcomponentestipo.ATIVO",
										"trtipo as trtipopai" => "trtipopai.CODCLIENTE = trcomponentestipo.CODCLIENTE AND trtipopai.CODTIPO = trcomponentestipo.CODTIPOPAI",
										"trtipo as trtipofilho" => "trtipofilho.CODCLIENTE = trcomponentestipo.CODCLIENTE AND trtipofilho.CODTIPO = trcomponentestipo.CODTIPOFILHO"
										), 
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCcomponentesTipo",
						"method" => "post",
						"action" => "estoque/cadastros/componentestipo/CcomponentesTipo/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"trcomponentestipo.CODCOMPONENTESTIPO" => array (
										"tipo" => "label",
										"label" => "Código",
										"idname" => "codComponentesTipoCcomponentesTipo",
										"disabled" => true,
								),
								"trcomponentestipo.CODTIPOPAI" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Entrada Pai",
										"idname" => "selTipoPaiCdoacao",
										"required" => true,
										"values" => $arrDadosTipoEntradaTratadosPai
								),
								"trcomponentestipo.CODTIPOFILHO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tipo Entrada Filho",
										"idname" => "selTipoFilhoCdoacao",
										"required" => true,
										"values" => $arrDadosTipoEntradaTratadosFilho
								),
								"trcomponentestipo.ATIVO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Situação",
																		"idname" => "selSituacaoCcomponentesTipo",
																		"required" => true,
																		"values" => array("1" => "Ativo",
																						  "0" => "Inativo"
																						  )
								),
								"trcomponentestipo.OBRIGATORIO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Obrigatorio Cadastro",
																		"idname" => "selObrigatorioCadastroCcomponentesTipo",
																		"required" => true,
																		"values" => array("0" => "Não",
																						  "1" => "Sim"
																						  )
								),
						) 
				) 
		);
		
		$this->strTabela = "trcomponentestipo";
	}
}

?>