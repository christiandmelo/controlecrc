<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Mcrud.
 * Classe para facilizar as ações de CRUD do sistema.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */

class Cempresas extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		/*Bloco para trazer todos os estados e carregar na tela*/
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("glestado");
		$arrDataEstados = array("cabecalho" => array("glestado.CODESTADO" => "CODESTADO",
													 "glestado.CODETD" => "CODETD")
								);
		$arrDadosEstados = $this->Mcrud->getDados($arrDataEstados);
		$arrDadosEstadosTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosEstados->result() as $row){
		    $arrDadosEstadosTratados = array($row->CODESTADO => $row->CODETD);
		}
		$this->Mcrud->setStrTable(null);
		
		
		/*inicio do array de configuração*/
		$this->data = array("tipo" => "CRUD",
							"lista" => array("Titulo" => "Empresas",
											 "classe" => "cempresas",
											 "precaminho" => "global/administracao/empresas/",
											 "autoinc" => "sim",
											 "chave" => "glempresa.CODEMPRESA",
											 ),
							"chave" => array("glempresa.CODEMPRESA" => "CODEMPRESA",
											 "glempresa.CODCLIENTE" => "CODCLIENTE",
											),
							"tabela" => array("cabecalho" => array( "glempresa.CODEMPRESA" => "Cod empresa",
														 			"glempresa.CODCLIENTE" => "Cod cliente",
														 			"glempresa.NOME" => "Nome",
																	),
											  //"join" => array("glempresa" => "glempresa.CODCLIENTE = glfilial.CODCLIENTE and glempresa.CODEMPRESA = glfilial.CODEMPRESA")
											 ),
							"edita" => array("cabecalho" => array("glempresa.CODEMPRESA" => "CODEMPRESA",
														 		  "glempresa.CODCLIENTE" => "CODCLIENTE",
														 		  "glempresa.NOME" => "NOME",
													 		 	  "glempresa.NOMEFANTASIA" => "NOMEFANTASIA",
													 		 	  "TELEFONE" => "TELEFONE",
													 		 	  "EMAIL" => "EMAIL",
													 		 	  "CGC" => "CGC",
													 		 	  "INSCRICAOESTADUAL" => "INSCRICAOESTADUAL",
													 		 	  "RUA" => "RUA",
													 		 	  "NUMERO" => "NUMERO",
													 		 	  "COMPLEMENTO" => "COMPLEMENTO",
													 		 	  "BAIRRO" => "BAIRRO"
																  ),
											 ),							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovocfiliais",
													 "method" => "post",
													 "action" => "global/administracao/empresas/cempresas/salvaEdicao"
													 ),
							"form" => array("Identificação" => array("glempresa.CODEMPRESA" => array(	"tipo" => "label",
																				"label" => "Código da empresa",
																				"idname" => "txtempresa",
																				"placeholder" => "",
																				"disabled" => true
																				),
												"glempresa.CODCLIENTE" => array(	"tipo" => "label",
																				"label" => "Código do cliente",
																				"idname" => "txtcliente",
																				"placeholder" => "",
																				"disabled" => true
																				),
												"glempresa.NOME" => array(	"tipo" => "label",
																				//"filtro" => true,
																				"label" => "Nome",
																				"idname" => "txtclientenome",
																				"placeholder" => "",
																				),
												"glempresa.NOMEFANTASIA" => array(	"tipo" => "label",
																				//"filtro" => true,
																				"label" => "Nome Fántasia",
																				"idname" => "txtclientefantasia",
																				"placeholder" => "",
																				"required" => true,
																				),
												"glempresa.CGC" => array(	"tipo" => "label",
																				//"filtro" => true,
																				"label" => "CNPJ / CPF / CEI",
																				"idname" => "txtclienteCGC",
																				"placeholder" => "",
																				"required" => true,
																				),
												"glempresa.INSCRICAOESTADUAL" => array(	"tipo" => "label",
																				"label" => "Inscrição Estadual",
																				"idname" => "txtclienteinscricaoestadual",
																				"placeholder" => "",
																				),
												"glempresa.TELEFONE" => array(	"tipo" => "label",
																				"label" => "Telefone Fixo",
																				"idname" => "txtclientetelefonefixo",
																				"placeholder" => "(099) 9999-9999",
																				"mask" => "(999) 9999-9999",
																				),
												"glempresa.EMAIL" => array(	"tipo" => "label",
																				"label" => "Email",
																				"idname" => "txtclienteemail",
																				"placeholder" => "email@email.com.br",
																				"required-type" => "email",
																				),
												),
												
												
											"Endereço" => array("glempresa.RUA" => array(	"tipo" => "label",
																				"label" => "Rua",
																				"idname" => "txtclienterua",
																				"placeholder" => "",
																				),
																"glempresa.NUMERO" => array("tipo" => "label",
																				"label" => "Número",
																				"idname" => "txtclientenumero",
																				"placeholder" => "",
																				),
																"glempresa.COMPLEMENTO" => array("tipo" => "label",
																				"label" => "Complemento",
																				"idname" => "txtclientecomplemento",
																				"placeholder" => "",
																				),
																"glempresa.BAIRRO" => array("tipo" => "label",
																				"label" => "Bairro",
																				"idname" => "txtclientebairro",
																				"placeholder" => "",
																				),
																"glcliente.ESTADO" => 
																				array(
																					  "tipo" => "select",
																					  "label" => "Acesso Liberado?",
																					  "idname" => "selclienteestado",
																					  "values" => $arrDadosEstadosTratados
																					),
											),
										),
							);
		
		$this->strTabela = "glempresa";
		
    }
	
	
}