<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Mcrud.
 * Classe para facilizar as ações de CRUD do sistema.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */

class Clinks extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		$this->data = array("tipo" => "CRUD",
							"lista" => array("Titulo" => "Menu",
											 "classe" => "Clinks",
											 "precaminho" => "global/administracao/links/",
											 "autoinc" => "nao",
											 "codigo" => "gllink.CODLINK",
											 ),
							"tabela" => array("cabecalho" => array( "gllink.CODLINK" => "Código",
														 			"gllink.NOME" => "Nome",
														 			"gllink.ORDEM" => "Ordem",
														 			"gllink.CAMINHO" => "Caminho",
														 			"gllink.DESCRICAO" => "Descricao",
																	),
											 ),
							"edita" => array("cabecalho" => array("gllink.CODLINK" => "CODLINK",
														 		  "gllink.NOME" => "NOME",
														 		  "gllink.CAMINHO" => "CAMINHO",
														 		  "gllink.DESCRICAO" => "DESCRICAO",
														 		  "gllink.ADMIN" => "ADMIN",
														 		  "gllink.ORDEM" => "ORDEM",
														 		  "gllink.ICONEFA" => "ICONEFA",
																  ),
											 ),							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovoClinks",
													 "method" => "post",
													 "action" => "global/administracao/links/Clinks/salvaEdicao",
													 "validate-form" => "data-parsley-validate='true'"
													 ),
							"form" => array("Geral" => array("gllink.CODLINK" => array("tipo" => "label",
																"label" => "Código do Link",
																"idname" => "codlinkLink",
																"placeholder" => "",
																"disabled" => true
																),
											   "gllink.NOME" => array("tipo" => "label",
																"label" => "Nome",
																"idname" => "txtNomeLink",
																"placeholder" => "Digite o nome do link",
																"required" => true,
																),
												"gllink.CAMINHO" => array("tipo" => "label",
																"label" => "Caminho",
																"idname" => "txtCaminhoLink",
																"placeholder" => "#/global/novo/novo/cnovo",
																"required" => true,
																),
												"gllink.DESCRICAO" => array("tipo" => "label",
																"label" => "Descrição",
																"idname" => "txtDescricaoLink",
																"placeholder" => "Digite a descrição do link",
																"required" => true,
																),
												"gllink.ADMIN" => array("tipo" => "select",
																	"label" => "Admin",
																	"idname" => "selAdminLink",
																	"placeholder" => "",
																	"required" => true,
																	"values" => array("" => "Escolha um valor!",
																					  "0" => "Não",
																					  "1" => "Administrador",
																					  )
																	),
												"gllink.ORDEM" => array("tipo" => "label",
																"label" => "Ordem",
																"idname" => "txtOrdemLink",
																"placeholder" => "99.99.99",
																"required" => true,
																"mask" => "99.99.99"
																),
												"gllink.ICONEFA" => array("tipo" => "label",
																"label" => "Icone",
																"idname" => "txtIconLink",
																"placeholder" => "fa fa-globe",
																"required" => true,
																),
												),
											),
							);
		
		$this->strTabela = "gllink";
		
    }
	
	
}