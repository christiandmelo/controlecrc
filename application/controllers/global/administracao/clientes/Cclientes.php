<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Mcrud.
 * Classe para facilizar as ações de CRUD do sistema.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */

class Cclientes extends MY_Controller{
	
	/**
	 * Contrutor da Classe
	 * Utiliza dos parametros passados no array data para auto-configurar o sistema
	 * 
	 */
	public function __construct() {
        parent::__construct ();
		
		$this->data = array("tipo" => "CRUD",
							"lista" => array("Titulo" => "Cliente",
											 "classe" => "cclientes",
											 "precaminho" => "global/administracao/clientes/",
											 "autoinc" => "nao",
											 "chave" => "glcliente.CODCLIENTE",
											 ),
							"chave" => array("glcliente.CODCLIENTE" => "CODCLIENTE",
											),
							"tabela" => array("cabecalho" => array( "glcliente.CODCLIENTE" => "Código",
														 			"glcliente.NOMEFANTASIA" => "Nome fantasia",
														 			"glcliente.LIBERADO" => "Liberado",
														 			"glcliente.QTDNOTAS" => "Qtd notas",
														 			"glcliente.QTDUSUARIOS" => "Qtd usuários",
														 			"glcliente.QTDUSUARIOSUSADOS" => "Usuários utilizados",
														 			"glcliente.QTDEMPRESASUSADAS" => "Empresas utilizadas"
																	),
											 ),
							"edita" => array("cabecalho" => array("glcliente.CODCLIENTE" => "CODCLIENTE",
														 		  "glcliente.NOME" => "NOME",
														 		  "glcliente.NOMEFANTASIA" => "NOMEFANTASIA",
														 		  "glcliente.LIBERADO" => "LIBERADO",
														 		  "glcliente.QTDNOTAS" => "QTDNOTAS",
														 		  "glcliente.QTDUSUARIOS" => "QTDUSUARIOS",
														 		  "glcliente.QTDEMPRESAS" => "QTDEMPRESAS",
																  ),
											 ),							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovocclientes",
													 "method" => "post",
													 "action" => "global/administracao/clientes/cclientes/salvaEdicao",
													 "validate-form" => "data-parsley-validate='true'"
													 ),
							"form" => array("Dados Cadastrais" => 
												array("glcliente.CODCLIENTE" => array("tipo" => "label",
																				"label" => "Código do cliente",
																				"idname" => "codcliente",
																				"placeholder" => "",
																				"disabled" => true
																				),
													   "glcliente.NOME" => array("tipo" => "label",
													   					"filtro" => true,
																		"label" => "Razão Social",
																		"idname" => "txtRazaoSocialCliente",
																		"placeholder" => "",
																		"disabled" => false,
																		"required" => true,
																		),
														"glcliente.NOMEFANTASIA" => array("tipo" => "label",
													   					"filtro" => true,
																		"label" => "Nome Fantasia",
																		"idname" => "txtNomeFantasiaCliente",
																		"placeholder" => "",
																		"disabled" => false,
																		"required" => true,
																		),
														
													),
													"Conta" => 
														array("glcliente.LIBERADO" => array("tipo" => "select",
																			"campotabela" => "LIBERADO",
																			"label" => "Acesso Liberado?",
																			"idname" => "selAcessoLiberadoCliente",
																			"required" => true,
																			"values" => array("" => "Escolha um valor!",
																							  "1" => "Sim",
																							  "2" => "Não"
																							  )
																			),
															"glcliente.QTDNOTAS" => array("tipo" => "label",
																		"label" => "Quantidade de Notas",
																		"idname" => "txtQtdNotasCliente",
																		"placeholder" => "",
																		"disabled" => false,
																		"required" => true,
																		"required-type" => "number",
																		),
															"glcliente.QTDEMPRESAS" => array("tipo" => "label",
																		"label" => "Quantidade de Empresas",
																		"idname" => "txtQtdEmpresasCliente",
																		"placeholder" => "",
																		"disabled" => false,
																		"required" => true,
																		"required-type" => "number",
																		),
															"glcliente.QTDUSUARIOS" => array("tipo" => "label",
																		"label" => "Quantidade de Usuários",
																		"idname" => "txtQtdUsuariosCliente",
																		"placeholder" => "",
																		"disabled" => false,
																		"required" => true,
																		"required-type" => "number",
																		),
														),
											
									),
										
							);
		
		$this->strTabela = "glcliente";
		
    }
	
}