<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ccontexto extends CI_Controller{
	
	public function __construct() {
		parent::__construct ();
    }
	
	public $data;
	public $strTabela;
	
	public function index(){
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->data['contexto']['tipo'] = $_GET['tipo'];
		
		switch($this->data['contexto']['tipo']){
			case "conta":
				$this->strTabela = "glcliente";
				$this->data['contexto']['CODIGO'] = 'codcliente';
				$this->data['cabecalho'] = array("CODCLIENTE" => 'CODIGO',
												 	"NOMEFANTASIA" => 'NOMEFANTASIA');
				if($_SESSION['tipousuario'] != 1){
					$this->data['where'] = array("CODCLIENTE" => $_SESSION['codcliente']);
				}	
				break;
			case "empresa":
				$this->strTabela = "glempresa";
				$this->data['contexto']['CODIGO'] = 'codempresa';
				$this->data['cabecalho'] = array("CODEMPRESA" => 'CODIGO',
												 	"NOMEFANTASIA" => 'NOMEFANTASIA');
				$this->data['where'] = array("CODCLIENTE" => $_SESSION['codcliente']);					
				break;
			case "filial":
				$this->strTabela = "glfilial";
				$this->data['contexto']['CODIGO'] = 'codfilial';
				$this->data['cabecalho'] = array("glfilial.CODFILIAL" => 'CODIGO',
												 	"glfilial.NOMEFANTASIA" => 'NOMEFANTASIA');
				$this->data['join'] = array("glusuariofilial" => "glusuariofilial.CODCLIENTE = glfilial.CODCLIENTE and glusuariofilial.CODEMPRESA = glfilial.CODEMPRESA and glusuariofilial.CODFILIAL = glfilial.CODFILIAL");
				$this->data['where'] = array("glfilial.CODCLIENTE" => $_SESSION['codcliente'],
											 "glfilial.CODEMPRESA" => $_SESSION['codempresa'],
											 "glusuariofilial.CODUSUARIO" => $_SESSION['codusuario']);
				break;
		}
		
		$this->Mcrud->setStrTable($this->strTabela);
		$this->data['lista']['dados'] = $this->Mcrud->getDados($this->data);
		
		$this->load->view("global/contexto/vcontexto",$this->data);
	}





	public function Trocacontexto(){
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("glusuario");
		$this->load->model("global/contexto/Mcontexto","Mcontexto");
		$return = $this->Mcontexto->setContexto($_POST['selTrocaContexto'],$_POST['txtContextoTipo']);
		
		echo $return;
		
	}
	
}