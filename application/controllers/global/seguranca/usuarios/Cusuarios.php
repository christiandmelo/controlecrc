<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Cusuarios extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Edição de Usuários",
						"classe" => "cusuarios",
						"precaminho" => "global/seguranca/usuarios/",
						"autoinc" => "nao",
						"codigo" => "glusuario.CODUSUARIO" 
				),
				"botoes" => array(
								"excluir" => "inativo",
							),
				"tabela" => array (
						"cabecalho" => array (
								"glusuario.CODUSUARIO" => "CodUsuario",
								"glusuario.NOME" => "Nome usuário",
								"glcliente.NOME" => "Nome cliente" 
						),
						"left-join" => array (
								"glcliente" => "glcliente.codcliente = glusuario.codcliente" 
						) 
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovocusuarios",
						"method" => "post",
						"action" => "global/seguranca/usuarios/cusuarios/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"glusuario.CODUSUARIO" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "codigCusuarios",
										"disabled" => true 
								),
								"glusuario.CODCLIENTE" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "CODCLIENTECusuarios",
										"disabled" => true,
										"visible" => false,
										"value" => 1
								),
								"glusuario.CODEMPRESA" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "CODEMPRESACusuarios",
										"disabled" => true,
										"visible" => false,
										"value" => 1
								),
								"glusuario.CODFILIAL" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "CODFILIALCusuarios",
										"disabled" => true,
										"visible" => false,
										"value" => 1
								),
								"glusuario.TIPOUSUARIO" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "TIPOUSUARIOCusuarios",
										"disabled" => true,
										"visible" => false,
										"value" => 1
								),
								"glusuario.ATIVO" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "ATIVOCusuarios",
										"disabled" => true,
										"visible" => false,
										"value" => 1
								),
								"glusuario.SENHA" => array (
										"tipo" => "label",
										"label" => "Senha:",
										"idname" => "SenhaCusuario","disabled" => true,
										"visible" => false,
										"value" => 'e10adc3949ba59abbe56e057f20f883e'
								),
								"glusuario.NOME" => array (
										"tipo" => "label",
										"label" => "Nome:",
										"idname" => "NomeCusuarios",
								),
								"glusuario.EMAIL" => array (
										"tipo" => "label",
										"label" => "Email:",
										"idname" => "EmailCusuario",
								),
						) 
				) 
		);
		
		$this->strTabela = "glusuario";
	}
}

?>