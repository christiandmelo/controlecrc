<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Cperfil extends CI_Controller{
	public function __construct() {
        parent::__construct ();
    }
	
	/**
	 * Página inicial
	 */
	public function index(){
		
		// Inicia os modulos necessarios
		$this->load->model("global/Mcrud","Mcrud");
		
		//informa qual tabela vai consultar
		$this->Mcrud->setStrTable('glusuario');
		$arrDataUsuario = array("cabecalho" => array("EMAIL" => "EMAIL",
													 "NOME" => "NOME",
													 "CAMINHOFOTO" => "CAMINHOFOTO"),
								"where" => array("CODUSUARIO" => $_SESSION['codusuario'])
								);
		$this->data['lista']['dados'] = $this->Mcrud->getDados($arrDataUsuario);
		
		$this->load->view("global/seguranca/usuarios/vperfil",$this->data);
		
	}
	
	public function salvaEdicao(){
		// CRUD
		$this->load->model("global/Mcrud","Mcrud");
		
		
		$arrDadosEdicao['glusuario.NOME'] = $_REQUEST['txtnomeCperfil'];
		$arrDadosEdicao['glusuario.EMAIL'] = $_REQUEST['txtemailCperfil'];
		
		/*id para update*/
		$arrFiltroEdicao['glusuario.CODUSUARIO'] = $_SESSION['codusuario'];
		
		
		$this->Mcrud->setStrTable('glusuario');
		$retorno = $this->Mcrud->setDados($arrDadosEdicao,null,null,$arrFiltroEdicao,'glusuario.CODUSUARIO');
		
			
		if(isset($retorno) and $retorno !== false){
			if( !isset( $data['texto'] ) )
				$data['texto'] = 'Inserção/Atualização com Sucesso!';
			if( !isset( $data['action'] ) )
				$data['action'] = 'atualizar';
			if( !isset( $data['class'] ) )
				$data['class'] = 'gritter-success';
			if( !isset( $data['id_dest'] ) )
				$data['id_dest'] = 'corpo'.str_replace(' ','_','Perfil');
			
			//Recarregar as variaveis de sistema do usuários com os novos dados
			$_SESSION['nomeusuario'] = $_REQUEST['txtnomeCperfil'];
			?>
			<script type="text/javascript">
				$("#VcabecalhoNomeUsuario").html("<?=$_REQUEST['txtnomeCperfil'];?>");
			</script>
			<?php
			
			
		}else{
			if( !isset( $data['texto'] ) )
				$data['texto'] = 'Erro para Inserir/Atualizar o registro, gentileza tentar novamente ou procurar o administrador do sistema!';
			if( !isset( $data['action'] ) )
				$data['action'] = '';
			if( !isset( $data['class'] ) )
				$data['class'] = 'gritter-danger';
		}
		
		$this->load->view("global/crud/Valert",$data);
		
		//$this->load->view("global/seguranca/usuarios/vperfil");
	
	}
	
}


?>