<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class CusuarioFilial extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("glusuario");
		$arrDataUsuario = array("cabecalho" => array("CODUSUARIO" => "CODUSUARIO",
													 "NOME" => "NOME")
								);
		$arrDadosUsuario = $this->Mcrud->getDados($arrDataUsuario);
		$arrDadosUsuarioTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosUsuario->result() as $row){
		    $arrDadosUsuarioTratados[$row->CODUSUARIO] = $row->NOME;
		}
		
		$this->Mcrud->setStrTable("glfilial");
		$arrDataFilial = array("cabecalho" => array("CODFILIAL" => "CODFILIAL",
													 "NOMEFANTASIA" => "NOMEFANTASIA")
								);
		$arrDadosFilial = $this->Mcrud->getDados($arrDataFilial);
		$arrDadosFilialTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosFilial->result() as $row){
		    $arrDadosFilialTratados[$row->CODFILIAL] = $row->NOMEFANTASIA;
		}
		$this->Mcrud->setStrTable(null);
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Edição dos Usuários por Tele Centro",
						"classe" => "CusuarioFilial",
						"precaminho" => "global/seguranca/grupos/",
						"autoinc" => "sim",
						"autolista" => false,
						"codigo" => "glusuariofilial.CODUSUARIOFILIAL" 
				),
				"chave" => array (
						"glusuariofilial.CODCLIENTE" => "CODCLIENTE",
						"glusuariofilial.CODEMPRESA" => "CODEMPRESA"
				),
				"tabela" => array (
						"cabecalho" => array (
								"glusuariofilial.CODUSUARIOFILIAL" => "Codigo",
								"glusuario.NOME" => "Usuario",
								"glfilial.NOMEFANTASIA" => "TeleCentro"
						),
						"join" => array(
								"glusuario" => "glusuario.CODUSUARIO = glusuariofilial.CODUSUARIO",
								"glfilial" => "glfilial.CODCLIENTE = glusuariofilial.CODCLIENTE AND glfilial.CODEMPRESA = glusuariofilial.CODEMPRESA AND glfilial.CODFILIAL = glusuariofilial.CODFILIAL"
						),
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCusuarioFilial",
						"method" => "post",
						"action" => "global/seguranca/grupos/CusuarioFilial/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"glusuariofilial.CODUSUARIOFILIAL" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "txtcodusuariofilialCusuarioFilial",
										"disabled" => true,
								),
								"glusuariofilial.CODUSUARIO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Usuário:",
										"idname" => "usuarioCusuarioFilial",
										"required" => true,
										"values" => $arrDadosUsuarioTratados
								),
								"glusuariofilial.CODFILIAL" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Tele Centro:",
										"idname" => "filialCusuarioFilial",
										"required" => true,
										"values" => $arrDadosFilialTratados
								),
						) 
				) 
		);
		
		$this->strTabela = "glusuariofilial";
	}
}

?>