<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class CgrupoLink extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("gllink");
		$arrDataLink = array("cabecalho" => array("CODLINK" => "CODLINK",
													 "NOME" => "NOME"),
							 "where" => array("ATIVO" => "1"),
							 "order" => array("ORDEM" => "asc"), 
								);
		$arrDadosLink = $this->Mcrud->getDados($arrDataLink);
		$arrDadosLinkTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosLink->result() as $row){
		    $arrDadosLinkTratados[$row->CODLINK] = $row->NOME;
		}
		
		
		
		$this->Mcrud->setStrTable("glgrupo");
		$arrDataGrupo = array("cabecalho" => array("CODGRUPO" => "CODGRUPO",
													 "NOME" => "NOME")
								);
		$arrDadosGrupo = $this->Mcrud->getDados($arrDataGrupo);
		$arrDadosGrupoTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosGrupo->result() as $row){
		    $arrDadosGrupoTratados[$row->CODGRUPO] = $row->NOME;
		}
		$this->Mcrud->setStrTable(null);
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Edição de Permissões dos Grupos",
						"classe" => "CgrupoLink",
						"precaminho" => "global/seguranca/grupos/",
						"autoinc" => "sim",
						"autolista" => false,
						"codigo" => "glgrupolink.CODGRUPOLINK" 
				),
				"chave" => array (
						"glgrupolink.CODCLIENTE" => "CODCLIENTE",
						"glgrupolink.CODEMPRESA" => "CODEMPRESA",
						"glgrupolink.CODFILIAL" => "CODFILIAL",
				),
				"tabela" => array (
						"cabecalho" => array (
								"glgrupolink.CODGRUPOLINK" => "Codigo",
								"gllink.NOME" => "Menu",
								"glgrupo.NOME" => "GRUPO",
								"gllink.ORDEM" => "ORDEM"
						),
						"join" => array(
								"gllink" => "gllink.CODLINK = glgrupolink.CODLINK",
								"glgrupo" => "glgrupo.CODCLIENTE = glgrupolink.CODCLIENTE AND glgrupo.CODEMPRESA = glgrupolink.CODEMPRESA AND glgrupo.CODGRUPO = glgrupolink.CODGRUPO"
						),
						"where" => array(
								"gllink.ATIVO" => 1
						),
						"order" => array("glgrupo.NOME" => "asc",
										 "gllink.ORDEM" => "asc")
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCgrupoLink",
						"method" => "post",
						"action" => "global/seguranca/grupos/CgrupoLink/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"glgrupolink.CODGRUPOLINK" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "txtcodusuariofilialCgrupoLink",
										"disabled" => true,
								),
								
								"glgrupolink.CODGRUPO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Grupo:",
										"idname" => "grupoCgrupoLink",
										"required" => true,
										"values" => $arrDadosGrupoTratados
								),
								"glgrupolink.CODLINK" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Menu:",
										"idname" => "linkCgrupoLink",
										"required" => true,
										"values" => $arrDadosLinkTratados
								),
						) 
				) 
		);
		
		$this->strTabela = "glgrupolink";
	}
}

?>