<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Cgrupos extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Edição dos Grupos de Acesso",
						"classe" => "cgrupos",
						"precaminho" => "global/seguranca/grupos/",
						"autoinc" => "sim",
						"codigo" => "glgrupo.CODGRUPO" 
				),
				"chave" => array (
						"glgrupo.CODCLIENTE" => "CODCLIENTE",
						"glgrupo.CODEMPRESA" => "CODEMPRESA"
				),
				"tabela" => array (
						"cabecalho" => array (
								"glgrupo.CODGRUPO" => "CodGrupo",
								"glgrupo.NOME" => "Nome"
						) 
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovocgrupos",
						"method" => "post",
						"action" => "global/seguranca/grupos/cgrupos/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"glgrupo.CODGRUPO" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "txtgrupo",
										"disabled" => true,
								),
								"glgrupo.NOME" => array (
										"tipo" => "label",
										"label" => "Nome do Grupo:",
										"idname" => "txtNome",
										"filtro" => true,
										"required" => true,
								) 
						) 
				) 
		);
		
		$this->strTabela = "glgrupo";
	}
}

?>