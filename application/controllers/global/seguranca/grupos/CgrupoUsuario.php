<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class CgrupoUsuario extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("glusuario");
		$arrUsuarioLink = array("cabecalho" => array("CODUSUARIO" => "CODUSUARIO",
													 "NOME" => "NOME")
								);
		$arrUsurarioLink = $this->Mcrud->getDados($arrUsuarioLink);
		$arrDadosUsuarioTratados = array ("" => "Escolha um valor!");
		foreach ($arrUsurarioLink->result() as $row){
		    $arrDadosUsuarioTratados[$row->CODUSUARIO] = $row->NOME;
		}
		
		
		
		$this->Mcrud->setStrTable("glgrupo");
		$arrDataGrupo = array("cabecalho" => array("CODGRUPO" => "CODGRUPO",
													 "NOME" => "NOME")
								);
		$arrDadosGrupo = $this->Mcrud->getDados($arrDataGrupo);
		$arrDadosGrupoTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosGrupo->result() as $row){
		    $arrDadosGrupoTratados[$row->CODGRUPO] = $row->NOME;
		}
		
		
		
		
		$this->Mcrud->setStrTable("glfilial");
		$arrDataFilial = array("cabecalho" => array("CODFILIAL" => "CODFILIAL",
													 "NOMEFANTASIA" => "NOMEFANTASIA")
								);
		$arrDadosFilial = $this->Mcrud->getDados($arrDataFilial);
		$arrDadosFilialTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosFilial->result() as $row){
		    $arrDadosFilialTratados[$row->CODFILIAL] = $row->NOMEFANTASIA;
		}
		$this->Mcrud->setStrTable(null);
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Grupos do usuário",
						"classe" => "CgrupoUsuario",
						"precaminho" => "global/seguranca/grupos/",
						"autoinc" => "sim",
						"autolista" => false,
						"codigo" => "glgrupousuario.CODGRUPOUSUARIO" 
				),
				"chave" => array (
						"glgrupousuario.CODCLIENTE" => "CODCLIENTE",
						"glgrupousuario.CODEMPRESA" => "CODEMPRESA",
				),
				"tabela" => array (
						"cabecalho" => array (
								"glgrupousuario.CODGRUPOUSUARIO" => "Código",
								"glusuario.NOME" => "Usuário",
								"glgrupo.NOME" => "Grupo",
								"glfilial.NOMEFANTASIA" => "Tele Centro"
						),
						"join" => array(
								"glusuario" => "glusuario.CODUSUARIO = glgrupousuario.CODUSUARIO",
								"glgrupo" => "glgrupo.CODCLIENTE = glgrupousuario.CODCLIENTE AND glgrupo.CODEMPRESA = glgrupousuario.CODEMPRESA AND glgrupo.CODGRUPO = glgrupousuario.CODGRUPO",
								"glfilial" => "glfilial.CODCLIENTE = glgrupousuario.CODCLIENTE AND glfilial.CODEMPRESA = glgrupousuario.CODEMPRESA AND glfilial.CODFILIAL = glgrupousuario.CODFILIAL"
						)
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovoCgrupoUsuario",
						"method" => "post",
						"action" => "global/seguranca/grupos/CgrupoUsuario/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'"
				),
				"form" => array (
						"Geral" => array (
								"glgrupousuario.CODGRUPOUSUARIO" => array (
										"tipo" => "label",
										"label" => "Código do Grupo:",
										"idname" => "txtcodusuariofilialCgrupoUsuario",
										"disabled" => true,
								),
								"glgrupousuario.CODUSUARIO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Usuário:",
										"idname" => "usuarioCgrupoUsuario",
										"required" => true,
										"values" => $arrDadosUsuarioTratados
								),
								"glgrupousuario.CODGRUPO" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Grupo:",
										"idname" => "grupoCgrupoUsuario",
										"required" => true,
										"values" => $arrDadosGrupoTratados
								),
								"glgrupousuario.CODFILIAL" => array("tipo" => "select",
										"filtro" => true,
										"label" => "Filial:",
										"idname" => "filialCusuarioFilial",
										"required" => true,
										"values" => $arrDadosFilialTratados
								),
						) 
				) 
		);
		
		$this->strTabela = "glgrupousuario";
	}
}

?>