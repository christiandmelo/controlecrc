<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

/**
 * Mcrud.
 * Classe para facilizar as ações de CRUD do sistema.
 *
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */
class CparametrosGlobal extends MY_Controller {
	
	/**
	 * Contrutor da Classe
	 * Utiliza dos parametros passados no array data para auto-configurar o sistema
	 */
	public function __construct() {
		parent::__construct ();
		
		$this->data = array (
				"tipo" => "Parametro",
				"lista" => array (
						"Titulo" => "Parametros Globais",
						"classe" => "cparametrosglobal",
						"precaminho" => "global/cadastros/",
						"autoinc" => "nao",
						"chave" => "glcliente.CODCLIENTE" 
				),
				"chave" => array (
						"glcliente.CODCLIENTE" => "CODCLIENTE" 
				),
				"tabela" => array (
						"cabecalho" => array (
								"glcliente.CODCLIENTE" => "Código",
								"glcliente.NOMEFANTASIA" => "Nome fantasia",
								"glcliente.LIBERADO" => "Liberado",
								"glcliente.QTDNOTAS" => "Qtd notas",
								"glcliente.QTDUSUARIOS" => "Qtd usuários",
								"glcliente.QTDUSUARIOSUSADOS" => "Usuários utilizados",
								"glcliente.QTDEMPRESASUSADAS" => "Empresas utilizadas" 
						) 
				),
				"edita" => array (
						"cabecalho" => array (
								"glcliente.CODCLIENTE" => "CODCLIENTE",
								"glcliente.NOME" => "NOME",
								"glcliente.NOMEFANTASIA" => "NOMEFANTASIA",
								"glcliente.LIBERADO" => "LIBERADO",
								"glcliente.QTDNOTAS" => "QTDNOTAS",
								"glcliente.QTDUSUARIOS" => "QTDUSUARIOS",
								"glcliente.QTDEMPRESAS" => "QTDEMPRESAS" 
						) 
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovocclientes",
						"method" => "post",
						"action" => "global/administracao/clientes/cclientes/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'" 
				),
				"form" => array (
						"Aba 1" => array (
								"glparametro.P1" => array (
										"tipo" => "label",
										"label" => "Parametro 1",
										"idname" => "txtP1",
										"placeholder" => "",
										"disabled" => true 
								),
								"glparametro.P2" => array (
										"tipo" => "label",
										"filtro" => true,
										"label" => "Parametro 2",
										"idname" => "txtP2",
										"placeholder" => "",
										"disabled" => false,
										"required" => true 
								) 
						)
						,
						"Aba 2" => array (
								"glparametro.P3" => array (
										"tipo" => "select",
										"campotabela" => "LIBERADO",
										"label" => "Parametro 3",
										"idname" => "selP3",
										"required" => true,
										"values" => array (
												"" => "Escolha um valor!",
												"1" => "Sim",
												"2" => "Não" 
										) 
								),
								"glparametro.P4" => array (
										"tipo" => "label",
										"label" => "Parametro 4",
										"idname" => "intP4",
										"placeholder" => "",
										"disabled" => false,
										"required" => true,
										"required-type" => "number" 
								) 
						) 
				)
				 
		)
		;
		
		$this->strTabela = "glparametro";
	}
}