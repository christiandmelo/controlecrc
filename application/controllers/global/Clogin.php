<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Clogin extends CI_Controller{
	
	public function __construct() {
        parent::__construct ();
    }
	
	/**
	 * Metodo para carregar o formulário
	 * 
	 */
	public function login(){
		
		$data['loginFail'] = ( $this->session->flashdata('falha') !== null ? $this->session->flashdata('falha') : false );
		$this->load->view("global/vlogin",$data);
		
	}
	
	/**
	 * Metodo para autenticar o usuário
	 */
	public function autenticar(){
		
		$this->load->model("global/Mlogin","Mlogin") ;
		
		$email = $this->input->post('txtLoginUsuario');
		$senha = $this->input->post('txtLoginSenha');
		
		/*monta o array com os campos fornecidos pelo formulario*/
		$dadosForm = array("email" => $email,
						   "senha" => md5($senha));
		
		$this->Mlogin->getAutentica($dadosForm);
		
		if( isset($_SESSION['logado']) ){
			redirect('app');
		}else{
			$this->session->set_flashdata('falha', 'Usuario ou Senha Invalidos!');
			redirect('login');
		}
	}
	
	
		
	/**
	 * Método para sair do sistema
	 */
	public function setSair(){
		
		session_destroy();
		
		redirect('login');
		
	}
	
}
