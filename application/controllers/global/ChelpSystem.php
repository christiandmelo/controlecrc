<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ChelpSystem extends CI_Controller{
	
	/**
	 * __construct.
	 * Contrutor da classe. S� valida se o usuario esta logado. A permiss�o n�o �, teoricamente, necessaria.
	 * 
	 */
	public function __construct() {
		parent::__construct ();
		usuarioLogado (true);
    }
	
    /**
     * helpers.
     * Busca os helpers e retorna um json deles.
     * 
     */
    public function helpers(){
    	// Busca os helpers
    	$this->load->model("global/MhelpSystem","MhelpSystem");
    	$return = $this->MhelpSystem->getSystemHelps($this->MhelpSystem::$json_help_system);
    	// For�a o retorno do json
    	header('Content-Type: application/json');
    	echo json_encode( $return );
    }
    
}
