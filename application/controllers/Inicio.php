<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		
		// Verifica se o usuario esta logado
		if( !usuarioLogado( false )){
			redirect('/login');	
		}
		
		// Carrega a classe de acessos
		$this->load->model("global/Macesso","Macesso");
		//$this->load->model("global/Marmazenamento","Marmazenamento");
		
		// Não sei se realmente vai ficar assim...
		$this->load->view('global/layout/Vhead');
		$data['menu'] = $this->Macesso->getMenu(
			$_SESSION['codusuario'],
			null,
			'busca'
		);
		$data['breadcrumb'] = $this->Macesso->getBreadcrumb( );
		$this->load->view('global/layout/Vcabecalho',$data);
		$data['menu'] = $this->Macesso->getMenu(
			$_SESSION['codusuario'],
			null,
			'lateral'
		);

		$this->load->view('global/layout/Vmenu',$data);
		/*$data['menu'] = $this->Macesso->getMenu(
			$_SESSION['codusuario'],
			'00.00.00',
			'central'
		);*/
	    $periodo=6;
		$data['data_inicial']=strftime('%m/%Y', strtotime('- '.$periodo.' months'));
		$data['data_final']=strftime('%m/%Y', strtotime('+ '.$periodo.' months'));
		$this->load->model('financeiro/contas/lancamentos/Mlancamentos','Mlancamentos');
		$data['LancamentosPorMes'] = $this->Mlancamentos->getLancamentosAgrupadosPorMes($data['data_inicial'],$data['data_final']);
		$this->load->view('global/layout/Vcorpo',$data);
		$this->load->view('global/layout/Vfoot');
		
	}
	
	/**
	 * Pagina de Notificações.
	 */
	public function menu( $ordem = null ){
		
		// Verifica se o usuario esta logado
		usuarioLogado( true );
		
		// Carrega a classe de acessos
		$this->load->model("global/Macesso","Macesso");
		
		// Carrega o Menu
		$data['html'] = $this->Macesso->getMenu(
			$_SESSION['codusuario'],
			$ordem,
			'central'
		);
		$data['breadcrumb'] = $this->Macesso->getBreadcrumb($ordem);
		$this->load->view('global/crud/Vblank',$data);
		
	}
	
	/**
	 * Pagina de Notificações.
	 */
	public function notificacoes(){
		
		echo var_dump( $this->uri->segment(2) );
		
		// $this->load->view('global/layout/Verror_404');
		
	}
	
}
