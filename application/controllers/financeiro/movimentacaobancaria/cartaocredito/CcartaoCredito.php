<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CcartaoCredito.
 * Classe que controla os cartões de crédito.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */
 
class CcartaoCredito extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		$this->data = array(
							"tipo" => "	CRUD",
							"lista" => array("Titulo" => "Cartão de crédito",
											 "classe" => "CcartaoCredito",
											 "precaminho" => "financeiro/movimentacaobancaria/cartaocredito/",
											 "autoinc" => "sim",
											 "codigo" => "ficartaocredito.CODCARTAOCREDITO" 
											 ),
											  
							"chave" => array(	"ficartaocredito.CODCLIENTE" => "CODCLIENTE",
												"ficartaocredito.CODEMPRESA" => "CODEMPRESA",
												"ficartaocredito.CODFILIAL" => "CODFILIAL",
											),
							"botoes" => array(
								"excluir" => "inativo",
							),				
							"tabela" => array("cabecalho" => array( "ficartaocredito.CODCARTAOCREDITO" => "CODIGO",
																	"ficartaocredito.DESCRICAO" => "Descrição",
														 			"ficartaocredito.DIAVENCIMENTO" => "Dia Vencimento",
																	),
											 ),
											 							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovoCcartaoCredito",
													 "method" => "post",
													 "action" => "financeiro/movimentacaobancaria/cartaocredito/CcartaoCredito/salvaEdicao",
													 "validate-form" => "data-parsley-validate='true'"
													 ),
													 
							"form" => array("Geral" => array("ficartaocredito.CODCARTAOCREDITO" => array(
																	"tipo" => "label",
																	"label" => "Código",
																	"idname" => "codigoCcartaoCredito",
																	"disabled" => true,
													),
													"ficartaocredito.DESCRICAO" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Descrição",
																	"idname" => "DescricaoCcartaoCredito",
																	"required" => true,
													),
													"ficartaocredito.DIAFECHAMENTO" => array(
																	"tipo" => "label",
																	"label" => "Dia Fechamento",
																	"idname" => "DiaFechamentoCcartaoCredito",
																	"required" => true,
																	"mask" => "99"
													),
													"ficartaocredito.DIAVENCIMENTO" => array(
																	"tipo" => "label",
																	"label" => "Dia Vencimento",
																	"idname" => "DiaVencimentoCcartaoCredito",
																	"required" => true,
																	"mask" => "99"
													),
													"ficartaocredito.Limite" => array(
																	"tipo" => "label",
																	"label" => "Limite",
																	"idname" => "LimiteCcartaoCredito",
																	"required" => true,
																	"mask" => "#.###,##",
																	"mask-reverse" => "true",
													),
												),
										
									),
										
							);
		
		$this->strTabela = "ficartaocredito";
		
    }
	
	
}