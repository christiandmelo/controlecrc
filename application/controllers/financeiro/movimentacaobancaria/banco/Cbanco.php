<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Cbanco.
 * Classe que controla os bancos.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */
 
class Cbanco extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		$this->data = array(
							"tipo" => "	CRUD",
							"lista" => array("Titulo" => "Bancos",
											 "classe" => "Cbanco",
											 "precaminho" => "financeiro/movimentacaobancaria/banco/",
											 "autoinc" => "sim",
											 "codigo" => "glbanco.CODBANCO" 
											 ),
											  
							"chave" => array(	"glbanco.CODCLIENTE" => "CODCLIENTE",
											),
							"botoes" => array(
								"excluir" => "inativo",
							),				
							"tabela" => array("cabecalho" => array( "glbanco.CODBANCO" => "CODBANCO",
														 			"glbanco.NOME" => "NOME",
														 			"glbanco.NOMEREDUZIDO" => "NOMEREDUZIDO",
																	),
											 ),
											 							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovoCbanco",
													 "method" => "post",
													 "action" => "financeiro/movimentacaobancaria/banco/Cbanco/salvaEdicao",
													 "validate-form" => "data-parsley-validate='true'"
													 ),
													 
							"form" => array("Geral" => array("glbanco.CODBANCO" => array(
																	"tipo" => "label",
																	"label" => "Código Banco",
																	"idname" => "codBancoCbanco",
																	"disabled" => true,
													),
													"glbanco.NOME" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Nome",
																	"idname" => "NomeCbanco",
																	"required" => true,
													),
													"glbanco.NOMEREDUZIDO" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Nome Reduzido",
																	"idname" => "NomeReduzidoCbanco",
																	"required" => true,
													),
													"glbanco.CODFEBRABAN" => array(
																	"tipo" => "label",
																	"label" => "Código Febraban",
																	"idname" => "CodFebrabanCbanco",
																	"required" => true,
																	"required-type" => "number",
													),
													"glbanco.DIGITO" => array(
																	"tipo" => "label",
																	"label" => "Digito",
																	"idname" => "DigitoCbanco",
																	"required" => true,
																	"required-type" => "number",
													),
												),
										
									),
										
							);
		
		$this->strTabela = "glbanco";
		
    }
	
	
}