<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CcontaBancaria.
 * Classe que controla os bancos.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */
 
class CcontaBancaria extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		/*Bloco para trazer todos os banco e carregar na tela*/
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("glbanco");
		$arrDataBancos = array("cabecalho" => array("glbanco.CODBANCO" => "CODBANCO",
													 "glbanco.NOMEREDUZIDO" => "NOMEREDUZIDO"),
							   "where" => array("glbanco.CODCLIENTE" => $_SESSION['codcliente'])
								);
		$arrDadosBancos = $this->Mcrud->getDados($arrDataBancos);
		$arrDadosBancosTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosBancos->result() as $row){
		    $arrDadosBancosTratados = array($row->CODBANCO => $row->NOMEREDUZIDO);
		}
		$this->Mcrud->setStrTable(null);
		
		$this->data = array(
							"tipo" => "	CRUD",
							"lista" => array("Titulo" => "Contas Corrente",
											 "classe" => "CcontaBancaria",
											 "precaminho" => "financeiro/movimentacaobancaria/contabancaria/",
											 "autoinc" => "sim",
											 "codigo" => "ficontacorrente.CODCONTACORRENTE" 
											 ),
											  
							"chave" => array(	"ficontacorrente.CODCLIENTE" => "CODCLIENTE",
												"ficontacorrente.CODEMPRESA" => "CODEMPRESA",
												"ficontacorrente.CODFILIAL" => "CODFILIAL",
											),
							"botoes" => array(
								"excluir" => "inativo",
							),				
							"tabela" => array("cabecalho" => array( "ficontacorrente.CODCONTACORRENTE" => "CODIGO",
																	"glbanco.NOME" => "BANCO",
														 			"ficontacorrente.DESCRICAO" => "DESCRIÇÃO",
														 			"glativo.DESCRICAO" => "SITUAÇÃO",
																	),
											  "left-join" => array("glbanco" => "glbanco.CODCLIENTE = ficontacorrente.CODCLIENTE AND glbanco.CODBANCO = ficontacorrente.CODBANCO"
											  					  ),
											  "join" => array("glativo" => "glativo.CODATIVO = ficontacorrente.SITUACAO"
											  				 ), 
											 ),
											 							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovoCcontaBancaria",
													 "method" => "post",
													 "action" => "financeiro/movimentacaobancaria/contabancaria/CcontaBancaria/salvaEdicao",
													 "validate-form" => "data-parsley-validate='true'"
													 ),
													 
							"form" => array("Geral" => array("ficontacorrente.CODCONTACORRENTE" => array(
																	"tipo" => "label",
																	"label" => "Código",
																	"idname" => "codigoCcontaBancaria",
																	"disabled" => true,
													),
													"ficontacorrente.CODBANCO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Banco",
																		"idname" => "selBancoCcontaBancaria",
																		"required" => true,
																		"values" => $arrDadosBancosTratados
													),
													"ficontacorrente.DESCRICAO" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Descrição",
																	"idname" => "DescricaoCcontaBancaria",
																	"required" => true,
													),
													"ficontacorrente.SITUACAO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Situação",
																		"idname" => "selSituacaoCcontaBancaria",
																		"required" => true,
																		"values" => array("" => "Escolha um valor!",
																						  "1" => "Ativo",
																						  "0" => "Inativo"
																						  )
													),
												),
										
									),
										
							);
		
		$this->strTabela = "ficontacorrente";
		
    }
	
	
}