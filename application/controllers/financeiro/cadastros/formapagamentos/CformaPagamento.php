<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CformaPagamento.
 * Classe que controla as Formas de Pagamento.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */
 
class CformaPagamento extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		$this->data = array(
							"tipo" => "	CRUD",
							"lista" => array("Titulo" => "Formas de Pagamento",
											 "classe" => "CformaPagamento",
											 "precaminho" => "financeiro/cadastros/formapagamentos/",
											 "autoinc" => "sim",
											 "codigo" => "fiformadepagamento.CODPAGAMENTO" 
											 ),
											  
							"chave" => array(	"fiformadepagamento.CODCLIENTE" => "CODCLIENTE"
											),
							"botoes" => array(
								"excluir" => "inativo",
							),				
							"tabela" => array("cabecalho" => array( "fiformadepagamento.CODPAGAMENTO" => "CODSITUACAO",
														 			"fiformadepagamento.DESCRICAO" => "DESCRICAO",
														 			"fiformadepagamento.SIGLA" => "SIGLA",
														 			"glativo.DESCRICAO" => "SITUACAO",
																	),
											  "join" => array( "glativo" => "glativo.CODATIVO = fiformadepagamento.SITUACAO" )
											 ),
											 							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovoCformaPagamento",
													 "method" => "post",
													 "action" => "financeiro/cadastros/formapagamentos/CformaPagamento/salvaEdicao",
													 "validate-form" => "data-parsley-validate='true'"
													 ),
													 
							"form" => array("Geral" => array("fiformadepagamento.CODPAGAMENTO" => array(
																	"tipo" => "label",
																	"label" => "Código Forma de Pagmento",
																	"idname" => "codpagamentoCformaPagamento",
																	"disabled" => true,
													),
													"fiformadepagamento.DESCRICAO" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Descrição",
																	"idname" => "descricaoCformaPagamento",
																	"placeholder" => "Débito",
																	"required" => true,
													),
													"fiformadepagamento.SIGLA" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Sigla",
																	"idname" => "siglaCformaPagamento",
																	"placeholder" => "DB",
																	"required" => true,
													),
													"fiformadepagamento.SITUACAO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Situação",
																		"idname" => "selSituacaoCformaPagamento",
																		"required" => true,
																		"values" => array("" => "Escolha um valor!",
																						  "1" => "Ativo",
																						  "0" => "Inativo"
																						  )
																		),
												    ),
										
									),
										
							);
		
		$this->strTabela = "fiformadepagamento";
		
    }
	
	
}