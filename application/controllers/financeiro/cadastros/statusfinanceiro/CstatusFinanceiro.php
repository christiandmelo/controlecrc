<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CstatusAcompFornecedor.
 * Classe que controla os status de acompanhamento.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */
 
class CstatusFinanceiro extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		$this->data = array(
							"tipo" => "	CRUD",
							"lista" => array("Titulo" => "Status",
											 "classe" => "CstatusFinanceiro",
											 "precaminho" => "financeiro/cadastros/statusfinanceiro/",
											 "autoinc" => "false",
											 "codigo" => "fistatus.CODFISTATUS" 
											 ),
											
							"tabela" => array("cabecalho" => array( "fistatus.CODFISTATUS" => "CODFISTATUS",
														 			"fistatus.NOME" => "NOME",
														 			"glativo.DESCRICAO" => "ATIVO",
														 			"fistatus.TIPO" => "TIPO",
																	),
											  "join" => array( "glativo" => "glativo.CODATIVO = fistatus.ATIVO" )
											 ),
											 							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovoCstatusFinanceiro",
													 "method" => "post",
													 "action" => "financeiro/cadastros/statusfinanceiro/CstatusFinanceiro/salvaEdicao",
													 "validate-form" => "data-parsley-validate='true'"
													 ),
													 
							"form" => array("Geral" => array("fistatus.CODFISTATUS" => array(
																	"tipo" => "label",
																	"label" => "Código Status",
																	"idname" => "CODFISTATUSCstatusFinanceiro",
																	"disabled" => true,
													),
													"fistatus.NOME" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Nome",
																	"idname" => "nomeCstatusFinanceiro",
																	"required" => true,
													),
													"fistatus.ATIVO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Ativo",
																		"idname" => "selAtivoCstatusFinanceiro",
																		"required" => true,
																		"values" => array("" => "Escolha um valor!",
																						  "1" => "Ativo",
																						  "0" => "Inativo"
																						  )
													),
													"fistatus.TIPO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Tipo",
																		"idname" => "selTipoCstatusFinanceiro",
																		"required" => true,
																		"values" => array("" => "Escolha um valor!",
																						  "1" => "1",
																						  )
																		),
												),
										
									),
										
							);
		
		$this->strTabela = "fistatus";
		
    }
	
	
}