<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Cfornecedor.
 * Classe que controla os fornecedores.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */
 
class Cfornecedor extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		$this->load->model("financeiro/cadastros/Mfornecedor","Mfornecedor");
		
		/*Bloco para trazer todos os municipios e carregar na tela*/
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("glmunicipio");
		$arrDataMunicipios = array("cabecalho" => array("glmunicipio.CODMUNICIPIO" => "CODMUNICIPIO",
													 	"glmunicipio.DESCRICAO" => "DESCRICAO"),
								);
		$arrDadosMunicipios = $this->Mcrud->getDados($arrDataMunicipios);
		$arrDadosMunicipiosTratados = array ("" => "Escolha um valor!");
		foreach ($arrDadosMunicipios->result() as $row){
		    $arrDadosMunicipiosTratados[$row->CODMUNICIPIO] = $row->DESCRICAO;
		}
		$this->Mcrud->setStrTable(null);
		
		$this->data = array(
							"tipo" => "	CRUD",
							"lista" => array("Titulo" => "Edição de Doadores",
											 "classe" => "Cfornecedor",
											 "precaminho" => "financeiro/cadastros/fornecedor/",
											 "autoinc" => "sim",
											 "codigo" => "fifornecedor.CODFORNECEDOR" 
											 ),
											  
							"chave" => array(	"fifornecedor.CODCLIENTE" => "CODCLIENTE",
											),
							"botoes" => array(
								"excluir" => "inativo",
							),											
							"tabela" => array("cabecalho" => array( "fifornecedor.CODFORNECEDOR" => "CODIGO",
														 			"fifornecedor.NOMEFANTASIA" => "NOMEFANTASIA",
														 			"fifornecedor.CNPJ" => "CNPJ",
																	),
											 ),
											 							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovoCfornecedor",
													 "method" => "post",
													 "action" => "financeiro/cadastros/fornecedor/Cfornecedor/salvaEdicao",
													 "validate-form" => "data-parsley-validate='true'"
													 ),
													 
							"form" => array("Identificação" => array("fifornecedor.CODFORNECEDOR" => array(
																	"tipo" => "label",
																	"label" => "Código:",
																	"idname" => "codigoCfornecedor",
																	"disabled" => true,
													),
													"fifornecedor.RAZAOSOCIAL" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Razão Social:",
																	"idname" => "RazaoSocialCfornecedor",
																	"required" => true,
													),
													"fifornecedor.NOMEFANTASIA" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Nome Fantasia:",
																	"idname" => "NomeFantasiaCfornecedor",
																	"required" => true,
													),
													"fifornecedor.CNPJ" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "CNPJ:",
																	"idname" => "CNPJCfornecedor",
																	"required" => true,
																	"mask" => "99.999.999/9999-99",
													),
											),
											"Endereço" => array(
												"fifornecedor.RUA" => array(
																	"tipo" => "label",
																	"label" => "Rua:",
																	"idname" => "RuaCfornecedor",
													),
													"fifornecedor.NUMERO" => array(
																	"tipo" => "label",
																	"label" => "Número:",
																	"idname" => "NumeroCfornecedor",
													),
													"fifornecedor.BAIRRO" => array(
																	"tipo" => "label",
																	"label" => "Bairro:",
																	"idname" => "BairroCfornecedor",
													),
													"fifornecedor.CODMUNICIPIO" => array("tipo" => "select",
																	"label" => "Municipio",
																	"idname" => "MunicipioCfornecedor",
																	"required" => true,
																	"values" => $arrDadosMunicipiosTratados
													),
													"fifornecedor.CEP" => array(
																	"tipo" => "label",
																	"label" => "CEP:",
																	"idname" => "CEPCfornecedor",
																	"mask" => "99999-999",
													),
													"fifornecedor.TELEFONE" => array(
																	"tipo" => "label",
																	"label" => "Telefone Fixo:",
																	"idname" => "TelefoneCfornecedor",
																	"mask" => "(99) 9999-9999",
													),
													"fifornecedor.TELEFONE2" => array(
																	"tipo" => "label",
																	"label" => "Telefone Celular:",
																	"idname" => "Telefone2Cfornecedor",
																	"mask" => "(99) 9 9999-9999",
													),
													"fifornecedor.EMAIL" => array(
																	"tipo" => "label",
																	"label" => "Email:",
																	"idname" => "EmailCfornecedor",
																	"required-type" => "email",
													),
											),
										
									),
										
							);
		
		$this->strTabela = "fifornecedor";
		
    }
    
	
	public function getFornecedoresClientesAtivosOptions(){
		$arrDadosFornecedor = $this->Mfornecedor->getFornecedoresClientesAtivos();
		$arrDadosFornecedorTratados = "<option value=''>Escolha um valor!</option>";
		foreach ($arrDadosFornecedor as $row){
			$arrDadosFornecedorTratados .= "<option value='".$row->CODFORNECEDOR."'>".$row->NOMEFANTASIA."</option>";
		}
		
		echo $arrDadosFornecedorTratados;
	}
	
	
}