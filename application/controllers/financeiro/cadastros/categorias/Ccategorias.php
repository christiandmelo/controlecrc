<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Ccategorias.
 * Classe que controla as categorias de contas a pagar ou receber.
 * 
 * @package financeiro
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */
 
class Ccategorias extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		$this->load->model("financeiro/cadastros/categorias/Mcategorias","Mcategorias") ;
		
		$this->load->model("global/Mcrud","Mcrud");
		$this->Mcrud->setStrTable("ficategoria");
		$arrDataCategoria = array("cabecalho" => array("ficategoria.NOME" => "NOME",
													 "ficategoria.ORDEM" => "ORDEM"),
							   "where" => array("ficategoria.CODCLIENTE" => $_SESSION['codcliente']),
							   "order" => array("ficategoria.ORDEM" => "ASC")
								);
		$arrDadosCategoria = $this->Mcrud->getDados($arrDataCategoria);
		$arrDadosCategoriaTratados[""] = "Escolha um valor!";
		foreach ($arrDadosCategoria->result() as $row){
		    $arrDadosCategoriaTratados[$row->ORDEM] = $row->ORDEM." - ".$row->NOME;
		}
		$this->Mcrud->setStrTable(null);	
		
		
		$this->data = array(
							"tipo" => "	CRUD",
							"lista" => array("Titulo" => "Categorias",
											 "classe" => "Ccategorias",
											 "precaminho" => "financeiro/cadastros/categorias/",
											 "autoinc" => "sim",
											 "codigo" => "ficategoria.CODCATEGORIA" 
											 ),
											  
							"chave" => array(	"ficategoria.CODCLIENTE" => "CODCLIENTE",
											),
							"botoes" => array(
								"excluir" => "inativo",
							),				
							"tabela" => array("cabecalho" => array( "ficategoria.CODCATEGORIA" => "CODIGO",
																	"ficategoria.NOME" => "Nome",
														 			"ficategoria.ORDEM" => "Ordem",
														 			"glativo.DESCRICAO" => "SITUACAO",
																	),
												"join" => array( "glativo" => "glativo.CODATIVO = ficategoria.SITUACAO" ),
												"order" => array("ORDEM" => "asc"),
											 ),
											 							 		
							"cabecalhoform" => array("data-ajajax" => "#divRetornoNovoCcategorias",
													 "method" => "post",
													 "action" => "financeiro/cadastros/categorias/Ccategorias/salvaEdicao",
													 "validate-form" => "data-parsley-validate='true'"
													 ),
													 
							"form" => array("Geral" => array("ficategoria.CODCATEGORIA" => array(
																	"tipo" => "label",
																	"label" => "Código",
																	"idname" => "codigoCcategorias",
																	"disabled" => true,
													),
													"ficategoria.NOME" => array(
																	"tipo" => "label",
																	"filtro" => true,
																	"label" => "Nome",
																	"idname" => "NomeCcategorias",
																	"required" => true,
													),
													"categoria.pai" => array("tipo" => "select",
																		"ativo_salvar" => false,
																		"label" => "Categoria Pai",
																		"idname" => "selCategoriaPai",
																		"onchange" => "buscarOrdem()",
																		"values" => $arrDadosCategoriaTratados
													),
													"ficategoria.ORDEM" => array(
																	"tipo" => "label",
																	"label" => "Ordem",
																	"idname" => "OrdemCcategorias",
																	"disabled" => true,
																	"value" => $this->getMaxOrdem(),
													),
													"ficategoria.SITUACAO" => array("tipo" => "select",
																		"filtro" => true,
																		"label" => "Situação",
																		"idname" => "selSituacaoCformaPagamento",
																		"required" => true,
																		"values" => array("" => "Escolha um valor!",
																						  "1" => "Ativo",
																						  "0" => "Inativo"
																						  )
													),
												),
										
									),
							"funcao_js" => "assets/js/funcoes/financeiro/cadastros/funcoesCategorias.js",
										
							);
		
		$this->strTabela = "ficategoria";
		
    }

	public function getMaxOrdemPorPai(){
		$OrdemModel = $this->Mcategorias->getMaxOrdemPorPai($_POST['ordemPai']);
		if($OrdemModel != 0){
			$arrOrdem = explode(".",$OrdemModel);
			$qtdIndiceOrdem = count($arrOrdem);
			$ordem = $arrOrdem[$qtdIndiceOrdem-1]+1;
			if(strlen($ordem) < 2){
				echo "0".$ordem;
			}else{
				echo $ordem;
			}
		}else{
			echo "01";
		}
	}
	
	public function getMaxOrdem(){
		$OrdemModel = $this->Mcategorias->getMaxOrdem();
		$arrOrdem = explode(".",$OrdemModel);
			$qtdIndiceOrdem = count($arrOrdem);
			$ordem = $arrOrdem[$qtdIndiceOrdem-1]+1;
			if(strlen($ordem) < 2){
				return "0".$ordem;
			}else{
				return $ordem;
			}
	}
	
	
}