<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

/**
 * Classe criada para controlar todas as interações com a tela de lançamentos
 * @author Christian Melo <christiandmelo@gmail.com>
 * @access public
 */
class Clancamentos extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->load->model("financeiro/contas/lancamentos/Mlancamentos","Mlancamentos") ;
		
		$this->data = array(
							"tipo" => "	CRUD",
							"lista" => array("Titulo" => "Lançamentos",
											 "classe" => "Clancamentos",
											 "precaminho" => "financeiro/contas/lancamentos/",
											 "autoinc" => "sim",
											 "codigo" => "filancamento.CODLANCAMENTO" 
											 ),
											  
							"chave" => array(	"filancamento.CODCLIENTE" => "CODCLIENTE",
												"filancamento.CODEMPRESA" => "CODEMPRESA",
												"filancamento.CODFILIAL" => "CODFILIAL",
											),
																								 
							"form" => array("Geral" => array("filancamento.CODLANCAMENTO" => array(
																	"tipo" => "label",
																	"idname" => "txtcodigoClancamentos",
																	"disabled" => true,
													),
													"filancamento.CODFISTATUS" => array(
																	"tipo" => "label",
																	"idname" => "txtstatusClancamentos",
													),
													"filancamento.CODCATEGORIA" => array(
																	"tipo" => "label",
																	"idname" => "selCategoriaClancamentos",
													),
													"filancamento.CODCONTACORRENTE" => array(
																	"tipo" => "label",
																	"idname" => "selContaCorrenteClancamentos",
													),
													"filancamento.CODPAGAMENTO" => array(
																	"tipo" => "label",
																	"idname" => "selFormaDePagamentoClancamentos",
													),
													"filancamento.DESCRICAO" => array(
																	"tipo" => "label",
																	"idname" => "txtDescricaoClancamentos",
													),
													"filancamento.CODTIPOLANCAMENTO" => array(
																	"tipo" => "label",
																	"idname" => "selTipoDeLancamentoClancamentos",
													),
													"filancamento.DATAVENCIMENTO" => array(
																	"tipo" => "label",
																	"idname" => "txtVencimentoClancamentos",
																	"datepicker" => true,
													),
													"filancamento.VALORORIGINAL" => array(
																	"tipo" => "label",
																	"idname" => "txtValorOriginalClancamentos",
																	"money" => true
													),
													"filancamento.DATABAIXA" => array(
																	"tipo" => "label",
																	"idname" => "txtPagamentoClancamentos",
																	"datepicker" => true,
													),
													"filancamento.VALORBAIXADO" => array(
																	"tipo" => "label",
																	"idname" => "txtValorPagoClancamentos",
																	"money" => true
													),
													
												),
									),
										
							);
		
		$this->strTabela = "filancamento";
	}
	
	public function index(){
		/*iniciando as variaveis que podem ser passadas por post*/
		if(isset($_POST['mes']) && $_POST['mes'] != '') $mes = $_POST['mes']; else $mes = date("m");
		if(isset($_POST['ano']) && $_POST['ano'] != '') $ano = $_POST['ano']; else $ano = date("Y");
		if(isset($_POST['tipoListagem']) && $_POST['tipoListagem'] != '') $tipoListagem = $_POST['tipoListagem']; else $tipoListagem = 1;
		
		
		/*pesquisa o valor do saldo anterior*/
		$arrDadosListaLancamentos['saldoAnterior'] = $this->Mlancamentos->getSaldoAnterior($mes,$ano);
		
		/*pesquisa os lançamentos do Mês e Anoselecionado*/
		$arrDadosListaLancamentos['registros'] = $this->Mlancamentos->getLancamentosDoMes($mes,$ano);		
		
		/*carrega a view de acordo com a listagem solicitada*/
		/* 1 => carrega o filtro tambem */
		/* 2 => recarrega somente o corpo da view */
		switch ($tipoListagem){
			case 1:
				$this->load->view("financeiro/contas/lancamentos/VlancamentosFiltro",$arrDadosListaLancamentos);
				break;
			case 2:
				$this->load->view("financeiro/contas/lancamentos/VlancamentosLista",$arrDadosListaLancamentos); 
				break;
		}
		
	}


	/**
	 * Metodo criado para listar os campos para uma nova inserção
	 * @author Christian Melo <christiandmelo@gmail.com>
	 * @access public
	 * @package ControllerLancamentos
	 * @version 1.0.1
	 * 
	 * 
	 * @return string retornando os dados para criar a view
	 */
	public function novoLancamento(){
		$arrDados['valueExiste'] = 1;
		
		/*busca as categorias*/
		$this->load->model("financeiro/cadastros/categorias/Mcategorias","Mcategorias") ;
		$arrDados['categorias'] = $this->Mcategorias->getTodasCategorias();
		
		/*busca as contas bancárias*/
		$this->load->model("financeiro/movimentacaobancaria/contabancaria/McontaBancaria","McontaBancaria");
		$this->McontaBancaria->codcliente = $_SESSION['codcliente'];
		$this->McontaBancaria->codempresa = $_SESSION['codempresa'];
		$this->McontaBancaria->codfilial = $_SESSION['codfilial'];
		$arrDados['contaBancaria'] = $this->McontaBancaria->getTodasContas();
		
		/*busca as formas de pagamento*/
		$this->load->model("financeiro/cadastros/formapagamentos/MformaPagamento","MformaPagamento") ;
		$arrDados['formapagamento'] = $this->MformaPagamento->getTodasFormasPagamentoAtivas();
		
		/*busca todos os tipos de pagamento*/
		$this->load->model("financeiro/cadastros/MtipoLancamento","MtipoLancamento") ;
		$arrDados['tipolancamento'] = $this->MtipoLancamento->getTodosTipos();
		
		$this->load->view("financeiro/contas/lancamentos/VlancamentosNovoOuEditar",$arrDados);
	}
	
}

?>