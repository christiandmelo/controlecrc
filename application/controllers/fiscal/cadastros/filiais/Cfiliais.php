<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

/**
 * Mcrud.
 * Classe para facilizar as ações de CRUD do sistema.
 *
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */
class Cfiliais extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		
		$this->data = array (
				"tipo" => "CRUD",
				"lista" => array (
						"Titulo" => "Edição de Tele Centros",
						"classe" => "cfiliais",
						"precaminho" => "fiscal/cadastros/filiais/",
						"autoinc" => "sim",
						"codigo" => "glfilial.CODFILIAL" 
				),
				"chave" => array (
						"glfilial.CODCLIENTE" => "CODCLIENTE",
						"glfilial.CODEMPRESA" => "CODEMPRESA",
						
				),
				"botoes" => array(
								"excluir" => "inativo",
							),
				"tabela" => array (
						"cabecalho" => array (
								"glfilial.CODFILIAL" => "Código",
								"glfilial.NOMEFANTASIA" => "Nome" 
						) 
				),
				"cabecalhoform" => array (
						"data-ajajax" => "#divRetornoNovocfiliais",
						"method" => "post",
						"action" => "fiscal/cadastros/filiais/cfiliais/salvaEdicao",
						"validate-form" => "data-parsley-validate='true'",
				),
				"form" => array (
						"Geral" => array (
								"glfilial.CODFILIAL" => array (
										"tipo" => "label",
										"label" => "Código:",
										"idname" => "filialCfiliais",
										"disabled" => true 
								),
								"glfilial.RAZAOSOCIAL" => array (
										"tipo" => "label",
										"filtro" => true,
										"label" => "Razão Social:",
										"idname" => "razaosocialCfiliais",
										"required" => true,
								),
								"glfilial.NOMEFANTASIA" => array (
										"tipo" => "label",
										"filtro" => true,
										"label" => "Nome Fantasia:",
										"idname" => "txtfantasiaCfiliais",
										"required" => true,
								),
								"glfilial.CNPJ" => array (
										"tipo" => "label",
										"label" => "CNPJ:",
										"idname" => "cnpjCfiliais",
										"required" => true,
										"mask" => "99.999.999/9999-99",
								),
						),
						"Endereço" => array (
								"glfilial.TELEFONE" => array (
										"tipo" => "label",
										"label" => "Telefone:",
										"idname" => "telefoneCfiliais",
								),
								"glfilial.EMAIL" => array (
										"tipo" => "label",
										"label" => "Email:",
										"required-type" => "email",
										"idname" => "emailCfiliais",
										"required" => true,
								),
								"glfilial.LOGRADOURO" => array (
										"tipo" => "label",
										"label" => "Logradouro:",
										"idname" => "logradouroCfiliais",
								),
								"glfilial.NUMERO" => array (
										"tipo" => "label",
										"label" => "Número:",
										"required-type" => "number",
										"idname" => "numeroCfiliais",
								),
								"glfilial.COMPLEMENTO" => array (
										"tipo" => "label",
										"label" => "Complemento:",
										"idname" => "complementoCfiliais",
								),
								"glfilial.CEP" => array (
										"tipo" => "label",
										"label" => "Cep:",
										"idname" => "cepCfiliais",
								),
						),
						"Gestão" => array(
								"glfilial.QUANTIDADEMAQUINAS" => array (
											"tipo" => "label",
											"label" => "Capacidade de Máquinas:",
											"required-type" => "number",
											"idname" => "CapacidadeMaquinasCfiliais",
								),
								"glfilial.ESTADOFISICO" => array("tipo" => "select",
																		"label" => "Situação",
																		"idname" => "selSituacaoCfiliais",
																		"values" => array("" => "Escolha um valor!",
																						  "Ótimo" => "Ótimo",
																						  "Bom" => "Bom",
																						  "Regular" => "Regular",
																						  "Ruim" => "Ruim"
																						  )
													),	
						)
				) 
		);
		
		$this->strTabela = "glfilial";
	}
	
	
}