<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Mcrud.
 * Classe para facilizar as ações de CRUD do sistema.
 * 
 * @package global
 * @author Christian Melo <christiandmelo@gmail.com>
 * @version 0.1
 */

//IMPORTATE => TODAS AS LINHAS COM O CÓDIGO /***/ NA FRENTE É OBRIGATORIO*/ 
 
class Cclientes extends MY_Controller{
	public function __construct() {
        parent::__construct ();
		
		/*array contendo algumas informações sobre a página que está sendo criada*/
		$this->data = array(
							// Op��o de Controlador ( Ate o momento as op��es s�o CRUD ou Parametro )
							"tipo" => "	CRUD",
							
							// Array com dados para Gerai para funcionamento do CRUD
							"lista" => array(
											 /*Nome da funcionalidade*/
											 /***/"Titulo" => "Cliente",
											 
											 /*Nome da Classe*/
											 /***/"classe" => "cclientes",
											 
											 /*Caminho de onde fica os arquivos referentes a classe*/
											 /***/"precaminho" => "global/administracao/clientes/",
											 
											 /*Responsável por descobrir se a tabela utiliza a Glautoindice*/
											 /***/"autoinc" => "nao",
											 
											 /*Código da tabela*/
											 /***/"codigo" => "fiformadepagamento.CODPAGAMENTO" 
											 ),
							"botoes" => array(
										"excluir" => "inativo",
							),
											 
							/*array para informar como é composta a chave da tabela (se só codcliente ou codcliente e codempresa ou codcliente,codempresa e codfilial*/
							/*só adiciona a chave se existir*/ 
							"chave" => array(
											"fiformadepagamento.CODCLIENTE" => "CODCLIENTE",
											),
											
							/*array que compõe os campos que vão aparecer na visão de listar, sempre o código da tabela primeiro*/
							"tabela" => array("cabecalho" => array( 
																	/***/"fiformadepagamento.CODCLIENTE" => "CodCliente",
														 			"fiformadepagamento.NOME" => "Nome",
														 			"fiformadepagamento.LIBERADO" => "Liberado",
														 			"fiformadepagamento.QTDNOTAS" => "Qtd notas",
														 			"fiformadepagamento.QTDUSUARIOS" => "Qtd usuários",
														 			"fiformadepagamento.QTDEMPRESAS" => "Qtd empresas"
																	),
											  "join" => array (
											  		"glcliente" => "glcliente.codcliente = glusuario.codcliente" 
											  	)
											 ),
											 
							/*array que compõe o cabeçalho do formulario de edição*/							 		
							"cabecalhoform" => array(
													 /*ID da div retorno do Ajax*/
													 /***/"data-ajajax" => "#divRetornoNovocclientes",
													 
													 /*Metodo de envio do Ajax*/
													 /***/"method" => "post",
													 
													 /*Caminho do arquivo que vai enviar o formulario*/
													 /***/"action" => "global/administracao/clientes/cclientes/salvaEdicao",
													 
													 /*Se o formulario possuir validação deve informar essa linha*/
													 /***/"validate-form" => "data-parsley-validate='true'"
													 ),
							/*array responsável por montar o formulario de edição*/
							"form" => 
							
									/*array que monta os "tabs", esse com o nome de "Geral"*/
									array(/***/"Geral" =>
											/*lembrar sempre de colocar os campos CODCLIENTE,CODEMPRESA,CODFILIAL caso a tabela utilize algum deles*/
											/*sempre inicia com o nome do campo na tabela mesmo do array "edita"*/ 
											array("fiformadepagamento.CODCLIENTE" => 
													array(
													/***/"tipo" => "label",
													/***/"filtro" => true,
													/***/"label" => "Código do cliente",
													/*Sempre colocar "-'Nome_da_clase'" para não ocorrer erro quando a classe for chamada"*/
													/***/"idname" => "codclienteCclientes",
													"disabled" => true,
													"placeholder" => "",
													/*esse campo pode ser true,false e não é obrigatorio informar*/
													"required" => true,
													
													/*tipos => email - number - integer - digits - alphanum - url*/
													/*Esse campo pode ser utilizado sem o required*/
													"required-type" => "number",
													
													"datepicker" => true,
													"mask" => "99",
													/*Caso o campo não deve ser salvo na tabela adicione esse valor */
													"ativo_salvar" => false,
													),

													"fiformadepagamento.LIBERADO" => 
														array(
															  /***/"tipo" => "select",
															  /***/"filtro" => true,
															  /***/"label" => "Acesso Liberado?",
															  /*Sempre colocar "-'Nome_da_clase'" para não ocorrer erro quando a classe for chamada"*/
															  /***/"idname" => "selAcessoLiberadoCclientes",
															  "required" => true,
															  /***/"values" => array(
															  					/*Deve sempre estar em branco para dar certo*/
															  					/***/"" => "Escolha um valor!",
																			    "1" => "Sim",
																			    "2" => "Não"
																			    )
															),
												),
										
									),
										
							);
		
		/*informa com qual tabela estamos trabalhando*/
		$this->strTabela = "fiformadepagamento";
		
    }
	
	
}