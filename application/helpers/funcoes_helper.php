<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if(! function_exists('usuarioLogado')){
	/**
	 * Verificar se o usuário está logado
	 * 
	 * @return mixed
	 */
	 function usuarioLogado($redirecionaLogin = false){
		
		if(isset ($_SESSION['logado'])){
			return TRUE;
		}else{
			if($redirecionaLogin){
				//die();
				?>
				<script type="text/javascript">
					$(function(){
						window.location.href = "login";	
					});
					
				</script>
				<?php
				die();
				//redirect(APPPATH . '/login');
			}
			return FALSE;
		}
	 }
	 
	 function convertDataBrasil($data){
		return(implode("/",array_reverse(explode("-", $data))));
	}
	
	function convertData($data){
		return(implode("-",array_reverse(explode("/", $data))));
	}
	
	function converteDataHora($data) {
	
		$data1=substr($data,0,10);
		$hora=substr($data,11,8);
		return implode(!strstr($data1, '/') ? "/" : "-", array_reverse(explode(!strstr($data1, '/') ? "-" : "/", $data1)))." ".$hora;
	
	}
}


?>