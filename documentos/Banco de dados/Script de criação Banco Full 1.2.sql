-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: portalcrc
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('0200ad025a03051f4bbf726835198946f03ebec0','127.0.0.1',1463073856,'__ci_last_regenerate|i:1463073856;'),('14c6a2981bb7b9622269761ceb1835c824377630','127.0.0.1',1463073859,'__ci_last_regenerate|i:1463073859;'),('1c6324e7c4ee904c25b14b5f2e4dd6c6cfe777eb','127.0.0.1',1463073858,'__ci_last_regenerate|i:1463073858;'),('26d6333b5261c28b546a3afac2ee7d6eb53965cb','127.0.0.1',1463084639,'__ci_last_regenerate|i:1463084639;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('3cb0ca00e080fdd9297f5a33a87dd44885b5ddf5','127.0.0.1',1463085843,'__ci_last_regenerate|i:1463085843;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('5d2409374ed69d319922b286e6f346e490c4cd7b','127.0.0.1',1463073858,'__ci_last_regenerate|i:1463073858;'),('73b8502ffa94a76d1f8a5d5145a7f7bb77e4f090','127.0.0.1',1463073923,'__ci_last_regenerate|i:1463073858;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('7d6a434b60030f605577568700fd57df5a085e20','127.0.0.1',1463085845,'__ci_last_regenerate|i:1463085843;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('80e1680bbe16fd3bf674a3811416eebf4b8e3f55','127.0.0.1',1463082165,'__ci_last_regenerate|i:1463082165;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('844c7fdc70bbc15c56b81661de211f4c58bb26e9','127.0.0.1',1463073859,'__ci_last_regenerate|i:1463073859;'),('93ca447a41d7d6e948a00ec48a3f629a38dd6de7','127.0.0.1',1463081806,'__ci_last_regenerate|i:1463081806;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('96d8f4d366e90c36d97b1358ab84c4dd41e3d257','127.0.0.1',1463082521,'__ci_last_regenerate|i:1463082521;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('b01e806eb7753e447757529e4ecbf355ae999cd4','127.0.0.1',1463073858,'__ci_last_regenerate|i:1463073858;'),('b5233684db3565f9b11b9cf5acade3aafcb618fc','127.0.0.1',1463073858,'__ci_last_regenerate|i:1463073858;'),('bc77def9b224436edb9db685644b7b6b0569f162','127.0.0.1',1463073859,'__ci_last_regenerate|i:1463073859;'),('c03a444f3d19fefdb00773da4d4049fb3be5dc4c','127.0.0.1',1463073858,'__ci_last_regenerate|i:1463073858;'),('c3c3970ccec2ce79ebf93ca6f45517c9d24bec28','127.0.0.1',1463073858,'__ci_last_regenerate|i:1463073858;'),('d674b90555f7947bc6736b4b8000dff345781931','127.0.0.1',1463073859,'__ci_last_regenerate|i:1463073859;'),('deef0d14eed80105a76f04cb352ac8282c8b1d75','127.0.0.1',1463073858,'__ci_last_regenerate|i:1463073858;'),('f89ade57c6a6cc8b1b057552fadcc16520be7393','127.0.0.1',1463073858,'__ci_last_regenerate|i:1463073858;'),('f94420280571631dcd111bb5c2e095574bcec377','127.0.0.1',1463084004,'__ci_last_regenerate|i:1463084004;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('f94469ab0eaf535e39db42215c9037ba0f1eacaf','127.0.0.1',1463073858,'__ci_last_regenerate|i:1463073858;'),('fb7fb440a1670cbdad2c64ab40a2180aad196eeb','127.0.0.1',1463082999,'__ci_last_regenerate|i:1463082999;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fiacomplancamento`
--

DROP TABLE IF EXISTS `fiacomplancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fiacomplancamento` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODACOMPLANCAMENTO` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODSTATUS` int(11) DEFAULT NULL,
  `VALOR` decimal(5,2) DEFAULT NULL,
  `DATA_RETORNO` date DEFAULT NULL,
  `RETORNO` int(2) DEFAULT NULL,
  `OBSERVACAO` varchar(200) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) DEFAULT NULL,
  `REGCRIADOEM` datetime DEFAULT NULL,
  `REGMODIFPOR` varchar(80) DEFAULT NULL,
  `REGMODIFEM` datetime DEFAULT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODACOMPLANCAMENTO`),
  KEY `FK_ACOMPLANCAMENTO_STATUS_IDX` (`CODCLIENTE`,`CODEMPRESA`,`CODSTATUS`),
  KEY `FK_FIACOMPLANCAMENTO_FILANCAMENTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  CONSTRAINT `FK_FIACOMPLANCAMENTO_FILANCAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) REFERENCES `filancamento` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIACOMPLANCAMENTO_FISTATUS` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODSTATUS`) REFERENCES `fistatus` (`CODCLIENTE`, `CODEMPRESA`, `CODSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fiacomplancamento`
--

LOCK TABLES `fiacomplancamento` WRITE;
/*!40000 ALTER TABLE `fiacomplancamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `fiacomplancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fiboleto`
--

DROP TABLE IF EXISTS `fiboleto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fiboleto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODBOLETO` int(11) NOT NULL,
  `DTEMISSAO` datetime NOT NULL,
  `DTVENCIMENTO` datetime NOT NULL,
  `DTBAIXA` datetime DEFAULT NULL,
  `VALOR` decimal(10,2) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODBOLETO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fiboleto`
--

LOCK TABLES `fiboleto` WRITE;
/*!40000 ALTER TABLE `fiboleto` DISABLE KEYS */;
/*!40000 ALTER TABLE `fiboleto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficartaocredito`
--

DROP TABLE IF EXISTS `ficartaocredito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ficartaocredito` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCARTAOCREDITO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  `DIAFECHAMENTO` date NOT NULL,
  `DIAVENCIMENTO` date NOT NULL,
  `LIMITE` decimal(10,2) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCARTAOCREDITO`),
  CONSTRAINT `FK_FICARTAOCREDITO_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficartaocredito`
--

LOCK TABLES `ficartaocredito` WRITE;
/*!40000 ALTER TABLE `ficartaocredito` DISABLE KEYS */;
/*!40000 ALTER TABLE `ficartaocredito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficategoria`
--

DROP TABLE IF EXISTS `ficategoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ficategoria` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCATEGORIA` int(11) NOT NULL,
  `NOME` varchar(45) NOT NULL,
  `ORDEM` varchar(45) NOT NULL,
  `SITUACAO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODCATEGORIA`),
  CONSTRAINT `FK_FICATEGORIA_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficategoria`
--

LOCK TABLES `ficategoria` WRITE;
/*!40000 ALTER TABLE `ficategoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `ficategoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficontacorrente`
--

DROP TABLE IF EXISTS `ficontacorrente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ficontacorrente` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCONTACORRENTE` int(11) NOT NULL,
  `CODBANCO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  `SITUACAO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  KEY `FK_FICONTACORRENTE_GLBANCO_idx` (`CODCLIENTE`,`CODBANCO`),
  CONSTRAINT `FK_FICONTACORRENTE_GLBANCO` FOREIGN KEY (`CODCLIENTE`, `CODBANCO`) REFERENCES `glbanco` (`CODCLIENTE`, `CODBANCO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FICONTACORRENTE_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficontacorrente`
--

LOCK TABLES `ficontacorrente` WRITE;
/*!40000 ALTER TABLE `ficontacorrente` DISABLE KEYS */;
/*!40000 ALTER TABLE `ficontacorrente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fifaturacartao`
--

DROP TABLE IF EXISTS `fifaturacartao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fifaturacartao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODFATURACARTAO` int(11) NOT NULL,
  `CODCARTAOCREDITO` int(11) NOT NULL,
  `VALORFATURA` decimal(10,2) DEFAULT NULL,
  `DATAFECHAMENTO` date DEFAULT NULL,
  `DATAVENCIMENTO` date DEFAULT NULL,
  `SITUACAO` int(11) DEFAULT NULL,
  `CODCONTACORRENTE` int(11) DEFAULT NULL,
  `CODPAGAMENTO` int(11) DEFAULT NULL,
  `DESCRICAO` varchar(45) DEFAULT NULL,
  `DATAPAGAMENTO` datetime DEFAULT NULL,
  `VALORPAGO` decimal(10,2) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODFATURACARTAO`),
  KEY `FK_FIFATURACARTAO_FICARTAOCREDITO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCARTAOCREDITO`),
  KEY `FK_FIFATURACARTAO_FICONTACORRENTE_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  KEY `FK_FIFATURACARTAO_FIFORMAPAGAMENTO_idx` (`CODCLIENTE`,`CODPAGAMENTO`),
  CONSTRAINT `FK_FIFATURACARTAO_FICARTAOCREDITO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCARTAOCREDITO`) REFERENCES `ficartaocredito` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCARTAOCREDITO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIFATURACARTAO_FICONTACORRENTE` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) REFERENCES `ficontacorrente` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIFATURACARTAO_FIFORMAPAGAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODPAGAMENTO`) REFERENCES `fiformadepagamento` (`CODCLIENTE`, `CODPAGAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fifaturacartao`
--

LOCK TABLES `fifaturacartao` WRITE;
/*!40000 ALTER TABLE `fifaturacartao` DISABLE KEYS */;
/*!40000 ALTER TABLE `fifaturacartao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fiformadepagamento`
--

DROP TABLE IF EXISTS `fiformadepagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fiformadepagamento` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODPAGAMENTO` int(11) NOT NULL,
  `DESCRICAO` varchar(80) NOT NULL,
  `SIGLA` varchar(2) NOT NULL,
  `SITUACAO` int(1) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODPAGAMENTO`),
  CONSTRAINT `FK_FIFORMADEPAGAMENTO_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fiformadepagamento`
--

LOCK TABLES `fiformadepagamento` WRITE;
/*!40000 ALTER TABLE `fiformadepagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `fiformadepagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fifornecedor`
--

DROP TABLE IF EXISTS `fifornecedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fifornecedor` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) NOT NULL,
  `CODMUNICIPIO` int(11) DEFAULT NULL,
  `RAZAOSOCIAL` varchar(200) NOT NULL,
  `NOMEFANTASIA` varchar(200) NOT NULL,
  `REGISTRO` int(11) DEFAULT NULL,
  `CNPJ` varchar(19) NOT NULL,
  `RUA` varchar(80) DEFAULT NULL,
  `NUMERO` varchar(45) DEFAULT NULL,
  `BAIRRO` varchar(50) DEFAULT NULL,
  `CEP` varchar(9) DEFAULT NULL,
  `LOGRADOURO` varchar(300) DEFAULT NULL,
  `TELEFONE` varchar(20) DEFAULT NULL,
  `TELEFONE2` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(80) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODFORNECEDOR`),
  KEY `FK_FIFORNECEDOR_FICONVECAO2_idx` (`CODCLIENTE`),
  KEY `FK_FIFORNECEDOR_GLMUNICIPIO_idx` (`CODMUNICIPIO`),
  CONSTRAINT `FK_FIFORNECEDOR_GLMUNICIPIO` FOREIGN KEY (`CODMUNICIPIO`) REFERENCES `glmunicipio` (`CODMUNICIPIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fifornecedor`
--

LOCK TABLES `fifornecedor` WRITE;
/*!40000 ALTER TABLE `fifornecedor` DISABLE KEYS */;
INSERT INTO `fifornecedor` VALUES (1,1,NULL,'Teste','Teste',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','0000-00-00 00:00:00','','0000-00-00 00:00:00'),(1,2,NULL,'Testando a inserção de um novo fornecedor','Testando a inserção de um novo fornecedor',NULL,'22.222.222/2222-22','','','','',NULL,'','','','1','2016-05-03 22:32:49','1','2016-05-03 22:32:49');
/*!40000 ALTER TABLE `fifornecedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filanboleto`
--

DROP TABLE IF EXISTS `filanboleto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filanboleto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODBOLETO` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  KEY `FK_FILANBOLETO_FIBOLETO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODBOLETO`),
  CONSTRAINT `FK_FILANBOLETO_FIBOLETO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODBOLETO`) REFERENCES `fiboleto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODBOLETO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANBOLETO_FILANCAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) REFERENCES `filancamento` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filanboleto`
--

LOCK TABLES `filanboleto` WRITE;
/*!40000 ALTER TABLE `filanboleto` DISABLE KEYS */;
/*!40000 ALTER TABLE `filanboleto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filancamento`
--

DROP TABLE IF EXISTS `filancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filancamento` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) DEFAULT NULL,
  `CODSTATUS` int(11) NOT NULL,
  `CODACOMPSTATUS` int(11) DEFAULT NULL,
  `CODCONTACORRENTE` int(11) DEFAULT NULL,
  `CODPREVISAOLAN` int(11) DEFAULT NULL,
  `CODRECORRENTELAN` int(11) DEFAULT NULL,
  `CODCATEGORIA` int(11) DEFAULT NULL,
  `CODPAGAMENTO` int(11) DEFAULT NULL,
  `CODTIPOLANCAMENTO` int(11) NOT NULL,
  `ANO` int(4) NOT NULL,
  `DATAEMISSAO` date DEFAULT NULL,
  `DATAVENCIMENTO` date DEFAULT NULL,
  `DATAMOVIMENTACAO` date DEFAULT NULL,
  `DATABAIXA` date DEFAULT NULL,
  `VALORORIGINAL` decimal(10,2) DEFAULT NULL,
  `VALORBAIXADO` decimal(10,2) DEFAULT NULL,
  `VALORCONTA` decimal(10,2) DEFAULT NULL,
  `TIPOLAN` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` varchar(45) NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  KEY `FK_FILANCAMENTO_FIRECORRENTELAN_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODRECORRENTELAN`),
  KEY `FK_FILANCAMENTO_FICATEGORIA_idx` (`CODCLIENTE`,`CODCATEGORIA`),
  KEY `FK_FILANCAMENTO_FIFORMADEPAGAMENTO_idx` (`CODCLIENTE`,`CODPAGAMENTO`),
  KEY `FK_FILANCAMENTO_FICONTACORRENTE_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  KEY `FK_FILANCAMENTO_FITIPOLANCAMENTO_idx` (`CODTIPOLANCAMENTO`),
  CONSTRAINT `FK_FILANCAMENTO_FICATEGORIA` FOREIGN KEY (`CODCLIENTE`, `CODCATEGORIA`) REFERENCES `ficategoria` (`CODCLIENTE`, `CODCATEGORIA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_FICONTACORRENTE` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) REFERENCES `ficontacorrente` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_FIFORMADEPAGAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODPAGAMENTO`) REFERENCES `fiformadepagamento` (`CODCLIENTE`, `CODPAGAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_FIRECORRENTELAN` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODRECORRENTELAN`) REFERENCES `firecorrentelan` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODRECORRENTELAN`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_FITIPOLANCAMENTO` FOREIGN KEY (`CODTIPOLANCAMENTO`) REFERENCES `fitipolancamento` (`CODTIPOLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filancamento`
--

LOCK TABLES `filancamento` WRITE;
/*!40000 ALTER TABLE `filancamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `filancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filanfaturacartao`
--

DROP TABLE IF EXISTS `filanfaturacartao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filanfaturacartao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODFATURACARTAO` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  KEY `FK_FILANFATURACARTAO_FIFATURACARTAO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODFATURACARTAO`),
  KEY `FK_FILANFATURACARTAO_FILANCAMENTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  CONSTRAINT `FK_FILANFATURACARTAO_FIFATURACARTAO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODFATURACARTAO`) REFERENCES `fifaturacartao` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODFATURACARTAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANFATURACARTAO_FILANCAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) REFERENCES `filancamento` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filanfaturacartao`
--

LOCK TABLES `filanfaturacartao` WRITE;
/*!40000 ALTER TABLE `filanfaturacartao` DISABLE KEYS */;
/*!40000 ALTER TABLE `filanfaturacartao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firecorrentelan`
--

DROP TABLE IF EXISTS `firecorrentelan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `firecorrentelan` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODRECORRENTELAN` int(11) NOT NULL,
  `CODTIPOLANCAMENTO` int(11) NOT NULL,
  `CODCATEGORIA` int(11) NOT NULL,
  `CODCONTACORRENTE` int(11) NOT NULL,
  `CODPAGAMENTO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  `VALOR` decimal(10,2) NOT NULL,
  `INTERVALO` varchar(20) NOT NULL,
  `DIAFIXO` int(11) NOT NULL,
  `DATAINICIO` date NOT NULL,
  `DATAFIM` date DEFAULT NULL,
  `BAIXAAUTOMATICA` int(11) NOT NULL DEFAULT '0',
  `DIAUTIL` int(11) DEFAULT NULL,
  `PREVISAO` int(11) DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODRECORRENTELAN`),
  KEY `FK_FIRECORRENTELAN_FITIPOLANCAMENTO_idx` (`CODTIPOLANCAMENTO`),
  KEY `FK_FIRECORRENTELAN_FICATEGORIA_idx` (`CODCLIENTE`,`CODCATEGORIA`),
  KEY `FK_FIRECORRENTELAN_FICONTACORRENTE_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  KEY `FK_FIRECORRENTELAN_FIFORMAPAGAMENTO_idx` (`CODCLIENTE`,`CODPAGAMENTO`),
  CONSTRAINT `FK_FIRECORRENTELAN_FICATEGORIA` FOREIGN KEY (`CODCLIENTE`, `CODCATEGORIA`) REFERENCES `ficategoria` (`CODCLIENTE`, `CODCATEGORIA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIRECORRENTELAN_FICONTACORRENTE` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) REFERENCES `ficontacorrente` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIRECORRENTELAN_FIFORMAPAGAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODPAGAMENTO`) REFERENCES `fiformadepagamento` (`CODCLIENTE`, `CODPAGAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIRECORRENTELAN_FITIPOLANCAMENTO` FOREIGN KEY (`CODTIPOLANCAMENTO`) REFERENCES `fitipolancamento` (`CODTIPOLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIRECORRENTELAN_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firecorrentelan`
--

LOCK TABLES `firecorrentelan` WRITE;
/*!40000 ALTER TABLE `firecorrentelan` DISABLE KEYS */;
/*!40000 ALTER TABLE `firecorrentelan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fistatus`
--

DROP TABLE IF EXISTS `fistatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fistatus` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODSTATUS` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `ATIVO` int(1) NOT NULL,
  `TIPO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODSTATUS`),
  CONSTRAINT `FK_FISTATUS_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fistatus`
--

LOCK TABLES `fistatus` WRITE;
/*!40000 ALTER TABLE `fistatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `fistatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitipolancamento`
--

DROP TABLE IF EXISTS `fitipolancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitipolancamento` (
  `CODTIPOLANCAMENTO` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(10) NOT NULL,
  `SINAL` varchar(1) NOT NULL,
  PRIMARY KEY (`CODTIPOLANCAMENTO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitipolancamento`
--

LOCK TABLES `fitipolancamento` WRITE;
/*!40000 ALTER TABLE `fitipolancamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `fitipolancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glativo`
--

DROP TABLE IF EXISTS `glativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glativo` (
  `CODATIVO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  PRIMARY KEY (`CODATIVO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glativo`
--

LOCK TABLES `glativo` WRITE;
/*!40000 ALTER TABLE `glativo` DISABLE KEYS */;
INSERT INTO `glativo` VALUES (1,'Ativo'),(2,'Inativo');
/*!40000 ALTER TABLE `glativo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glautoindice`
--

DROP TABLE IF EXISTS `glautoindice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glautoindice` (
  `CODINDICE` int(11) NOT NULL AUTO_INCREMENT,
  `CODCLIENTE` int(11) DEFAULT NULL,
  `CODEMPRESA` int(11) DEFAULT NULL,
  `CODFILIAL` int(11) DEFAULT NULL,
  `TABELA` varchar(45) NOT NULL,
  `VALOR` varchar(45) NOT NULL,
  PRIMARY KEY (`CODINDICE`),
  KEY `FK_GLAUTOINDICE_GLEMPRESA_idx` (`CODCLIENTE`,`CODEMPRESA`),
  KEY `FK_GLAUTOINDICE_GLFILIAL_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`),
  CONSTRAINT `FK_GLAUTOINDICE_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLAUTOINDICE_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLAUTOINDICE_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glautoindice`
--

LOCK TABLES `glautoindice` WRITE;
/*!40000 ALTER TABLE `glautoindice` DISABLE KEYS */;
INSERT INTO `glautoindice` VALUES (1,1,1,1,'glgrupo','10'),(4,1,1,1,'glfilial','14'),(5,1,1,NULL,'glusuariofilial','25'),(6,1,1,1,'glgrupolink','115'),(7,1,1,1,'glgrupousuario','24'),(9,1,NULL,NULL,'fifornecedor','2'),(11,1,NULL,NULL,'trtipo','8'),(12,1,NULL,NULL,'trcampostipo','1'),(13,1,NULL,NULL,'trcomponenteslocacao','2'),(14,1,NULL,NULL,'trcomponentestipo','1');
/*!40000 ALTER TABLE `glautoindice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glbanco`
--

DROP TABLE IF EXISTS `glbanco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glbanco` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODBANCO` int(11) NOT NULL,
  `CODFEBRABAN` int(11) NOT NULL,
  `DIGITO` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `NOMEREDUZIDO` varchar(45) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODBANCO`),
  KEY `FK_GBANCO_GCLIENTE_idx` (`CODCLIENTE`),
  CONSTRAINT `FK_GLBANCO_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glbanco`
--

LOCK TABLES `glbanco` WRITE;
/*!40000 ALTER TABLE `glbanco` DISABLE KEYS */;
/*!40000 ALTER TABLE `glbanco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glcliente`
--

DROP TABLE IF EXISTS `glcliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glcliente` (
  `CODCLIENTE` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(80) DEFAULT NULL,
  `LIBERADO` int(1) NOT NULL DEFAULT '0',
  `NOMEFANTASIA` varchar(80) DEFAULT NULL,
  `QTDNOTAS` int(11) NOT NULL DEFAULT '0',
  `QTDUSUARIOS` int(11) NOT NULL DEFAULT '0',
  `QTDUSUARIOSUSADOS` int(11) NOT NULL,
  `QTDEMPRESAS` int(11) NOT NULL DEFAULT '0',
  `QTDEMPRESASUSADAS` int(11) NOT NULL,
  `PERMISSAO` int(11) NOT NULL DEFAULT '1' COMMENT '1 = GLOBAL\n2 = POR FILIAL',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glcliente`
--

LOCK TABLES `glcliente` WRITE;
/*!40000 ALTER TABLE `glcliente` DISABLE KEYS */;
INSERT INTO `glcliente` VALUES (1,'CRC',1,'CRC',100,5,0,1,1,1,'1','2016-02-24 00:00:00','1','2016-02-24 00:00:00');
/*!40000 ALTER TABLE `glcliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glempresa`
--

DROP TABLE IF EXISTS `glempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glempresa` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `NOMEFANTASIA` varchar(80) NOT NULL,
  `QTDFILIAL` int(11) NOT NULL,
  `TELEFONE` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(45) DEFAULT NULL,
  `CGC` varchar(25) DEFAULT NULL,
  `INSCRICAOESTADUAL` varchar(20) DEFAULT NULL,
  `RUA` varchar(50) DEFAULT NULL,
  `NUMERO` varchar(20) DEFAULT NULL,
  `COMPLEMENTO` varchar(20) DEFAULT NULL,
  `BAIRRO` varchar(50) DEFAULT NULL,
  `PAIS` varchar(50) DEFAULT NULL,
  `CEP` varchar(20) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` varchar(45) NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`),
  CONSTRAINT `FK_GLEMPRESA_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glempresa`
--

LOCK TABLES `glempresa` WRITE;
/*!40000 ALTER TABLE `glempresa` DISABLE KEYS */;
INSERT INTO `glempresa` VALUES (1,1,'CRC','CRC',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2016-02-24 00:00:00','1','2016-02-24 00:00:00');
/*!40000 ALTER TABLE `glempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glestado`
--

DROP TABLE IF EXISTS `glestado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glestado` (
  `CODESTADO` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(80) NOT NULL,
  `CODETD` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`CODESTADO`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glestado`
--

LOCK TABLES `glestado` WRITE;
/*!40000 ALTER TABLE `glestado` DISABLE KEYS */;
INSERT INTO `glestado` VALUES (1,'Minas Gerais','MG');
/*!40000 ALTER TABLE `glestado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glfilial`
--

DROP TABLE IF EXISTS `glfilial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glfilial` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `RAZAOSOCIAL` varchar(80) DEFAULT NULL,
  `NOMEFANTASIA` varchar(80) DEFAULT NULL,
  `CNPJ` varchar(45) DEFAULT NULL,
  `TELEFONE` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `LOGRADOURO` varchar(80) DEFAULT NULL,
  `NUMERO` varchar(10) DEFAULT NULL,
  `COMPLEMENTO` varchar(20) DEFAULT NULL,
  `CEP` varchar(10) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`),
  CONSTRAINT `FK_GLFILIAL_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glfilial`
--

LOCK TABLES `glfilial` WRITE;
/*!40000 ALTER TABLE `glfilial` DISABLE KEYS */;
INSERT INTO `glfilial` VALUES (1,1,1,'CRC','CRC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2016-02-24 00:00:00','1','2016-02-24 00:00:00'),(1,1,2,'Centro 1','Centro 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2015-03-04 00:00:00','1','2015-03-04 00:00:00'),(1,1,5,'CRC Ipiranga','CRC Ipiranga','12.345.678/9101-11','','crc@gmail.com','','','','','18','2016-05-04 20:39:43','18','2016-05-04 20:39:43'),(1,1,7,'Espaço BH Cidadania - CRAS Petrópoli','CRAS Petrópolis','12.345.687/8888-88','','crc@gmail.com','','','','','18','2016-05-04 20:48:02','18','2016-05-04 20:48:28'),(1,1,8,'Centro Cultural Lindéia Regina','Centro Cultural Lindéia Regina','11.111.111/1111-11','','crc@gmail.com','','','','','18','2016-05-04 20:49:31','18','2016-05-04 20:49:31'),(1,1,10,'Centro De Convivência Cézar Campos','Centro De Convivência Cézar Campos','55.555.555/5555-55','','crc@gkdksk.com','','','','','18','2016-05-04 20:53:52','18','2016-05-04 20:53:52'),(1,1,11,'Centro Cultural Da Vila Marçola','Centro Cultural Da Vila Marçola','54.444.444/4444-44','','daianebjnknj@gmail.com','','','','','18','2016-05-04 20:57:41','18','2016-05-04 20:57:41'),(1,1,12,'Centro De Integração Martinho','Centro De Integração Martinho','77.777.777/7777-77','','crc@gmail.com','','','','','18','2016-05-04 20:59:09','18','2016-05-04 20:59:09');
/*!40000 ALTER TABLE `glfilial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glgrupo`
--

DROP TABLE IF EXISTS `glgrupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glgrupo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODGRUPO` int(11) NOT NULL,
  `NOME` varchar(50) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glgrupo`
--

LOCK TABLES `glgrupo` WRITE;
/*!40000 ALTER TABLE `glgrupo` DISABLE KEYS */;
INSERT INTO `glgrupo` VALUES (1,1,1,'Administrador','1','2016-03-01 10:15:00','1','2016-04-27 14:22:07'),(1,1,2,'Básico','1','2016-03-04 13:43:00','1','2016-03-04 13:43:00'),(1,1,6,'Secretaria','1','2016-04-27 14:22:17','1','2016-04-27 14:22:17'),(1,1,7,'Trilhagem','1','2016-04-27 14:22:30','1','2016-04-27 14:22:30'),(1,1,8,'criado em sala','1','2016-04-28 00:20:40','1','2016-04-28 00:20:40'),(1,1,9,'Mestre - Somente funcionando','1','2016-05-02 23:02:46','1','2016-05-02 23:02:46');
/*!40000 ALTER TABLE `glgrupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glgrupolink`
--

DROP TABLE IF EXISTS `glgrupolink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glgrupolink` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODGRUPOLINK` int(11) NOT NULL,
  `CODGRUPO` int(11) NOT NULL,
  `CODLINK` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`,`CODLINK`,`CODGRUPOLINK`,`CODFILIAL`),
  KEY `FK_GLGRUPOLINK_GLLINK_idx` (`CODLINK`),
  CONSTRAINT `FK_GLGRUPOLINK_GLGRUPO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) REFERENCES `glgrupo` (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLGRUPOLINK_GLLINK` FOREIGN KEY (`CODLINK`) REFERENCES `gllink` (`CODLINK`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glgrupolink`
--

LOCK TABLES `glgrupolink` WRITE;
/*!40000 ALTER TABLE `glgrupolink` DISABLE KEYS */;
INSERT INTO `glgrupolink` VALUES (1,1,1,1,1,1,'1','2016-03-01 10:45:01','1','2016-03-01 10:45:01'),(1,1,1,4,1,4,'1','2016-03-01 10:45:01','1','2016-03-01 10:45:01'),(1,1,1,5,1,5,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,6,1,6,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,7,1,7,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,8,1,8,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,9,1,9,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,10,1,10,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,18,1,18,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,19,1,19,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,20,1,20,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,21,1,21,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,22,1,22,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,23,1,23,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,24,1,24,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,26,1,26,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,27,1,27,'1','2016-03-01 10:45:01','1','2016-03-01 10:45:01'),(1,1,1,28,1,28,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,29,1,29,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,30,1,30,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,31,1,31,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,32,1,32,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,41,1,41,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,42,1,42,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,43,1,43,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,44,1,44,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,45,1,45,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,46,1,46,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,47,1,47,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,48,1,48,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,115,1,49,'1','2016-05-12 17:12:36','1','2016-05-12 17:12:36'),(1,1,1,50,1,50,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,83,1,51,'1','2016-04-27 21:31:56','1','2016-04-27 21:31:56'),(1,1,1,110,1,53,'1','2016-05-12 17:10:30','1','2016-05-12 17:10:30'),(1,1,1,112,1,55,'1','2016-05-12 17:10:47','1','2016-05-12 17:10:47'),(1,1,1,108,1,56,'1','2016-05-12 17:10:20','1','2016-05-12 17:10:20'),(1,1,1,109,1,57,'1','2016-05-12 17:10:26','1','2016-05-12 17:10:26'),(1,1,1,113,1,59,'1','2016-05-12 17:10:51','1','2016-05-12 17:10:51'),(1,1,1,114,1,60,'1','2016-05-12 17:10:56','1','2016-05-12 17:10:56'),(1,1,1,51,2,18,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,52,2,19,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,53,2,20,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,54,2,21,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,55,2,22,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,56,2,23,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,57,2,24,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,59,2,26,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,60,2,27,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,61,2,28,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,62,2,29,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,63,2,30,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,64,2,31,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,65,2,32,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,66,6,18,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,67,6,19,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,68,6,26,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,69,6,42,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,70,6,43,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,71,6,48,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,72,7,18,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,73,7,19,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,74,7,26,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,75,7,42,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,76,7,43,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,77,7,44,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,78,7,45,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,79,7,46,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,80,7,48,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,81,7,49,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,84,8,1,'1','2016-04-28 00:29:26','1','2016-04-28 00:29:26'),(1,1,1,85,8,4,'1','2016-04-28 00:29:45','1','2016-04-28 00:29:45'),(1,1,1,86,8,50,'1','2016-04-28 00:30:08','1','2016-04-28 00:30:08'),(1,1,1,87,9,1,'1','2016-05-01 15:16:39','1','2016-05-01 15:16:39'),(1,1,1,89,9,5,'1','2016-05-01 15:16:47','1','2016-05-01 15:16:47'),(1,1,1,90,9,8,'1','2016-05-01 15:24:14','1','2016-05-01 15:24:14'),(1,1,1,91,9,9,'1','2016-05-01 15:24:17','1','2016-05-01 15:24:17'),(1,1,1,92,9,10,'1','2016-05-01 15:24:22','1','2016-05-01 15:24:22'),(1,1,1,93,9,18,'1','2016-05-01 15:24:27','1','2016-05-01 15:24:27'),(1,1,1,94,9,19,'1','2016-05-01 15:24:31','1','2016-05-01 15:24:31'),(1,1,1,95,9,20,'1','2016-05-01 15:24:42','1','2016-05-01 15:24:42'),(1,1,1,96,9,26,'1','2016-05-01 15:24:52','1','2016-05-01 15:24:52'),(1,1,1,99,9,41,'1','2016-05-01 15:24:57','1','2016-05-01 15:24:57'),(1,1,1,100,9,42,'1','2016-05-01 15:25:13','1','2016-05-01 15:25:13'),(1,1,1,101,9,43,'1','2016-05-01 15:25:18','1','2016-05-01 15:25:18'),(1,1,1,102,9,44,'1','2016-05-01 15:25:22','1','2016-05-01 15:25:22'),(1,1,1,87,9,45,'1','2016-05-03 22:44:11','1','2016-05-03 22:44:11'),(1,1,1,103,9,47,'1','2016-05-01 15:25:31','1','2016-05-01 15:25:31'),(1,1,1,104,9,48,'1','2016-05-01 15:25:34','1','2016-05-01 15:25:34'),(1,1,1,105,9,50,'1','2016-05-01 15:25:38','1','2016-05-01 15:25:38'),(1,1,1,106,9,51,'1','2016-05-01 15:25:42','1','2016-05-01 15:25:42');
/*!40000 ALTER TABLE `glgrupolink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glgrupousuario`
--

DROP TABLE IF EXISTS `glgrupousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glgrupousuario` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODGRUPOUSUARIO` int(11) NOT NULL,
  `CODGRUPO` int(11) NOT NULL,
  `CODUSUARIO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPOUSUARIO`,`CODGRUPO`,`CODUSUARIO`),
  KEY `FK_GLGRUPOUSUARIO_GLUSUARIO_idx` (`CODUSUARIO`),
  KEY `FK_GLGRUPOUSUARIO_GLGRUPO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`),
  CONSTRAINT `FK_GLGRUPOUSUARIO_GLGRUPO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) REFERENCES `glgrupo` (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLGRUPOUSUARIO_GLUSUARIO` FOREIGN KEY (`CODUSUARIO`) REFERENCES `glusuario` (`CODUSUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glgrupousuario`
--

LOCK TABLES `glgrupousuario` WRITE;
/*!40000 ALTER TABLE `glgrupousuario` DISABLE KEYS */;
INSERT INTO `glgrupousuario` VALUES (1,1,1,1,1,1,'1','2016-03-01 10:23:00','1','2016-03-01 10:23:00'),(1,1,1,2,1,2,'1','2016-03-01 10:23:00','1','2016-03-01 10:23:00'),(1,1,1,3,2,3,'1','2016-03-01 10:23:00','1','2016-03-01 10:23:00'),(1,1,1,4,6,4,'1','2015-03-04 00:00:00','1','2015-03-04 00:00:00'),(1,1,1,5,7,5,'1','2015-03-04 00:00:00','1','2015-03-04 00:00:00'),(1,1,1,7,2,1,'1','2016-04-27 21:45:46','1','2016-04-27 21:45:46'),(1,1,1,8,8,1,'1','2016-04-28 00:32:04','1','2016-04-28 00:32:04'),(1,1,1,9,9,6,'1','2016-05-01 15:15:38','1','2016-05-01 15:15:38'),(1,1,1,10,9,7,'1','2016-05-01 15:15:42','1','2016-05-01 15:15:42'),(1,1,1,11,9,8,'1','2016-05-01 15:15:45','1','2016-05-01 15:15:45'),(1,1,1,12,9,9,'1','2016-05-01 15:15:48','1','2016-05-01 15:15:48'),(1,1,1,13,9,10,'1','2016-05-01 15:15:51','1','2016-05-01 15:15:51'),(1,1,1,14,9,11,'1','2016-05-01 15:15:54','1','2016-05-01 15:15:54'),(1,1,1,15,9,12,'1','2016-05-01 15:15:57','1','2016-05-01 15:15:57'),(1,1,1,16,9,13,'1','2016-05-01 15:16:01','1','2016-05-01 15:16:01'),(1,1,1,17,9,14,'1','2016-05-01 15:16:04','1','2016-05-01 15:16:04'),(1,1,1,18,9,15,'1','2016-05-01 15:16:08','1','2016-05-01 15:16:08'),(1,1,1,19,9,16,'1','2016-05-01 15:16:11','1','2016-05-01 15:16:11'),(1,1,1,20,9,17,'1','2016-05-01 15:16:14','1','2016-05-01 15:16:14'),(1,1,1,21,9,18,'1','2016-05-01 15:16:18','1','2016-05-01 15:16:18'),(1,1,1,22,9,19,'1','2016-05-01 15:16:21','1','2016-05-01 15:16:21'),(1,1,1,23,9,21,'1','2016-05-03 14:38:48','1','2016-05-03 14:38:48');
/*!40000 ALTER TABLE `glgrupousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glhelpsystem`
--

DROP TABLE IF EXISTS `glhelpsystem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glhelpsystem` (
  `CODHELPSYSTEM` int(11) NOT NULL AUTO_INCREMENT,
  `CODLINK` int(11) DEFAULT NULL,
  `ID` varchar(45) DEFAULT NULL,
  `TEXTO` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`CODHELPSYSTEM`),
  KEY `FK_GLLINK_GLHELPSYSTEM_idx` (`CODLINK`),
  CONSTRAINT `FK_GLLINK_GLHELPSYSTEM` FOREIGN KEY (`CODLINK`) REFERENCES `gllink` (`CODLINK`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glhelpsystem`
--

LOCK TABLES `glhelpsystem` WRITE;
/*!40000 ALTER TABLE `glhelpsystem` DISABLE KEYS */;
INSERT INTO `glhelpsystem` VALUES (1,NULL,'global.contexto','Neste local você escolhe qual Tele centro deseja trabalhar.'),(4,NULL,'global.infoUsuario','Neste local você pode verificar e alterar suas informações pessoais.'),(5,NULL,'global.buscaMenu','Neste local você pode filtrar as funcionalidades disponíveis.'),(6,NULL,'global.menuLateral','Neste local fica o Menu Lateral. Você pode utilizar o mouse para navegar por ele.'),(7,NULL,'global.404','Bom... Eu so tenho a pedir desculpas... Você não deveria ter que ver essa pagina...'),(8,NULL,'global.404Botao','Aqui você consegue voltar para a tela inicial. Ate porque não tem nada de bom aqui...'),(9,NULL,'global.oriscenter','Logomarca do sistema.');
/*!40000 ALTER TABLE `glhelpsystem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gllink`
--

DROP TABLE IF EXISTS `gllink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gllink` (
  `CODLINK` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(45) NOT NULL,
  `CAMINHO` varchar(80) NOT NULL,
  `DESCRICAO` varchar(80) DEFAULT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `ADMIN` int(1) DEFAULT '1',
  `ORDEM` varchar(12) DEFAULT NULL,
  `ICONEFA` varchar(45) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODLINK`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gllink`
--

LOCK TABLES `gllink` WRITE;
/*!40000 ALTER TABLE `gllink` DISABLE KEYS */;
INSERT INTO `gllink` VALUES (1,'Gestão Global','#','global',1,0,'01.00.00','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(4,'Administração','#','global/administração',1,1,'01.04.00','fa fa-suitcase','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(5,'Cliente','#/global/administracao/clientes/Cclientes','global/administração/clientes',0,1,'01.04.05','fa fa-money','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(6,'Links','#/global/administracao/links/Clinks','global/administração/links',0,1,'01.04.06','fa fa-anchor','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(7,'Empresas','#/global/administracao/empresas/Cempresas','global/administração/empresas',0,1,'01.04.07','fa fa-building','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(8,'Segurança','#','global/segurança',1,0,'01.08.00','fa fa-key','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(9,'Usuários','#/global/seguranca/usuarios/Cusuarios','global/segurança/usuários',1,0,'01.08.01','fa fa-user','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(10,'Grupos','#/global/seguranca/grupos/Cgrupos','global/segurança/grupos',1,0,'01.08.02','fa fa-users','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(18,'Gestão Financeira','#','financeiro',1,0,'03.00.00','fa fa-money','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(19,'Cadastros','#','financeiro/cadastros',1,0,'03.01.00','fa fa-gears','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(20,'Movimentação Bancária','#','financeiro/movimentacaobancaria',0,0,'03.02.00','fa fa-bank','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(21,'Banco','#/financeiro/movimentacaobancaria/banco/Cbanco','financeiro/movimentacaobancaria/bancos',0,0,'03.02.01','fa fa-bank','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(22,'Cartão de Crédito','#/financeiro/movimentacaobancaria/cartaocredito/CcartaoCredito','financeiro/movimentacaobancaria/cartãocrédito',0,0,'03.02.03','fa fa-credit-card','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(23,'Conta Bancária','#/financeiro/movimentacaobancaria/contabancaria/CcontaBancaria','financeiro/movimentacaobancaria/contabancaria',0,0,'03.02.02','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(24,'Status de Acompanhamento','#/financeiro/cadastros/statusfinanceiro/CstatusFinanceiro','financeiro/cadastros/statufinanceiro',0,0,'03.01.01','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(26,'Cliente/Fornecedor','#/financeiro/cadastros/fornecedor/Cfornecedor','financeiro/cadastros/situacaofornecedor',1,0,'03.01.03','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(27,'Formas de Pagamento','#/financeiro/cadastros/formapagamentos/CformaPagamento','financeiro/cadastros/situacaofornecedor',0,0,'03.01.04','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(28,'Contas a pagar / Despesas','#','financeiro/contas/',0,0,'03.03.00','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(29,'Lançamentos','#/financeiro/contas/lancamentos/Clancamentos','financeiro/contas/lancamentos/',0,0,'03.03.01','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(30,'Lançamentos Recorrentes','#/financeiro/contas/lancamentosrecorrentes/ClancamentosRecorrentes','financeiro/contas/lancamentosrecorrentes/',0,0,'03.03.02','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(31,'Previsão de Lançamentos','#/financeiro/contas/previsaolancamentos/CprevisaoLancamentos','financeiro/contas/previsaolancamentos/',0,0,'03.03.03','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(32,'Boletos','#/financeiro/contas/boletos/Cboletos','financeiro/contas/boletos/',0,0,'03.03.04','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(41,'Tele Centros','#/fiscal/cadastros/filiais/Cfiliais','global/administração/Tele Centros',1,0,'01.04.08','fa fa-institution alias','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(42,'Gestão Estoque e Doações','#','estoque',1,0,'04.00.00','fa fa-gift','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(43,'Cadastros','#','estoque/cadastros/',1,0,'04.01.00','fa fa-gears','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(44,'Tipos de Doação','#/estoque/cadastros/tipo/Ctipo','estoque/cadastros/tipo',1,0,'04.01.01','fa fa-filter','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(45,'Campos do Tipo de Doação','#/estoque/cadastros/campostipo/CcamposTipo','estoque/cadastros/camposTipoEntrada',1,0,'04.01.02','fa fa-sitemap','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(46,'Triagem','#/estoque/estoque/triagem/Ctriagem','',1,0,'04.03.01','fa fa-gift','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(47,'Permissões do grupo','#/global/seguranca/grupos/CgrupoLink','global/segurança/Permissao Grupo',1,0,'01.08.03','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(48,'Entrada Doação','#/estoque/secretaria/entrada/CentradaDoacao','',1,0,'04.02.01','fa fa-globe','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(49,'Estoque','#',NULL,1,0,'04.03.00','fa fa-globe','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(50,'Tele centros do usuário','#/global/seguranca/grupos/CusuarioFilial','global/segurança/Usuario filial',1,0,'01.08.04','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(51,'Grupos do Usuário','#/global/seguranca/grupos/CgrupoUsuario','global/segurança/Grupo Usuario',1,0,'01.08.05','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(53,'Secretaria','#',NULL,1,0,'04.02.00','fa fa-globe','1','2016-05-12 11:50:00','1','2016-05-12 11:50:00'),(55,'Locação','#/estoque/estoque/locacao/Clocacao',NULL,1,0,'04.03.02','fa fa-globe','1','2016-05-12 11:50:00','1','2016-05-12 11:50:00'),(56,'Componentes do Tipo de Doação','#/estoque/cadastros/componentestipo/CcomponentesTipo',NULL,1,0,'04.01.03','fa fa-globe','1','2016-05-12 11:50:00','1','2016-05-12 11:50:00'),(57,'Componentes para locação','#/estoque/cadastros/componenteslocacao/CcomponentesLocacao',NULL,1,0,'04.01.04','fa fa-globe','1','2016-05-12 11:50:00','1','2016-05-12 11:50:00'),(59,'Venda','#/estoque/estoque/venda/Cvenda',NULL,1,0,'04.03.03','fa fa-globe','1','2016-05-12 11:50:00','1','2016-05-12 11:50:00'),(60,'Doações Externas','#/estoque/estoque/doacaoexterna/CdoacaoExterna',NULL,1,0,'04.03.04','fa fa-globe','1','2016-05-12 11:50:00','1','2016-05-12 11:50:00');
/*!40000 ALTER TABLE `gllink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glmunicipio`
--

DROP TABLE IF EXISTS `glmunicipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glmunicipio` (
  `CODMUNICIPIO` int(11) NOT NULL AUTO_INCREMENT,
  `CODESTADO` int(11) NOT NULL,
  `DESCRICAO` varchar(80) NOT NULL,
  `OBSERVACAO` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`CODMUNICIPIO`),
  KEY `FK_MUNICIPIO_ESTADO_IDX` (`CODESTADO`),
  CONSTRAINT `FK_GLMUNICIPIO_GLESTADO` FOREIGN KEY (`CODESTADO`) REFERENCES `glestado` (`CODESTADO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glmunicipio`
--

LOCK TABLES `glmunicipio` WRITE;
/*!40000 ALTER TABLE `glmunicipio` DISABLE KEYS */;
INSERT INTO `glmunicipio` VALUES (1,1,'Belo Horizonte',NULL);
/*!40000 ALTER TABLE `glmunicipio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glusuario`
--

DROP TABLE IF EXISTS `glusuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glusuario` (
  `CODUSUARIO` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(80) NOT NULL,
  `NOME` varchar(80) DEFAULT NULL,
  `SENHA` varchar(80) NOT NULL,
  `ATIVO` int(1) DEFAULT NULL,
  `CAMINHOFOTO` varchar(200) DEFAULT NULL,
  `CODCLIENTE` int(11) DEFAULT NULL,
  `CODEMPRESA` int(11) DEFAULT NULL,
  `CODFILIAL` int(11) DEFAULT NULL,
  `TIPOUSUARIO` int(11) DEFAULT '4',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODUSUARIO`),
  UNIQUE KEY `INDEX_GLUSUARIO` (`EMAIL`),
  KEY `idx_glusuario_CODUSUARIO_CODCLIENTE` (`CODUSUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glusuario`
--

LOCK TABLES `glusuario` WRITE;
/*!40000 ALTER TABLE `glusuario` DISABLE KEYS */;
INSERT INTO `glusuario` VALUES (1,'christiandmelo@gmail.com','Christian Luis de Melo','e10adc3949ba59abbe56e057f20f883e',1,'/assets/img/perfil/1-christiandmelo@gmail.com.jpg',1,1,1,1,'1','2016-02-24 00:00:00','1','2016-05-07 15:09:05'),(2,'oswaldo@crc.com.br','Oswaldo','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-02-24 00:00:00','1','2016-02-24 00:00:00'),(3,'israel@crc.com.br','Israel','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-02-24 00:00:00','1','2016-02-24 00:00:00'),(4,'secretaria@crc.com.br','Secretaria - CRC','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,3,'1','2015-03-04 00:00:00','1','2015-03-04 00:00:00'),(5,'trilhagem@crc.com.br','Trilhagem - CRC','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,3,'1','2015-03-04 00:00:00','1','2015-03-04 00:00:00'),(6,'pollyanna.m.abreu@gmail.com','Pollyanna Abreu','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(7,'davi.davipires@gmail.com','Davi Pires','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(8,'proflucasscf@gmail.com','Lucas','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(9,'diegogm38@hotmail.com','Diego','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(10,'israel.teixeira17@gmail.com','Israel Teixeira','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(11,'alissonking3@gmail.com','Alisson','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(12,'juliocesarsbrito@gmail.com','Julio Cesar','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(13,'fatimabastos05@gmail.com','Fatima Bastos','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(14,'lucianotkd00@gmail.com','Luciano','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(15,'diegogm38@gmail.com','Diego','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(16,'juningzn13@gmail.com','Osvaldo','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(17,'davi.lotus@gmail.com','Davi','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(18,'daianelirio@gmail.com','Daiane Lirio','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(19,'gustavomloiola@gmail.com','Gustavo Loiola','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(20,'ericksonguilherme@gmail.com','Erickson Guilherme','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(21,'wesley.cadete@gmail.com','Wesley','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00'),(22,'lucasalves.souza01@gmail.com','Lucas Alves','e10adc3949ba59abbe56e057f20f883e',1,NULL,1,1,1,1,'1','2016-05-01 12:02:00','1','2016-05-01 12:02:00');
/*!40000 ALTER TABLE `glusuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glusuariofilial`
--

DROP TABLE IF EXISTS `glusuariofilial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glusuariofilial` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODUSUARIOFILIAL` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODUSUARIO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) DEFAULT NULL,
  `REGCRIADOEM` datetime DEFAULT NULL,
  `REGMODIFPOR` varchar(80) DEFAULT NULL,
  `REGMODIFEM` datetime DEFAULT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODUSUARIOFILIAL`),
  KEY `FK_GLUSUARIOFILIAL_FK_GLUSUARIO_idx` (`CODUSUARIO`),
  KEY `FK_GLUSUARIOFILIAL_GLEMPRESA_idx` (`CODCLIENTE`,`CODEMPRESA`),
  CONSTRAINT `FK_GLUSUARIOFILIAL_FK_GLUSUARIO` FOREIGN KEY (`CODUSUARIO`) REFERENCES `glusuario` (`CODUSUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLUSUARIOFILIAL_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glusuariofilial`
--

LOCK TABLES `glusuariofilial` WRITE;
/*!40000 ALTER TABLE `glusuariofilial` DISABLE KEYS */;
INSERT INTO `glusuariofilial` VALUES (1,1,1,10,1,NULL,NULL,'1','2016-05-07 05:13:41'),(1,1,2,1,2,NULL,NULL,NULL,NULL),(1,1,3,1,3,NULL,NULL,NULL,NULL),(1,1,4,1,4,NULL,NULL,NULL,NULL),(1,1,5,1,5,NULL,NULL,NULL,NULL),(1,1,6,2,1,NULL,NULL,NULL,NULL),(1,1,8,2,2,'1','2016-04-27 21:02:27','1','2016-04-27 21:02:27'),(1,1,9,4,1,'1','2016-04-28 00:19:18','1','2016-04-28 00:19:18'),(1,1,10,1,6,'1','2016-05-01 15:11:56','1','2016-05-01 15:11:56'),(1,1,11,1,7,'1','2016-05-01 15:12:00','1','2016-05-01 15:12:00'),(1,1,12,1,8,'1','2016-05-01 15:12:04','1','2016-05-01 15:12:04'),(1,1,13,1,9,'1','2016-05-01 15:12:07','1','2016-05-01 15:12:07'),(1,1,14,1,10,'1','2016-05-01 15:12:11','1','2016-05-01 15:12:11'),(1,1,15,1,11,'1','2016-05-01 15:12:15','1','2016-05-01 15:12:15'),(1,1,16,1,12,'1','2016-05-01 15:12:19','1','2016-05-01 15:12:19'),(1,1,17,1,13,'1','2016-05-01 15:12:22','1','2016-05-01 15:12:22'),(1,1,18,1,14,'1','2016-05-01 15:12:26','1','2016-05-01 15:12:26'),(1,1,19,1,15,'1','2016-05-01 15:12:30','1','2016-05-01 15:12:30'),(1,1,20,1,16,'1','2016-05-01 15:12:34','1','2016-05-01 15:12:34'),(1,1,21,1,17,'1','2016-05-01 15:12:37','1','2016-05-01 15:12:37'),(1,1,22,1,18,'1','2016-05-01 15:12:41','1','2016-05-01 15:12:41'),(1,1,23,1,19,'1','2016-05-01 15:12:44','1','2016-05-01 15:12:44'),(1,1,25,1,1,'10','2016-05-07 15:08:48','10','2016-05-07 15:08:48');
/*!40000 ALTER TABLE `glusuariofilial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcamposproduto`
--

DROP TABLE IF EXISTS `trcamposproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcamposproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCAMPOSPRODUTO` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `CODCAMPOSTIPO` int(11) NOT NULL,
  `VALOR` varchar(200) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCAMPOSPRODUTO`),
  KEY `FK_TRCAMPOSPRODUTO_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  KEY `FK_TRCAMPOSPRODUTO_TRCAMPOSTIPO_idx` (`CODCLIENTE`,`CODCAMPOSTIPO`),
  CONSTRAINT `FK_TRCAMPOSPRODUTO_TRCAMPOSTIPO` FOREIGN KEY (`CODCLIENTE`, `CODCAMPOSTIPO`) REFERENCES `trcampostipo` (`CODCLIENTE`, `CODCAMPOSTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRCAMPOSPRODUTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcamposproduto`
--

LOCK TABLES `trcamposproduto` WRITE;
/*!40000 ALTER TABLE `trcamposproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trcamposproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcampostipo`
--

DROP TABLE IF EXISTS `trcampostipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcampostipo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCAMPOSTIPO` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `NOME` varchar(200) NOT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODCAMPOSTIPO`),
  KEY `FK_TRCAMPOSTIPO_TRTIPO_idx` (`CODCLIENTE`,`CODTIPO`),
  CONSTRAINT `FK_TRCAMPOSTIPO_TRTIPO` FOREIGN KEY (`CODCLIENTE`, `CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcampostipo`
--

LOCK TABLES `trcampostipo` WRITE;
/*!40000 ALTER TABLE `trcampostipo` DISABLE KEYS */;
INSERT INTO `trcampostipo` VALUES (1,1,2,'Capacidade',1,'1','2016-05-12 21:50:52','1','2016-05-12 21:50:52');
/*!40000 ALTER TABLE `trcampostipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcomponenteslocacao`
--

DROP TABLE IF EXISTS `trcomponenteslocacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcomponenteslocacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCOMPONENTESLOCACAO` int(11) NOT NULL,
  `CODTIPOPAI` int(11) NOT NULL,
  `CODTIPOFILHO` int(11) NOT NULL,
  `ATIVO` int(11) NOT NULL,
  `OBRIGATORIO` int(11) NOT NULL DEFAULT '0',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODCOMPONENTESLOCACAO`),
  KEY `FK_TRCOMPONENTESLOCACAO_TRTIPO_idx` (`CODCLIENTE`,`CODTIPOPAI`),
  KEY `FK_TRCOMPOENTESLOCACAO_TRTIPOFILHO_idx` (`CODCLIENTE`,`CODTIPOFILHO`),
  CONSTRAINT `FK_TRCOMPOENTESLOCACAO_TRTIPOFILHO` FOREIGN KEY (`CODCLIENTE`, `CODTIPOFILHO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRCOMPONENTESLOCACAO_TRTIPOPAI` FOREIGN KEY (`CODCLIENTE`, `CODTIPOPAI`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcomponenteslocacao`
--

LOCK TABLES `trcomponenteslocacao` WRITE;
/*!40000 ALTER TABLE `trcomponenteslocacao` DISABLE KEYS */;
INSERT INTO `trcomponenteslocacao` VALUES (1,2,1,7,1,1,'1','2016-05-12 22:16:06','1','2016-05-12 22:23:55');
/*!40000 ALTER TABLE `trcomponenteslocacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcomponentesproduto`
--

DROP TABLE IF EXISTS `trcomponentesproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcomponentesproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCOMPONENTESPRODUTO` int(11) NOT NULL,
  `CODCOMPONENTESTIPO` int(11) NOT NULL,
  `CODPRODUTOPAI` int(11) NOT NULL,
  `CODPRODUTOFILHO` int(11) NOT NULL,
  `REGCRIADPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCOMPONENTESPRODUTO`),
  KEY `FK_COMPONENTESEQUIPAMENTO_EQUIPAMENTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOPAI`),
  KEY `FK_COMPONENTESPRODUTO_COMPONENTESTIPO_idx` (`CODCLIENTE`,`CODCOMPONENTESTIPO`),
  KEY `FK_COMPONENTESPRODUTO_PRODUTO_FILHO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOFILHO`),
  CONSTRAINT `FK_COMPONENTESPRODUTO_COMPONENTESTIPO` FOREIGN KEY (`CODCLIENTE`, `CODCOMPONENTESTIPO`) REFERENCES `trcomponentestipo` (`CODCLIENTE`, `CODCOMPONENTESTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_COMPONENTESPRODUTO_PRODUTO_FILHO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTOFILHO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_COMPONENTESPRODUTO_PRODUTO_PAI` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTOPAI`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcomponentesproduto`
--

LOCK TABLES `trcomponentesproduto` WRITE;
/*!40000 ALTER TABLE `trcomponentesproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trcomponentesproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcomponentestipo`
--

DROP TABLE IF EXISTS `trcomponentestipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcomponentestipo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCOMPONENTESTIPO` int(11) NOT NULL,
  `CODTIPOPAI` int(11) NOT NULL,
  `CODTIPOFILHO` int(11) NOT NULL,
  `OBRIGATORIO` int(11) NOT NULL DEFAULT '0',
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODCOMPONENTESTIPO`),
  KEY `FK_TRCOMPONENTESTIPO_idx` (`CODCLIENTE`,`CODTIPOPAI`),
  KEY `FK_TRCOMPONENTESTIPO_TRTIPOFILHO_idx` (`CODCLIENTE`,`CODTIPOFILHO`),
  CONSTRAINT `FK_TRCOMPONENTESTIPO_TRTIPOFILHO` FOREIGN KEY (`CODCLIENTE`, `CODTIPOFILHO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRCOMPONENTESTIPO_TRTIPOPAI` FOREIGN KEY (`CODCLIENTE`, `CODTIPOPAI`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcomponentestipo`
--

LOCK TABLES `trcomponentestipo` WRITE;
/*!40000 ALTER TABLE `trcomponentestipo` DISABLE KEYS */;
INSERT INTO `trcomponentestipo` VALUES (1,1,1,3,1,1,'1','2016-05-12 22:43:58','1','2016-05-12 22:43:58');
/*!40000 ALTER TABLE `trcomponentestipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trdoacao`
--

DROP TABLE IF EXISTS `trdoacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trdoacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODDOACAO` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `CODLOTE` int(11) DEFAULT NULL,
  `DESCRICAO` varchar(200) NOT NULL,
  `QUANTIDADE` int(11) DEFAULT '1',
  `ETIQUETA` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`),
  KEY `INX_TRDOACAO_ETIQUETA` (`ETIQUETA`),
  KEY `FK_TRDOACAO_FIFORNECEDOR` (`CODCLIENTE`,`CODFORNECEDOR`),
  KEY `FK_TRDOACAO_TRTIPO` (`CODCLIENTE`,`CODTIPO`),
  KEY `FK_TRDOACAO_TRLOTE` (`CODCLIENTE`,`CODLOTE`),
  CONSTRAINT `FK_TRDOACAO_FIFORNECEDOR` FOREIGN KEY (`CODCLIENTE`, `CODFORNECEDOR`) REFERENCES `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRDOACAO_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRDOACAO_TRLOTE` FOREIGN KEY (`CODCLIENTE`, `CODLOTE`) REFERENCES `trlote` (`CODCLIENTE`, `CODLOTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRDOACAO_TRTIPO` FOREIGN KEY (`CODCLIENTE`, `CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trdoacao`
--

LOCK TABLES `trdoacao` WRITE;
/*!40000 ALTER TABLE `trdoacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `trdoacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trlocacao`
--

DROP TABLE IF EXISTS `trlocacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trlocacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLOCACAO` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `INTERNA` int(11) NOT NULL COMMENT '1 = INTERNA\n0 = NÃO',
  `CODCOMPONENTESLOCACAO` int(11) DEFAULT NULL,
  `CODFORNECEDOR` int(11) DEFAULT NULL,
  `CODCLIENTELOCADO` int(11) DEFAULT NULL,
  `CODEMPRESALOCADO` int(11) DEFAULT NULL,
  `CODFILIALLOCADO` int(11) DEFAULT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLOCACAO`),
  KEY `FK_TRLOCACAO_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  KEY `FK_TRLOCACAO_TRCOMPONENTESLOCACAO_idx` (`CODCLIENTE`,`CODCOMPONENTESLOCACAO`),
  KEY `FK_TRLOCACAO_TRFORNECEDOR_idx` (`CODCLIENTE`,`CODFORNECEDOR`),
  KEY `FK_TRLOCACAO_GLFILIALLOCACAO_idx` (`CODCLIENTELOCADO`,`CODEMPRESALOCADO`,`CODFILIALLOCADO`),
  CONSTRAINT `FK_TRLOCACAO_GLFILIALLOCACAO` FOREIGN KEY (`CODCLIENTELOCADO`, `CODEMPRESALOCADO`, `CODFILIALLOCADO`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOCACAO_TRCOMPONENTESLOCACAO` FOREIGN KEY (`CODCLIENTE`, `CODCOMPONENTESLOCACAO`) REFERENCES `trcomponenteslocacao` (`CODCLIENTE`, `CODCOMPONENTESLOCACAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOCACAO_TRFORNECEDOR` FOREIGN KEY (`CODCLIENTE`, `CODFORNECEDOR`) REFERENCES `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOCACAO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trlocacao`
--

LOCK TABLES `trlocacao` WRITE;
/*!40000 ALTER TABLE `trlocacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `trlocacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trlogcomponentesproduto`
--

DROP TABLE IF EXISTS `trlogcomponentesproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trlogcomponentesproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCOMPONENTESPRODUTO` int(11) NOT NULL,
  `CODPRODUTOFILHO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCOMPONENTESPRODUTO`),
  KEY `FK_TRLOGCOMPONENTES_PRODUTOFILHO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOFILHO`),
  CONSTRAINT `FK_TRLOGCOMPONENTESPRODUTO_TRCOMPONENTESPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCOMPONENTESPRODUTO`) REFERENCES `trcomponentesproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCOMPONENTESPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOGCOMPONENTES_PRODUTOFILHO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTOFILHO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trlogcomponentesproduto`
--

LOCK TABLES `trlogcomponentesproduto` WRITE;
/*!40000 ALTER TABLE `trlogcomponentesproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trlogcomponentesproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trlogproduto`
--

DROP TABLE IF EXISTS `trlogproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trlogproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLOGPRODUTO` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL,
  `CODLOCACAO` int(11) DEFAULT NULL,
  `CODVENDA` int(11) DEFAULT NULL,
  `CODSUCATA` int(11) DEFAULT NULL,
  `COMPONENTEPAI` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLOGPRODUTO`),
  KEY `FK_TRLOGPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  KEY `FK_TRLOGPRODUTO_TRSTATUS_idx` (`CODTRSTATUS`),
  CONSTRAINT `FK_TRLOGPRODUTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOGPRODUTO_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trlogproduto`
--

LOCK TABLES `trlogproduto` WRITE;
/*!40000 ALTER TABLE `trlogproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trlogproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trlote`
--

DROP TABLE IF EXISTS `trlote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trlote` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODLOTE` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODLOTE`),
  KEY `FK_TRLOTE_FIFORNECEDOR` (`CODCLIENTE`,`CODFORNECEDOR`),
  KEY `FK_TRLOTE_TRTIPO` (`CODCLIENTE`,`CODTIPO`),
  CONSTRAINT `FK_TRLOTE_FIFORNECEDOR` FOREIGN KEY (`CODCLIENTE`, `CODFORNECEDOR`) REFERENCES `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOTE_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOTE_TRTIPO` FOREIGN KEY (`CODCLIENTE`, `CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trlote`
--

LOCK TABLES `trlote` WRITE;
/*!40000 ALTER TABLE `trlote` DISABLE KEYS */;
/*!40000 ALTER TABLE `trlote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trproduto`
--

DROP TABLE IF EXISTS `trproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `CODDOACAO` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  KEY `FK_TRPRODUTO_TRDOACAO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`),
  KEY `FK_TRPRODUTO_TRSTATUS_idx` (`CODTRSTATUS`),
  KEY `FK_TRPRODUTO_TRTIPO_idx` (`CODCLIENTE`,`CODTIPO`),
  CONSTRAINT `FK_TREQUIPAMENTO_TRDOACAO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODDOACAO`) REFERENCES `trdoacao` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODDOACAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TREQUIPAMENTO_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRPRODUTO_TRTIPO` FOREIGN KEY (`CODCLIENTE`, `CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trproduto`
--

LOCK TABLES `trproduto` WRITE;
/*!40000 ALTER TABLE `trproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trstatus`
--

DROP TABLE IF EXISTS `trstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trstatus` (
  `CODTRSTATUS` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(80) NOT NULL,
  `TIPO` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODTRSTATUS`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trstatus`
--

LOCK TABLES `trstatus` WRITE;
/*!40000 ALTER TABLE `trstatus` DISABLE KEYS */;
INSERT INTO `trstatus` VALUES (1,'Aguardando Triagem',1),(2,'Triagem Realizada',1),(3,'Em estoque',1),(4,'Enviado para Sucata',1),(5,'Doação Externamente',1),(6,'Doação Internamente',1),(7,'Alocado Equipamento',1),(8,'Alocado Produto',1),(9,'Enviado para Venda',1),(10,'Vendido',1),(11,'Com Defeito',1),(12,'Funcionando',1);
/*!40000 ALTER TABLE `trstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trtipo`
--

DROP TABLE IF EXISTS `trtipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trtipo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `TIPOENTRADA` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Componente\n2 = Equipamento',
  `COMPONENTEPRODUTO` int(11) NOT NULL DEFAULT '0' COMMENT '1 = é um componentes que forma um produto\n0 = não',
  `COMPONENTELOCACAO` int(11) NOT NULL DEFAULT '0' COMMENT '1 = é um componente que forma uma locação\n0 = não',
  `PRODUTO` int(11) NOT NULL COMMENT '1 = Vira o pai de um produto\n0 = Não',
  `DESCRICAO` varchar(100) NOT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODTIPO`),
  CONSTRAINT `FK_TRTIPO_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trtipo`
--

LOCK TABLES `trtipo` WRITE;
/*!40000 ALTER TABLE `trtipo` DISABLE KEYS */;
INSERT INTO `trtipo` VALUES (1,1,2,0,0,1,'Gabinete',1,'1','2016-05-12 21:39:58','1','2016-05-12 22:06:37'),(1,2,1,1,0,0,'HD',1,'1','2016-05-12 21:50:17','1','2016-05-12 21:50:17'),(1,3,1,1,0,0,'Memória',1,'1','2016-05-12 22:17:58','1','2016-05-12 22:17:58'),(1,4,1,1,0,0,'Fonte',1,'1','2016-05-12 22:18:10','1','2016-05-12 22:18:10'),(1,5,1,1,0,0,'Placa-Mãe',1,'1','2016-05-12 22:18:19','1','2016-05-12 22:18:19'),(1,6,2,0,0,1,'Impressora',1,'1','2016-05-12 22:22:22','1','2016-05-12 22:22:22'),(1,7,2,0,1,0,'Monitor',1,'1','2016-05-12 22:22:34','1','2016-05-12 22:22:34'),(1,8,1,0,1,0,'Mouse',1,'1','2016-05-12 22:24:47','1','2016-05-12 22:24:47');
/*!40000 ALTER TABLE `trtipo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-12 17:56:52
