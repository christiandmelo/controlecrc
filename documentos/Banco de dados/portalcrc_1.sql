-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02-Jun-2016 às 18:02
-- Versão do servidor: 10.1.10-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portalcrc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fiacomplancamento`
--

CREATE TABLE `fiacomplancamento` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODACOMPLANCAMENTO` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODSTATUS` int(11) DEFAULT NULL,
  `VALOR` decimal(5,2) DEFAULT NULL,
  `DATA_RETORNO` date DEFAULT NULL,
  `RETORNO` int(2) DEFAULT NULL,
  `OBSERVACAO` varchar(200) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) DEFAULT NULL,
  `REGCRIADOEM` datetime DEFAULT NULL,
  `REGMODIFPOR` varchar(80) DEFAULT NULL,
  `REGMODIFEM` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fiboleto`
--

CREATE TABLE `fiboleto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODBOLETO` int(11) NOT NULL,
  `DTEMISSAO` datetime NOT NULL,
  `DTVENCIMENTO` datetime NOT NULL,
  `DTBAIXA` datetime DEFAULT NULL,
  `VALOR` decimal(10,2) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ficartaocredito`
--

CREATE TABLE `ficartaocredito` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCARTAOCREDITO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  `DIAFECHAMENTO` date NOT NULL,
  `DIAVENCIMENTO` date NOT NULL,
  `LIMITE` decimal(10,2) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ficategoria`
--

CREATE TABLE `ficategoria` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCATEGORIA` int(11) NOT NULL,
  `NOME` varchar(45) NOT NULL,
  `ORDEM` varchar(45) NOT NULL,
  `SITUACAO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ficontacorrente`
--

CREATE TABLE `ficontacorrente` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCONTACORRENTE` int(11) NOT NULL,
  `CODBANCO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  `SITUACAO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fifaturacartao`
--

CREATE TABLE `fifaturacartao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODFATURACARTAO` int(11) NOT NULL,
  `CODCARTAOCREDITO` int(11) NOT NULL,
  `VALORFATURA` decimal(10,2) DEFAULT NULL,
  `DATAFECHAMENTO` date DEFAULT NULL,
  `DATAVENCIMENTO` date DEFAULT NULL,
  `SITUACAO` int(11) DEFAULT NULL,
  `CODCONTACORRENTE` int(11) DEFAULT NULL,
  `CODPAGAMENTO` int(11) DEFAULT NULL,
  `DESCRICAO` varchar(45) DEFAULT NULL,
  `DATAPAGAMENTO` datetime DEFAULT NULL,
  `VALORPAGO` decimal(10,2) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fiformadepagamento`
--

CREATE TABLE `fiformadepagamento` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODPAGAMENTO` int(11) NOT NULL,
  `DESCRICAO` varchar(80) NOT NULL,
  `SIGLA` varchar(2) NOT NULL,
  `SITUACAO` int(1) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fifornecedor`
--

CREATE TABLE `fifornecedor` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) NOT NULL,
  `CODMUNICIPIO` int(11) DEFAULT NULL,
  `CLIENTE` varchar(45) NOT NULL DEFAULT '0' COMMENT '1 = CLIENTE\n0 = FORNECEDOR',
  `RAZAOSOCIAL` varchar(200) NOT NULL,
  `NOMEFANTASIA` varchar(200) NOT NULL,
  `REGISTRO` int(11) DEFAULT NULL,
  `CNPJ` varchar(19) NOT NULL,
  `RUA` varchar(80) DEFAULT NULL,
  `NUMERO` varchar(45) DEFAULT NULL,
  `BAIRRO` varchar(50) DEFAULT NULL,
  `CEP` varchar(9) DEFAULT NULL,
  `LOGRADOURO` varchar(300) DEFAULT NULL,
  `TELEFONE` varchar(20) DEFAULT NULL,
  `TELEFONE2` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(80) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fifornecedor`
--

INSERT INTO `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`, `CODMUNICIPIO`, `CLIENTE`, `RAZAOSOCIAL`, `NOMEFANTASIA`, `REGISTRO`, `CNPJ`, `RUA`, `NUMERO`, `BAIRRO`, `CEP`, `LOGRADOURO`, `TELEFONE`, `TELEFONE2`, `EMAIL`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, '0', 'Dell Computadores LTDA', 'Dell Computadores', NULL, '11.111.111/1111-11', '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', '1', '2016-05-15 19:11:23'),
(1, 2, 1, '0', 'Banco do Brasil LTDA', 'Banco do Brasil', NULL, '22.222.222/2222-22', '', '', '', '', NULL, '', '', '', '1', '2016-05-03 22:32:49', '1', '2016-05-15 19:11:48'),
(1, 3, 1, '0', 'UNI-BH Ltda', 'UNI-BH', NULL, '11.111.111/1111-11', '', '', '', '', NULL, '', '', '', '1', '2016-05-21 16:11:11', '1', '2016-05-21 16:12:16'),
(1, 4, 1, '1', 'Maria', 'Maria', NULL, '22.222.222/2222-22', '', '', '', '', NULL, '', '', '', '1', '2016-05-31 22:11:55', '1', '2016-05-31 22:11:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `filanboleto`
--

CREATE TABLE `filanboleto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODBOLETO` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `filancamento`
--

CREATE TABLE `filancamento` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) DEFAULT NULL,
  `CODFISTATUS` int(11) NOT NULL,
  `CODACOMPSTATUS` int(11) DEFAULT NULL,
  `CODCONTACORRENTE` int(11) DEFAULT NULL,
  `CODPREVISAOLAN` int(11) DEFAULT NULL,
  `CODRECORRENTELAN` int(11) DEFAULT NULL,
  `CODCATEGORIA` int(11) DEFAULT NULL,
  `CODPAGAMENTO` int(11) DEFAULT NULL,
  `CODTIPOLANCAMENTO` int(11) NOT NULL,
  `DATAEMISSAO` date DEFAULT NULL,
  `DATAVENCIMENTO` date DEFAULT NULL,
  `DATABAIXA` date DEFAULT NULL,
  `LOJA` varchar(80) DEFAULT NULL,
  `IDLOJA` varchar(80) DEFAULT NULL,
  `VALORORIGINAL` decimal(10,2) DEFAULT NULL,
  `VALORBAIXADO` decimal(10,2) DEFAULT NULL,
  `CODPRODUTO` int(11) DEFAULT NULL COMMENT 'Campos de ligação com a tabela trproduto',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `filancamento`
--

INSERT INTO `filancamento` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`, `CODFORNECEDOR`, `CODFISTATUS`, `CODACOMPSTATUS`, `CODCONTACORRENTE`, `CODPREVISAOLAN`, `CODRECORRENTELAN`, `CODCATEGORIA`, `CODPAGAMENTO`, `CODTIPOLANCAMENTO`, `DATAEMISSAO`, `DATAVENCIMENTO`, `DATABAIXA`, `LOJA`, `IDLOJA`, `VALORORIGINAL`, `VALORBAIXADO`, `CODPRODUTO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '25', 'Mercado Livre', '25.00', NULL, 60, '1', '2016-06-01 16:04:20', '1', '2016-06-01 16:04:20'),
(1, 1, 1, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '26', 'Mercado Livre', '25.00', NULL, 79, '1', '2016-06-01 16:15:32', '1', '2016-06-01 16:15:32'),
(1, 1, 1, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '27', 'Mercado Livre', '25.00', NULL, 56, '1', '2016-06-01 16:17:06', '1', '2016-06-01 16:17:06'),
(1, 1, 1, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '28', 'Mercado Livre', '28.00', NULL, 55, '1', '2016-06-01 16:17:42', '1', '2016-06-01 16:17:42'),
(1, 1, 1, 5, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '31', 'Mercado Livre', '25.00', NULL, 37, '1', '2016-06-01 16:58:14', '1', '2016-06-01 16:58:14'),
(1, 1, 1, 6, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '31', 'Mercado Livre', '25.00', NULL, 39, '1', '2016-06-01 17:08:22', '1', '2016-06-01 17:08:22'),
(1, 1, 1, 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '32', 'Mercado Livre', '25.00', NULL, 42, '1', '2016-06-01 17:10:07', '1', '2016-06-01 17:10:07'),
(1, 1, 1, 8, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '33', 'Mercado Livre', '25.00', NULL, 50, '1', '2016-06-01 17:12:05', '1', '2016-06-01 17:12:05'),
(1, 1, 1, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '34', 'Mercado Livre', '25.00', NULL, 52, '1', '2016-06-01 17:12:48', '1', '2016-06-01 17:12:48'),
(1, 1, 1, 10, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '34', 'Mercado Livre', '25.00', NULL, 81, '1', '2016-06-01 17:19:12', '1', '2016-06-01 17:19:12'),
(1, 1, 1, 11, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-01', NULL, NULL, '38', 'Mercado Livre', '25.00', NULL, 82, '1', '2016-06-01 17:22:15', '1', '2016-06-01 17:22:15'),
(1, 1, 1, 12, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2016-06-02', NULL, NULL, '45678', 'Mercado Livre', '25.00', NULL, 89, '1', '2016-06-02 02:14:11', '1', '2016-06-02 02:14:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `filanfaturacartao`
--

CREATE TABLE `filanfaturacartao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODFATURACARTAO` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `firecorrentelan`
--

CREATE TABLE `firecorrentelan` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODRECORRENTELAN` int(11) NOT NULL,
  `CODTIPOLANCAMENTO` int(11) NOT NULL,
  `CODCATEGORIA` int(11) NOT NULL,
  `CODCONTACORRENTE` int(11) NOT NULL,
  `CODPAGAMENTO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  `VALOR` decimal(10,2) NOT NULL,
  `INTERVALO` varchar(20) NOT NULL,
  `DIAFIXO` int(11) NOT NULL,
  `DATAINICIO` date NOT NULL,
  `DATAFIM` date DEFAULT NULL,
  `BAIXAAUTOMATICA` int(11) NOT NULL DEFAULT '0',
  `DIAUTIL` int(11) DEFAULT NULL,
  `PREVISAO` int(11) DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fistatus`
--

CREATE TABLE `fistatus` (
  `CODFISTATUS` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `ATIVO` int(1) NOT NULL,
  `TIPO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fistatus`
--

INSERT INTO `fistatus` (`CODFISTATUS`, `NOME`, `ATIVO`, `TIPO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 'Em aberto', 1, 0, '1', '2016-06-01 09:30:00', '1', '2016-06-01 09:30:00'),
(2, 'Baixado', 1, 0, '1', '2016-06-01 09:30:00', '1', '2016-06-01 09:30:00'),
(3, 'Baixado por acordo', 1, 0, '1', '2016-06-01 09:30:00', '1', '2016-06-01 09:30:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fitipolancamento`
--

CREATE TABLE `fitipolancamento` (
  `CODTIPOLANCAMENTO` int(11) NOT NULL,
  `DESCRICAO` varchar(10) NOT NULL,
  `SINAL` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fitipolancamento`
--

INSERT INTO `fitipolancamento` (`CODTIPOLANCAMENTO`, `DESCRICAO`, `SINAL`) VALUES
(1, 'Pagar', '-'),
(2, 'Receber', '+');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glativo`
--

CREATE TABLE `glativo` (
  `CODATIVO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glativo`
--

INSERT INTO `glativo` (`CODATIVO`, `DESCRICAO`) VALUES
(1, 'Ativo'),
(2, 'Inativo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glautoindice`
--

CREATE TABLE `glautoindice` (
  `CODINDICE` int(11) NOT NULL,
  `CODCLIENTE` int(11) DEFAULT NULL,
  `CODEMPRESA` int(11) DEFAULT NULL,
  `CODFILIAL` int(11) DEFAULT NULL,
  `TABELA` varchar(45) NOT NULL,
  `VALOR` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glautoindice`
--

INSERT INTO `glautoindice` (`CODINDICE`, `CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `TABELA`, `VALOR`) VALUES
(1, 1, 1, 1, 'glgrupo', '10'),
(4, 1, 1, 1, 'glfilial', '14'),
(5, 1, 1, NULL, 'glusuariofilial', '25'),
(6, 1, 1, 1, 'glgrupolink', '119'),
(7, 1, 1, 1, 'glgrupousuario', '24'),
(9, 1, NULL, NULL, 'fifornecedor', '4'),
(11, 1, NULL, NULL, 'trtipo', '8'),
(12, 1, NULL, NULL, 'trcampostipo', '7'),
(13, 1, NULL, NULL, 'trcomponenteslocacao', '3'),
(14, 1, NULL, NULL, 'trcomponentestipo', '4'),
(16, 1, NULL, NULL, 'trlote', '14'),
(18, 1, 1, 1, 'trdoacao', '24'),
(19, 1, 1, 1, 'trproduto', '93'),
(20, 1, 1, 1, 'trcamposproduto', '103'),
(21, 1, 1, 1, 'trlogproduto', '132'),
(22, 1, 1, 1, 'trcomponentesproduto', '21'),
(23, 1, 1, 1, 'trlogcomponentesproduto', '36'),
(24, 1, 1, 1, 'trsucata', '31'),
(25, 1, 1, 1, 'filancamento', '12'),
(26, 1, 1, 1, 'trlocacao', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glbanco`
--

CREATE TABLE `glbanco` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODBANCO` int(11) NOT NULL,
  `CODFEBRABAN` int(11) NOT NULL,
  `DIGITO` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `NOMEREDUZIDO` varchar(45) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `glcliente`
--

CREATE TABLE `glcliente` (
  `CODCLIENTE` int(11) NOT NULL,
  `NOME` varchar(80) DEFAULT NULL,
  `LIBERADO` int(1) NOT NULL DEFAULT '0',
  `NOMEFANTASIA` varchar(80) DEFAULT NULL,
  `QTDNOTAS` int(11) NOT NULL DEFAULT '0',
  `QTDUSUARIOS` int(11) NOT NULL DEFAULT '0',
  `QTDUSUARIOSUSADOS` int(11) NOT NULL,
  `QTDEMPRESAS` int(11) NOT NULL DEFAULT '0',
  `QTDEMPRESASUSADAS` int(11) NOT NULL,
  `PERMISSAO` int(11) NOT NULL DEFAULT '1' COMMENT '1 = GLOBAL\n2 = POR FILIAL',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glcliente`
--

INSERT INTO `glcliente` (`CODCLIENTE`, `NOME`, `LIBERADO`, `NOMEFANTASIA`, `QTDNOTAS`, `QTDUSUARIOS`, `QTDUSUARIOSUSADOS`, `QTDEMPRESAS`, `QTDEMPRESASUSADAS`, `PERMISSAO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 'CRC', 1, 'CRC', 100, 5, 0, 1, 1, 1, '1', '2016-02-24 00:00:00', '1', '2016-02-24 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glempresa`
--

CREATE TABLE `glempresa` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `NOMEFANTASIA` varchar(80) NOT NULL,
  `QTDFILIAL` int(11) NOT NULL,
  `TELEFONE` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(45) DEFAULT NULL,
  `CGC` varchar(25) DEFAULT NULL,
  `INSCRICAOESTADUAL` varchar(20) DEFAULT NULL,
  `RUA` varchar(50) DEFAULT NULL,
  `NUMERO` varchar(20) DEFAULT NULL,
  `COMPLEMENTO` varchar(20) DEFAULT NULL,
  `BAIRRO` varchar(50) DEFAULT NULL,
  `PAIS` varchar(50) DEFAULT NULL,
  `CEP` varchar(20) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glempresa`
--

INSERT INTO `glempresa` (`CODCLIENTE`, `CODEMPRESA`, `NOME`, `NOMEFANTASIA`, `QTDFILIAL`, `TELEFONE`, `EMAIL`, `CGC`, `INSCRICAOESTADUAL`, `RUA`, `NUMERO`, `COMPLEMENTO`, `BAIRRO`, `PAIS`, `CEP`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 'CRC', 'CRC', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2016-02-24 00:00:00', '1', '2016-02-24 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glestado`
--

CREATE TABLE `glestado` (
  `CODESTADO` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `CODETD` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='		';

--
-- Extraindo dados da tabela `glestado`
--

INSERT INTO `glestado` (`CODESTADO`, `NOME`, `CODETD`) VALUES
(1, 'Minas Gerais', 'MG');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glfilial`
--

CREATE TABLE `glfilial` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `RAZAOSOCIAL` varchar(80) DEFAULT NULL,
  `NOMEFANTASIA` varchar(80) DEFAULT NULL,
  `CNPJ` varchar(45) DEFAULT NULL,
  `TELEFONE` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `LOGRADOURO` varchar(80) DEFAULT NULL,
  `NUMERO` varchar(10) DEFAULT NULL,
  `COMPLEMENTO` varchar(20) DEFAULT NULL,
  `CEP` varchar(10) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glfilial`
--

INSERT INTO `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `RAZAOSOCIAL`, `NOMEFANTASIA`, `CNPJ`, `TELEFONE`, `EMAIL`, `LOGRADOURO`, `NUMERO`, `COMPLEMENTO`, `CEP`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 'CRC', 'CRC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2016-02-24 00:00:00', '1', '2016-02-24 00:00:00'),
(1, 1, 2, 'Centro 1', 'Centro 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2015-03-04 00:00:00', '1', '2015-03-04 00:00:00'),
(1, 1, 5, 'CRC Ipiranga', 'CRC Ipiranga', '12.345.678/9101-11', '', 'crc@gmail.com', '', '', '', '', '18', '2016-05-04 20:39:43', '18', '2016-05-04 20:39:43'),
(1, 1, 7, 'Espaço BH Cidadania - CRAS Petrópoli', 'CRAS Petrópolis', '12.345.687/8888-88', '', 'crc@gmail.com', '', '', '', '', '18', '2016-05-04 20:48:02', '18', '2016-05-04 20:48:28'),
(1, 1, 8, 'Centro Cultural Lindéia Regina', 'Centro Cultural Lindéia Regina', '11.111.111/1111-11', '', 'crc@gmail.com', '', '', '', '', '18', '2016-05-04 20:49:31', '18', '2016-05-04 20:49:31'),
(1, 1, 10, 'Centro De Convivência Cézar Campos', 'Centro De Convivência Cézar Campos', '55.555.555/5555-55', '', 'crc@gkdksk.com', '', '', '', '', '18', '2016-05-04 20:53:52', '18', '2016-05-04 20:53:52'),
(1, 1, 11, 'Centro Cultural Da Vila Marçola', 'Centro Cultural Da Vila Marçola', '54.444.444/4444-44', '', 'daianebjnknj@gmail.com', '', '', '', '', '18', '2016-05-04 20:57:41', '18', '2016-05-04 20:57:41'),
(1, 1, 12, 'Centro De Integração Martinho', 'Centro De Integração Martinho', '77.777.777/7777-77', '', 'crc@gmail.com', '', '', '', '', '18', '2016-05-04 20:59:09', '18', '2016-05-04 20:59:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glgrupo`
--

CREATE TABLE `glgrupo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODGRUPO` int(11) NOT NULL,
  `NOME` varchar(50) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glgrupo`
--

INSERT INTO `glgrupo` (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`, `NOME`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 'Administrador', '1', '2016-03-01 10:15:00', '1', '2016-04-27 14:22:07'),
(1, 1, 2, 'Básico', '1', '2016-03-04 13:43:00', '1', '2016-03-04 13:43:00'),
(1, 1, 6, 'Secretaria', '1', '2016-04-27 14:22:17', '1', '2016-04-27 14:22:17'),
(1, 1, 7, 'Trilhagem', '1', '2016-04-27 14:22:30', '1', '2016-04-27 14:22:30'),
(1, 1, 8, 'criado em sala', '1', '2016-04-28 00:20:40', '1', '2016-04-28 00:20:40'),
(1, 1, 9, 'Mestre - Somente funcionando', '1', '2016-05-02 23:02:46', '1', '2016-05-02 23:02:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glgrupolink`
--

CREATE TABLE `glgrupolink` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODGRUPOLINK` int(11) NOT NULL,
  `CODGRUPO` int(11) NOT NULL,
  `CODLINK` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glgrupolink`
--

INSERT INTO `glgrupolink` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODGRUPOLINK`, `CODGRUPO`, `CODLINK`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 1, 1, 1, '1', '2016-03-01 10:45:01', '1', '2016-03-01 10:45:01'),
(1, 1, 1, 4, 1, 4, '1', '2016-03-01 10:45:01', '1', '2016-03-01 10:45:01'),
(1, 1, 1, 5, 1, 5, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 6, 1, 6, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 7, 1, 7, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 8, 1, 8, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 9, 1, 9, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 10, 1, 10, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 18, 1, 18, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 19, 1, 19, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 20, 1, 20, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 21, 1, 21, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 22, 1, 22, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 23, 1, 23, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 24, 1, 24, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 26, 1, 26, '1', '2016-03-01 10:45:02', '1', '2016-03-01 10:45:02'),
(1, 1, 1, 27, 1, 27, '1', '2016-03-01 10:45:01', '1', '2016-03-01 10:45:01'),
(1, 1, 1, 28, 1, 28, '1', '2016-03-01 10:45:03', '1', '2016-03-01 10:45:03'),
(1, 1, 1, 29, 1, 29, '1', '2016-03-01 10:45:03', '1', '2016-03-01 10:45:03'),
(1, 1, 1, 30, 1, 30, '1', '2016-03-01 10:45:03', '1', '2016-03-01 10:45:03'),
(1, 1, 1, 31, 1, 31, '1', '2016-03-01 10:45:03', '1', '2016-03-01 10:45:03'),
(1, 1, 1, 32, 1, 32, '1', '2016-03-01 10:45:03', '1', '2016-03-01 10:45:03'),
(1, 1, 1, 41, 1, 41, '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(1, 1, 1, 42, 1, 42, '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(1, 1, 1, 43, 1, 43, '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(1, 1, 1, 44, 1, 44, '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(1, 1, 1, 45, 1, 45, '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(1, 1, 1, 46, 1, 46, '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(1, 1, 1, 47, 1, 47, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 48, 1, 48, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 115, 1, 49, '1', '2016-05-12 17:12:36', '1', '2016-05-12 17:12:36'),
(1, 1, 1, 50, 1, 50, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 83, 1, 51, '1', '2016-04-27 21:31:56', '1', '2016-04-27 21:31:56'),
(1, 1, 1, 110, 1, 53, '1', '2016-05-12 17:10:30', '1', '2016-05-12 17:10:30'),
(1, 1, 1, 112, 1, 55, '1', '2016-05-12 17:10:47', '1', '2016-05-12 17:10:47'),
(1, 1, 1, 108, 1, 56, '1', '2016-05-12 17:10:20', '1', '2016-05-12 17:10:20'),
(1, 1, 1, 109, 1, 57, '1', '2016-05-12 17:10:26', '1', '2016-05-12 17:10:26'),
(1, 1, 1, 113, 1, 59, '1', '2016-05-12 17:10:51', '1', '2016-05-12 17:10:51'),
(1, 1, 1, 114, 1, 60, '1', '2016-05-12 17:10:56', '1', '2016-05-12 17:10:56'),
(1, 1, 1, 118, 1, 61, '1', '2016-05-31 01:07:04', '1', '2016-05-31 01:07:04'),
(1, 1, 1, 117, 1, 62, '1', '2016-05-31 01:06:57', '1', '2016-05-31 01:06:57'),
(1, 1, 1, 119, 1, 63, '1', '2016-05-31 03:07:13', '1', '2016-05-31 03:07:13'),
(1, 1, 1, 51, 2, 18, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 52, 2, 19, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 53, 2, 20, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 54, 2, 21, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 55, 2, 22, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 56, 2, 23, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 57, 2, 24, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 59, 2, 26, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 60, 2, 27, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 61, 2, 28, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 62, 2, 29, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 63, 2, 30, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 64, 2, 31, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 65, 2, 32, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 66, 6, 18, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 67, 6, 19, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 68, 6, 26, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 69, 6, 42, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 70, 6, 43, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 71, 6, 48, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 72, 7, 18, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 73, 7, 19, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 74, 7, 26, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 75, 7, 42, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 76, 7, 43, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 77, 7, 44, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 78, 7, 45, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 79, 7, 46, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 80, 7, 48, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 81, 7, 49, '1', '2015-03-04 13:59:00', '1', '2015-03-04 13:59:00'),
(1, 1, 1, 84, 8, 1, '1', '2016-04-28 00:29:26', '1', '2016-04-28 00:29:26'),
(1, 1, 1, 85, 8, 4, '1', '2016-04-28 00:29:45', '1', '2016-04-28 00:29:45'),
(1, 1, 1, 86, 8, 50, '1', '2016-04-28 00:30:08', '1', '2016-04-28 00:30:08'),
(1, 1, 1, 87, 9, 1, '1', '2016-05-01 15:16:39', '1', '2016-05-01 15:16:39'),
(1, 1, 1, 89, 9, 5, '1', '2016-05-01 15:16:47', '1', '2016-05-01 15:16:47'),
(1, 1, 1, 90, 9, 8, '1', '2016-05-01 15:24:14', '1', '2016-05-01 15:24:14'),
(1, 1, 1, 91, 9, 9, '1', '2016-05-01 15:24:17', '1', '2016-05-01 15:24:17'),
(1, 1, 1, 92, 9, 10, '1', '2016-05-01 15:24:22', '1', '2016-05-01 15:24:22'),
(1, 1, 1, 93, 9, 18, '1', '2016-05-01 15:24:27', '1', '2016-05-01 15:24:27'),
(1, 1, 1, 94, 9, 19, '1', '2016-05-01 15:24:31', '1', '2016-05-01 15:24:31'),
(1, 1, 1, 95, 9, 20, '1', '2016-05-01 15:24:42', '1', '2016-05-01 15:24:42'),
(1, 1, 1, 96, 9, 26, '1', '2016-05-01 15:24:52', '1', '2016-05-01 15:24:52'),
(1, 1, 1, 99, 9, 41, '1', '2016-05-01 15:24:57', '1', '2016-05-01 15:24:57'),
(1, 1, 1, 100, 9, 42, '1', '2016-05-01 15:25:13', '1', '2016-05-01 15:25:13'),
(1, 1, 1, 101, 9, 43, '1', '2016-05-01 15:25:18', '1', '2016-05-01 15:25:18'),
(1, 1, 1, 102, 9, 44, '1', '2016-05-01 15:25:22', '1', '2016-05-01 15:25:22'),
(1, 1, 1, 87, 9, 45, '1', '2016-05-03 22:44:11', '1', '2016-05-03 22:44:11'),
(1, 1, 1, 103, 9, 47, '1', '2016-05-01 15:25:31', '1', '2016-05-01 15:25:31'),
(1, 1, 1, 104, 9, 48, '1', '2016-05-01 15:25:34', '1', '2016-05-01 15:25:34'),
(1, 1, 1, 105, 9, 50, '1', '2016-05-01 15:25:38', '1', '2016-05-01 15:25:38'),
(1, 1, 1, 106, 9, 51, '1', '2016-05-01 15:25:42', '1', '2016-05-01 15:25:42');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glgrupousuario`
--

CREATE TABLE `glgrupousuario` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODGRUPOUSUARIO` int(11) NOT NULL,
  `CODGRUPO` int(11) NOT NULL,
  `CODUSUARIO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glgrupousuario`
--

INSERT INTO `glgrupousuario` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODGRUPOUSUARIO`, `CODGRUPO`, `CODUSUARIO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 1, 1, 1, '1', '2016-03-01 10:23:00', '1', '2016-03-01 10:23:00'),
(1, 1, 1, 2, 1, 2, '1', '2016-03-01 10:23:00', '1', '2016-03-01 10:23:00'),
(1, 1, 1, 3, 1, 3, '1', '2016-03-01 10:23:00', '1', '2016-03-01 10:23:00'),
(1, 1, 1, 4, 1, 4, '1', '2015-03-04 00:00:00', '1', '2015-03-04 00:00:00'),
(1, 1, 1, 5, 1, 5, '1', '2015-03-04 00:00:00', '1', '2015-03-04 00:00:00'),
(1, 1, 1, 7, 1, 1, '1', '2016-04-27 21:45:46', '1', '2016-04-27 21:45:46'),
(1, 1, 1, 8, 1, 1, '1', '2016-04-28 00:32:04', '1', '2016-04-28 00:32:04'),
(1, 1, 1, 9, 1, 6, '1', '2016-05-01 15:15:38', '1', '2016-05-01 15:15:38'),
(1, 1, 1, 10, 1, 7, '1', '2016-05-01 15:15:42', '1', '2016-05-01 15:15:42'),
(1, 1, 1, 11, 1, 8, '1', '2016-05-01 15:15:45', '1', '2016-05-01 15:15:45'),
(1, 1, 1, 12, 1, 9, '1', '2016-05-01 15:15:48', '1', '2016-05-01 15:15:48'),
(1, 1, 1, 13, 1, 10, '1', '2016-05-01 15:15:51', '1', '2016-05-01 15:15:51'),
(1, 1, 1, 14, 1, 11, '1', '2016-05-01 15:15:54', '1', '2016-05-01 15:15:54'),
(1, 1, 1, 15, 1, 12, '1', '2016-05-01 15:15:57', '1', '2016-05-01 15:15:57'),
(1, 1, 1, 16, 1, 13, '1', '2016-05-01 15:16:01', '1', '2016-05-01 15:16:01'),
(1, 1, 1, 17, 1, 14, '1', '2016-05-01 15:16:04', '1', '2016-05-01 15:16:04'),
(1, 1, 1, 18, 1, 15, '1', '2016-05-01 15:16:08', '1', '2016-05-01 15:16:08'),
(1, 1, 1, 19, 1, 16, '1', '2016-05-01 15:16:11', '1', '2016-05-01 15:16:11'),
(1, 1, 1, 20, 1, 17, '1', '2016-05-01 15:16:14', '1', '2016-05-01 15:16:14'),
(1, 1, 1, 21, 1, 18, '1', '2016-05-01 15:16:18', '1', '2016-05-01 15:16:18'),
(1, 1, 1, 22, 1, 19, '1', '2016-05-01 15:16:21', '1', '2016-05-01 15:16:21'),
(1, 1, 1, 23, 1, 21, '1', '2016-05-03 14:38:48', '1', '2016-05-03 14:38:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glhelpsystem`
--

CREATE TABLE `glhelpsystem` (
  `CODHELPSYSTEM` int(11) NOT NULL,
  `CODLINK` int(11) DEFAULT NULL,
  `ID` varchar(45) DEFAULT NULL,
  `TEXTO` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glhelpsystem`
--

INSERT INTO `glhelpsystem` (`CODHELPSYSTEM`, `CODLINK`, `ID`, `TEXTO`) VALUES
(1, NULL, 'global.contexto', 'Neste local você escolhe qual Tele centro deseja trabalhar.'),
(4, NULL, 'global.infoUsuario', 'Neste local você pode verificar e alterar suas informações pessoais.'),
(5, NULL, 'global.buscaMenu', 'Neste local você pode filtrar as funcionalidades disponíveis.'),
(6, NULL, 'global.menuLateral', 'Neste local fica o Menu Lateral. Você pode utilizar o mouse para navegar por ele.'),
(7, NULL, 'global.404', 'Bom... Eu so tenho a pedir desculpas... Você não deveria ter que ver essa pagina...'),
(8, NULL, 'global.404Botao', 'Aqui você consegue voltar para a tela inicial. Ate porque não tem nada de bom aqui...'),
(9, NULL, 'global.oriscenter', 'Logomarca do sistema.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `gllink`
--

CREATE TABLE `gllink` (
  `CODLINK` int(11) NOT NULL,
  `NOME` varchar(45) NOT NULL,
  `CAMINHO` varchar(80) NOT NULL,
  `DESCRICAO` varchar(80) DEFAULT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `ADMIN` int(1) DEFAULT '1',
  `ORDEM` varchar(12) DEFAULT NULL,
  `ICONEFA` varchar(45) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `gllink`
--

INSERT INTO `gllink` (`CODLINK`, `NOME`, `CAMINHO`, `DESCRICAO`, `ATIVO`, `ADMIN`, `ORDEM`, `ICONEFA`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 'Gestão Global', '#', 'global', 1, 0, '01.00.00', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(4, 'Administração', '#', 'global/administração', 1, 1, '01.04.00', 'fa fa-suitcase', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(5, 'Cliente', '#/global/administracao/clientes/Cclientes', 'global/administração/clientes', 0, 1, '01.04.05', 'fa fa-money', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(6, 'Links', '#/global/administracao/links/Clinks', 'global/administração/links', 0, 1, '01.04.06', 'fa fa-anchor', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(7, 'Empresas', '#/global/administracao/empresas/Cempresas', 'global/administração/empresas', 0, 1, '01.04.07', 'fa fa-building', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(8, 'Segurança', '#', 'global/segurança', 1, 0, '01.08.00', 'fa fa-key', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(9, 'Usuários', '#/global/seguranca/usuarios/Cusuarios', 'global/segurança/usuários', 1, 0, '01.08.01', 'fa fa-user', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(10, 'Grupos', '#/global/seguranca/grupos/Cgrupos', 'global/segurança/grupos', 1, 0, '01.08.02', 'fa fa-users', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(18, 'Gestão Financeira', '#', 'financeiro', 1, 0, '03.00.00', 'fa fa-money', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(19, 'Cadastros', '#', 'financeiro/cadastros', 1, 0, '03.01.00', 'fa fa-gears', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(20, 'Movimentação Bancária', '#', 'financeiro/movimentacaobancaria', 0, 0, '03.02.00', 'fa fa-bank', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(21, 'Banco', '#/financeiro/movimentacaobancaria/banco/Cbanco', 'financeiro/movimentacaobancaria/bancos', 0, 0, '03.02.01', 'fa fa-bank', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(22, 'Cartão de Crédito', '#/financeiro/movimentacaobancaria/cartaocredito/CcartaoCredito', 'financeiro/movimentacaobancaria/cartãocrédito', 0, 0, '03.02.03', 'fa fa-credit-card', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(23, 'Conta Bancária', '#/financeiro/movimentacaobancaria/contabancaria/CcontaBancaria', 'financeiro/movimentacaobancaria/contabancaria', 0, 0, '03.02.02', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(24, 'Status de Acompanhamento', '#/financeiro/cadastros/statusfinanceiro/CstatusFinanceiro', 'financeiro/cadastros/statufinanceiro', 0, 0, '03.01.01', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(26, 'Cliente/Fornecedor', '#/financeiro/cadastros/fornecedor/Cfornecedor', 'financeiro/cadastros/situacaofornecedor', 1, 0, '03.01.03', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(27, 'Formas de Pagamento', '#/financeiro/cadastros/formapagamentos/CformaPagamento', 'financeiro/cadastros/situacaofornecedor', 0, 0, '03.01.04', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(28, 'Contas a receber', '#', 'financeiro/contas/', 1, 0, '03.03.00', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(29, 'Lançamentos', '#/financeiro/contas/lancamentos/Clancamentos', 'financeiro/contas/lancamentos/', 0, 0, '03.03.02', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(30, 'Lançamentos Recorrentes', '#/financeiro/contas/lancamentosrecorrentes/ClancamentosRecorrentes', 'financeiro/contas/lancamentosrecorrentes/', 0, 0, '03.03.03', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(31, 'Previsão de Lançamentos', '#/financeiro/contas/previsaolancamentos/CprevisaoLancamentos', 'financeiro/contas/previsaolancamentos/', 0, 0, '03.03.04', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(32, 'Boletos', '#/financeiro/contas/boletos/Cboletos', 'financeiro/contas/boletos/', 0, 0, '03.03.05', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(41, 'Tele Centros', '#/fiscal/cadastros/filiais/Cfiliais', 'global/administração/Tele Centros', 1, 0, '01.04.08', 'fa fa-institution alias', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(42, 'Gestão Estoque e Doações', '#', 'estoque', 1, 0, '04.00.00', 'fa fa-gift', '1', '2016-04-24 15:02:00', '1', '2016-05-12 11:50:00'),
(43, 'Cadastros', '#', 'estoque/cadastros/', 1, 0, '04.01.00', 'fa fa-gears', '1', '2016-04-24 15:02:00', '1', '2016-05-12 11:50:00'),
(44, 'Tipos de Doação', '#/estoque/cadastros/tipo/Ctipo', 'estoque/cadastros/tipo', 1, 0, '04.01.01', 'fa fa-filter', '1', '2016-04-24 15:02:00', '1', '2016-05-12 11:50:00'),
(45, 'Campos do Tipo de Doação', '#/estoque/cadastros/campostipo/CcamposTipo', 'estoque/cadastros/camposTipoEntrada', 1, 0, '04.01.02', 'fa fa-sitemap', '1', '2016-04-24 15:02:00', '1', '2016-05-12 11:50:00'),
(46, 'Triagem', '#/estoque/estoque/triagem/Ctriagem', '', 1, 0, '04.03.01', 'fa fa-gift', '1', '2016-04-24 15:02:00', '1', '2016-05-30 11:50:00'),
(47, 'Permissões do grupo', '#/global/seguranca/grupos/CgrupoLink', 'global/segurança/Permissao Grupo', 1, 0, '01.08.03', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(48, 'Entrada Doação', '#/estoque/secretaria/entrada/CentradaDoacao', '', 1, 0, '04.02.01', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-05-12 11:50:00'),
(49, 'Estoque', '#', NULL, 1, 0, '04.04.00', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-05-30 11:50:00'),
(50, 'Tele centros do usuário', '#/global/seguranca/grupos/CusuarioFilial', 'global/segurança/Usuario filial', 1, 0, '01.08.04', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(51, 'Grupos do Usuário', '#/global/seguranca/grupos/CgrupoUsuario', 'global/segurança/Grupo Usuario', 1, 0, '01.08.05', 'fa fa-globe', '1', '2016-04-24 15:02:00', '1', '2016-04-24 15:02:00'),
(53, 'Secretaria', '#', NULL, 1, 0, '04.02.00', 'fa fa-globe', '1', '2016-05-12 11:50:00', '1', '2016-05-12 11:50:00'),
(55, 'Locação', '#/estoque/estoque/locacao/Clocacao', NULL, 1, 0, '04.04.02', 'fa fa-globe', '1', '2016-05-30 11:50:00', '1', '2016-05-30 11:50:00'),
(56, 'Componentes do Equipamento', '#/estoque/cadastros/componentestipo/CcomponentesTipo', NULL, 1, 0, '04.01.03', 'fa fa-globe', '1', '2016-05-12 11:50:00', '1', '2016-05-12 11:50:00'),
(57, 'Componentes para locação', '#/estoque/cadastros/componenteslocacao/CcomponentesLocacao', NULL, 1, 0, '04.01.04', 'fa fa-globe', '1', '2016-05-12 11:50:00', '1', '2016-05-12 11:50:00'),
(59, 'Venda', '#/estoque/estoque/venda/Cvenda', NULL, 1, 0, '03.03.01', 'fa fa-globe', '1', '2016-05-30 11:50:00', '1', '2016-05-30 11:50:00'),
(60, 'Doações Externas', '#/estoque/estoque/doacaoexterna/CdoacaoExterna', NULL, 0, 0, '04.04.04', 'fa fa-globe', '1', '2016-05-30 11:50:00', '1', '2016-05-30 11:50:00'),
(61, 'Doações', '#', NULL, 1, 0, '04.03.00', 'fa fa-globe', '1', '2016-05-30 19:46:00', '1', '2016-05-30 19:46:00'),
(62, 'Estoque', '#/estoque/estoque/Cestoque', NULL, 1, 0, '04.04.01', 'fa fa-globe', '1', '2016-05-30 11:50:00', '1', '2016-05-30 11:50:00'),
(63, 'Sucata', '#/estoque/estoque/sucata/Csucata', NULL, 1, 0, '04.04.05', 'fa fa-globe', '1', '2016-05-30 22:06:00', '1', '2016-05-30 22:06:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glmunicipio`
--

CREATE TABLE `glmunicipio` (
  `CODMUNICIPIO` int(11) NOT NULL,
  `CODESTADO` int(11) NOT NULL,
  `DESCRICAO` varchar(80) NOT NULL,
  `OBSERVACAO` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glmunicipio`
--

INSERT INTO `glmunicipio` (`CODMUNICIPIO`, `CODESTADO`, `DESCRICAO`, `OBSERVACAO`) VALUES
(1, 1, 'Belo Horizonte', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `glusuario`
--

CREATE TABLE `glusuario` (
  `CODUSUARIO` int(11) NOT NULL,
  `EMAIL` varchar(80) NOT NULL,
  `NOME` varchar(80) DEFAULT NULL,
  `SENHA` varchar(80) NOT NULL,
  `ATIVO` int(1) DEFAULT NULL,
  `CAMINHOFOTO` varchar(200) DEFAULT NULL,
  `CODCLIENTE` int(11) DEFAULT NULL,
  `CODEMPRESA` int(11) DEFAULT NULL,
  `CODFILIAL` int(11) DEFAULT NULL,
  `TIPOUSUARIO` int(11) DEFAULT '4',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glusuario`
--

INSERT INTO `glusuario` (`CODUSUARIO`, `EMAIL`, `NOME`, `SENHA`, `ATIVO`, `CAMINHOFOTO`, `CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `TIPOUSUARIO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 'christiandmelo@gmail.com', 'Christian Luis de Melo', 'e10adc3949ba59abbe56e057f20f883e', 1, '/assets/img/perfil/1-christiandmelo@gmail.com.jpg', 1, 1, 1, 1, '1', '2016-02-24 00:00:00', '1', '2016-05-07 15:09:05'),
(2, 'oswaldo@crc.com.br', 'Oswaldo', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-02-24 00:00:00', '1', '2016-02-24 00:00:00'),
(3, 'israel@crc.com.br', 'Israel', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-02-24 00:00:00', '1', '2016-02-24 00:00:00'),
(4, 'secretaria@crc.com.br', 'Secretaria - CRC', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 3, '1', '2015-03-04 00:00:00', '1', '2015-03-04 00:00:00'),
(5, 'trilhagem@crc.com.br', 'Trilhagem - CRC', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 3, '1', '2015-03-04 00:00:00', '1', '2015-03-04 00:00:00'),
(6, 'pollyanna.m.abreu@gmail.com', 'Pollyanna Abreu', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(7, 'davi.davipires@gmail.com', 'Davi Pires', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(8, 'proflucasscf@gmail.com', 'Lucas', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(9, 'diegogm38@hotmail.com', 'Diego', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(10, 'israel.teixeira17@gmail.com', 'Israel Teixeira', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(11, 'alissonking3@gmail.com', 'Alisson', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(12, 'juliocesarsbrito@gmail.com', 'Julio Cesar', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(13, 'fatimabastos05@gmail.com', 'Fatima Bastos', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(14, 'lucianotkd00@gmail.com', 'Luciano', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(15, 'diegogm38@gmail.com', 'Diego', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(16, 'juningzn13@gmail.com', 'Osvaldo', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(17, 'davi.lotus@gmail.com', 'Davi', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(18, 'daianelirio@gmail.com', 'Daiane Lirio', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(19, 'gustavomloiola@gmail.com', 'Gustavo Loiola', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(20, 'ericksonguilherme@gmail.com', 'Erickson Guilherme', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(21, 'wesley.cadete@gmail.com', 'Wesley', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00'),
(22, 'lucasalves.souza01@gmail.com', 'Lucas Alves', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 1, 1, 1, 1, '1', '2016-05-01 12:02:00', '1', '2016-05-01 12:02:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `glusuariofilial`
--

CREATE TABLE `glusuariofilial` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODUSUARIOFILIAL` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODUSUARIO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) DEFAULT NULL,
  `REGCRIADOEM` datetime DEFAULT NULL,
  `REGMODIFPOR` varchar(80) DEFAULT NULL,
  `REGMODIFEM` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `glusuariofilial`
--

INSERT INTO `glusuariofilial` (`CODCLIENTE`, `CODEMPRESA`, `CODUSUARIOFILIAL`, `CODFILIAL`, `CODUSUARIO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 10, 1, NULL, NULL, '1', '2016-05-07 05:13:41'),
(1, 1, 2, 1, 2, NULL, NULL, NULL, NULL),
(1, 1, 3, 1, 3, NULL, NULL, NULL, NULL),
(1, 1, 4, 1, 4, NULL, NULL, NULL, NULL),
(1, 1, 5, 1, 5, NULL, NULL, NULL, NULL),
(1, 1, 6, 2, 1, NULL, NULL, NULL, NULL),
(1, 1, 8, 2, 2, '1', '2016-04-27 21:02:27', '1', '2016-04-27 21:02:27'),
(1, 1, 9, 4, 1, '1', '2016-04-28 00:19:18', '1', '2016-04-28 00:19:18'),
(1, 1, 10, 1, 6, '1', '2016-05-01 15:11:56', '1', '2016-05-01 15:11:56'),
(1, 1, 11, 1, 7, '1', '2016-05-01 15:12:00', '1', '2016-05-01 15:12:00'),
(1, 1, 12, 1, 8, '1', '2016-05-01 15:12:04', '1', '2016-05-01 15:12:04'),
(1, 1, 13, 1, 9, '1', '2016-05-01 15:12:07', '1', '2016-05-01 15:12:07'),
(1, 1, 14, 1, 10, '1', '2016-05-01 15:12:11', '1', '2016-05-01 15:12:11'),
(1, 1, 15, 1, 11, '1', '2016-05-01 15:12:15', '1', '2016-05-01 15:12:15'),
(1, 1, 16, 1, 12, '1', '2016-05-01 15:12:19', '1', '2016-05-01 15:12:19'),
(1, 1, 17, 1, 13, '1', '2016-05-01 15:12:22', '1', '2016-05-01 15:12:22'),
(1, 1, 18, 1, 14, '1', '2016-05-01 15:12:26', '1', '2016-05-01 15:12:26'),
(1, 1, 19, 1, 15, '1', '2016-05-01 15:12:30', '1', '2016-05-01 15:12:30'),
(1, 1, 20, 1, 16, '1', '2016-05-01 15:12:34', '1', '2016-05-01 15:12:34'),
(1, 1, 21, 1, 17, '1', '2016-05-01 15:12:37', '1', '2016-05-01 15:12:37'),
(1, 1, 22, 1, 18, '1', '2016-05-01 15:12:41', '1', '2016-05-01 15:12:41'),
(1, 1, 23, 1, 19, '1', '2016-05-01 15:12:44', '1', '2016-05-01 15:12:44'),
(1, 1, 25, 1, 1, '10', '2016-05-07 15:08:48', '10', '2016-05-07 15:08:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trcamposproduto`
--

CREATE TABLE `trcamposproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCAMPOSPRODUTO` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `CODCAMPOSTIPO` int(11) NOT NULL,
  `VALOR` varchar(200) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trcamposproduto`
--

INSERT INTO `trcamposproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCAMPOSPRODUTO`, `CODPRODUTO`, `CODCAMPOSTIPO`, `VALOR`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 1, 26, 2, 'dell teste', '1', '2016-05-24 22:44:04', '1', '2016-05-25 17:38:39'),
(1, 1, 1, 2, 26, 3, 'optico', '1', '2016-05-24 22:44:05', '1', '2016-05-25 17:38:39'),
(1, 1, 1, 3, 27, 2, 'dell', '1', '2016-05-24 22:47:45', '1', '2016-05-24 22:47:45'),
(1, 1, 1, 4, 27, 3, 'Bolinha', '1', '2016-05-24 22:47:45', '1', '2016-05-24 22:47:45'),
(1, 1, 1, 5, 28, 2, 'dell', '1', '2016-05-24 22:59:00', '1', '2016-05-25 17:39:26'),
(1, 1, 1, 6, 28, 3, 'optico ALL', '1', '2016-05-24 22:59:01', '1', '2016-05-25 17:39:26'),
(1, 1, 1, 7, 29, 2, 'dell', '1', '2016-05-24 23:01:24', '1', '2016-05-25 17:46:39'),
(1, 1, 1, 8, 29, 3, 'optico 2', '1', '2016-05-24 23:01:25', '1', '2016-05-25 17:46:39'),
(1, 1, 1, 9, 30, 2, 'dell', '1', '2016-05-24 23:03:20', '1', '2016-05-24 23:03:20'),
(1, 1, 1, 10, 30, 3, 'optico', '1', '2016-05-24 23:03:20', '1', '2016-05-24 23:03:20'),
(1, 1, 1, 11, 31, 2, 'Intel', '1', '2016-05-25 17:47:41', '1', '2016-05-25 17:47:41'),
(1, 1, 1, 12, 31, 3, 'optico', '1', '2016-05-25 17:47:41', '1', '2016-05-25 17:47:41'),
(1, 1, 1, 13, 32, 2, 'maria', '1', '2016-05-25 17:53:37', '1', '2016-05-25 17:53:37'),
(1, 1, 1, 14, 32, 3, 'fio', '1', '2016-05-25 17:53:37', '1', '2016-05-25 17:53:37'),
(1, 1, 1, 15, 33, 2, 'wee', '1', '2016-05-25 17:55:28', '1', '2016-05-25 17:55:28'),
(1, 1, 1, 16, 33, 3, 'Bolinha', '1', '2016-05-25 17:55:28', '1', '2016-05-25 17:55:28'),
(1, 1, 1, 17, 34, 2, 'dell teste', '1', '2016-05-25 17:58:03', '1', '2016-05-25 17:58:03'),
(1, 1, 1, 18, 34, 3, 'optico', '1', '2016-05-25 17:58:03', '1', '2016-05-25 17:58:03'),
(1, 1, 1, 21, 36, 1, '50 GB', '1', '2016-05-25 18:05:50', '1', '2016-05-25 18:05:50'),
(1, 1, 1, 22, 37, 1, '2GB', '1', '2016-05-25 18:08:09', '1', '2016-05-25 18:08:09'),
(1, 1, 1, 25, 39, 1, '150 GB', '1', '2016-05-25 18:19:33', '1', '2016-05-25 18:19:33'),
(1, 1, 1, 26, 40, 2, 'dell', '1', '2016-05-25 18:21:36', '1', '2016-05-25 18:21:36'),
(1, 1, 1, 27, 40, 3, 'Bolinha', '1', '2016-05-25 18:21:36', '1', '2016-05-25 18:21:36'),
(1, 1, 1, 28, 41, 4, 'DDR2', '1', '2016-05-25 20:42:31', '1', '2016-05-25 20:42:31'),
(1, 1, 1, 29, 41, 5, '150 GB', '1', '2016-05-25 20:42:31', '1', '2016-05-25 20:42:31'),
(1, 1, 1, 30, 42, 1, '2GB', '1', '2016-05-25 20:53:49', '1', '2016-05-25 20:53:49'),
(1, 1, 1, 31, 43, 4, 'DDR2', '1', '2016-05-25 20:58:41', '1', '2016-05-25 20:58:41'),
(1, 1, 1, 32, 43, 5, '10 GB', '1', '2016-05-25 20:58:42', '1', '2016-05-25 20:58:42'),
(1, 1, 1, 33, 44, 4, 'DDR2', '1', '2016-05-25 20:59:38', '1', '2016-05-25 20:59:38'),
(1, 1, 1, 34, 44, 5, '10 GB', '1', '2016-05-25 20:59:39', '1', '2016-05-25 20:59:39'),
(1, 1, 1, 35, 45, 4, 'DDR2', '1', '2016-05-25 21:39:11', '1', '2016-05-25 21:39:11'),
(1, 1, 1, 36, 45, 5, '10 GB', '1', '2016-05-25 21:39:11', '1', '2016-05-25 21:39:11'),
(1, 1, 1, 37, 46, 4, 'DDR2', '1', '2016-05-25 21:40:06', '1', '2016-05-25 21:40:06'),
(1, 1, 1, 38, 46, 5, '10 GB', '1', '2016-05-25 21:40:06', '1', '2016-05-25 21:40:06'),
(1, 1, 1, 39, 47, 4, 'DDR2', '1', '2016-05-25 21:55:54', '1', '2016-05-25 21:55:54'),
(1, 1, 1, 40, 47, 5, '10 GB', '1', '2016-05-25 21:55:55', '1', '2016-05-25 21:55:55'),
(1, 1, 1, 41, 48, 4, 'DDR2', '1', '2016-05-25 21:59:43', '1', '2016-05-25 21:59:43'),
(1, 1, 1, 42, 48, 5, '10 GB', '1', '2016-05-25 21:59:43', '1', '2016-05-25 21:59:43'),
(1, 1, 1, 43, 49, 4, 'DDR2', '1', '2016-05-25 22:01:45', '1', '2016-05-25 22:01:45'),
(1, 1, 1, 44, 49, 5, '521 MB', '1', '2016-05-25 22:01:45', '1', '2016-05-25 22:01:45'),
(1, 1, 1, 45, 50, 1, '10 GB', '1', '2016-05-26 01:35:14', '1', '2016-05-26 01:35:14'),
(1, 1, 1, 46, 51, 1, '50 GB', '1', '2016-05-26 01:39:02', '1', '2016-05-26 01:39:02'),
(1, 1, 1, 47, 51, 6, 'Seagate', '1', '2016-05-26 01:39:02', '1', '2016-05-26 01:39:02'),
(1, 1, 1, 48, 52, 1, '150 GB', '1', '2016-05-27 14:33:39', '1', '2016-05-27 14:33:39'),
(1, 1, 1, 49, 52, 6, 'dell', '1', '2016-05-27 14:33:39', '1', '2016-05-27 14:33:39'),
(1, 1, 1, 50, 53, 1, '2 GB', '1', '2016-05-27 14:39:56', '1', '2016-05-27 14:39:56'),
(1, 1, 1, 51, 53, 6, 'asdfa', '1', '2016-05-27 14:39:56', '1', '2016-05-27 14:39:56'),
(1, 1, 1, 52, 54, 1, '10 GB', '1', '2016-05-27 14:40:27', '1', '2016-05-27 14:40:27'),
(1, 1, 1, 53, 54, 6, 'wee', '1', '2016-05-27 14:40:28', '1', '2016-05-27 14:40:28'),
(1, 1, 1, 54, 55, 1, '10 GB', '1', '2016-05-27 14:40:58', '1', '2016-05-27 14:40:58'),
(1, 1, 1, 55, 55, 6, 'wee', '1', '2016-05-27 14:40:58', '1', '2016-05-27 14:40:58'),
(1, 1, 1, 56, 56, 1, '10 GB', '1', '2016-05-27 14:41:36', '1', '2016-05-27 14:41:36'),
(1, 1, 1, 57, 56, 6, 'wee', '1', '2016-05-27 14:41:36', '1', '2016-05-27 14:41:36'),
(1, 1, 1, 58, 57, 4, 'DDR2', '1', '2016-05-28 00:15:32', '1', '2016-05-28 00:15:32'),
(1, 1, 1, 59, 57, 5, '2 GB', '1', '2016-05-28 00:15:32', '1', '2016-05-28 00:15:32'),
(1, 1, 1, 60, 58, 4, 'DDR2', '1', '2016-05-28 00:16:36', '1', '2016-05-28 00:16:36'),
(1, 1, 1, 61, 58, 5, '2 GB', '1', '2016-05-28 00:16:36', '1', '2016-05-28 00:16:36'),
(1, 1, 1, 62, 59, 1, '50 GB', '1', '2016-05-28 02:30:16', '1', '2016-05-28 02:30:16'),
(1, 1, 1, 63, 59, 6, 'asdfa', '1', '2016-05-28 02:30:17', '1', '2016-05-28 02:30:17'),
(1, 1, 1, 64, 60, 1, '10 GB', '1', '2016-05-28 16:48:17', '1', '2016-05-28 16:48:17'),
(1, 1, 1, 65, 60, 6, 'dell', '1', '2016-05-28 16:48:17', '1', '2016-05-28 16:48:17'),
(1, 1, 1, 66, 66, 4, 'DDR2', '1', '2016-05-30 21:24:45', '1', '2016-05-30 21:24:45'),
(1, 1, 1, 67, 66, 5, '2 GB', '1', '2016-05-30 21:24:45', '1', '2016-05-30 21:24:45'),
(1, 1, 1, 68, 67, 1, '150 GB', '1', '2016-05-30 21:35:01', '1', '2016-05-30 21:35:01'),
(1, 1, 1, 69, 67, 6, 'Dell', '1', '2016-05-30 21:35:01', '1', '2016-05-30 21:35:01'),
(1, 1, 1, 70, 68, 4, 'DDR2', '1', '2016-05-30 22:32:36', '1', '2016-05-30 22:32:54'),
(1, 1, 1, 71, 68, 5, '2 GB', '1', '2016-05-30 22:32:37', '1', '2016-05-30 22:32:54'),
(1, 1, 1, 72, 69, 4, 'DDR3', '1', '2016-05-30 22:44:19', '1', '2016-05-30 22:44:19'),
(1, 1, 1, 73, 69, 5, '8 GB', '1', '2016-05-30 22:44:19', '1', '2016-05-30 22:44:19'),
(1, 1, 1, 74, 70, 4, 'DDR3', '1', '2016-05-31 18:44:44', '1', '2016-05-31 18:44:44'),
(1, 1, 1, 75, 70, 5, '4 GB', '1', '2016-05-31 18:44:44', '1', '2016-05-31 18:44:44'),
(1, 1, 1, 76, 71, 1, '150 GB', '1', '2016-05-31 18:46:57', '1', '2016-05-31 18:46:57'),
(1, 1, 1, 77, 71, 6, 'Seagate', '1', '2016-05-31 18:46:57', '1', '2016-05-31 18:46:57'),
(1, 1, 1, 78, 72, 7, 'PC-CHIPS', '1', '2016-05-31 18:47:21', '1', '2016-05-31 18:47:21'),
(1, 1, 1, 79, 73, 4, 'DDR2', '1', '2016-05-31 18:50:59', '1', '2016-05-31 18:50:59'),
(1, 1, 1, 80, 73, 5, '1 GB', '1', '2016-05-31 18:51:00', '1', '2016-05-31 18:51:00'),
(1, 1, 1, 81, 74, 1, '50 GB', '1', '2016-05-31 18:51:21', '1', '2016-05-31 18:51:21'),
(1, 1, 1, 82, 74, 6, 'Seagate', '1', '2016-05-31 18:51:21', '1', '2016-05-31 18:51:21'),
(1, 1, 1, 83, 75, 7, 'Gigabyte', '1', '2016-05-31 18:51:37', '1', '2016-05-31 18:51:37'),
(1, 1, 1, 84, 76, 4, 'DDR3', '1', '2016-05-31 18:54:05', '1', '2016-05-31 18:54:05'),
(1, 1, 1, 85, 76, 5, '8 GB', '1', '2016-05-31 18:54:05', '1', '2016-05-31 18:54:05'),
(1, 1, 1, 86, 77, 1, '750 GB', '1', '2016-05-31 18:54:19', '1', '2016-05-31 18:54:19'),
(1, 1, 1, 87, 77, 6, 'Seagate', '1', '2016-05-31 18:54:20', '1', '2016-05-31 18:54:20'),
(1, 1, 1, 88, 78, 7, 'Asus', '1', '2016-05-31 18:54:35', '1', '2016-05-31 18:54:35'),
(1, 1, 1, 89, 79, 1, '2 GB', '1', '2016-06-01 14:08:56', '1', '2016-06-01 14:08:56'),
(1, 1, 1, 90, 79, 6, 'dell', '1', '2016-06-01 14:08:56', '1', '2016-06-01 14:08:56'),
(1, 1, 1, 91, 81, 1, '150 GB', '1', '2016-06-01 17:17:55', '1', '2016-06-01 17:17:55'),
(1, 1, 1, 92, 82, 1, '150 GB', '1', '2016-06-01 17:21:40', '1', '2016-06-01 17:21:40'),
(1, 1, 1, 93, 86, 4, 'DDR2', '1', '2016-06-01 20:51:37', '1', '2016-06-01 20:51:37'),
(1, 1, 1, 94, 86, 5, '2 GB', '1', '2016-06-01 20:51:37', '1', '2016-06-01 20:51:37'),
(1, 1, 1, 95, 87, 4, 'DDR2', '1', '2016-06-01 21:16:35', '1', '2016-06-01 21:16:35'),
(1, 1, 1, 96, 87, 5, '2 GB', '1', '2016-06-01 21:16:35', '1', '2016-06-01 21:16:35'),
(1, 1, 1, 97, 89, 4, 'DDR2', '1', '2016-06-02 02:04:12', '1', '2016-06-02 02:04:12'),
(1, 1, 1, 98, 89, 5, '2GB', '1', '2016-06-02 02:04:12', '1', '2016-06-02 02:04:12'),
(1, 1, 1, 99, 90, 4, 'DDR2', '1', '2016-06-02 02:30:43', '1', '2016-06-02 02:30:43'),
(1, 1, 1, 100, 90, 5, '2 GB', '1', '2016-06-02 02:30:43', '1', '2016-06-02 02:30:43'),
(1, 1, 1, 101, 91, 1, '50 GB', '1', '2016-06-02 02:34:52', '1', '2016-06-02 02:34:52'),
(1, 1, 1, 102, 92, 7, 'PC-CHIPS', '1', '2016-06-02 02:38:20', '1', '2016-06-02 02:38:20'),
(1, 1, 1, 103, 93, 1, '2GB', '1', '2016-06-02 17:06:23', '1', '2016-06-02 17:06:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trcampostipo`
--

CREATE TABLE `trcampostipo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCAMPOSTIPO` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `NOME` varchar(200) NOT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trcampostipo`
--

INSERT INTO `trcampostipo` (`CODCLIENTE`, `CODCAMPOSTIPO`, `CODTIPO`, `NOME`, `ATIVO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 2, 'Capacidade', 1, '1', '2016-05-12 21:50:52', '1', '2016-05-12 21:50:52'),
(1, 2, 8, 'Fabricante', 1, '1', '2016-05-24 20:11:06', '1', '2016-05-24 20:11:06'),
(1, 3, 8, 'Tipo', 1, '1', '2016-05-24 20:11:23', '1', '2016-05-24 20:11:23'),
(1, 4, 3, 'Tipo', 1, '1', '2016-05-25 18:51:46', '1', '2016-05-25 18:51:46'),
(1, 5, 3, 'Capacidade', 1, '1', '2016-05-25 18:51:58', '1', '2016-05-25 18:51:58'),
(1, 6, 2, 'Fabricante', 0, '1', '2016-05-26 01:36:41', '1', '2016-06-01 16:54:58'),
(1, 7, 5, 'Fabricante', 1, '1', '2016-05-31 18:44:10', '1', '2016-05-31 18:44:10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trcomponentesdalocacao`
--

CREATE TABLE `trcomponentesdalocacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCOMPONENTESDALOCACAO` int(11) NOT NULL,
  `CODCOMPONENTESLOCACAO` int(11) NOT NULL,
  `CODPRODUTOPAI` int(11) NOT NULL,
  `CODPRODUTOFILHO` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `trcomponenteslocacao`
--

CREATE TABLE `trcomponenteslocacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCOMPONENTESLOCACAO` int(11) NOT NULL,
  `CODTIPOPAI` int(11) NOT NULL,
  `CODTIPOFILHO` int(11) NOT NULL,
  `ATIVO` int(11) NOT NULL,
  `OBRIGATORIO` int(11) NOT NULL DEFAULT '0',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trcomponenteslocacao`
--

INSERT INTO `trcomponenteslocacao` (`CODCLIENTE`, `CODCOMPONENTESLOCACAO`, `CODTIPOPAI`, `CODTIPOFILHO`, `ATIVO`, `OBRIGATORIO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 2, 1, 7, 1, 1, '1', '2016-05-12 22:16:06', '1', '2016-05-12 22:23:55'),
(1, 3, 1, 8, 1, 1, '1', '2016-06-01 23:56:15', '1', '2016-06-01 23:56:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trcomponentesproduto`
--

CREATE TABLE `trcomponentesproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCOMPONENTESPRODUTO` int(11) NOT NULL,
  `CODCOMPONENTESTIPO` int(11) NOT NULL,
  `CODPRODUTOPAI` int(11) NOT NULL,
  `CODPRODUTOFILHO` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trcomponentesproduto`
--

INSERT INTO `trcomponentesproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCOMPONENTESPRODUTO`, `CODCOMPONENTESTIPO`, `CODPRODUTOPAI`, `CODPRODUTOFILHO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 3, 1, 1, 48, '1', '2016-05-25 21:59:43', '1', '2016-06-01 21:53:01'),
(1, 1, 1, 4, 2, 1, NULL, '1', '2016-05-26 01:39:03', '1', '2016-05-30 21:12:29'),
(1, 1, 1, 5, 1, 2, NULL, '1', '2016-05-28 00:16:37', '1', '2016-06-01 15:40:41'),
(1, 1, 1, 6, 2, 2, NULL, '1', '2016-05-28 02:30:17', '1', '2016-05-29 18:37:26'),
(1, 1, 1, 7, 1, 61, 41, '1', '2016-05-30 21:24:45', '1', '2016-05-30 22:26:32'),
(1, 1, 1, 8, 2, 61, 67, '1', '2016-05-30 21:35:02', '1', '2016-05-30 21:35:02'),
(1, 1, 1, 9, 1, 65, NULL, '1', '2016-05-31 18:44:45', '1', '2016-06-01 04:03:40'),
(1, 1, 1, 10, 2, 65, NULL, '1', '2016-05-31 18:46:58', '1', '2016-06-01 04:04:15'),
(1, 1, 1, 11, 3, 65, NULL, '1', '2016-05-31 18:47:22', '1', '2016-06-01 04:04:25'),
(1, 1, 1, 12, 1, 64, 70, '1', '2016-05-31 18:51:00', '1', '2016-06-01 15:52:21'),
(1, 1, 1, 13, 2, 64, 36, '1', '2016-05-31 18:51:22', '1', '2016-06-01 15:49:17'),
(1, 1, 1, 14, 3, 64, 75, '1', '2016-05-31 18:51:38', '1', '2016-05-31 18:51:38'),
(1, 1, 1, 15, 1, 62, 76, '1', '2016-05-31 18:54:06', '1', '2016-05-31 18:54:06'),
(1, 1, 1, 16, 2, 62, 77, '1', '2016-05-31 18:54:20', '1', '2016-05-31 18:54:20'),
(1, 1, 1, 17, 3, 62, 78, '1', '2016-05-31 18:54:36', '1', '2016-05-31 18:54:36'),
(1, 1, 1, 18, 1, 85, NULL, '1', '2016-06-01 21:16:35', '1', '2016-06-01 21:17:18'),
(1, 1, 1, 19, 1, 83, 87, '1', '2016-06-02 02:30:43', '1', '2016-06-02 02:33:40'),
(1, 1, 1, 20, 2, 83, 53, '1', '2016-06-02 02:34:52', '1', '2016-06-02 02:37:06'),
(1, 1, 1, 21, 3, 83, 92, '1', '2016-06-02 02:38:21', '1', '2016-06-02 02:38:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trcomponentestipo`
--

CREATE TABLE `trcomponentestipo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCOMPONENTESTIPO` int(11) NOT NULL,
  `CODTIPOPAI` int(11) NOT NULL,
  `CODTIPOFILHO` int(11) NOT NULL,
  `OBRIGATORIO` int(11) NOT NULL DEFAULT '0',
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trcomponentestipo`
--

INSERT INTO `trcomponentestipo` (`CODCLIENTE`, `CODCOMPONENTESTIPO`, `CODTIPOPAI`, `CODTIPOFILHO`, `OBRIGATORIO`, `ATIVO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 3, 1, 1, '1', '2016-05-12 22:43:58', '1', '2016-05-12 22:43:58'),
(1, 2, 1, 2, 1, 1, '1', '2016-05-23 20:19:42', '1', '2016-05-23 20:19:42'),
(1, 3, 1, 5, 1, 1, '1', '2016-05-23 20:19:51', '1', '2016-05-23 20:19:51'),
(1, 4, 1, 4, 1, 0, '1', '2016-05-23 20:19:58', '1', '2016-05-26 01:40:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trdoacao`
--

CREATE TABLE `trdoacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODDOACAO` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `CODLOTE` int(11) DEFAULT NULL,
  `CODTIPOENTRADA` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL DEFAULT '1',
  `DESCRICAO` varchar(200) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL DEFAULT '1',
  `QUANTIDADETRIADA` int(11) NOT NULL DEFAULT '0',
  `ETIQUETA` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trdoacao`
--

INSERT INTO `trdoacao` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODDOACAO`, `CODFORNECEDOR`, `CODTIPO`, `CODLOTE`, `CODTIPOENTRADA`, `CODTRSTATUS`, `DESCRICAO`, `QUANTIDADE`, `QUANTIDADETRIADA`, `ETIQUETA`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 1, 2, 3, 6, 1, 1, 'sdfgsfs', 34, 7, NULL, '1', '2016-05-21 16:08:26', '1', '2016-05-30 22:44:19'),
(1, 1, 1, 3, 2, 2, 8, 1, 1, 'Hds', 50, 14, NULL, '1', '2016-05-21 17:04:13', '1', '2016-06-02 17:06:23'),
(1, 1, 1, 4, 1, 1, NULL, 2, 2, 'Gabinete Vostro', 1, 1, 2345, '1', '2016-05-21 17:05:14', '1', '0000-00-00 00:00:00'),
(1, 1, 1, 5, 1, 1, NULL, 2, 1, 'Gabinete Vostro', 1, 0, 2346, '1', '2016-05-21 17:06:04', '1', '0000-00-00 00:00:00'),
(1, 1, 1, 6, 3, 8, 10, 1, 2, 'Chegada de Mouse', 10, 10, NULL, '1', '2016-05-23 00:21:34', '1', '2016-05-25 18:21:36'),
(1, 1, 1, 7, 3, 2, 11, 1, 1, 'Teste em sala', 5, 5, NULL, '1', '2016-05-26 01:21:40', '1', '2016-05-27 13:45:57'),
(1, 1, 1, 10, 1, 1, NULL, 2, 1, 'Gabinete Vostro', 1, 0, 87654, '1', '2016-05-30 14:29:41', '1', '2016-05-30 14:29:41'),
(1, 1, 1, 11, 1, 1, NULL, 2, 2, 'Gabinete Vostro', 1, 1, 87655, '1', '2016-05-30 14:33:17', '1', '2016-05-31 18:54:50'),
(1, 1, 1, 12, 1, 1, NULL, 2, 2, 'Gabinete Vostro', 1, 1, 87656, '1', '2016-05-30 14:33:40', '1', '2016-06-01 15:58:43'),
(1, 1, 1, 13, 1, 1, NULL, 2, 2, 'Gabinete Vostro', 1, 1, 87657, '1', '2016-05-30 14:35:39', '1', '2016-06-01 15:28:31'),
(1, 1, 1, 14, 1, 1, NULL, 2, 2, 'Gabinete Vostro', 1, 1, 87658, '1', '2016-05-30 14:38:12', '1', '2016-06-01 01:25:36'),
(1, 1, 1, 15, 1, 3, 12, 1, 1, 'Gabinete Vostro', 10, 3, NULL, '1', '2016-05-30 14:38:30', '1', '2016-06-02 02:08:13'),
(1, 1, 1, 17, 3, 1, NULL, 2, 2, 'Novo Equipamento', 1, 1, 56780, '1', '2016-06-01 16:01:30', '1', '2016-06-01 16:03:18'),
(1, 1, 1, 18, 2, 1, NULL, 2, 2, 'Dell', 1, 1, 12345680, '1', '2016-06-01 19:50:17', '1', '2016-06-02 02:38:33'),
(1, 1, 1, 19, 2, 1, NULL, 2, 1, 'HP', 1, 0, 12345681, '1', '2016-06-01 20:27:18', '1', '2016-06-01 20:27:18'),
(1, 1, 1, 21, 2, 1, NULL, 2, 1, 'Gabinete Vostro', 1, 0, 12345682, '1', '2016-06-01 20:46:41', '1', '2016-06-01 20:46:41'),
(1, 1, 1, 22, 3, 3, 13, 1, 1, 'Cadastrado 01-05', 5, 0, NULL, '1', '2016-06-02 01:52:57', '1', '2016-06-02 01:52:57'),
(1, 1, 1, 23, 3, 3, 14, 1, 1, 'Cadastrado 01/05', 5, 0, NULL, '1', '2016-06-02 01:53:13', '1', '2016-06-02 01:53:13'),
(1, 1, 1, 24, 3, 1, NULL, 2, 2, 'Cadastrado 01 05', 1, 1, 12345688, '1', '2016-06-02 01:54:03', '1', '2016-06-02 02:28:10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trlocacao`
--

CREATE TABLE `trlocacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLOCACAO` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `INTERNA` int(11) NOT NULL COMMENT '1 = INTERNA\n0 = NÃO',
  `CODCOMPONENTESLOCACAO` int(11) DEFAULT NULL,
  `CODFORNECEDOR` int(11) DEFAULT NULL,
  `CODCLIENTELOCADO` int(11) DEFAULT NULL,
  `CODEMPRESALOCADO` int(11) DEFAULT NULL,
  `CODFILIALLOCADO` int(11) DEFAULT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `trlogcomponentesproduto`
--

CREATE TABLE `trlogcomponentesproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLOGCOMPONENTESPRODUTO` int(11) NOT NULL,
  `CODCOMPONENTESPRODUTO` int(11) NOT NULL,
  `CODPRODUTOFILHO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trlogcomponentesproduto`
--

INSERT INTO `trlogcomponentesproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLOGCOMPONENTESPRODUTO`, `CODCOMPONENTESPRODUTO`, `CODPRODUTOFILHO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 1, 3, 48, '1', '2016-05-25 21:59:44', '1', '2016-05-25 21:59:44'),
(1, 1, 1, 2, 4, 51, '1', '2016-05-26 01:39:03', '1', '2016-05-26 01:39:03'),
(1, 1, 1, 3, 5, 58, '1', '2016-05-28 00:16:37', '1', '2016-05-28 00:16:37'),
(1, 1, 1, 4, 6, 59, '1', '2016-05-28 02:30:18', '1', '2016-05-28 02:30:18'),
(1, 1, 1, 5, 7, 66, '1', '2016-05-30 21:24:46', '1', '2016-05-30 21:24:46'),
(1, 1, 1, 6, 8, 67, '1', '2016-05-30 21:35:02', '1', '2016-05-30 21:35:02'),
(1, 1, 1, 7, 7, 41, '1', '2016-05-30 22:07:30', '1', '2016-05-30 22:07:30'),
(1, 1, 1, 8, 7, 41, '1', '2016-05-30 22:16:04', '1', '2016-05-30 22:16:04'),
(1, 1, 1, 9, 7, 49, '1', '2016-05-30 22:18:17', '1', '2016-05-30 22:18:17'),
(1, 1, 1, 10, 7, 41, '1', '2016-05-30 22:26:32', '1', '2016-05-30 22:26:32'),
(1, 1, 1, 11, 5, 68, '1', '2016-05-30 22:41:33', '1', '2016-05-30 22:41:33'),
(1, 1, 1, 12, 5, 69, '1', '2016-05-30 23:10:12', '1', '2016-05-30 23:10:12'),
(1, 1, 1, 13, 9, 70, '1', '2016-05-31 18:44:45', '1', '2016-05-31 18:44:45'),
(1, 1, 1, 14, 10, 71, '1', '2016-05-31 18:46:58', '1', '2016-05-31 18:46:58'),
(1, 1, 1, 15, 11, 72, '1', '2016-05-31 18:47:22', '1', '2016-05-31 18:47:22'),
(1, 1, 1, 16, 12, 73, '1', '2016-05-31 18:51:00', '1', '2016-05-31 18:51:00'),
(1, 1, 1, 17, 13, 74, '1', '2016-05-31 18:51:22', '1', '2016-05-31 18:51:22'),
(1, 1, 1, 18, 14, 75, '1', '2016-05-31 18:51:38', '1', '2016-05-31 18:51:38'),
(1, 1, 1, 19, 15, 76, '1', '2016-05-31 18:54:06', '1', '2016-05-31 18:54:06'),
(1, 1, 1, 20, 16, 77, '1', '2016-05-31 18:54:20', '1', '2016-05-31 18:54:20'),
(1, 1, 1, 21, 17, 78, '1', '2016-05-31 18:54:36', '1', '2016-05-31 18:54:36'),
(1, 1, 1, 22, 5, 69, '1', '2016-05-31 18:58:38', '1', '2016-05-31 18:58:38'),
(1, 1, 1, 23, 5, 70, '1', '2016-06-01 15:35:06', '1', '2016-06-01 15:35:06'),
(1, 1, 1, 24, 13, 36, '1', '2016-06-01 15:49:17', '1', '2016-06-01 15:49:17'),
(1, 1, 1, 25, 12, 70, '1', '2016-06-01 15:49:55', '1', '2016-06-01 15:49:55'),
(1, 1, 1, 26, 12, 70, '1', '2016-06-01 15:52:21', '1', '2016-06-01 15:52:21'),
(1, 1, 1, 27, 3, 48, '1', '2016-06-01 21:15:07', '1', '2016-06-01 21:15:07'),
(1, 1, 1, 28, 18, 87, '1', '2016-06-01 21:16:36', '1', '2016-06-01 21:16:36'),
(1, 1, 1, 29, 3, 86, '1', '2016-06-01 21:41:12', '1', '2016-06-01 21:41:12'),
(1, 1, 1, 30, 3, 48, '1', '2016-06-01 21:53:01', '1', '2016-06-01 21:53:01'),
(1, 1, 1, 31, 19, 90, '1', '2016-06-02 02:30:44', '1', '2016-06-02 02:30:44'),
(1, 1, 1, 32, 19, 87, '1', '2016-06-02 02:33:40', '1', '2016-06-02 02:33:40'),
(1, 1, 1, 33, 20, 91, '1', '2016-06-02 02:34:52', '1', '2016-06-02 02:34:52'),
(1, 1, 1, 34, 20, 54, '1', '2016-06-02 02:35:11', '1', '2016-06-02 02:35:11'),
(1, 1, 1, 35, 20, 53, '1', '2016-06-02 02:37:06', '1', '2016-06-02 02:37:06'),
(1, 1, 1, 36, 21, 92, '1', '2016-06-02 02:38:21', '1', '2016-06-02 02:38:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trlogproduto`
--

CREATE TABLE `trlogproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLOGPRODUTO` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL,
  `CODLOCACAO` int(11) DEFAULT NULL,
  `CODVENDA` int(11) DEFAULT NULL,
  `CODSUCATA` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trlogproduto`
--

INSERT INTO `trlogproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLOGPRODUTO`, `CODPRODUTO`, `CODTRSTATUS`, `CODLOCACAO`, `CODVENDA`, `CODSUCATA`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 1, 28, 3, NULL, NULL, NULL, '1', '2016-05-24 22:59:01', '1', '2016-05-24 22:59:01'),
(1, 1, 1, 2, 30, 3, NULL, NULL, NULL, '1', '2016-05-24 23:03:20', '1', '2016-05-24 23:03:20'),
(1, 1, 1, 3, 31, 3, NULL, NULL, NULL, '1', '2016-05-25 17:47:41', '1', '2016-05-25 17:47:41'),
(1, 1, 1, 4, 32, 3, NULL, NULL, NULL, '1', '2016-05-25 17:53:37', '1', '2016-05-25 17:53:37'),
(1, 1, 1, 5, 33, 3, NULL, NULL, NULL, '1', '2016-05-25 17:55:28', '1', '2016-05-25 17:55:28'),
(1, 1, 1, 6, 34, 3, NULL, NULL, NULL, '1', '2016-05-25 17:58:03', '1', '2016-05-25 17:58:03'),
(1, 1, 1, 8, 36, 3, NULL, NULL, NULL, '1', '2016-05-25 18:05:50', '1', '2016-05-25 18:05:50'),
(1, 1, 1, 9, 37, 3, NULL, NULL, NULL, '1', '2016-05-25 18:08:09', '1', '2016-05-25 18:08:09'),
(1, 1, 1, 11, 39, 3, NULL, NULL, NULL, '1', '2016-05-25 18:19:33', '1', '2016-05-25 18:19:33'),
(1, 1, 1, 12, 40, 3, NULL, NULL, NULL, '1', '2016-05-25 18:21:36', '1', '2016-05-25 18:21:36'),
(1, 1, 1, 13, 41, 3, NULL, NULL, NULL, '1', '2016-05-25 20:42:31', '1', '2016-05-25 20:42:31'),
(1, 1, 1, 14, 42, 3, NULL, NULL, NULL, '1', '2016-05-25 20:53:49', '1', '2016-05-25 20:53:49'),
(1, 1, 1, 15, 43, 3, NULL, NULL, NULL, '1', '2016-05-25 20:58:42', '1', '2016-05-25 20:58:42'),
(1, 1, 1, 16, 44, 3, NULL, NULL, NULL, '1', '2016-05-25 20:59:39', '1', '2016-05-25 20:59:39'),
(1, 1, 1, 17, 45, 14, NULL, NULL, NULL, '1', '2016-05-25 21:39:11', '1', '2016-05-25 21:39:11'),
(1, 1, 1, 18, 46, 14, NULL, NULL, NULL, '1', '2016-05-25 21:40:06', '1', '2016-05-25 21:40:06'),
(1, 1, 1, 19, 47, 14, NULL, NULL, NULL, '1', '2016-05-25 21:55:55', '1', '2016-05-25 21:55:55'),
(1, 1, 1, 20, 48, 14, NULL, NULL, NULL, '1', '2016-05-25 21:59:43', '1', '2016-05-25 21:59:43'),
(1, 1, 1, 21, 49, 3, NULL, NULL, NULL, '1', '2016-05-25 22:01:45', '1', '2016-05-25 22:01:45'),
(1, 1, 1, 22, 50, 3, NULL, NULL, NULL, '1', '2016-05-26 01:35:14', '1', '2016-05-26 01:35:14'),
(1, 1, 1, 23, 51, 14, NULL, NULL, NULL, '1', '2016-05-26 01:39:03', '1', '2016-05-26 01:39:03'),
(1, 1, 1, 24, 52, 3, NULL, NULL, NULL, '1', '2016-05-27 14:33:39', '1', '2016-05-27 14:33:39'),
(1, 1, 1, 25, 53, 3, NULL, NULL, NULL, '1', '2016-05-27 14:39:56', '1', '2016-05-27 14:39:56'),
(1, 1, 1, 26, 54, 3, NULL, NULL, NULL, '1', '2016-05-27 14:40:28', '1', '2016-05-27 14:40:28'),
(1, 1, 1, 27, 55, 3, NULL, NULL, NULL, '1', '2016-05-27 14:40:58', '1', '2016-05-27 14:40:58'),
(1, 1, 1, 28, 56, 3, NULL, NULL, NULL, '1', '2016-05-27 14:41:36', '1', '2016-05-27 14:41:36'),
(1, 1, 1, 29, 57, 14, NULL, NULL, NULL, '1', '2016-05-28 00:15:32', '1', '2016-05-28 00:15:32'),
(1, 1, 1, 30, 58, 14, NULL, NULL, NULL, '1', '2016-05-28 00:16:37', '1', '2016-05-28 00:16:37'),
(1, 1, 1, 31, 58, 14, NULL, NULL, NULL, '1', '2016-05-28 00:16:37', '1', '2016-05-28 00:16:37'),
(1, 1, 1, 32, 59, 14, NULL, NULL, NULL, '1', '2016-05-28 02:30:17', '1', '2016-05-28 02:30:17'),
(1, 1, 1, 33, 60, 3, NULL, NULL, NULL, '1', '2016-05-28 16:48:18', '1', '2016-05-28 16:48:18'),
(1, 1, 1, 34, 61, 1, NULL, NULL, NULL, '1', '2016-05-30 14:29:41', '1', '2016-05-30 14:29:41'),
(1, 1, 1, 35, 62, 1, NULL, NULL, NULL, '1', '2016-05-30 14:33:18', '1', '2016-05-30 14:33:18'),
(1, 1, 1, 36, 63, 1, NULL, NULL, NULL, '1', '2016-05-30 14:33:41', '1', '2016-05-30 14:33:41'),
(1, 1, 1, 37, 64, 1, NULL, NULL, NULL, '1', '2016-05-30 14:35:39', '1', '2016-05-30 14:35:39'),
(1, 1, 1, 38, 65, 1, NULL, NULL, NULL, '1', '2016-05-30 14:38:12', '1', '2016-05-30 14:38:12'),
(1, 1, 1, 39, 51, 4, NULL, NULL, 13, '1', '2016-05-30 21:12:29', '1', '2016-05-30 21:12:29'),
(1, 1, 1, 40, 66, 14, NULL, NULL, NULL, '1', '2016-05-30 21:24:45', '1', '2016-05-30 21:24:45'),
(1, 1, 1, 41, 67, 14, NULL, NULL, NULL, '1', '2016-05-30 21:35:02', '1', '2016-05-30 21:35:02'),
(1, 1, 1, 42, 66, 4, NULL, NULL, 14, '1', '2016-05-30 22:07:30', '1', '2016-05-30 22:07:30'),
(1, 1, 1, 43, 41, 4, NULL, NULL, NULL, '1', '2016-05-30 22:07:31', '1', '2016-05-30 22:07:31'),
(1, 1, 1, 44, 41, 4, NULL, NULL, 15, '1', '2016-05-30 22:16:04', '1', '2016-05-30 22:16:04'),
(1, 1, 1, 45, 41, 4, NULL, NULL, 16, '1', '2016-05-30 22:18:16', '1', '2016-05-30 22:18:16'),
(1, 1, 1, 46, 49, 14, NULL, NULL, NULL, '1', '2016-05-30 22:18:17', '1', '2016-05-30 22:18:17'),
(1, 1, 1, 47, 49, 4, NULL, NULL, 17, '1', '2016-05-30 22:26:32', '1', '2016-05-30 22:26:32'),
(1, 1, 1, 48, 41, 15, NULL, NULL, NULL, '1', '2016-05-30 22:26:33', '1', '2016-05-30 22:26:33'),
(1, 1, 1, 49, 68, 3, NULL, NULL, NULL, '1', '2016-05-30 22:32:37', '1', '2016-05-30 22:32:37'),
(1, 1, 1, 50, 68, 15, NULL, NULL, NULL, '1', '2016-05-30 22:41:34', '1', '2016-05-30 22:41:34'),
(1, 1, 1, 51, 69, 3, NULL, NULL, NULL, '1', '2016-05-30 22:44:20', '1', '2016-05-30 22:44:20'),
(1, 1, 1, 52, 68, 4, NULL, NULL, 21, '1', '2016-05-30 23:10:11', '1', '2016-05-30 23:10:11'),
(1, 1, 1, 53, 69, 15, NULL, NULL, NULL, '1', '2016-05-30 23:10:12', '1', '2016-05-30 23:10:12'),
(1, 1, 1, 54, 69, 3, NULL, NULL, NULL, '1', '2016-05-30 23:31:33', '1', '2016-05-30 23:31:33'),
(1, 1, 1, 55, 69, 3, NULL, NULL, NULL, '1', '2016-05-30 23:33:11', '1', '2016-05-30 23:33:11'),
(1, 1, 1, 56, 70, 14, NULL, NULL, NULL, '1', '2016-05-31 18:44:45', '1', '2016-05-31 18:44:45'),
(1, 1, 1, 57, 71, 14, NULL, NULL, NULL, '1', '2016-05-31 18:46:57', '1', '2016-05-31 18:46:57'),
(1, 1, 1, 58, 72, 14, NULL, NULL, NULL, '1', '2016-05-31 18:47:22', '1', '2016-05-31 18:47:22'),
(1, 1, 1, 59, 65, 3, NULL, NULL, NULL, '1', '2016-05-31 18:48:46', '1', '2016-05-31 18:48:46'),
(1, 1, 1, 60, 73, 14, NULL, NULL, NULL, '1', '2016-05-31 18:51:00', '1', '2016-05-31 18:51:00'),
(1, 1, 1, 61, 74, 14, NULL, NULL, NULL, '1', '2016-05-31 18:51:22', '1', '2016-05-31 18:51:22'),
(1, 1, 1, 62, 75, 14, NULL, NULL, NULL, '1', '2016-05-31 18:51:37', '1', '2016-05-31 18:51:37'),
(1, 1, 1, 63, 64, 3, NULL, NULL, NULL, '1', '2016-05-31 18:51:51', '1', '2016-05-31 18:51:51'),
(1, 1, 1, 64, 76, 14, NULL, NULL, NULL, '1', '2016-05-31 18:54:06', '1', '2016-05-31 18:54:06'),
(1, 1, 1, 65, 77, 14, NULL, NULL, NULL, '1', '2016-05-31 18:54:20', '1', '2016-05-31 18:54:20'),
(1, 1, 1, 66, 78, 14, NULL, NULL, NULL, '1', '2016-05-31 18:54:36', '1', '2016-05-31 18:54:36'),
(1, 1, 1, 67, 62, 3, NULL, NULL, NULL, '1', '2016-05-31 18:54:50', '1', '2016-05-31 18:54:50'),
(1, 1, 1, 68, 69, 15, NULL, NULL, NULL, '1', '2016-05-31 18:58:38', '1', '2016-05-31 18:58:38'),
(1, 1, 1, 69, 65, 3, NULL, NULL, NULL, '1', '2016-06-01 01:20:34', '1', '2016-06-01 01:20:34'),
(1, 1, 1, 70, 65, 3, NULL, NULL, NULL, '1', '2016-06-01 01:22:08', '1', '2016-06-01 01:22:08'),
(1, 1, 1, 71, 65, 3, NULL, NULL, NULL, '1', '2016-06-01 01:25:35', '1', '2016-06-01 01:25:35'),
(1, 1, 1, 72, 70, 3, NULL, NULL, NULL, '1', '2016-06-01 04:03:40', '1', '2016-06-01 04:03:40'),
(1, 1, 1, 73, 71, 3, NULL, NULL, NULL, '1', '2016-06-01 04:04:14', '1', '2016-06-01 04:04:14'),
(1, 1, 1, 74, 72, 3, NULL, NULL, NULL, '1', '2016-06-01 04:04:25', '1', '2016-06-01 04:04:25'),
(1, 1, 1, 75, 65, 4, NULL, NULL, NULL, '1', '2016-06-01 04:05:10', '1', '2016-06-01 04:05:10'),
(1, 1, 1, 76, 79, 3, NULL, NULL, NULL, '1', '2016-06-01 14:08:56', '1', '2016-06-01 14:08:56'),
(1, 1, 1, 77, 64, 3, NULL, NULL, NULL, '1', '2016-06-01 15:28:30', '1', '2016-06-01 15:28:30'),
(1, 1, 1, 78, 73, 4, NULL, NULL, 22, '1', '2016-06-01 15:28:32', '1', '2016-06-01 15:28:32'),
(1, 1, 1, 79, 74, 4, NULL, NULL, 23, '1', '2016-06-01 15:29:38', '1', '2016-06-01 15:29:38'),
(1, 1, 1, 80, 69, 4, NULL, NULL, 24, '1', '2016-06-01 15:30:41', '1', '2016-06-01 15:30:41'),
(1, 1, 1, 81, 70, 15, NULL, NULL, NULL, '1', '2016-06-01 15:35:06', '1', '2016-06-01 15:35:06'),
(1, 1, 1, 82, 70, 3, NULL, NULL, NULL, '1', '2016-06-01 15:40:40', '1', '2016-06-01 15:40:40'),
(1, 1, 1, 83, 2, 4, NULL, NULL, NULL, '1', '2016-06-01 15:40:55', '1', '2016-06-01 15:40:55'),
(1, 1, 1, 84, 36, 15, NULL, NULL, NULL, '1', '2016-06-01 15:49:17', '1', '2016-06-01 15:49:17'),
(1, 1, 1, 85, 70, 15, NULL, NULL, NULL, '1', '2016-06-01 15:49:56', '1', '2016-06-01 15:49:56'),
(1, 1, 1, 86, 70, 3, NULL, NULL, NULL, '1', '2016-06-01 15:50:04', '1', '2016-06-01 15:50:04'),
(1, 1, 1, 87, 70, 15, NULL, NULL, NULL, '1', '2016-06-01 15:52:22', '1', '2016-06-01 15:52:22'),
(1, 1, 1, 88, 63, 4, NULL, NULL, NULL, '1', '2016-06-01 15:58:43', '1', '2016-06-01 15:58:43'),
(1, 1, 1, 89, 80, 1, NULL, NULL, NULL, '1', '2016-06-01 16:01:30', '1', '2016-06-01 16:01:30'),
(1, 1, 1, 90, 80, 4, NULL, NULL, NULL, '1', '2016-06-01 16:03:18', '1', '2016-06-01 16:03:18'),
(1, 1, 1, 91, 60, 9, NULL, NULL, NULL, '1', '2016-06-01 16:04:20', '1', '2016-06-01 16:04:20'),
(1, 1, 1, 92, 79, 9, NULL, NULL, NULL, '1', '2016-06-01 16:15:31', '1', '2016-06-01 16:15:31'),
(1, 1, 1, 93, 56, 9, NULL, NULL, NULL, '1', '2016-06-01 16:17:05', '1', '2016-06-01 16:17:05'),
(1, 1, 1, 94, 55, 9, NULL, NULL, NULL, '1', '2016-06-01 16:17:42', '1', '2016-06-01 16:17:42'),
(1, 1, 1, 95, 37, 9, NULL, NULL, NULL, '1', '2016-06-01 16:58:14', '1', '2016-06-01 16:58:14'),
(1, 1, 1, 96, 39, 9, NULL, NULL, NULL, '1', '2016-06-01 17:08:22', '1', '2016-06-01 17:08:22'),
(1, 1, 1, 97, 42, 9, NULL, NULL, NULL, '1', '2016-06-01 17:10:07', '1', '2016-06-01 17:10:07'),
(1, 1, 1, 98, 50, 9, NULL, NULL, NULL, '1', '2016-06-01 17:12:04', '1', '2016-06-01 17:12:04'),
(1, 1, 1, 99, 52, 9, NULL, NULL, NULL, '1', '2016-06-01 17:12:47', '1', '2016-06-01 17:12:47'),
(1, 1, 1, 100, 81, 3, NULL, NULL, NULL, '1', '2016-06-01 17:17:56', '1', '2016-06-01 17:17:56'),
(1, 1, 1, 101, 81, 9, NULL, NULL, NULL, '1', '2016-06-01 17:19:12', '1', '2016-06-01 17:19:12'),
(1, 1, 1, 102, 82, 3, NULL, NULL, NULL, '1', '2016-06-01 17:21:40', '1', '2016-06-01 17:21:40'),
(1, 1, 1, 103, 82, 9, NULL, NULL, NULL, '1', '2016-06-01 17:22:15', '1', '2016-06-01 17:22:15'),
(1, 1, 1, 104, 71, 4, NULL, NULL, 25, '1', '2016-06-01 18:45:26', '1', '2016-06-01 18:45:26'),
(1, 1, 1, 105, 71, 4, NULL, NULL, 26, '1', '2016-06-01 18:58:47', '1', '2016-06-01 18:58:47'),
(1, 1, 1, 106, 83, 1, NULL, NULL, NULL, '1', '2016-06-01 19:50:18', '1', '2016-06-01 19:50:18'),
(1, 1, 1, 107, 84, 1, NULL, NULL, NULL, '1', '2016-06-01 20:27:18', '1', '2016-06-01 20:27:18'),
(1, 1, 1, 108, 85, 1, NULL, NULL, NULL, '1', '2016-06-01 20:46:41', '1', '2016-06-01 20:46:41'),
(1, 1, 1, 109, 86, 3, NULL, NULL, NULL, '1', '2016-06-01 20:51:37', '1', '2016-06-01 20:51:37'),
(1, 1, 1, 110, 48, 3, NULL, NULL, NULL, '1', '2016-06-01 21:12:41', '1', '2016-06-01 21:12:41'),
(1, 1, 1, 111, 48, 15, NULL, NULL, NULL, '1', '2016-06-01 21:15:08', '1', '2016-06-01 21:15:08'),
(1, 1, 1, 112, 87, 14, NULL, NULL, NULL, '1', '2016-06-01 21:16:35', '1', '2016-06-01 21:16:35'),
(1, 1, 1, 113, 87, 3, NULL, NULL, NULL, '1', '2016-06-01 21:17:18', '1', '2016-06-01 21:17:18'),
(1, 1, 1, 114, 48, 3, NULL, NULL, NULL, '1', '2016-06-01 21:41:12', '1', '2016-06-01 21:41:12'),
(1, 1, 1, 115, 86, 15, NULL, NULL, NULL, '1', '2016-06-01 21:41:13', '1', '2016-06-01 21:41:13'),
(1, 1, 1, 116, 86, 4, NULL, NULL, 28, '1', '2016-06-01 21:53:01', '1', '2016-06-01 21:53:01'),
(1, 1, 1, 117, 48, 15, NULL, NULL, NULL, '1', '2016-06-01 21:53:02', '1', '2016-06-01 21:53:02'),
(1, 1, 1, 118, 88, 1, NULL, NULL, NULL, '1', '2016-06-02 01:54:04', '1', '2016-06-02 01:54:04'),
(1, 1, 1, 119, 89, 3, NULL, NULL, NULL, '1', '2016-06-02 02:04:13', '1', '2016-06-02 02:04:13'),
(1, 1, 1, 120, 89, 9, NULL, NULL, NULL, '1', '2016-06-02 02:14:11', '1', '2016-06-02 02:14:11'),
(1, 1, 1, 121, 88, 4, NULL, NULL, NULL, '1', '2016-06-02 02:28:10', '1', '2016-06-02 02:28:10'),
(1, 1, 1, 122, 90, 14, NULL, NULL, NULL, '1', '2016-06-02 02:30:43', '1', '2016-06-02 02:30:43'),
(1, 1, 1, 123, 90, 3, NULL, NULL, NULL, '1', '2016-06-02 02:33:40', '1', '2016-06-02 02:33:40'),
(1, 1, 1, 124, 87, 15, NULL, NULL, NULL, '1', '2016-06-02 02:33:41', '1', '2016-06-02 02:33:41'),
(1, 1, 1, 125, 91, 14, NULL, NULL, NULL, '1', '2016-06-02 02:34:52', '1', '2016-06-02 02:34:52'),
(1, 1, 1, 126, 91, 4, NULL, NULL, 30, '1', '2016-06-02 02:35:11', '1', '2016-06-02 02:35:11'),
(1, 1, 1, 127, 54, 15, NULL, NULL, NULL, '1', '2016-06-02 02:35:12', '1', '2016-06-02 02:35:12'),
(1, 1, 1, 128, 54, 4, NULL, NULL, 31, '1', '2016-06-02 02:36:33', '1', '2016-06-02 02:36:33'),
(1, 1, 1, 129, 53, 15, NULL, NULL, NULL, '1', '2016-06-02 02:37:07', '1', '2016-06-02 02:37:07'),
(1, 1, 1, 130, 92, 14, NULL, NULL, NULL, '1', '2016-06-02 02:38:20', '1', '2016-06-02 02:38:20'),
(1, 1, 1, 131, 83, 3, NULL, NULL, NULL, '1', '2016-06-02 02:38:33', '1', '2016-06-02 02:38:33'),
(1, 1, 1, 132, 93, 3, NULL, NULL, NULL, '1', '2016-06-02 17:06:23', '1', '2016-06-02 17:06:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trlote`
--

CREATE TABLE `trlote` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODLOTE` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trlote`
--

INSERT INTO `trlote` (`CODCLIENTE`, `CODLOTE`, `CODFORNECEDOR`, `CODTIPO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 2, 2, 3, '1', '2016-05-21 16:03:11', '1', '2016-05-21 16:03:11'),
(1, 3, 2, 3, '1', '2016-05-21 16:05:21', '1', '2016-05-21 16:05:21'),
(1, 4, 2, 3, '1', '2016-05-21 16:05:55', '1', '2016-05-21 16:05:55'),
(1, 5, 2, 3, '1', '2016-05-21 16:07:01', '1', '2016-05-21 16:07:01'),
(1, 6, 2, 3, '1', '2016-05-21 16:08:25', '1', '2016-05-21 16:08:25'),
(1, 7, 1, 1, '1', '2016-05-21 16:48:35', '1', '2016-05-21 16:48:35'),
(1, 8, 2, 2, '1', '2016-05-21 17:04:13', '1', '2016-05-21 17:04:13'),
(1, 9, 3, 8, '1', '2016-05-23 00:20:33', '1', '2016-05-23 00:20:33'),
(1, 10, 3, 8, '1', '2016-05-23 00:21:34', '1', '2016-05-23 00:21:34'),
(1, 11, 3, 2, '1', '2016-05-26 01:21:40', '1', '2016-05-26 01:21:40'),
(1, 12, 1, 3, '1', '2016-05-30 14:38:30', '1', '2016-05-30 14:38:30'),
(1, 13, 3, 3, '1', '2016-06-02 01:52:57', '1', '2016-06-02 01:52:57'),
(1, 14, 3, 3, '1', '2016-06-02 01:53:13', '1', '2016-06-02 01:53:13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trproduto`
--

CREATE TABLE `trproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `CODDOACAO` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `ETIQUETA` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trproduto`
--

INSERT INTO `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`, `CODDOACAO`, `CODTRSTATUS`, `CODTIPO`, `ETIQUETA`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 1, 5, 1, 1, 2346, '1', '2016-05-23 12:42:00', '1', '2016-05-23 12:42:00'),
(1, 1, 1, 2, 4, 4, 1, 2345, '1', '2016-05-23 12:42:00', '1', '2016-06-01 15:40:55'),
(1, 1, 1, 26, 6, 3, 8, 55, '1', '2016-05-24 22:44:04', '1', '2016-05-24 22:44:04'),
(1, 1, 1, 27, 6, 3, 8, 22, '1', '2016-05-24 22:47:44', '1', '2016-05-24 22:47:44'),
(1, 1, 1, 28, 6, 3, 8, 25, '1', '2016-05-24 22:59:00', '1', '2016-05-24 22:59:00'),
(1, 1, 1, 29, 6, 3, 8, 26, '1', '2016-05-24 23:01:24', '1', '2016-05-24 23:01:24'),
(1, 1, 1, 30, 6, 3, 8, 27, '1', '2016-05-24 23:03:19', '1', '2016-05-24 23:03:19'),
(1, 1, 1, 31, 6, 3, 8, 134, '1', '2016-05-25 17:47:40', '1', '2016-05-25 17:47:40'),
(1, 1, 1, 32, 6, 3, 8, 136, '1', '2016-05-25 17:53:36', '1', '2016-05-25 17:53:36'),
(1, 1, 1, 33, 6, 3, 8, 4567, '1', '2016-05-25 17:55:28', '1', '2016-05-25 17:55:28'),
(1, 1, 1, 34, 6, 3, 8, 3333, '1', '2016-05-25 17:58:03', '1', '2016-05-25 17:58:03'),
(1, 1, 1, 36, 3, 15, 2, 5678, '1', '2016-05-25 18:05:49', '1', '2016-06-01 15:49:17'),
(1, 1, 1, 37, 3, 9, 2, 3456789, '1', '2016-05-25 18:08:09', '1', '2016-06-01 16:58:14'),
(1, 1, 1, 39, 3, 9, 2, 1234, '1', '2016-05-25 18:19:33', '1', '2016-06-01 17:08:22'),
(1, 1, 1, 40, 6, 3, 8, 54321, '1', '2016-05-25 18:21:35', '1', '2016-05-25 18:21:35'),
(1, 1, 1, 41, 1, 15, 3, 99, '1', '2016-05-25 20:42:31', '1', '2016-05-30 22:26:33'),
(1, 1, 1, 42, 3, 9, 2, 991, '1', '2016-05-25 20:53:49', '1', '2016-06-01 17:10:07'),
(1, 1, 1, 43, 4, 3, 3, NULL, '1', '2016-05-25 20:58:41', '1', '2016-05-25 20:58:41'),
(1, 1, 1, 44, 4, 3, 3, NULL, '1', '2016-05-25 20:59:38', '1', '2016-05-25 20:59:38'),
(1, 1, 1, 45, 4, 14, 3, NULL, '1', '2016-05-25 21:39:11', '1', '2016-05-25 21:39:11'),
(1, 1, 1, 46, 4, 14, 3, NULL, '1', '2016-05-25 21:40:06', '1', '2016-05-25 21:40:06'),
(1, 1, 1, 47, 5, 14, 3, NULL, '1', '2016-05-25 21:55:54', '1', '2016-05-25 21:55:54'),
(1, 1, 1, 48, 5, 15, 3, 12345686, '1', '2016-05-25 21:59:43', '1', '2016-06-01 21:53:02'),
(1, 1, 1, 49, 1, 4, 3, 4563, '1', '2016-05-25 22:01:45', '1', '2016-05-30 22:26:32'),
(1, 1, 1, 50, 7, 9, 2, 5674, '1', '2016-05-26 01:35:13', '1', '2016-06-01 17:12:04'),
(1, 1, 1, 51, 5, 4, 2, NULL, '1', '2016-05-26 01:39:02', '1', '2016-05-30 21:12:29'),
(1, 1, 1, 52, 3, 9, 2, 34567, '1', '2016-05-27 14:33:38', '1', '2016-06-01 17:12:47'),
(1, 1, 1, 53, 3, 15, 2, 1234567, '1', '2016-05-27 14:39:56', '1', '2016-06-02 02:37:07'),
(1, 1, 1, 54, 3, 4, 2, 2234, '1', '2016-05-27 14:40:27', '1', '2016-06-02 02:36:33'),
(1, 1, 1, 55, 3, 9, 2, 22345, '1', '2016-05-27 14:40:57', '1', '2016-06-01 16:17:42'),
(1, 1, 1, 56, 3, 9, 2, 22346, '1', '2016-05-27 14:41:35', '1', '2016-06-01 16:17:05'),
(1, 1, 1, 57, 4, 14, 3, NULL, '1', '2016-05-28 00:15:32', '1', '2016-05-28 00:15:32'),
(1, 1, 1, 58, 4, 4, 3, NULL, '1', '2016-05-28 00:16:36', '1', '2016-05-28 18:25:19'),
(1, 1, 1, 59, 4, 4, 2, NULL, '1', '2016-05-28 02:30:16', '1', '2016-05-29 18:37:26'),
(1, 1, 1, 60, 3, 9, 2, 34562, '1', '2016-05-28 16:48:17', '1', '2016-06-01 16:04:20'),
(1, 1, 1, 61, 10, 1, 1, 87654, '1', '2016-05-30 14:29:41', '1', '2016-05-30 14:29:41'),
(1, 1, 1, 62, 11, 3, 1, 87655, '1', '2016-05-30 14:33:17', '1', '2016-05-31 18:54:50'),
(1, 1, 1, 63, 12, 4, 1, 87656, '1', '2016-05-30 14:33:41', '1', '2016-06-01 15:58:43'),
(1, 1, 1, 64, 13, 3, 1, 87657, '1', '2016-05-30 14:35:39', '1', '2016-06-01 15:28:30'),
(1, 1, 1, 65, 14, 4, 1, 87658, '1', '2016-05-30 14:38:12', '1', '2016-06-01 04:05:10'),
(1, 1, 1, 66, 10, 14, 3, NULL, '1', '2016-05-30 21:24:45', '1', '2016-05-30 21:24:45'),
(1, 1, 1, 67, 10, 14, 2, NULL, '1', '2016-05-30 21:35:01', '1', '2016-05-30 21:35:01'),
(1, 1, 1, 68, 1, 4, 3, 66789, '1', '2016-05-30 22:32:36', '1', '2016-05-30 23:10:11'),
(1, 1, 1, 69, 1, 4, 3, 56783, '1', '2016-05-30 22:44:19', '1', '2016-06-01 15:30:41'),
(1, 1, 1, 70, 14, 15, 3, 5432, '1', '2016-05-31 18:44:44', '1', '2016-06-01 15:52:22'),
(1, 1, 1, 71, 14, 4, 2, 54322, '1', '2016-05-31 18:46:57', '1', '2016-06-01 18:58:46'),
(1, 1, 1, 72, 14, 3, 5, 54323, '1', '2016-05-31 18:47:21', '1', '2016-06-01 04:04:25'),
(1, 1, 1, 73, 13, 4, 3, NULL, '1', '2016-05-31 18:50:59', '1', '2016-06-01 15:28:31'),
(1, 1, 1, 74, 13, 4, 2, NULL, '1', '2016-05-31 18:51:20', '1', '2016-06-01 15:29:38'),
(1, 1, 1, 75, 13, 14, 5, NULL, '1', '2016-05-31 18:51:37', '1', '2016-05-31 18:51:37'),
(1, 1, 1, 76, 11, 14, 3, NULL, '1', '2016-05-31 18:54:05', '1', '2016-05-31 18:54:05'),
(1, 1, 1, 77, 11, 14, 2, NULL, '1', '2016-05-31 18:54:19', '1', '2016-05-31 18:54:19'),
(1, 1, 1, 78, 11, 14, 5, NULL, '1', '2016-05-31 18:54:35', '1', '2016-05-31 18:54:35'),
(1, 1, 1, 79, 3, 9, 2, 56789, '1', '2016-06-01 14:08:55', '1', '2016-06-01 16:15:31'),
(1, 1, 1, 80, 17, 4, 1, 56780, '1', '2016-06-01 16:01:30', '1', '2016-06-01 16:03:18'),
(1, 1, 1, 81, 3, 9, 2, 12345678, '1', '2016-06-01 17:17:55', '1', '2016-06-01 17:19:12'),
(1, 1, 1, 82, 3, 9, 2, 12345679, '1', '2016-06-01 17:21:40', '1', '2016-06-01 17:22:14'),
(1, 1, 1, 83, 18, 3, 1, 12345680, '1', '2016-06-01 19:50:18', '1', '2016-06-02 02:38:33'),
(1, 1, 1, 84, 19, 1, 1, 12345681, '1', '2016-06-01 20:27:18', '1', '2016-06-01 20:27:18'),
(1, 1, 1, 85, 21, 1, 1, 12345682, '1', '2016-06-01 20:46:41', '1', '2016-06-01 20:46:41'),
(1, 1, 1, 86, 15, 4, 3, 12345683, '1', '2016-06-01 20:51:36', '1', '2016-06-01 21:53:01'),
(1, 1, 1, 87, 21, 15, 3, 12345685, '1', '2016-06-01 21:16:35', '1', '2016-06-02 02:33:41'),
(1, 1, 1, 88, 24, 4, 1, 12345688, '1', '2016-06-02 01:54:03', '1', '2016-06-02 02:28:10'),
(1, 1, 1, 89, 15, 9, 3, 12345689, '1', '2016-06-02 02:04:12', '1', '2016-06-02 02:14:11'),
(1, 1, 1, 90, 18, 3, 3, 12345691, '1', '2016-06-02 02:30:43', '1', '2016-06-02 02:33:40'),
(1, 1, 1, 91, 18, 4, 2, NULL, '1', '2016-06-02 02:34:51', '1', '2016-06-02 02:35:11'),
(1, 1, 1, 92, 18, 14, 5, NULL, '1', '2016-06-02 02:38:20', '1', '2016-06-02 02:38:20'),
(1, 1, 1, 93, 3, 3, 2, 12345692, '1', '2016-06-02 17:06:23', '1', '2016-06-02 17:06:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trstatus`
--

CREATE TABLE `trstatus` (
  `CODTRSTATUS` int(11) NOT NULL,
  `DESCRICAO` varchar(80) NOT NULL,
  `TIPO` int(11) DEFAULT NULL,
  `DESCTIPO` varchar(45) DEFAULT NULL,
  `UTILIZADOCODIGO` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trstatus`
--

INSERT INTO `trstatus` (`CODTRSTATUS`, `DESCRICAO`, `TIPO`, `DESCTIPO`, `UTILIZADOCODIGO`) VALUES
(1, 'Aguardando Triagem', 1, 'Triagem', 'Sim'),
(2, 'Triagem Finalizada', 1, 'Triagem', 'Sim'),
(3, 'Estoque', 2, 'Estoque - Sistema', 'Sim'),
(4, 'Sucata', 5, 'Sucata - Sistema', 'Sim'),
(5, 'Doação Externa', 0, NULL, NULL),
(6, 'Doação Interna', 0, NULL, NULL),
(7, 'Equipamento Alocado', 0, NULL, NULL),
(8, 'Produto Alocado', 0, NULL, 'Sim'),
(9, 'Enviado para Venda', 0, NULL, 'Sim'),
(10, 'Vendido', 0, NULL, NULL),
(11, 'Com Defeito', 0, NULL, NULL),
(12, 'Funcionando', 0, NULL, NULL),
(13, 'Em Triagem', 1, 'Triagem', 'Sim'),
(14, 'Cadastro Componente Produto', 5, 'Triagem - Sistema', 'Sim'),
(15, 'Componente vinculado ao produto', 5, 'Triagem - Sistema', 'Sim');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trsucata`
--

CREATE TABLE `trsucata` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODSUCATA` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL,
  `CODDOACAO` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `CODPRODUTO` int(11) DEFAULT NULL,
  `CODPRODUTOPAI` int(11) DEFAULT NULL,
  `REGCRIADOPOR` int(11) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` int(11) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trsucata`
--

INSERT INTO `trsucata` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODSUCATA`, `CODTRSTATUS`, `CODDOACAO`, `CODTIPO`, `CODPRODUTO`, `CODPRODUTOPAI`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 1, 1, 4, 7, 2, NULL, NULL, 1, '2016-05-26 20:58:28', 1, '2016-05-26 20:58:28'),
(1, 1, 1, 2, 4, 7, 2, NULL, NULL, 1, '2016-05-26 20:59:21', 1, '2016-05-26 20:59:21'),
(1, 1, 1, 3, 4, 7, 2, NULL, NULL, 1, '2016-05-27 13:45:16', 1, '2016-05-27 13:45:16'),
(1, 1, 1, 4, 4, 7, 2, NULL, NULL, 1, '2016-05-27 13:45:57', 1, '2016-05-27 13:45:57'),
(1, 1, 1, 5, 4, 3, 2, NULL, NULL, 1, '2016-05-28 16:58:41', 1, '2016-05-28 16:58:41'),
(1, 1, 1, 8, 4, 4, 3, 58, 2, 1, '2016-05-28 18:09:54', 1, '2016-05-28 18:09:54'),
(1, 1, 1, 12, 4, 4, 2, 59, 2, 1, '2016-05-29 18:37:26', 1, '2016-05-29 18:37:26'),
(1, 1, 1, 13, 4, 5, 2, 51, 1, 1, '2016-05-30 21:12:29', 1, '2016-05-30 21:12:29'),
(1, 1, 1, 14, 4, 1, 3, 66, NULL, 1, '2016-05-30 22:07:30', 1, '2016-05-30 22:07:30'),
(1, 1, 1, 17, 4, 1, 3, 49, NULL, 1, '2016-05-30 22:26:31', 1, '2016-05-30 22:26:31'),
(1, 1, 1, 18, 4, 1, 3, NULL, NULL, 1, '2016-05-30 22:32:23', 1, '2016-05-30 22:32:23'),
(1, 1, 1, 19, 4, 1, 3, NULL, NULL, 1, '2016-05-30 22:33:32', 1, '2016-05-30 22:33:32'),
(1, 1, 1, 20, 4, 1, 3, NULL, NULL, 1, '2016-05-30 22:34:49', 1, '2016-05-30 22:34:49'),
(1, 1, 1, 21, 4, 1, 3, 68, NULL, 1, '2016-05-30 23:10:11', 1, '2016-05-30 23:10:11'),
(1, 1, 1, 22, 4, 13, 3, 73, NULL, 1, '2016-06-01 15:28:31', 1, '2016-06-01 15:28:31'),
(1, 1, 1, 23, 4, 13, 2, 74, NULL, 1, '2016-06-01 15:29:37', 1, '2016-06-01 15:29:37'),
(1, 1, 1, 24, 4, 4, 3, 69, NULL, 1, '2016-06-01 15:30:41', 1, '2016-06-01 15:30:41'),
(1, 1, 1, 26, 4, 14, 2, NULL, NULL, 1, '2016-06-01 18:58:46', 1, '2016-06-01 18:58:46'),
(1, 1, 1, 27, 4, 3, 2, NULL, NULL, 1, '2016-06-01 19:40:06', 1, '2016-06-01 19:40:06'),
(1, 1, 1, 28, 4, 5, 3, 86, NULL, 1, '2016-06-01 21:53:00', 1, '2016-06-01 21:53:00'),
(1, 1, 1, 29, 4, 15, 3, NULL, NULL, 1, '2016-06-02 02:08:13', 1, '2016-06-02 02:08:13'),
(1, 1, 1, 30, 4, 3, 2, 91, NULL, 1, '2016-06-02 02:35:11', 1, '2016-06-02 02:35:11'),
(1, 1, 1, 31, 4, 18, 2, 54, NULL, 1, '2016-06-02 02:36:32', 1, '2016-06-02 02:36:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trtipo`
--

CREATE TABLE `trtipo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `TIPOENTRADA` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Componente\n2 = Equipamento',
  `COMPONENTEPRODUTO` int(11) NOT NULL DEFAULT '0' COMMENT '1 = é um componentes que forma um produto\n0 = não',
  `COMPONENTELOCACAO` int(11) NOT NULL DEFAULT '0' COMMENT '1 = é um componente que forma uma locação\n0 = não',
  `PRODUTO` int(11) NOT NULL COMMENT '1 = Vira o pai de um produto\n0 = Não',
  `DESCRICAO` varchar(100) NOT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trtipo`
--

INSERT INTO `trtipo` (`CODCLIENTE`, `CODTIPO`, `TIPOENTRADA`, `COMPONENTEPRODUTO`, `COMPONENTELOCACAO`, `PRODUTO`, `DESCRICAO`, `ATIVO`, `REGCRIADOPOR`, `REGCRIADOEM`, `REGMODIFPOR`, `REGMODIFEM`) VALUES
(1, 1, 2, 0, 0, 1, 'Gabinete', 1, '1', '2016-05-12 21:39:58', '1', '2016-05-26 18:49:55'),
(1, 2, 1, 1, 0, 0, 'HD', 1, '1', '2016-05-12 21:50:17', '1', '2016-05-12 21:50:17'),
(1, 3, 1, 1, 0, 0, 'Memória', 1, '1', '2016-05-12 22:17:58', '1', '2016-05-12 22:17:58'),
(1, 4, 1, 1, 0, 0, 'Fonte', 1, '1', '2016-05-12 22:18:10', '1', '2016-05-12 22:18:10'),
(1, 5, 1, 1, 0, 0, 'Placa-Mãe', 1, '1', '2016-05-12 22:18:19', '1', '2016-05-12 22:18:19'),
(1, 6, 2, 0, 0, 1, 'Impressora', 1, '1', '2016-05-12 22:22:22', '1', '2016-05-12 22:22:22'),
(1, 7, 2, 0, 1, 0, 'Monitor', 1, '1', '2016-05-12 22:22:34', '1', '2016-05-12 22:22:34'),
(1, 8, 1, 0, 1, 0, 'Mouse', 1, '1', '2016-05-12 22:24:47', '1', '2016-05-12 22:24:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trtipoentrada`
--

CREATE TABLE `trtipoentrada` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODTIPOENTRADA` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `trtipoentrada`
--

INSERT INTO `trtipoentrada` (`CODCLIENTE`, `CODTIPOENTRADA`, `DESCRICAO`) VALUES
(1, 1, 'Componente'),
(1, 2, 'Equipamento');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `fiacomplancamento`
--
ALTER TABLE `fiacomplancamento`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODACOMPLANCAMENTO`),
  ADD KEY `FK_FIACOMPLANCAMENTO_FILANCAMENTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`);

--
-- Indexes for table `fiboleto`
--
ALTER TABLE `fiboleto`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODBOLETO`);

--
-- Indexes for table `ficartaocredito`
--
ALTER TABLE `ficartaocredito`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCARTAOCREDITO`);

--
-- Indexes for table `ficategoria`
--
ALTER TABLE `ficategoria`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODCATEGORIA`);

--
-- Indexes for table `ficontacorrente`
--
ALTER TABLE `ficontacorrente`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  ADD KEY `FK_FICONTACORRENTE_GLBANCO_idx` (`CODCLIENTE`,`CODBANCO`);

--
-- Indexes for table `fifaturacartao`
--
ALTER TABLE `fifaturacartao`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODFATURACARTAO`),
  ADD KEY `FK_FIFATURACARTAO_FICARTAOCREDITO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCARTAOCREDITO`),
  ADD KEY `FK_FIFATURACARTAO_FICONTACORRENTE_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  ADD KEY `FK_FIFATURACARTAO_FIFORMAPAGAMENTO_idx` (`CODCLIENTE`,`CODPAGAMENTO`);

--
-- Indexes for table `fiformadepagamento`
--
ALTER TABLE `fiformadepagamento`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODPAGAMENTO`);

--
-- Indexes for table `fifornecedor`
--
ALTER TABLE `fifornecedor`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODFORNECEDOR`),
  ADD KEY `FK_FIFORNECEDOR_FICONVECAO2_idx` (`CODCLIENTE`),
  ADD KEY `FK_FIFORNECEDOR_GLMUNICIPIO_idx` (`CODMUNICIPIO`);

--
-- Indexes for table `filanboleto`
--
ALTER TABLE `filanboleto`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  ADD KEY `FK_FILANBOLETO_FIBOLETO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODBOLETO`);

--
-- Indexes for table `filancamento`
--
ALTER TABLE `filancamento`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  ADD KEY `FK_FILANCAMENTO_FIRECORRENTELAN_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODRECORRENTELAN`),
  ADD KEY `FK_FILANCAMENTO_FICATEGORIA_idx` (`CODCLIENTE`,`CODCATEGORIA`),
  ADD KEY `FK_FILANCAMENTO_FIFORMADEPAGAMENTO_idx` (`CODCLIENTE`,`CODPAGAMENTO`),
  ADD KEY `FK_FILANCAMENTO_FICONTACORRENTE_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  ADD KEY `FK_FILANCAMENTO_FITIPOLANCAMENTO_idx` (`CODTIPOLANCAMENTO`),
  ADD KEY `FK_FILANCAMENTO_FISTATUS_idx` (`CODFISTATUS`),
  ADD KEY `FK_FILANCAMENTO_TRPRODUTO_idx` (`CODPRODUTO`,`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`),
  ADD KEY `FK_FILANCAMENTO_TRPRODUTO` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`);

--
-- Indexes for table `filanfaturacartao`
--
ALTER TABLE `filanfaturacartao`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  ADD KEY `FK_FILANFATURACARTAO_FIFATURACARTAO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODFATURACARTAO`),
  ADD KEY `FK_FILANFATURACARTAO_FILANCAMENTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`);

--
-- Indexes for table `firecorrentelan`
--
ALTER TABLE `firecorrentelan`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODRECORRENTELAN`),
  ADD KEY `FK_FIRECORRENTELAN_FITIPOLANCAMENTO_idx` (`CODTIPOLANCAMENTO`),
  ADD KEY `FK_FIRECORRENTELAN_FICATEGORIA_idx` (`CODCLIENTE`,`CODCATEGORIA`),
  ADD KEY `FK_FIRECORRENTELAN_FICONTACORRENTE_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  ADD KEY `FK_FIRECORRENTELAN_FIFORMAPAGAMENTO_idx` (`CODCLIENTE`,`CODPAGAMENTO`);

--
-- Indexes for table `fistatus`
--
ALTER TABLE `fistatus`
  ADD PRIMARY KEY (`CODFISTATUS`);

--
-- Indexes for table `fitipolancamento`
--
ALTER TABLE `fitipolancamento`
  ADD PRIMARY KEY (`CODTIPOLANCAMENTO`);

--
-- Indexes for table `glativo`
--
ALTER TABLE `glativo`
  ADD PRIMARY KEY (`CODATIVO`);

--
-- Indexes for table `glautoindice`
--
ALTER TABLE `glautoindice`
  ADD PRIMARY KEY (`CODINDICE`),
  ADD KEY `FK_GLAUTOINDICE_GLEMPRESA_idx` (`CODCLIENTE`,`CODEMPRESA`),
  ADD KEY `FK_GLAUTOINDICE_GLFILIAL_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`);

--
-- Indexes for table `glbanco`
--
ALTER TABLE `glbanco`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODBANCO`),
  ADD KEY `FK_GBANCO_GCLIENTE_idx` (`CODCLIENTE`);

--
-- Indexes for table `glcliente`
--
ALTER TABLE `glcliente`
  ADD PRIMARY KEY (`CODCLIENTE`);

--
-- Indexes for table `glempresa`
--
ALTER TABLE `glempresa`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`);

--
-- Indexes for table `glestado`
--
ALTER TABLE `glestado`
  ADD PRIMARY KEY (`CODESTADO`);

--
-- Indexes for table `glfilial`
--
ALTER TABLE `glfilial`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`);

--
-- Indexes for table `glgrupo`
--
ALTER TABLE `glgrupo`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`);

--
-- Indexes for table `glgrupolink`
--
ALTER TABLE `glgrupolink`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`,`CODLINK`,`CODGRUPOLINK`,`CODFILIAL`),
  ADD KEY `FK_GLGRUPOLINK_GLLINK_idx` (`CODLINK`);

--
-- Indexes for table `glgrupousuario`
--
ALTER TABLE `glgrupousuario`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPOUSUARIO`,`CODGRUPO`,`CODUSUARIO`),
  ADD KEY `FK_GLGRUPOUSUARIO_GLUSUARIO_idx` (`CODUSUARIO`),
  ADD KEY `FK_GLGRUPOUSUARIO_GLGRUPO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`);

--
-- Indexes for table `glhelpsystem`
--
ALTER TABLE `glhelpsystem`
  ADD PRIMARY KEY (`CODHELPSYSTEM`),
  ADD KEY `FK_GLLINK_GLHELPSYSTEM_idx` (`CODLINK`);

--
-- Indexes for table `gllink`
--
ALTER TABLE `gllink`
  ADD PRIMARY KEY (`CODLINK`);

--
-- Indexes for table `glmunicipio`
--
ALTER TABLE `glmunicipio`
  ADD PRIMARY KEY (`CODMUNICIPIO`),
  ADD KEY `FK_MUNICIPIO_ESTADO_IDX` (`CODESTADO`);

--
-- Indexes for table `glusuario`
--
ALTER TABLE `glusuario`
  ADD PRIMARY KEY (`CODUSUARIO`),
  ADD UNIQUE KEY `INDEX_GLUSUARIO` (`EMAIL`),
  ADD KEY `idx_glusuario_CODUSUARIO_CODCLIENTE` (`CODUSUARIO`);

--
-- Indexes for table `glusuariofilial`
--
ALTER TABLE `glusuariofilial`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODUSUARIOFILIAL`),
  ADD KEY `FK_GLUSUARIOFILIAL_FK_GLUSUARIO_idx` (`CODUSUARIO`),
  ADD KEY `FK_GLUSUARIOFILIAL_GLEMPRESA_idx` (`CODCLIENTE`,`CODEMPRESA`);

--
-- Indexes for table `trcamposproduto`
--
ALTER TABLE `trcamposproduto`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCAMPOSPRODUTO`),
  ADD KEY `FK_TRCAMPOSPRODUTO_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  ADD KEY `FK_TRCAMPOSPRODUTO_TRCAMPOSTIPO_idx` (`CODCLIENTE`,`CODCAMPOSTIPO`);

--
-- Indexes for table `trcampostipo`
--
ALTER TABLE `trcampostipo`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODCAMPOSTIPO`),
  ADD KEY `FK_TRCAMPOSTIPO_TRTIPO_idx` (`CODCLIENTE`,`CODTIPO`);

--
-- Indexes for table `trcomponentesdalocacao`
--
ALTER TABLE `trcomponentesdalocacao`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`);

--
-- Indexes for table `trcomponenteslocacao`
--
ALTER TABLE `trcomponenteslocacao`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODCOMPONENTESLOCACAO`),
  ADD KEY `FK_TRCOMPONENTESLOCACAO_TRTIPO_idx` (`CODCLIENTE`,`CODTIPOPAI`),
  ADD KEY `FK_TRCOMPOENTESLOCACAO_TRTIPOFILHO_idx` (`CODCLIENTE`,`CODTIPOFILHO`);

--
-- Indexes for table `trcomponentesproduto`
--
ALTER TABLE `trcomponentesproduto`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCOMPONENTESPRODUTO`),
  ADD KEY `FK_COMPONENTESEQUIPAMENTO_EQUIPAMENTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOPAI`),
  ADD KEY `FK_COMPONENTESPRODUTO_COMPONENTESTIPO_idx` (`CODCLIENTE`,`CODCOMPONENTESTIPO`),
  ADD KEY `FK_COMPONENTESPRODUTO_PRODUTO_FILHO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOFILHO`);

--
-- Indexes for table `trcomponentestipo`
--
ALTER TABLE `trcomponentestipo`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODCOMPONENTESTIPO`),
  ADD KEY `FK_TRCOMPONENTESTIPO_idx` (`CODCLIENTE`,`CODTIPOPAI`),
  ADD KEY `FK_TRCOMPONENTESTIPO_TRTIPOFILHO_idx` (`CODCLIENTE`,`CODTIPOFILHO`);

--
-- Indexes for table `trdoacao`
--
ALTER TABLE `trdoacao`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`),
  ADD UNIQUE KEY `INX_TRDOACAO_ETIQUETA` (`ETIQUETA`,`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`),
  ADD KEY `FK_TRDOACAO_FIFORNECEDOR` (`CODCLIENTE`,`CODFORNECEDOR`),
  ADD KEY `FK_TRDOACAO_TRTIPO` (`CODCLIENTE`,`CODTIPO`),
  ADD KEY `FK_TRDOACAO_TRLOTE` (`CODCLIENTE`,`CODLOTE`),
  ADD KEY `FK_TRDOACAO_TRSTATUS_idx` (`CODTRSTATUS`);

--
-- Indexes for table `trlocacao`
--
ALTER TABLE `trlocacao`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLOCACAO`),
  ADD KEY `FK_TRLOCACAO_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  ADD KEY `FK_TRLOCACAO_TRCOMPONENTESLOCACAO_idx` (`CODCLIENTE`,`CODCOMPONENTESLOCACAO`),
  ADD KEY `FK_TRLOCACAO_TRFORNECEDOR_idx` (`CODCLIENTE`,`CODFORNECEDOR`),
  ADD KEY `FK_TRLOCACAO_GLFILIALLOCACAO_idx` (`CODCLIENTELOCADO`,`CODEMPRESALOCADO`,`CODFILIALLOCADO`);

--
-- Indexes for table `trlogcomponentesproduto`
--
ALTER TABLE `trlogcomponentesproduto`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLOGCOMPONENTESPRODUTO`),
  ADD KEY `FK_TRLOGCOMPONENTESPRODUTO_TRCOMPONENTESPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCOMPONENTESPRODUTO`),
  ADD KEY `FK_TRLOGCOMPONENTESPRODUTO_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOFILHO`);

--
-- Indexes for table `trlogproduto`
--
ALTER TABLE `trlogproduto`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLOGPRODUTO`),
  ADD KEY `FK_TRLOGPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  ADD KEY `FK_TRLOGPRODUTO_TRSTATUS_idx` (`CODTRSTATUS`);

--
-- Indexes for table `trlote`
--
ALTER TABLE `trlote`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODLOTE`),
  ADD KEY `FK_TRLOTE_FIFORNECEDOR` (`CODCLIENTE`,`CODFORNECEDOR`),
  ADD KEY `FK_TRLOTE_TRTIPO` (`CODCLIENTE`,`CODTIPO`);

--
-- Indexes for table `trproduto`
--
ALTER TABLE `trproduto`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  ADD UNIQUE KEY `INDEX_TRPRODUTO_ETIQUETA` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`ETIQUETA`),
  ADD KEY `FK_TRPRODUTO_TRDOACAO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`),
  ADD KEY `FK_TRPRODUTO_TRSTATUS_idx` (`CODTRSTATUS`),
  ADD KEY `FK_TRPRODUTO_TRTIPO_idx` (`CODCLIENTE`,`CODTIPO`);

--
-- Indexes for table `trstatus`
--
ALTER TABLE `trstatus`
  ADD PRIMARY KEY (`CODTRSTATUS`);

--
-- Indexes for table `trsucata`
--
ALTER TABLE `trsucata`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODSUCATA`),
  ADD UNIQUE KEY `IDX_TRSUCATA` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  ADD KEY `FK_TRSUCATA_TRSTATUS_idx` (`CODTRSTATUS`),
  ADD KEY `FK_TRSUCATA_TRDOACAO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`),
  ADD KEY `FK_TRSUCATA_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  ADD KEY `FK_TRSUCATA_TRPRODUTOPAI_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOPAI`),
  ADD KEY `FK_TRSUCATA_TRTIPO_idx` (`CODCLIENTE`,`CODTIPO`);

--
-- Indexes for table `trtipo`
--
ALTER TABLE `trtipo`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODTIPO`);

--
-- Indexes for table `trtipoentrada`
--
ALTER TABLE `trtipoentrada`
  ADD PRIMARY KEY (`CODCLIENTE`,`CODTIPOENTRADA`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fitipolancamento`
--
ALTER TABLE `fitipolancamento`
  MODIFY `CODTIPOLANCAMENTO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `glautoindice`
--
ALTER TABLE `glautoindice`
  MODIFY `CODINDICE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `glcliente`
--
ALTER TABLE `glcliente`
  MODIFY `CODCLIENTE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `glestado`
--
ALTER TABLE `glestado`
  MODIFY `CODESTADO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `glhelpsystem`
--
ALTER TABLE `glhelpsystem`
  MODIFY `CODHELPSYSTEM` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gllink`
--
ALTER TABLE `gllink`
  MODIFY `CODLINK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `glmunicipio`
--
ALTER TABLE `glmunicipio`
  MODIFY `CODMUNICIPIO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `glusuario`
--
ALTER TABLE `glusuario`
  MODIFY `CODUSUARIO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `trstatus`
--
ALTER TABLE `trstatus`
  MODIFY `CODTRSTATUS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `ficartaocredito`
--
ALTER TABLE `ficartaocredito`
  ADD CONSTRAINT `FK_FICARTAOCREDITO_GLFILIAL` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `ficategoria`
--
ALTER TABLE `ficategoria`
  ADD CONSTRAINT `FK_FICATEGORIA_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `ficontacorrente`
--
ALTER TABLE `ficontacorrente`
  ADD CONSTRAINT `FK_FICONTACORRENTE_GLBANCO` FOREIGN KEY (`CODCLIENTE`,`CODBANCO`) REFERENCES `glbanco` (`CODCLIENTE`, `CODBANCO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FICONTACORRENTE_GLFILIAL` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `fifaturacartao`
--
ALTER TABLE `fifaturacartao`
  ADD CONSTRAINT `FK_FIFATURACARTAO_FICARTAOCREDITO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCARTAOCREDITO`) REFERENCES `ficartaocredito` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCARTAOCREDITO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FIFATURACARTAO_FICONTACORRENTE` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`) REFERENCES `ficontacorrente` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FIFATURACARTAO_FIFORMAPAGAMENTO` FOREIGN KEY (`CODCLIENTE`,`CODPAGAMENTO`) REFERENCES `fiformadepagamento` (`CODCLIENTE`, `CODPAGAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `fiformadepagamento`
--
ALTER TABLE `fiformadepagamento`
  ADD CONSTRAINT `FK_FIFORMADEPAGAMENTO_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `fifornecedor`
--
ALTER TABLE `fifornecedor`
  ADD CONSTRAINT `FK_FIFORNECEDOR_GLMUNICIPIO` FOREIGN KEY (`CODMUNICIPIO`) REFERENCES `glmunicipio` (`CODMUNICIPIO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `filanboleto`
--
ALTER TABLE `filanboleto`
  ADD CONSTRAINT `FK_FILANBOLETO_FIBOLETO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODBOLETO`) REFERENCES `fiboleto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODBOLETO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FILANBOLETO_FILANCAMENTO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`) REFERENCES `filancamento` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `filancamento`
--
ALTER TABLE `filancamento`
  ADD CONSTRAINT `FK_FILANCAMENTO_FICATEGORIA` FOREIGN KEY (`CODCLIENTE`,`CODCATEGORIA`) REFERENCES `ficategoria` (`CODCLIENTE`, `CODCATEGORIA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FILANCAMENTO_FICONTACORRENTE` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`) REFERENCES `ficontacorrente` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FILANCAMENTO_FIFORMADEPAGAMENTO` FOREIGN KEY (`CODCLIENTE`,`CODPAGAMENTO`) REFERENCES `fiformadepagamento` (`CODCLIENTE`, `CODPAGAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FILANCAMENTO_FIRECORRENTELAN` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODRECORRENTELAN`) REFERENCES `firecorrentelan` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODRECORRENTELAN`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FILANCAMENTO_FISTATUS` FOREIGN KEY (`CODFISTATUS`) REFERENCES `fistatus` (`CODFISTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FILANCAMENTO_FITIPOLANCAMENTO` FOREIGN KEY (`CODTIPOLANCAMENTO`) REFERENCES `fitipolancamento` (`CODTIPOLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FILANCAMENTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `filanfaturacartao`
--
ALTER TABLE `filanfaturacartao`
  ADD CONSTRAINT `FK_FILANFATURACARTAO_FIFATURACARTAO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODFATURACARTAO`) REFERENCES `fifaturacartao` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODFATURACARTAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FILANFATURACARTAO_FILANCAMENTO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`) REFERENCES `filancamento` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `firecorrentelan`
--
ALTER TABLE `firecorrentelan`
  ADD CONSTRAINT `FK_FIRECORRENTELAN_FICATEGORIA` FOREIGN KEY (`CODCLIENTE`,`CODCATEGORIA`) REFERENCES `ficategoria` (`CODCLIENTE`, `CODCATEGORIA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FIRECORRENTELAN_FICONTACORRENTE` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`) REFERENCES `ficontacorrente` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FIRECORRENTELAN_FIFORMAPAGAMENTO` FOREIGN KEY (`CODCLIENTE`,`CODPAGAMENTO`) REFERENCES `fiformadepagamento` (`CODCLIENTE`, `CODPAGAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FIRECORRENTELAN_FITIPOLANCAMENTO` FOREIGN KEY (`CODTIPOLANCAMENTO`) REFERENCES `fitipolancamento` (`CODTIPOLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_FIRECORRENTELAN_GLFILIAL` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `glautoindice`
--
ALTER TABLE `glautoindice`
  ADD CONSTRAINT `FK_GLAUTOINDICE_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_GLAUTOINDICE_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_GLAUTOINDICE_GLFILIAL` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `glbanco`
--
ALTER TABLE `glbanco`
  ADD CONSTRAINT `FK_GLBANCO_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `glempresa`
--
ALTER TABLE `glempresa`
  ADD CONSTRAINT `FK_GLEMPRESA_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `glfilial`
--
ALTER TABLE `glfilial`
  ADD CONSTRAINT `FK_GLFILIAL_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `glgrupolink`
--
ALTER TABLE `glgrupolink`
  ADD CONSTRAINT `FK_GLGRUPOLINK_GLGRUPO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`) REFERENCES `glgrupo` (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_GLGRUPOLINK_GLLINK` FOREIGN KEY (`CODLINK`) REFERENCES `gllink` (`CODLINK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `glgrupousuario`
--
ALTER TABLE `glgrupousuario`
  ADD CONSTRAINT `FK_GLGRUPOUSUARIO_GLGRUPO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`) REFERENCES `glgrupo` (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_GLGRUPOUSUARIO_GLUSUARIO` FOREIGN KEY (`CODUSUARIO`) REFERENCES `glusuario` (`CODUSUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `glhelpsystem`
--
ALTER TABLE `glhelpsystem`
  ADD CONSTRAINT `FK_GLLINK_GLHELPSYSTEM` FOREIGN KEY (`CODLINK`) REFERENCES `gllink` (`CODLINK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `glmunicipio`
--
ALTER TABLE `glmunicipio`
  ADD CONSTRAINT `FK_GLMUNICIPIO_GLESTADO` FOREIGN KEY (`CODESTADO`) REFERENCES `glestado` (`CODESTADO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `glusuariofilial`
--
ALTER TABLE `glusuariofilial`
  ADD CONSTRAINT `FK_GLUSUARIOFILIAL_FK_GLUSUARIO` FOREIGN KEY (`CODUSUARIO`) REFERENCES `glusuario` (`CODUSUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_GLUSUARIOFILIAL_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trcamposproduto`
--
ALTER TABLE `trcamposproduto`
  ADD CONSTRAINT `FK_TRCAMPOSPRODUTO_TRCAMPOSTIPO` FOREIGN KEY (`CODCLIENTE`,`CODCAMPOSTIPO`) REFERENCES `trcampostipo` (`CODCLIENTE`, `CODCAMPOSTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRCAMPOSPRODUTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trcampostipo`
--
ALTER TABLE `trcampostipo`
  ADD CONSTRAINT `FK_TRCAMPOSTIPO_TRTIPO` FOREIGN KEY (`CODCLIENTE`,`CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trcomponenteslocacao`
--
ALTER TABLE `trcomponenteslocacao`
  ADD CONSTRAINT `FK_TRCOMPOENTESLOCACAO_TRTIPOFILHO` FOREIGN KEY (`CODCLIENTE`,`CODTIPOFILHO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRCOMPONENTESLOCACAO_TRTIPOPAI` FOREIGN KEY (`CODCLIENTE`,`CODTIPOPAI`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trcomponentesproduto`
--
ALTER TABLE `trcomponentesproduto`
  ADD CONSTRAINT `FK_COMPONENTESPRODUTO_COMPONENTESTIPO` FOREIGN KEY (`CODCLIENTE`,`CODCOMPONENTESTIPO`) REFERENCES `trcomponentestipo` (`CODCLIENTE`, `CODCOMPONENTESTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_COMPONENTESPRODUTO_PRODUTO_FILHO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOFILHO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_COMPONENTESPRODUTO_PRODUTO_PAI` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOPAI`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trcomponentestipo`
--
ALTER TABLE `trcomponentestipo`
  ADD CONSTRAINT `FK_TRCOMPONENTESTIPO_TRTIPOFILHO` FOREIGN KEY (`CODCLIENTE`,`CODTIPOFILHO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRCOMPONENTESTIPO_TRTIPOPAI` FOREIGN KEY (`CODCLIENTE`,`CODTIPOPAI`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trdoacao`
--
ALTER TABLE `trdoacao`
  ADD CONSTRAINT `FK_TRDOACAO_FIFORNECEDOR` FOREIGN KEY (`CODCLIENTE`,`CODFORNECEDOR`) REFERENCES `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRDOACAO_GLFILIAL` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRDOACAO_TRLOTE` FOREIGN KEY (`CODCLIENTE`,`CODLOTE`) REFERENCES `trlote` (`CODCLIENTE`, `CODLOTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRDOACAO_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRDOACAO_TRTIPO` FOREIGN KEY (`CODCLIENTE`,`CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trlocacao`
--
ALTER TABLE `trlocacao`
  ADD CONSTRAINT `FK_TRLOCACAO_GLFILIALLOCACAO` FOREIGN KEY (`CODCLIENTELOCADO`,`CODEMPRESALOCADO`,`CODFILIALLOCADO`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRLOCACAO_TRCOMPONENTESLOCACAO` FOREIGN KEY (`CODCLIENTE`,`CODCOMPONENTESLOCACAO`) REFERENCES `trcomponenteslocacao` (`CODCLIENTE`, `CODCOMPONENTESLOCACAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRLOCACAO_TRFORNECEDOR` FOREIGN KEY (`CODCLIENTE`,`CODFORNECEDOR`) REFERENCES `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRLOCACAO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trlogcomponentesproduto`
--
ALTER TABLE `trlogcomponentesproduto`
  ADD CONSTRAINT `FK_TRLOGCOMPONENTESPRODUTO_TRCOMPONENTESPRODUTO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCOMPONENTESPRODUTO`) REFERENCES `trcomponentesproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCOMPONENTESPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRLOGCOMPONENTESPRODUTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOFILHO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trlogproduto`
--
ALTER TABLE `trlogproduto`
  ADD CONSTRAINT `FK_TRLOGPRODUTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRLOGPRODUTO_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trlote`
--
ALTER TABLE `trlote`
  ADD CONSTRAINT `FK_TRLOTE_FIFORNECEDOR` FOREIGN KEY (`CODCLIENTE`,`CODFORNECEDOR`) REFERENCES `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRLOTE_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRLOTE_TRTIPO` FOREIGN KEY (`CODCLIENTE`,`CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trproduto`
--
ALTER TABLE `trproduto`
  ADD CONSTRAINT `FK_TREQUIPAMENTO_TRDOACAO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`) REFERENCES `trdoacao` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODDOACAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TREQUIPAMENTO_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRPRODUTO_TRTIPO` FOREIGN KEY (`CODCLIENTE`,`CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trsucata`
--
ALTER TABLE `trsucata`
  ADD CONSTRAINT `FK_TRSUCATA_TRDOACAO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`) REFERENCES `trdoacao` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODDOACAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRSUCATA_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRSUCATA_TRPRODUTOPAI` FOREIGN KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOPAI`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRSUCATA_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TRSUCATA_TRTIPO` FOREIGN KEY (`CODCLIENTE`,`CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `trtipo`
--
ALTER TABLE `trtipo`
  ADD CONSTRAINT `FK_TRTIPO_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
