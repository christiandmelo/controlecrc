CREATE DATABASE  IF NOT EXISTS `portalcrc` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `portalcrc`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: portalcrc
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('0158dc6879320c009bb7276d5b8f78ec8211d8b0','127.0.0.1',1468428416,'__ci_last_regenerate|i:1468428416;'),('02325a42ceb55b5f0d97a1125b86d48a35a7feb1','127.0.0.1',1468428416,'__ci_last_regenerate|i:1468428416;'),('028b53a8f1795b36b52d243e839322c7a0b61ee1','127.0.0.1',1467215695,'__ci_last_regenerate|i:1467215620;'),('0421ca8b438c8172306ae9f3205678895addb270','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('0a5a10de6418f852954b6a5b2a11833b7ff6d62e','127.0.0.1',1467206880,'__ci_last_regenerate|i:1467206880;'),('0d72c7d904129942e7a48bd485caa8e2da6bbe92','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('1197c54a5102681f5d22e93dc11f47a351e57f21','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('1283a23d1532b9636663c82a3e38210263ec5aa6','127.0.0.1',1467216358,'__ci_last_regenerate|i:1467216357;'),('1463d0e3533104abc011acf36453344243eb26b0','127.0.0.1',1467206880,'__ci_last_regenerate|i:1467206880;'),('157c1551e9958ebf33904970398991808ff0c627','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('16842c37045f60cbb84703249dbea0913f964ee9','192.168.173.44',1467413262,'__ci_last_regenerate|i:1467413261;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('18d9ffda1002604f3518d45c6d9631f47f253a61','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('192b7f964ffa76b287e9a5b6791855a4f40b975e','127.0.0.1',1468428416,'__ci_last_regenerate|i:1468428416;'),('1c7d129c2702243c81fc7af4e4125f2815b06b7d','127.0.0.1',1467206879,'__ci_last_regenerate|i:1467206879;'),('1f3d652e394e2e5e6a6344e99e61fbf904a3bda0','192.168.173.204',1467409229,'__ci_last_regenerate|i:1467409225;'),('20607099e03e4b05a77525727a9c513ba38fc0dd','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('22bbdc2d94cd0937f6514b33d1864d45947958d0','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('26b78c6ab844b449e837812199f19125bb7bdd9c','127.0.0.1',1468428416,'__ci_last_regenerate|i:1468428416;'),('27f5d7e9973c6f236e2e135eb23f8ee085a49c54','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('2d360115c2a0c8909feedb13dc68239490513a41','127.0.0.1',1468336289,'__ci_last_regenerate|i:1468336204;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('352caaa7258f2424eeee38642d918c865bd80d35','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('3655a1b7c2ae7c7c8b7e0442efc65ce406158b16','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('392dd35854ad3508920ed0b0befc6f729ba8baf9','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('40411281d890e8379412579c29ba525103314330','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('4116af0b33a6dbbb4a6c2b5ee69833dbe3dc131a','::1',1474927058,'__ci_last_regenerate|i:1474926780;'),('46d3306a8d7d7d9c4ac94df8814b1c11234cd419','127.0.0.1',1467204928,'__ci_last_regenerate|i:1467204927;'),('48fd56ba8e15a8b53e126aff85be15727b542d98','192.168.173.204',1467413357,'__ci_last_regenerate|i:1467413331;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('490f3b5fef0c218b71e0d9f510c255aeacb74354','127.0.0.1',1468428415,'__ci_last_regenerate|i:1468428115;'),('49ac6b415e7db1b776e41b367b9d2d70b7447e12','::1',1467483028,'__ci_last_regenerate|i:1467482756;'),('4d50454f1b65f13d635ac5863d3d734fdad2ce59','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('4da61407db75dbb2345a090aca6f7331c63ca887','127.0.0.1',1468868077,'__ci_last_regenerate|i:1468868060;logado|b:1;nomeusuario|s:6:\"Mestre\";tipousuario|s:1:\"1\";caminhoFoto|s:0:\"\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('4f649756469626dcd79645f36a700520008260f4','127.0.0.1',1467206880,'__ci_last_regenerate|i:1467206880;'),('4fc5ed555d21cc9e534d1d85d6a5a44ed3e3ad70','127.0.0.1',1468428416,'__ci_last_regenerate|i:1468428416;'),('507477170afe4f0c1e45b02d27993875cff31938','127.0.0.1',1467206879,'__ci_last_regenerate|i:1467206879;'),('510d2b1f90fd734c0f8ca8cd9262a2f808d7fea9','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('563e39fb131bb5608fc6ed41d2d97aa11b3b67c3','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('5694579dda4d57e5433507a261e9e5dadd4d3687','127.0.0.1',1467206879,'__ci_last_regenerate|i:1467206879;'),('56c18a439a37ffdb53d99667b1e773bd6e0ee975','192.168.173.221',1467216548,'__ci_last_regenerate|i:1467216242;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('58973c80a8980f75ceab1c94749005556be82b6e','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('58c9732329aa0b6793cd467bfbea6e31cf4e7e3b','127.0.0.1',1468868122,'__ci_last_regenerate|i:1468868122;'),('59ed71d2b1746d850ce045e17547a67e052190af','192.168.173.221',1467216695,'__ci_last_regenerate|i:1467216561;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('5da5a0865b14aabb94ec9a302228ac462cff59a8','127.0.0.1',1467206879,'__ci_last_regenerate|i:1467206879;'),('602120c66d7b8466cfbe4ad2213f8e3533b51ec9','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('646de04b5b14f6b4f91ae10e14de0d945f7981d0','127.0.0.1',1467409784,'__ci_last_regenerate|i:1467409784;'),('66ab4bd439a23197e9d7172c1325915fef9fe1ef','::1',1469134159,'__ci_last_regenerate|i:1469134158;'),('741d510d26d34e0eb6828b9c629307c22e19e27e','127.0.0.1',1474927162,'__ci_last_regenerate|i:1474927162;'),('74b25407035224fc0ff8f322361095dfa7c605f2','127.0.0.1',1467206063,'__ci_last_regenerate|i:1467206061;'),('76151b01a64cb9ab8403f8d8e9db05723fb8555b','127.0.0.1',1468868122,'__ci_last_regenerate|i:1468868122;'),('7708fd0770ed55134eddd74d5be96ad83963ac60','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('78276a940e301f0e4a60a3058443600674994443','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('7f5bdbbf36eaa39df6cf135d3b82f5bd715a672a','127.0.0.1',1468428416,'__ci_last_regenerate|i:1468428416;'),('8578169505a3b570ff0ab91ea31a0cc7bbd7a160','::1',1474927104,'__ci_last_regenerate|i:1474927102;'),('87dfaf7818538ac0c3032b0f03f22f0626439958','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('882eb43ca17af65451d4c1df98f1df0ac2aa6356','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('8a02006677047b7aa2ab9d3ce61820523ac0ee4a','192.168.173.183',1467413399,'__ci_last_regenerate|i:1467409072;logado|b:1;nomeusuario|s:15:\"Israel Teixeira\";tipousuario|s:1:\"1\";caminhoFoto|s:0:\"\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:2:\"10\";'),('8b97ccde8cb580e62add99a99578272faefcf73c','127.0.0.1',1467206879,'__ci_last_regenerate|i:1467206879;'),('8ba4126401096b267ac68ce00155d063d2322896','127.0.0.1',1468428645,'__ci_last_regenerate|i:1468428416;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('8c0c0c2358cef3238966e530d468f7e98179df02','127.0.0.1',1468429118,'__ci_last_regenerate|i:1468428957;'),('8d609d84677fe5afc3f112eccb76eab14fb19193','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('902f63b8e597088f1b4fc96a7ed3cdd8d7520c59','127.0.0.1',1467206880,'__ci_last_regenerate|i:1467206880;'),('9179121cf5e433e7dd4cb64c2277dabf16636ea4','127.0.0.1',1468868122,'__ci_last_regenerate|i:1468868122;'),('936d2e7df16eda1cd16282f9de47862c4ea7d9db','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('991ad1dd3eb996a84b08a0b7bfca55bbd0c3fd1e','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('9973da598752fb068e2f5ef8b6ee509bd5478885','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('9c4a3bd1f18f4d2358989fdff1e6f64ad5461216','127.0.0.1',1468868122,'__ci_last_regenerate|i:1468868122;'),('9d866f7ebc9cb9d0fa4c74628a80f4508ebcbad4','127.0.0.1',1467206879,'__ci_last_regenerate|i:1467206879;'),('a1310a78cd0855f439572bd0f3186166958516bf','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('a1e8af6fc77b0e9ddc6f87daa83bf8bba6523d38','127.0.0.1',1467413850,'__ci_last_regenerate|i:1467413372;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('a2deffa25794411de5ff7ff2ff66c7088b62c695','::1',1467205660,'__ci_last_regenerate|i:1467205660;'),('a5caf6d9962836cbbd953c62b6bbfaae9fd1b0d0','::1',1468868139,'__ci_last_regenerate|i:1468868129;'),('a82f3d88042ee4ff190fc63c886008223368bc21','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('ac7ba257993135d8a626478bfa9e366f452fc88d','127.0.0.1',1468868123,'__ci_last_regenerate|i:1468868123;'),('b62acf5ec09ea3847f0f3848845df613fc0b9177','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('b6316ffb802956e8c7de19a8aea754aa2e167fe8','127.0.0.1',1468429131,'__ci_last_regenerate|i:1468429129;'),('bedb0ec30b065cc1d96d2af654b1f99c6633e122','127.0.0.1',1468428113,'__ci_last_regenerate|i:1468428113;'),('c3300ad4e653307434fcb7a20c9802f20fcd13eb','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('c384098794695e63786097bbf974a255172e66e1','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('c45731fd274c5555fdbdc9d3e1230ed9b74736b5','127.0.0.1',1467408922,'__ci_last_regenerate|i:1467408850;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('c591bec01e39953a9ceeb7a4686500c218b11a18','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('ca9f595c1903de0e8cb10b4b5c006d674ca6f7d0','127.0.0.1',1468428416,'__ci_last_regenerate|i:1468428416;'),('d0100ecc4ae35665741d2ff72df8b252d795b48e','12.12.0.92',1467205510,'__ci_last_regenerate|i:1467205510;'),('d02d051678532b15b27e69021889e80931eda3d6','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('d3421359e84e50396e25015a4643e7331166c180','192.168.173.44',1467409010,'__ci_last_regenerate|i:1467409008;'),('d3945219666589346241505a36c36f07ec5f3d12','127.0.0.1',1468867869,'__ci_last_regenerate|i:1468867498;logado|b:1;nomeusuario|s:22:\"Christian Luis de Melo\";tipousuario|s:1:\"1\";caminhoFoto|s:49:\"/assets/img/perfil/1-christiandmelo@gmail.com.jpg\";codcliente|s:1:\"1\";nomecliente|s:3:\"CRC\";codempresa|s:1:\"1\";nomeempresa|s:3:\"CRC\";codfilial|s:1:\"1\";nomefilial|s:3:\"CRC\";codusuario|s:1:\"1\";'),('d4945ca447fcc372204106c81de0f57abbdb9985','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('d8cb1324dd423654ffc13a283270162692e8590e','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('dcd86e487f1f4e95edb3ed47b089417227b363d2','127.0.0.1',1468428114,'__ci_last_regenerate|i:1468428114;'),('dcdebe3cda78bf6b5f4556f83efbd8d7453022ed','::1',1467206923,'__ci_last_regenerate|i:1467206923;'),('dcf481a6932953952fd3110937cdde9f0aff0de5','127.0.0.1',1467206880,'__ci_last_regenerate|i:1467206880;'),('e44ee693b91902be5af10e2301ef39716e060060','127.0.0.1',1467206880,'__ci_last_regenerate|i:1467206880;'),('eb02b0e1c773bc564af3bb8f0a4db6d3ae417c6f','127.0.0.1',1468427542,'__ci_last_regenerate|i:1468427541;'),('eefb5e6da61b5d2b308a0b77c4f3751ef236b76f','192.168.173.237',1467206053,'__ci_last_regenerate|i:1467205865;'),('f0ce4a734d1715d28d682c1c99249d35ec2b2097','127.0.0.1',1467373389,'__ci_last_regenerate|i:1467373376;'),('f1f4d1a45245ed87cb35b298759040f9884ba7c4','127.0.0.1',1467408975,'__ci_last_regenerate|i:1467408975;'),('f6eecadfbd2ae580836ae3fa30e8ee83b3b132e9','::1',1467483709,'__ci_last_regenerate|i:1467483416;'),('f758b84cb27757f31d98c3e3c58735a2f818d9e9','127.0.0.1',1467206879,'__ci_last_regenerate|i:1467206879;'),('fa09eb21e6ad5272a6991b2fd696c039f66ff387','127.0.0.1',1467206880,'__ci_last_regenerate|i:1467206880;');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fiacomplancamento`
--

DROP TABLE IF EXISTS `fiacomplancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fiacomplancamento` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODACOMPLANCAMENTO` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODSTATUS` int(11) DEFAULT NULL,
  `VALOR` decimal(5,2) DEFAULT NULL,
  `DATA_RETORNO` date DEFAULT NULL,
  `RETORNO` int(2) DEFAULT NULL,
  `OBSERVACAO` varchar(200) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) DEFAULT NULL,
  `REGCRIADOEM` datetime DEFAULT NULL,
  `REGMODIFPOR` varchar(80) DEFAULT NULL,
  `REGMODIFEM` datetime DEFAULT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODACOMPLANCAMENTO`),
  KEY `FK_FIACOMPLANCAMENTO_FILANCAMENTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fiacomplancamento`
--

LOCK TABLES `fiacomplancamento` WRITE;
/*!40000 ALTER TABLE `fiacomplancamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `fiacomplancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fiboleto`
--

DROP TABLE IF EXISTS `fiboleto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fiboleto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODBOLETO` int(11) NOT NULL,
  `DTEMISSAO` datetime NOT NULL,
  `DTVENCIMENTO` datetime NOT NULL,
  `DTBAIXA` datetime DEFAULT NULL,
  `VALOR` decimal(10,2) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODBOLETO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fiboleto`
--

LOCK TABLES `fiboleto` WRITE;
/*!40000 ALTER TABLE `fiboleto` DISABLE KEYS */;
/*!40000 ALTER TABLE `fiboleto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficartaocredito`
--

DROP TABLE IF EXISTS `ficartaocredito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ficartaocredito` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCARTAOCREDITO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  `DIAFECHAMENTO` date NOT NULL,
  `DIAVENCIMENTO` date NOT NULL,
  `LIMITE` decimal(10,2) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCARTAOCREDITO`),
  CONSTRAINT `FK_FICARTAOCREDITO_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficartaocredito`
--

LOCK TABLES `ficartaocredito` WRITE;
/*!40000 ALTER TABLE `ficartaocredito` DISABLE KEYS */;
/*!40000 ALTER TABLE `ficartaocredito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficategoria`
--

DROP TABLE IF EXISTS `ficategoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ficategoria` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCATEGORIA` int(11) NOT NULL,
  `NOME` varchar(45) NOT NULL,
  `ORDEM` varchar(45) NOT NULL,
  `SITUACAO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODCATEGORIA`),
  CONSTRAINT `FK_FICATEGORIA_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficategoria`
--

LOCK TABLES `ficategoria` WRITE;
/*!40000 ALTER TABLE `ficategoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `ficategoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficontacorrente`
--

DROP TABLE IF EXISTS `ficontacorrente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ficontacorrente` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCONTACORRENTE` int(11) NOT NULL,
  `CODBANCO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  `SITUACAO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  KEY `FK_FICONTACORRENTE_GLBANCO_idx` (`CODCLIENTE`,`CODBANCO`),
  CONSTRAINT `FK_FICONTACORRENTE_GLBANCO` FOREIGN KEY (`CODCLIENTE`, `CODBANCO`) REFERENCES `glbanco` (`CODCLIENTE`, `CODBANCO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FICONTACORRENTE_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficontacorrente`
--

LOCK TABLES `ficontacorrente` WRITE;
/*!40000 ALTER TABLE `ficontacorrente` DISABLE KEYS */;
/*!40000 ALTER TABLE `ficontacorrente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fifaturacartao`
--

DROP TABLE IF EXISTS `fifaturacartao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fifaturacartao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODFATURACARTAO` int(11) NOT NULL,
  `CODCARTAOCREDITO` int(11) NOT NULL,
  `VALORFATURA` decimal(10,2) DEFAULT NULL,
  `DATAFECHAMENTO` date DEFAULT NULL,
  `DATAVENCIMENTO` date DEFAULT NULL,
  `SITUACAO` int(11) DEFAULT NULL,
  `CODCONTACORRENTE` int(11) DEFAULT NULL,
  `CODPAGAMENTO` int(11) DEFAULT NULL,
  `DESCRICAO` varchar(45) DEFAULT NULL,
  `DATAPAGAMENTO` datetime DEFAULT NULL,
  `VALORPAGO` decimal(10,2) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODFATURACARTAO`),
  KEY `FK_FIFATURACARTAO_FICARTAOCREDITO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCARTAOCREDITO`),
  KEY `FK_FIFATURACARTAO_FICONTACORRENTE_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  KEY `FK_FIFATURACARTAO_FIFORMAPAGAMENTO_idx` (`CODCLIENTE`,`CODPAGAMENTO`),
  CONSTRAINT `FK_FIFATURACARTAO_FICARTAOCREDITO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCARTAOCREDITO`) REFERENCES `ficartaocredito` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCARTAOCREDITO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIFATURACARTAO_FICONTACORRENTE` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) REFERENCES `ficontacorrente` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIFATURACARTAO_FIFORMAPAGAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODPAGAMENTO`) REFERENCES `fiformadepagamento` (`CODCLIENTE`, `CODPAGAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fifaturacartao`
--

LOCK TABLES `fifaturacartao` WRITE;
/*!40000 ALTER TABLE `fifaturacartao` DISABLE KEYS */;
/*!40000 ALTER TABLE `fifaturacartao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fiformadepagamento`
--

DROP TABLE IF EXISTS `fiformadepagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fiformadepagamento` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODPAGAMENTO` int(11) NOT NULL,
  `DESCRICAO` varchar(80) NOT NULL,
  `SIGLA` varchar(2) NOT NULL,
  `SITUACAO` int(1) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODPAGAMENTO`),
  CONSTRAINT `FK_FIFORMADEPAGAMENTO_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fiformadepagamento`
--

LOCK TABLES `fiformadepagamento` WRITE;
/*!40000 ALTER TABLE `fiformadepagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `fiformadepagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fifornecedor`
--

DROP TABLE IF EXISTS `fifornecedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fifornecedor` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) NOT NULL,
  `CODMUNICIPIO` int(11) DEFAULT NULL,
  `CLIENTE` varchar(45) NOT NULL DEFAULT '0' COMMENT '1 = CLIENTE\n0 = FORNECEDOR',
  `RAZAOSOCIAL` varchar(200) NOT NULL,
  `NOMEFANTASIA` varchar(200) NOT NULL,
  `REGISTRO` int(11) DEFAULT NULL,
  `CNPJ` varchar(19) NOT NULL,
  `RUA` varchar(80) DEFAULT NULL,
  `NUMERO` varchar(45) DEFAULT NULL,
  `BAIRRO` varchar(50) DEFAULT NULL,
  `CEP` varchar(9) DEFAULT NULL,
  `LOGRADOURO` varchar(300) DEFAULT NULL,
  `TELEFONE` varchar(20) DEFAULT NULL,
  `TELEFONE2` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(80) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODFORNECEDOR`),
  KEY `FK_FIFORNECEDOR_FICONVECAO2_idx` (`CODCLIENTE`),
  KEY `FK_FIFORNECEDOR_GLMUNICIPIO_idx` (`CODMUNICIPIO`),
  CONSTRAINT `FK_FIFORNECEDOR_GLMUNICIPIO` FOREIGN KEY (`CODMUNICIPIO`) REFERENCES `glmunicipio` (`CODMUNICIPIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fifornecedor`
--

LOCK TABLES `fifornecedor` WRITE;
/*!40000 ALTER TABLE `fifornecedor` DISABLE KEYS */;
INSERT INTO `fifornecedor` VALUES (1,1,1,'0','Dell Computadores LTDA','Dell Computadores',NULL,'11.111.111/1111-11','','','','',NULL,'','','','','0000-00-00 00:00:00','1','2016-05-15 19:11:23'),(1,2,1,'0','Banco do Brasil LTDA','Banco do Brasil',NULL,'22.222.222/2222-22','','','','',NULL,'','','','1','2016-05-03 22:32:49','1','2016-05-15 19:11:48'),(1,3,1,'0','UNI-BH Ltda','UNI-BH',NULL,'11.111.111/1111-11','','','','',NULL,'','','','1','2016-05-21 16:11:11','1','2016-05-21 16:12:16'),(1,4,1,'1','Maria','Maria',NULL,'22.222.222/2222-22','','','','',NULL,'','','','1','2016-05-31 22:11:55','1','2016-05-31 22:11:55');
/*!40000 ALTER TABLE `fifornecedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filanboleto`
--

DROP TABLE IF EXISTS `filanboleto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filanboleto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODBOLETO` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  KEY `FK_FILANBOLETO_FIBOLETO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODBOLETO`),
  CONSTRAINT `FK_FILANBOLETO_FIBOLETO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODBOLETO`) REFERENCES `fiboleto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODBOLETO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANBOLETO_FILANCAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) REFERENCES `filancamento` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filanboleto`
--

LOCK TABLES `filanboleto` WRITE;
/*!40000 ALTER TABLE `filanboleto` DISABLE KEYS */;
/*!40000 ALTER TABLE `filanboleto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filancamento`
--

DROP TABLE IF EXISTS `filancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filancamento` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) DEFAULT NULL,
  `CODFISTATUS` int(11) NOT NULL,
  `CODACOMPSTATUS` int(11) DEFAULT NULL,
  `CODCONTACORRENTE` int(11) DEFAULT NULL,
  `CODPREVISAOLAN` int(11) DEFAULT NULL,
  `CODRECORRENTELAN` int(11) DEFAULT NULL,
  `CODCATEGORIA` int(11) DEFAULT NULL,
  `CODPAGAMENTO` int(11) DEFAULT NULL,
  `CODTIPOLANCAMENTO` int(11) NOT NULL,
  `DATAEMISSAO` date DEFAULT NULL,
  `DATAVENCIMENTO` date DEFAULT NULL,
  `DATABAIXA` date DEFAULT NULL,
  `LOJA` varchar(80) DEFAULT NULL,
  `IDLOJA` varchar(80) DEFAULT NULL,
  `VALORORIGINAL` decimal(10,2) DEFAULT NULL,
  `VALORBAIXADO` decimal(10,2) DEFAULT NULL,
  `CODPRODUTO` int(11) DEFAULT NULL COMMENT 'Campos de ligação com a tabela trproduto',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` varchar(45) NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  KEY `FK_FILANCAMENTO_FIRECORRENTELAN_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODRECORRENTELAN`),
  KEY `FK_FILANCAMENTO_FICATEGORIA_idx` (`CODCLIENTE`,`CODCATEGORIA`),
  KEY `FK_FILANCAMENTO_FIFORMADEPAGAMENTO_idx` (`CODCLIENTE`,`CODPAGAMENTO`),
  KEY `FK_FILANCAMENTO_FICONTACORRENTE_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  KEY `FK_FILANCAMENTO_FITIPOLANCAMENTO_idx` (`CODTIPOLANCAMENTO`),
  KEY `FK_FILANCAMENTO_FISTATUS_idx` (`CODFISTATUS`),
  KEY `FK_FILANCAMENTO_TRPRODUTO_idx` (`CODPRODUTO`,`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`),
  KEY `FK_FILANCAMENTO_TRPRODUTO` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  CONSTRAINT `FK_FILANCAMENTO_FICATEGORIA` FOREIGN KEY (`CODCLIENTE`, `CODCATEGORIA`) REFERENCES `ficategoria` (`CODCLIENTE`, `CODCATEGORIA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_FICONTACORRENTE` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) REFERENCES `ficontacorrente` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_FIFORMADEPAGAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODPAGAMENTO`) REFERENCES `fiformadepagamento` (`CODCLIENTE`, `CODPAGAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_FIRECORRENTELAN` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODRECORRENTELAN`) REFERENCES `firecorrentelan` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODRECORRENTELAN`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_FISTATUS` FOREIGN KEY (`CODFISTATUS`) REFERENCES `fistatus` (`CODFISTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_FITIPOLANCAMENTO` FOREIGN KEY (`CODTIPOLANCAMENTO`) REFERENCES `fitipolancamento` (`CODTIPOLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANCAMENTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filancamento`
--

LOCK TABLES `filancamento` WRITE;
/*!40000 ALTER TABLE `filancamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `filancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filanfaturacartao`
--

DROP TABLE IF EXISTS `filanfaturacartao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filanfaturacartao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLANCAMENTO` int(11) NOT NULL,
  `CODFATURACARTAO` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  KEY `FK_FILANFATURACARTAO_FIFATURACARTAO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODFATURACARTAO`),
  KEY `FK_FILANFATURACARTAO_FILANCAMENTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLANCAMENTO`),
  CONSTRAINT `FK_FILANFATURACARTAO_FIFATURACARTAO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODFATURACARTAO`) REFERENCES `fifaturacartao` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODFATURACARTAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FILANFATURACARTAO_FILANCAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) REFERENCES `filancamento` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filanfaturacartao`
--

LOCK TABLES `filanfaturacartao` WRITE;
/*!40000 ALTER TABLE `filanfaturacartao` DISABLE KEYS */;
/*!40000 ALTER TABLE `filanfaturacartao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firecorrentelan`
--

DROP TABLE IF EXISTS `firecorrentelan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `firecorrentelan` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODRECORRENTELAN` int(11) NOT NULL,
  `CODTIPOLANCAMENTO` int(11) NOT NULL,
  `CODCATEGORIA` int(11) NOT NULL,
  `CODCONTACORRENTE` int(11) NOT NULL,
  `CODPAGAMENTO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  `VALOR` decimal(10,2) NOT NULL,
  `INTERVALO` varchar(20) NOT NULL,
  `DIAFIXO` int(11) NOT NULL,
  `DATAINICIO` date NOT NULL,
  `DATAFIM` date DEFAULT NULL,
  `BAIXAAUTOMATICA` int(11) NOT NULL DEFAULT '0',
  `DIAUTIL` int(11) DEFAULT NULL,
  `PREVISAO` int(11) DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODRECORRENTELAN`),
  KEY `FK_FIRECORRENTELAN_FITIPOLANCAMENTO_idx` (`CODTIPOLANCAMENTO`),
  KEY `FK_FIRECORRENTELAN_FICATEGORIA_idx` (`CODCLIENTE`,`CODCATEGORIA`),
  KEY `FK_FIRECORRENTELAN_FICONTACORRENTE_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCONTACORRENTE`),
  KEY `FK_FIRECORRENTELAN_FIFORMAPAGAMENTO_idx` (`CODCLIENTE`,`CODPAGAMENTO`),
  CONSTRAINT `FK_FIRECORRENTELAN_FICATEGORIA` FOREIGN KEY (`CODCLIENTE`, `CODCATEGORIA`) REFERENCES `ficategoria` (`CODCLIENTE`, `CODCATEGORIA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIRECORRENTELAN_FICONTACORRENTE` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) REFERENCES `ficontacorrente` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCONTACORRENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIRECORRENTELAN_FIFORMAPAGAMENTO` FOREIGN KEY (`CODCLIENTE`, `CODPAGAMENTO`) REFERENCES `fiformadepagamento` (`CODCLIENTE`, `CODPAGAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIRECORRENTELAN_FITIPOLANCAMENTO` FOREIGN KEY (`CODTIPOLANCAMENTO`) REFERENCES `fitipolancamento` (`CODTIPOLANCAMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FIRECORRENTELAN_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firecorrentelan`
--

LOCK TABLES `firecorrentelan` WRITE;
/*!40000 ALTER TABLE `firecorrentelan` DISABLE KEYS */;
/*!40000 ALTER TABLE `firecorrentelan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fistatus`
--

DROP TABLE IF EXISTS `fistatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fistatus` (
  `CODFISTATUS` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `ATIVO` int(1) NOT NULL,
  `TIPO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODFISTATUS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fistatus`
--

LOCK TABLES `fistatus` WRITE;
/*!40000 ALTER TABLE `fistatus` DISABLE KEYS */;
INSERT INTO `fistatus` VALUES (1,'Em aberto',1,0,'1','2016-06-01 09:30:00','1','2016-06-01 09:30:00'),(2,'Baixado',1,0,'1','2016-06-01 09:30:00','1','2016-06-01 09:30:00'),(3,'Baixado por acordo',1,0,'1','2016-06-01 09:30:00','1','2016-06-01 09:30:00');
/*!40000 ALTER TABLE `fistatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fitipolancamento`
--

DROP TABLE IF EXISTS `fitipolancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fitipolancamento` (
  `CODTIPOLANCAMENTO` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(10) NOT NULL,
  `SINAL` varchar(1) NOT NULL,
  PRIMARY KEY (`CODTIPOLANCAMENTO`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fitipolancamento`
--

LOCK TABLES `fitipolancamento` WRITE;
/*!40000 ALTER TABLE `fitipolancamento` DISABLE KEYS */;
INSERT INTO `fitipolancamento` VALUES (1,'Pagar','-'),(2,'Receber','+');
/*!40000 ALTER TABLE `fitipolancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glativo`
--

DROP TABLE IF EXISTS `glativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glativo` (
  `CODATIVO` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  PRIMARY KEY (`CODATIVO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glativo`
--

LOCK TABLES `glativo` WRITE;
/*!40000 ALTER TABLE `glativo` DISABLE KEYS */;
INSERT INTO `glativo` VALUES (1,'Ativo'),(2,'Inativo');
/*!40000 ALTER TABLE `glativo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glautoindice`
--

DROP TABLE IF EXISTS `glautoindice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glautoindice` (
  `CODINDICE` int(11) NOT NULL AUTO_INCREMENT,
  `CODCLIENTE` int(11) DEFAULT NULL,
  `CODEMPRESA` int(11) DEFAULT NULL,
  `CODFILIAL` int(11) DEFAULT NULL,
  `TABELA` varchar(45) NOT NULL,
  `VALOR` varchar(45) NOT NULL,
  PRIMARY KEY (`CODINDICE`),
  KEY `FK_GLAUTOINDICE_GLEMPRESA_idx` (`CODCLIENTE`,`CODEMPRESA`),
  KEY `FK_GLAUTOINDICE_GLFILIAL_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`),
  CONSTRAINT `FK_GLAUTOINDICE_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLAUTOINDICE_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLAUTOINDICE_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glautoindice`
--

LOCK TABLES `glautoindice` WRITE;
/*!40000 ALTER TABLE `glautoindice` DISABLE KEYS */;
INSERT INTO `glautoindice` VALUES (1,1,1,1,'glgrupo','10'),(4,1,1,1,'glfilial','14'),(5,1,1,NULL,'glusuariofilial','25'),(6,1,1,1,'glgrupolink','50'),(7,1,1,1,'glgrupousuario','24'),(9,1,NULL,NULL,'fifornecedor','4'),(11,1,NULL,NULL,'trtipo','8'),(12,1,NULL,NULL,'trcampostipo','7'),(13,1,NULL,NULL,'trcomponenteslocacao','3'),(14,1,NULL,NULL,'trcomponentestipo','4'),(16,1,NULL,NULL,'trlote','16'),(18,1,1,1,'trdoacao','1'),(19,1,1,1,'trproduto','1'),(20,1,1,1,'trcamposproduto','1'),(21,1,1,1,'trlogproduto','1'),(22,1,1,1,'trcomponentesproduto','1'),(23,1,1,1,'trlogcomponentesproduto','1'),(24,1,1,1,'trsucata','1'),(25,1,1,1,'filancamento','1'),(26,1,1,1,'trlocacao','1');
/*!40000 ALTER TABLE `glautoindice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glbanco`
--

DROP TABLE IF EXISTS `glbanco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glbanco` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODBANCO` int(11) NOT NULL,
  `CODFEBRABAN` int(11) NOT NULL,
  `DIGITO` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `NOMEREDUZIDO` varchar(45) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODBANCO`),
  KEY `FK_GBANCO_GCLIENTE_idx` (`CODCLIENTE`),
  CONSTRAINT `FK_GLBANCO_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glbanco`
--

LOCK TABLES `glbanco` WRITE;
/*!40000 ALTER TABLE `glbanco` DISABLE KEYS */;
/*!40000 ALTER TABLE `glbanco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glcliente`
--

DROP TABLE IF EXISTS `glcliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glcliente` (
  `CODCLIENTE` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(80) DEFAULT NULL,
  `LIBERADO` int(1) NOT NULL DEFAULT '0',
  `NOMEFANTASIA` varchar(80) DEFAULT NULL,
  `QTDNOTAS` int(11) NOT NULL DEFAULT '0',
  `QTDUSUARIOS` int(11) NOT NULL DEFAULT '0',
  `QTDUSUARIOSUSADOS` int(11) NOT NULL,
  `QTDEMPRESAS` int(11) NOT NULL DEFAULT '0',
  `QTDEMPRESASUSADAS` int(11) NOT NULL,
  `PERMISSAO` int(11) NOT NULL DEFAULT '1' COMMENT '1 = GLOBAL\n2 = POR FILIAL',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glcliente`
--

LOCK TABLES `glcliente` WRITE;
/*!40000 ALTER TABLE `glcliente` DISABLE KEYS */;
INSERT INTO `glcliente` VALUES (1,'CRC',1,'CRC',100,5,0,1,1,1,'1','2016-02-24 00:00:00','1','2016-02-24 00:00:00');
/*!40000 ALTER TABLE `glcliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glempresa`
--

DROP TABLE IF EXISTS `glempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glempresa` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `NOME` varchar(80) NOT NULL,
  `NOMEFANTASIA` varchar(80) NOT NULL,
  `QTDFILIAL` int(11) NOT NULL,
  `TELEFONE` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(45) DEFAULT NULL,
  `CGC` varchar(25) DEFAULT NULL,
  `INSCRICAOESTADUAL` varchar(20) DEFAULT NULL,
  `RUA` varchar(50) DEFAULT NULL,
  `NUMERO` varchar(20) DEFAULT NULL,
  `COMPLEMENTO` varchar(20) DEFAULT NULL,
  `BAIRRO` varchar(50) DEFAULT NULL,
  `PAIS` varchar(50) DEFAULT NULL,
  `CEP` varchar(20) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` varchar(45) NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`),
  CONSTRAINT `FK_GLEMPRESA_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glempresa`
--

LOCK TABLES `glempresa` WRITE;
/*!40000 ALTER TABLE `glempresa` DISABLE KEYS */;
INSERT INTO `glempresa` VALUES (1,1,'CRC','CRC',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2016-02-24 00:00:00','1','2016-02-24 00:00:00');
/*!40000 ALTER TABLE `glempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glestado`
--

DROP TABLE IF EXISTS `glestado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glestado` (
  `CODESTADO` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(80) NOT NULL,
  `CODETD` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`CODESTADO`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glestado`
--

LOCK TABLES `glestado` WRITE;
/*!40000 ALTER TABLE `glestado` DISABLE KEYS */;
INSERT INTO `glestado` VALUES (1,'Minas Gerais','MG');
/*!40000 ALTER TABLE `glestado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glfilial`
--

DROP TABLE IF EXISTS `glfilial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glfilial` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `RAZAOSOCIAL` varchar(80) DEFAULT NULL,
  `NOMEFANTASIA` varchar(80) DEFAULT NULL,
  `CNPJ` varchar(45) DEFAULT NULL,
  `TELEFONE` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `LOGRADOURO` varchar(80) DEFAULT NULL,
  `NUMERO` varchar(10) DEFAULT NULL,
  `COMPLEMENTO` varchar(20) DEFAULT NULL,
  `CEP` varchar(10) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`),
  CONSTRAINT `FK_GLFILIAL_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glfilial`
--

LOCK TABLES `glfilial` WRITE;
/*!40000 ALTER TABLE `glfilial` DISABLE KEYS */;
INSERT INTO `glfilial` VALUES (1,1,1,'CRC','CRC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2016-02-24 00:00:00','1','2016-02-24 00:00:00'),(1,1,2,'Centro 1','Centro 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2015-03-04 00:00:00','1','2015-03-04 00:00:00'),(1,1,5,'CRC Ipiranga','CRC Ipiranga','12.345.678/9101-11','','crc@gmail.com','','','','','18','2016-05-04 20:39:43','18','2016-05-04 20:39:43'),(1,1,7,'Espaço BH Cidadania - CRAS Petrópoli','CRAS Petrópolis','12.345.687/8888-88','','crc@gmail.com','','','','','18','2016-05-04 20:48:02','18','2016-05-04 20:48:28'),(1,1,8,'Centro Cultural Lindéia Regina','Centro Cultural Lindéia Regina','11.111.111/1111-11','','crc@gmail.com','','','','','18','2016-05-04 20:49:31','18','2016-05-04 20:49:31'),(1,1,10,'Centro De Convivência Cézar Campos','Centro De Convivência Cézar Campos','55.555.555/5555-55','','crc@gkdksk.com','','','','','18','2016-05-04 20:53:52','18','2016-05-04 20:53:52'),(1,1,11,'Centro Cultural Da Vila Marçola','Centro Cultural Da Vila Marçola','54.444.444/4444-44','','daianebjnknj@gmail.com','','','','','18','2016-05-04 20:57:41','18','2016-05-04 20:57:41'),(1,1,12,'Centro De Integração Martinho','Centro De Integração Martinho','77.777.777/7777-77','','crc@gmail.com','','','','','18','2016-05-04 20:59:09','18','2016-05-04 20:59:09');
/*!40000 ALTER TABLE `glfilial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glgrupo`
--

DROP TABLE IF EXISTS `glgrupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glgrupo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODGRUPO` int(11) NOT NULL,
  `NOME` varchar(50) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glgrupo`
--

LOCK TABLES `glgrupo` WRITE;
/*!40000 ALTER TABLE `glgrupo` DISABLE KEYS */;
INSERT INTO `glgrupo` VALUES (1,1,1,'Administrador','1','2016-03-01 10:15:00','1','2016-04-27 14:22:07'),(1,1,6,'Secretaria','1','2016-04-27 14:22:17','1','2016-04-27 14:22:17'),(1,1,7,'Triagem','1','2016-04-27 14:22:30','1','2016-04-27 14:22:30');
/*!40000 ALTER TABLE `glgrupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glgrupolink`
--

DROP TABLE IF EXISTS `glgrupolink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glgrupolink` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODGRUPOLINK` int(11) NOT NULL,
  `CODGRUPO` int(11) NOT NULL,
  `CODLINK` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`,`CODLINK`,`CODGRUPOLINK`,`CODFILIAL`),
  KEY `FK_GLGRUPOLINK_GLLINK_idx` (`CODLINK`),
  CONSTRAINT `FK_GLGRUPOLINK_GLGRUPO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) REFERENCES `glgrupo` (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLGRUPOLINK_GLLINK` FOREIGN KEY (`CODLINK`) REFERENCES `gllink` (`CODLINK`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glgrupolink`
--

LOCK TABLES `glgrupolink` WRITE;
/*!40000 ALTER TABLE `glgrupolink` DISABLE KEYS */;
INSERT INTO `glgrupolink` VALUES (1,1,1,1,1,1,'1','2016-03-01 10:45:01','1','2016-03-01 10:45:01'),(1,1,1,4,1,4,'1','2016-03-01 10:45:01','1','2016-03-01 10:45:01'),(1,1,1,5,1,5,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,6,1,6,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,7,1,7,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,8,1,8,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,9,1,9,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,10,1,10,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,18,1,18,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,19,1,19,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,20,1,20,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,21,1,21,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,22,1,22,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,23,1,23,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,24,1,24,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,26,1,26,'1','2016-03-01 10:45:02','1','2016-03-01 10:45:02'),(1,1,1,27,1,27,'1','2016-03-01 10:45:01','1','2016-03-01 10:45:01'),(1,1,1,28,1,28,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,29,1,29,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,30,1,30,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,31,1,31,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,32,1,32,'1','2016-03-01 10:45:03','1','2016-03-01 10:45:03'),(1,1,1,41,1,41,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,42,1,42,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,43,1,43,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,44,1,44,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,45,1,45,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,46,1,46,'1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(1,1,1,47,1,47,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,48,1,48,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,115,1,49,'1','2016-05-12 17:12:36','1','2016-05-12 17:12:36'),(1,1,1,50,1,50,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,83,1,51,'1','2016-04-27 21:31:56','1','2016-04-27 21:31:56'),(1,1,1,110,1,53,'1','2016-05-12 17:10:30','1','2016-05-12 17:10:30'),(1,1,1,112,1,55,'1','2016-05-12 17:10:47','1','2016-05-12 17:10:47'),(1,1,1,108,1,56,'1','2016-05-12 17:10:20','1','2016-05-12 17:10:20'),(1,1,1,109,1,57,'1','2016-05-12 17:10:26','1','2016-05-12 17:10:26'),(1,1,1,113,1,59,'1','2016-05-12 17:10:51','1','2016-05-12 17:10:51'),(1,1,1,114,1,60,'1','2016-05-12 17:10:56','1','2016-05-12 17:10:56'),(1,1,1,118,1,61,'1','2016-05-31 01:07:04','1','2016-05-31 01:07:04'),(1,1,1,117,1,62,'1','2016-05-31 01:06:57','1','2016-05-31 01:06:57'),(1,1,1,119,1,63,'1','2016-05-31 03:07:13','1','2016-05-31 03:07:13'),(1,1,1,66,6,18,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,67,6,19,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,68,6,26,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,69,6,42,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,70,6,43,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,71,6,48,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,72,7,18,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,73,7,19,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,74,7,26,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,75,7,42,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,76,7,43,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,77,7,44,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,78,7,45,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,79,7,46,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,80,7,48,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00'),(1,1,1,81,7,49,'1','2015-03-04 13:59:00','1','2015-03-04 13:59:00');
/*!40000 ALTER TABLE `glgrupolink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glgrupousuario`
--

DROP TABLE IF EXISTS `glgrupousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glgrupousuario` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODGRUPOUSUARIO` int(11) NOT NULL,
  `CODGRUPO` int(11) NOT NULL,
  `CODUSUARIO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPOUSUARIO`,`CODGRUPO`,`CODUSUARIO`),
  KEY `FK_GLGRUPOUSUARIO_GLUSUARIO_idx` (`CODUSUARIO`),
  KEY `FK_GLGRUPOUSUARIO_GLGRUPO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODGRUPO`),
  CONSTRAINT `FK_GLGRUPOUSUARIO_GLGRUPO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) REFERENCES `glgrupo` (`CODCLIENTE`, `CODEMPRESA`, `CODGRUPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLGRUPOUSUARIO_GLUSUARIO` FOREIGN KEY (`CODUSUARIO`) REFERENCES `glusuario` (`CODUSUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glgrupousuario`
--

LOCK TABLES `glgrupousuario` WRITE;
/*!40000 ALTER TABLE `glgrupousuario` DISABLE KEYS */;
INSERT INTO `glgrupousuario` VALUES (1,1,1,1,1,1,'','0000-00-00 00:00:00','','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `glgrupousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glhelpsystem`
--

DROP TABLE IF EXISTS `glhelpsystem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glhelpsystem` (
  `CODHELPSYSTEM` int(11) NOT NULL AUTO_INCREMENT,
  `CODLINK` int(11) DEFAULT NULL,
  `ID` varchar(45) DEFAULT NULL,
  `TEXTO` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`CODHELPSYSTEM`),
  KEY `FK_GLLINK_GLHELPSYSTEM_idx` (`CODLINK`),
  CONSTRAINT `FK_GLLINK_GLHELPSYSTEM` FOREIGN KEY (`CODLINK`) REFERENCES `gllink` (`CODLINK`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glhelpsystem`
--

LOCK TABLES `glhelpsystem` WRITE;
/*!40000 ALTER TABLE `glhelpsystem` DISABLE KEYS */;
INSERT INTO `glhelpsystem` VALUES (1,NULL,'global.contexto','Neste local você escolhe qual Tele centro deseja trabalhar.'),(4,NULL,'global.infoUsuario','Neste local você pode verificar e alterar suas informações pessoais.'),(5,NULL,'global.buscaMenu','Neste local você pode filtrar as funcionalidades disponíveis.'),(6,NULL,'global.menuLateral','Neste local fica o Menu Lateral. Você pode utilizar o mouse para navegar por ele.'),(7,NULL,'global.404','Bom... Eu so tenho a pedir desculpas... Você não deveria ter que ver essa pagina...'),(8,NULL,'global.404Botao','Aqui você consegue voltar para a tela inicial. Ate porque não tem nada de bom aqui...'),(9,NULL,'global.oriscenter','Logomarca do sistema.');
/*!40000 ALTER TABLE `glhelpsystem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gllink`
--

DROP TABLE IF EXISTS `gllink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gllink` (
  `CODLINK` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(45) NOT NULL,
  `CAMINHO` varchar(80) NOT NULL,
  `DESCRICAO` varchar(80) DEFAULT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `ADMIN` int(1) DEFAULT '1',
  `ORDEM` varchar(12) DEFAULT NULL,
  `ICONEFA` varchar(45) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODLINK`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gllink`
--

LOCK TABLES `gllink` WRITE;
/*!40000 ALTER TABLE `gllink` DISABLE KEYS */;
INSERT INTO `gllink` VALUES (1,'Gestão Global','#','global',1,0,'01.00.00','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(4,'Administração','#','global/administração',1,1,'01.04.00','fa fa-suitcase','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(5,'Cliente','#/global/administracao/clientes/Cclientes','global/administração/clientes',0,1,'01.04.05','fa fa-money','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(6,'Links','#/global/administracao/links/Clinks','global/administração/links',0,1,'01.04.06','fa fa-anchor','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(7,'Empresas','#/global/administracao/empresas/Cempresas','global/administração/empresas',0,1,'01.04.07','fa fa-building','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(8,'Segurança','#','global/segurança',1,0,'01.08.00','fa fa-key','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(9,'Usuários','#/global/seguranca/usuarios/Cusuarios','global/segurança/usuários',1,0,'01.08.01','fa fa-user','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(10,'Grupos','#/global/seguranca/grupos/Cgrupos','global/segurança/grupos',1,0,'01.08.02','fa fa-users','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(18,'Gestão Financeira','#','financeiro',1,0,'03.00.00','fa fa-money','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(19,'Cadastros','#','financeiro/cadastros',1,0,'03.01.00','fa fa-gears','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(20,'Movimentação Bancária','#','financeiro/movimentacaobancaria',0,0,'03.02.00','fa fa-bank','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(21,'Banco','#/financeiro/movimentacaobancaria/banco/Cbanco','financeiro/movimentacaobancaria/bancos',0,0,'03.02.01','fa fa-bank','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(22,'Cartão de Crédito','#/financeiro/movimentacaobancaria/cartaocredito/CcartaoCredito','financeiro/movimentacaobancaria/cartãocrédito',0,0,'03.02.03','fa fa-credit-card','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(23,'Conta Bancária','#/financeiro/movimentacaobancaria/contabancaria/CcontaBancaria','financeiro/movimentacaobancaria/contabancaria',0,0,'03.02.02','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(24,'Status de Acompanhamento','#/financeiro/cadastros/statusfinanceiro/CstatusFinanceiro','financeiro/cadastros/statufinanceiro',0,0,'03.01.01','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(26,'Cliente/Fornecedor','#/financeiro/cadastros/fornecedor/Cfornecedor','financeiro/cadastros/situacaofornecedor',1,0,'03.01.03','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(27,'Formas de Pagamento','#/financeiro/cadastros/formapagamentos/CformaPagamento','financeiro/cadastros/situacaofornecedor',0,0,'03.01.04','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(28,'Contas a receber','#','financeiro/contas/',1,0,'03.03.00','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(29,'Lançamentos','#/financeiro/contas/lancamentos/Clancamentos','financeiro/contas/lancamentos/',0,0,'03.03.02','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(30,'Lançamentos Recorrentes','#/financeiro/contas/lancamentosrecorrentes/ClancamentosRecorrentes','financeiro/contas/lancamentosrecorrentes/',0,0,'03.03.03','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(31,'Previsão de Lançamentos','#/financeiro/contas/previsaolancamentos/CprevisaoLancamentos','financeiro/contas/previsaolancamentos/',0,0,'03.03.04','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(32,'Boletos','#/financeiro/contas/boletos/Cboletos','financeiro/contas/boletos/',0,0,'03.03.05','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(41,'Tele Centros','#/fiscal/cadastros/filiais/Cfiliais','global/administração/Tele Centros',1,0,'01.04.08','fa fa-institution alias','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(42,'Gestão Estoque e Doações','#','estoque',1,0,'04.00.00','fa fa-gift','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(43,'Cadastros','#','estoque/cadastros/',1,0,'04.01.00','fa fa-gears','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(44,'Tipos de Doação','#/estoque/cadastros/tipo/Ctipo','estoque/cadastros/tipo',1,0,'04.01.01','fa fa-filter','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(45,'Campos do Tipo de Doação','#/estoque/cadastros/campostipo/CcamposTipo','estoque/cadastros/camposTipoEntrada',1,0,'04.01.02','fa fa-sitemap','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(46,'Triagem','#/estoque/estoque/triagem/Ctriagem','',1,0,'04.03.01','fa fa-gift','1','2016-04-24 15:02:00','1','2016-05-30 11:50:00'),(47,'Permissões do grupo','#/global/seguranca/grupos/CgrupoLink','global/segurança/Permissao Grupo',1,0,'01.08.03','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(48,'Entrada Doação','#/estoque/secretaria/entrada/CentradaDoacao','',1,0,'04.02.01','fa fa-globe','1','2016-04-24 15:02:00','1','2016-05-12 11:50:00'),(49,'Estoque','#',NULL,1,0,'04.04.00','fa fa-globe','1','2016-04-24 15:02:00','1','2016-05-30 11:50:00'),(50,'Tele centros do usuário','#/global/seguranca/grupos/CusuarioFilial','global/segurança/Usuario filial',1,0,'01.08.04','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(51,'Grupos do Usuário','#/global/seguranca/grupos/CgrupoUsuario','global/segurança/Grupo Usuario',1,0,'01.08.05','fa fa-globe','1','2016-04-24 15:02:00','1','2016-04-24 15:02:00'),(53,'Secretaria','#',NULL,1,0,'04.02.00','fa fa-globe','1','2016-05-12 11:50:00','1','2016-05-12 11:50:00'),(55,'Locação','#/estoque/estoque/locacao/Clocacao',NULL,0,0,'04.04.02','fa fa-globe','1','2016-05-30 11:50:00','1','2016-05-30 11:50:00'),(56,'Componentes do Equipamento','#/estoque/cadastros/componentestipo/CcomponentesTipo',NULL,1,0,'04.01.03','fa fa-globe','1','2016-05-12 11:50:00','1','2016-05-12 11:50:00'),(57,'Componentes para locação','#/estoque/cadastros/componenteslocacao/CcomponentesLocacao',NULL,1,0,'04.01.04','fa fa-globe','1','2016-05-12 11:50:00','1','2016-05-12 11:50:00'),(59,'Venda','#/estoque/estoque/venda/Cvenda',NULL,1,0,'03.03.01','fa fa-globe','1','2016-05-30 11:50:00','1','2016-05-30 11:50:00'),(60,'Doações Externas','#/estoque/estoque/doacaoexterna/CdoacaoExterna',NULL,0,0,'04.04.04','fa fa-globe','1','2016-05-30 11:50:00','1','2016-05-30 11:50:00'),(61,'Doações','#',NULL,1,0,'04.03.00','fa fa-globe','1','2016-05-30 19:46:00','1','2016-05-30 19:46:00'),(62,'Estoque','#/estoque/estoque/Cestoque',NULL,1,0,'04.04.01','fa fa-globe','1','2016-05-30 11:50:00','1','2016-05-30 11:50:00'),(63,'Sucata','#/estoque/estoque/sucata/Csucata',NULL,1,0,'04.04.05','fa fa-globe','1','2016-05-30 22:06:00','1','2016-05-30 22:06:00');
/*!40000 ALTER TABLE `gllink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glmunicipio`
--

DROP TABLE IF EXISTS `glmunicipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glmunicipio` (
  `CODMUNICIPIO` int(11) NOT NULL AUTO_INCREMENT,
  `CODESTADO` int(11) NOT NULL,
  `DESCRICAO` varchar(80) NOT NULL,
  `OBSERVACAO` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`CODMUNICIPIO`),
  KEY `FK_MUNICIPIO_ESTADO_IDX` (`CODESTADO`),
  CONSTRAINT `FK_GLMUNICIPIO_GLESTADO` FOREIGN KEY (`CODESTADO`) REFERENCES `glestado` (`CODESTADO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glmunicipio`
--

LOCK TABLES `glmunicipio` WRITE;
/*!40000 ALTER TABLE `glmunicipio` DISABLE KEYS */;
INSERT INTO `glmunicipio` VALUES (1,1,'Belo Horizonte',NULL);
/*!40000 ALTER TABLE `glmunicipio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glusuario`
--

DROP TABLE IF EXISTS `glusuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glusuario` (
  `CODUSUARIO` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(80) NOT NULL,
  `NOME` varchar(80) DEFAULT NULL,
  `SENHA` varchar(80) NOT NULL,
  `ATIVO` int(1) DEFAULT NULL,
  `CAMINHOFOTO` varchar(200) DEFAULT NULL,
  `CODCLIENTE` int(11) DEFAULT NULL,
  `CODEMPRESA` int(11) DEFAULT NULL,
  `CODFILIAL` int(11) DEFAULT NULL,
  `TIPOUSUARIO` int(11) DEFAULT '4',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODUSUARIO`),
  UNIQUE KEY `INDEX_GLUSUARIO` (`EMAIL`),
  KEY `idx_glusuario_CODUSUARIO_CODCLIENTE` (`CODUSUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glusuario`
--

LOCK TABLES `glusuario` WRITE;
/*!40000 ALTER TABLE `glusuario` DISABLE KEYS */;
INSERT INTO `glusuario` VALUES (1,'mestre@mestre.com.br','Mestre','e10adc3949ba59abbe56e057f20f883e',1,'',1,1,1,1,'1','2016-02-24 00:00:00','1','2016-05-07 15:09:05');
/*!40000 ALTER TABLE `glusuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glusuariofilial`
--

DROP TABLE IF EXISTS `glusuariofilial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glusuariofilial` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODUSUARIOFILIAL` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODUSUARIO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) DEFAULT NULL,
  `REGCRIADOEM` datetime DEFAULT NULL,
  `REGMODIFPOR` varchar(80) DEFAULT NULL,
  `REGMODIFEM` datetime DEFAULT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODUSUARIOFILIAL`),
  KEY `FK_GLUSUARIOFILIAL_FK_GLUSUARIO_idx` (`CODUSUARIO`),
  KEY `FK_GLUSUARIOFILIAL_GLEMPRESA_idx` (`CODCLIENTE`,`CODEMPRESA`),
  CONSTRAINT `FK_GLUSUARIOFILIAL_FK_GLUSUARIO` FOREIGN KEY (`CODUSUARIO`) REFERENCES `glusuario` (`CODUSUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GLUSUARIOFILIAL_GLEMPRESA` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`) REFERENCES `glempresa` (`CODCLIENTE`, `CODEMPRESA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glusuariofilial`
--

LOCK TABLES `glusuariofilial` WRITE;
/*!40000 ALTER TABLE `glusuariofilial` DISABLE KEYS */;
INSERT INTO `glusuariofilial` VALUES (1,1,1,10,1,NULL,NULL,'1','2016-05-07 05:13:41');
/*!40000 ALTER TABLE `glusuariofilial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcamposproduto`
--

DROP TABLE IF EXISTS `trcamposproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcamposproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCAMPOSPRODUTO` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `CODCAMPOSTIPO` int(11) NOT NULL,
  `VALOR` varchar(200) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCAMPOSPRODUTO`),
  KEY `FK_TRCAMPOSPRODUTO_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  KEY `FK_TRCAMPOSPRODUTO_TRCAMPOSTIPO_idx` (`CODCLIENTE`,`CODCAMPOSTIPO`),
  CONSTRAINT `FK_TRCAMPOSPRODUTO_TRCAMPOSTIPO` FOREIGN KEY (`CODCLIENTE`, `CODCAMPOSTIPO`) REFERENCES `trcampostipo` (`CODCLIENTE`, `CODCAMPOSTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRCAMPOSPRODUTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcamposproduto`
--

LOCK TABLES `trcamposproduto` WRITE;
/*!40000 ALTER TABLE `trcamposproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trcamposproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcampostipo`
--

DROP TABLE IF EXISTS `trcampostipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcampostipo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCAMPOSTIPO` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `NOME` varchar(200) NOT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODCAMPOSTIPO`),
  KEY `FK_TRCAMPOSTIPO_TRTIPO_idx` (`CODCLIENTE`,`CODTIPO`),
  CONSTRAINT `FK_TRCAMPOSTIPO_TRTIPO` FOREIGN KEY (`CODCLIENTE`, `CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcampostipo`
--

LOCK TABLES `trcampostipo` WRITE;
/*!40000 ALTER TABLE `trcampostipo` DISABLE KEYS */;
INSERT INTO `trcampostipo` VALUES (1,1,2,'Capacidade',1,'1','2016-05-12 21:50:52','1','2016-05-12 21:50:52'),(1,2,8,'Fabricante',1,'1','2016-05-24 20:11:06','1','2016-05-24 20:11:06'),(1,3,8,'Tipo',1,'1','2016-05-24 20:11:23','1','2016-05-24 20:11:23'),(1,4,3,'Tipo',1,'1','2016-05-25 18:51:46','1','2016-05-25 18:51:46'),(1,5,3,'Capacidade',1,'1','2016-05-25 18:51:58','1','2016-05-25 18:51:58'),(1,6,2,'Fabricante',0,'1','2016-05-26 01:36:41','1','2016-06-01 16:54:58'),(1,7,5,'Fabricante',1,'1','2016-05-31 18:44:10','1','2016-05-31 18:44:10');
/*!40000 ALTER TABLE `trcampostipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcomponentesdalocacao`
--

DROP TABLE IF EXISTS `trcomponentesdalocacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcomponentesdalocacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCOMPONENTESDALOCACAO` int(11) NOT NULL,
  `CODCOMPONENTESLOCACAO` int(11) NOT NULL,
  `CODPRODUTOPAI` int(11) NOT NULL,
  `CODPRODUTOFILHO` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcomponentesdalocacao`
--

LOCK TABLES `trcomponentesdalocacao` WRITE;
/*!40000 ALTER TABLE `trcomponentesdalocacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `trcomponentesdalocacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcomponenteslocacao`
--

DROP TABLE IF EXISTS `trcomponenteslocacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcomponenteslocacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCOMPONENTESLOCACAO` int(11) NOT NULL,
  `CODTIPOPAI` int(11) NOT NULL,
  `CODTIPOFILHO` int(11) NOT NULL,
  `ATIVO` int(11) NOT NULL,
  `OBRIGATORIO` int(11) NOT NULL DEFAULT '0',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODCOMPONENTESLOCACAO`),
  KEY `FK_TRCOMPONENTESLOCACAO_TRTIPO_idx` (`CODCLIENTE`,`CODTIPOPAI`),
  KEY `FK_TRCOMPOENTESLOCACAO_TRTIPOFILHO_idx` (`CODCLIENTE`,`CODTIPOFILHO`),
  CONSTRAINT `FK_TRCOMPOENTESLOCACAO_TRTIPOFILHO` FOREIGN KEY (`CODCLIENTE`, `CODTIPOFILHO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRCOMPONENTESLOCACAO_TRTIPOPAI` FOREIGN KEY (`CODCLIENTE`, `CODTIPOPAI`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcomponenteslocacao`
--

LOCK TABLES `trcomponenteslocacao` WRITE;
/*!40000 ALTER TABLE `trcomponenteslocacao` DISABLE KEYS */;
INSERT INTO `trcomponenteslocacao` VALUES (1,2,1,7,1,1,'1','2016-05-12 22:16:06','1','2016-05-12 22:23:55'),(1,3,1,8,1,1,'1','2016-06-01 23:56:15','1','2016-06-01 23:56:15');
/*!40000 ALTER TABLE `trcomponenteslocacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcomponentesproduto`
--

DROP TABLE IF EXISTS `trcomponentesproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcomponentesproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODCOMPONENTESPRODUTO` int(11) NOT NULL,
  `CODCOMPONENTESTIPO` int(11) NOT NULL,
  `CODPRODUTOPAI` int(11) NOT NULL,
  `CODPRODUTOFILHO` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCOMPONENTESPRODUTO`),
  KEY `FK_COMPONENTESEQUIPAMENTO_EQUIPAMENTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOPAI`),
  KEY `FK_COMPONENTESPRODUTO_COMPONENTESTIPO_idx` (`CODCLIENTE`,`CODCOMPONENTESTIPO`),
  KEY `FK_COMPONENTESPRODUTO_PRODUTO_FILHO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOFILHO`),
  CONSTRAINT `FK_COMPONENTESPRODUTO_COMPONENTESTIPO` FOREIGN KEY (`CODCLIENTE`, `CODCOMPONENTESTIPO`) REFERENCES `trcomponentestipo` (`CODCLIENTE`, `CODCOMPONENTESTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_COMPONENTESPRODUTO_PRODUTO_FILHO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTOFILHO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_COMPONENTESPRODUTO_PRODUTO_PAI` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTOPAI`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcomponentesproduto`
--

LOCK TABLES `trcomponentesproduto` WRITE;
/*!40000 ALTER TABLE `trcomponentesproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trcomponentesproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trcomponentestipo`
--

DROP TABLE IF EXISTS `trcomponentestipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trcomponentestipo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODCOMPONENTESTIPO` int(11) NOT NULL,
  `CODTIPOPAI` int(11) NOT NULL,
  `CODTIPOFILHO` int(11) NOT NULL,
  `OBRIGATORIO` int(11) NOT NULL DEFAULT '0',
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODCOMPONENTESTIPO`),
  KEY `FK_TRCOMPONENTESTIPO_idx` (`CODCLIENTE`,`CODTIPOPAI`),
  KEY `FK_TRCOMPONENTESTIPO_TRTIPOFILHO_idx` (`CODCLIENTE`,`CODTIPOFILHO`),
  CONSTRAINT `FK_TRCOMPONENTESTIPO_TRTIPOFILHO` FOREIGN KEY (`CODCLIENTE`, `CODTIPOFILHO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRCOMPONENTESTIPO_TRTIPOPAI` FOREIGN KEY (`CODCLIENTE`, `CODTIPOPAI`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trcomponentestipo`
--

LOCK TABLES `trcomponentestipo` WRITE;
/*!40000 ALTER TABLE `trcomponentestipo` DISABLE KEYS */;
INSERT INTO `trcomponentestipo` VALUES (1,1,1,3,1,1,'1','2016-05-12 22:43:58','1','2016-05-12 22:43:58'),(1,2,1,2,1,1,'1','2016-05-23 20:19:42','1','2016-05-23 20:19:42'),(1,3,1,5,1,1,'1','2016-05-23 20:19:51','1','2016-05-23 20:19:51'),(1,4,1,4,1,0,'1','2016-05-23 20:19:58','1','2016-05-26 01:40:17');
/*!40000 ALTER TABLE `trcomponentestipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trdoacao`
--

DROP TABLE IF EXISTS `trdoacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trdoacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODDOACAO` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `CODLOTE` int(11) DEFAULT NULL,
  `CODTIPOENTRADA` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL DEFAULT '1',
  `DESCRICAO` varchar(200) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL DEFAULT '1',
  `QUANTIDADETRIADA` int(11) NOT NULL DEFAULT '0',
  `ETIQUETA` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`),
  UNIQUE KEY `INX_TRDOACAO_ETIQUETA` (`ETIQUETA`,`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`),
  KEY `FK_TRDOACAO_FIFORNECEDOR` (`CODCLIENTE`,`CODFORNECEDOR`),
  KEY `FK_TRDOACAO_TRTIPO` (`CODCLIENTE`,`CODTIPO`),
  KEY `FK_TRDOACAO_TRLOTE` (`CODCLIENTE`,`CODLOTE`),
  KEY `FK_TRDOACAO_TRSTATUS_idx` (`CODTRSTATUS`),
  CONSTRAINT `FK_TRDOACAO_FIFORNECEDOR` FOREIGN KEY (`CODCLIENTE`, `CODFORNECEDOR`) REFERENCES `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRDOACAO_GLFILIAL` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRDOACAO_TRLOTE` FOREIGN KEY (`CODCLIENTE`, `CODLOTE`) REFERENCES `trlote` (`CODCLIENTE`, `CODLOTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRDOACAO_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRDOACAO_TRTIPO` FOREIGN KEY (`CODCLIENTE`, `CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trdoacao`
--

LOCK TABLES `trdoacao` WRITE;
/*!40000 ALTER TABLE `trdoacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `trdoacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trlocacao`
--

DROP TABLE IF EXISTS `trlocacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trlocacao` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLOCACAO` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `INTERNA` int(11) NOT NULL COMMENT '1 = INTERNA\n0 = NÃO',
  `CODCOMPONENTESLOCACAO` int(11) DEFAULT NULL,
  `CODFORNECEDOR` int(11) DEFAULT NULL,
  `CODCLIENTELOCADO` int(11) DEFAULT NULL,
  `CODEMPRESALOCADO` int(11) DEFAULT NULL,
  `CODFILIALLOCADO` int(11) DEFAULT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLOCACAO`),
  KEY `FK_TRLOCACAO_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  KEY `FK_TRLOCACAO_TRCOMPONENTESLOCACAO_idx` (`CODCLIENTE`,`CODCOMPONENTESLOCACAO`),
  KEY `FK_TRLOCACAO_TRFORNECEDOR_idx` (`CODCLIENTE`,`CODFORNECEDOR`),
  KEY `FK_TRLOCACAO_GLFILIALLOCACAO_idx` (`CODCLIENTELOCADO`,`CODEMPRESALOCADO`,`CODFILIALLOCADO`),
  CONSTRAINT `FK_TRLOCACAO_GLFILIALLOCACAO` FOREIGN KEY (`CODCLIENTELOCADO`, `CODEMPRESALOCADO`, `CODFILIALLOCADO`) REFERENCES `glfilial` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOCACAO_TRCOMPONENTESLOCACAO` FOREIGN KEY (`CODCLIENTE`, `CODCOMPONENTESLOCACAO`) REFERENCES `trcomponenteslocacao` (`CODCLIENTE`, `CODCOMPONENTESLOCACAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOCACAO_TRFORNECEDOR` FOREIGN KEY (`CODCLIENTE`, `CODFORNECEDOR`) REFERENCES `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOCACAO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trlocacao`
--

LOCK TABLES `trlocacao` WRITE;
/*!40000 ALTER TABLE `trlocacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `trlocacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trlogcomponentesproduto`
--

DROP TABLE IF EXISTS `trlogcomponentesproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trlogcomponentesproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLOGCOMPONENTESPRODUTO` int(11) NOT NULL,
  `CODCOMPONENTESPRODUTO` int(11) NOT NULL,
  `CODPRODUTOFILHO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLOGCOMPONENTESPRODUTO`),
  KEY `FK_TRLOGCOMPONENTESPRODUTO_TRCOMPONENTESPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODCOMPONENTESPRODUTO`),
  KEY `FK_TRLOGCOMPONENTESPRODUTO_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOFILHO`),
  CONSTRAINT `FK_TRLOGCOMPONENTESPRODUTO_TRCOMPONENTESPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCOMPONENTESPRODUTO`) REFERENCES `trcomponentesproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODCOMPONENTESPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOGCOMPONENTESPRODUTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTOFILHO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trlogcomponentesproduto`
--

LOCK TABLES `trlogcomponentesproduto` WRITE;
/*!40000 ALTER TABLE `trlogcomponentesproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trlogcomponentesproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trlogproduto`
--

DROP TABLE IF EXISTS `trlogproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trlogproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODLOGPRODUTO` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL,
  `CODLOCACAO` int(11) DEFAULT NULL,
  `CODVENDA` int(11) DEFAULT NULL,
  `CODSUCATA` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODLOGPRODUTO`),
  KEY `FK_TRLOGPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  KEY `FK_TRLOGPRODUTO_TRSTATUS_idx` (`CODTRSTATUS`),
  CONSTRAINT `FK_TRLOGPRODUTO_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOGPRODUTO_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trlogproduto`
--

LOCK TABLES `trlogproduto` WRITE;
/*!40000 ALTER TABLE `trlogproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trlogproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trlote`
--

DROP TABLE IF EXISTS `trlote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trlote` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODLOTE` int(11) NOT NULL,
  `CODFORNECEDOR` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODLOTE`),
  KEY `FK_TRLOTE_FIFORNECEDOR` (`CODCLIENTE`,`CODFORNECEDOR`),
  KEY `FK_TRLOTE_TRTIPO` (`CODCLIENTE`,`CODTIPO`),
  CONSTRAINT `FK_TRLOTE_FIFORNECEDOR` FOREIGN KEY (`CODCLIENTE`, `CODFORNECEDOR`) REFERENCES `fifornecedor` (`CODCLIENTE`, `CODFORNECEDOR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOTE_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRLOTE_TRTIPO` FOREIGN KEY (`CODCLIENTE`, `CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trlote`
--

LOCK TABLES `trlote` WRITE;
/*!40000 ALTER TABLE `trlote` DISABLE KEYS */;
INSERT INTO `trlote` VALUES (1,2,2,3,'1','2016-05-21 16:03:11','1','2016-05-21 16:03:11'),(1,3,2,3,'1','2016-05-21 16:05:21','1','2016-05-21 16:05:21'),(1,4,2,3,'1','2016-05-21 16:05:55','1','2016-05-21 16:05:55'),(1,5,2,3,'1','2016-05-21 16:07:01','1','2016-05-21 16:07:01'),(1,6,2,3,'1','2016-05-21 16:08:25','1','2016-05-21 16:08:25'),(1,7,1,1,'1','2016-05-21 16:48:35','1','2016-05-21 16:48:35'),(1,8,2,2,'1','2016-05-21 17:04:13','1','2016-05-21 17:04:13'),(1,9,3,8,'1','2016-05-23 00:20:33','1','2016-05-23 00:20:33'),(1,10,3,8,'1','2016-05-23 00:21:34','1','2016-05-23 00:21:34'),(1,11,3,2,'1','2016-05-26 01:21:40','1','2016-05-26 01:21:40'),(1,12,1,3,'1','2016-05-30 14:38:30','1','2016-05-30 14:38:30'),(1,13,3,3,'1','2016-06-02 01:52:57','1','2016-06-02 01:52:57'),(1,14,3,3,'1','2016-06-02 01:53:13','1','2016-06-02 01:53:13'),(1,15,2,3,'1','2016-06-06 22:57:50','1','2016-06-06 22:57:50'),(1,16,2,3,'1','2016-06-06 23:04:04','1','2016-06-06 23:04:04');
/*!40000 ALTER TABLE `trlote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trproduto`
--

DROP TABLE IF EXISTS `trproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trproduto` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODPRODUTO` int(11) NOT NULL,
  `CODDOACAO` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `ETIQUETA` int(11) DEFAULT NULL,
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  UNIQUE KEY `INDEX_TRPRODUTO_ETIQUETA` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`ETIQUETA`),
  KEY `FK_TRPRODUTO_TRDOACAO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`),
  KEY `FK_TRPRODUTO_TRSTATUS_idx` (`CODTRSTATUS`),
  KEY `FK_TRPRODUTO_TRTIPO_idx` (`CODCLIENTE`,`CODTIPO`),
  CONSTRAINT `FK_TREQUIPAMENTO_TRDOACAO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODDOACAO`) REFERENCES `trdoacao` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODDOACAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TREQUIPAMENTO_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRPRODUTO_TRTIPO` FOREIGN KEY (`CODCLIENTE`, `CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trproduto`
--

LOCK TABLES `trproduto` WRITE;
/*!40000 ALTER TABLE `trproduto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trstatus`
--

DROP TABLE IF EXISTS `trstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trstatus` (
  `CODTRSTATUS` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(80) NOT NULL,
  `TIPO` int(11) DEFAULT NULL,
  `DESCTIPO` varchar(45) DEFAULT NULL,
  `UTILIZADOCODIGO` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CODTRSTATUS`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trstatus`
--

LOCK TABLES `trstatus` WRITE;
/*!40000 ALTER TABLE `trstatus` DISABLE KEYS */;
INSERT INTO `trstatus` VALUES (1,'Aguardando Triagem',1,'Triagem','Sim'),(2,'Triagem Finalizada',1,'Triagem','Sim'),(3,'Estoque',2,'Estoque - Sistema','Sim'),(4,'Sucata',5,'Sucata - Sistema','Sim'),(5,'Doação Externa',0,NULL,NULL),(6,'Doação Interna',0,NULL,NULL),(7,'Equipamento Alocado',0,NULL,NULL),(8,'Produto Alocado',0,NULL,'Sim'),(9,'Enviado para Venda',0,NULL,'Sim'),(10,'Vendido',0,NULL,NULL),(11,'Com Defeito',0,NULL,NULL),(12,'Funcionando',0,NULL,NULL),(13,'Em Triagem',1,'Triagem','Sim'),(14,'Cadastro Componente Produto',5,'Triagem - Sistema','Sim'),(15,'Componente vinculado ao produto',5,'Triagem - Sistema','Sim');
/*!40000 ALTER TABLE `trstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trsucata`
--

DROP TABLE IF EXISTS `trsucata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trsucata` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODEMPRESA` int(11) NOT NULL,
  `CODFILIAL` int(11) NOT NULL,
  `CODSUCATA` int(11) NOT NULL,
  `CODTRSTATUS` int(11) NOT NULL,
  `CODDOACAO` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `CODPRODUTO` int(11) DEFAULT NULL,
  `CODPRODUTOPAI` int(11) DEFAULT NULL,
  `REGCRIADOPOR` int(11) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` int(11) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODSUCATA`),
  UNIQUE KEY `IDX_TRSUCATA` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  KEY `FK_TRSUCATA_TRSTATUS_idx` (`CODTRSTATUS`),
  KEY `FK_TRSUCATA_TRDOACAO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODDOACAO`),
  KEY `FK_TRSUCATA_TRPRODUTO_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTO`),
  KEY `FK_TRSUCATA_TRPRODUTOPAI_idx` (`CODCLIENTE`,`CODEMPRESA`,`CODFILIAL`,`CODPRODUTOPAI`),
  KEY `FK_TRSUCATA_TRTIPO_idx` (`CODCLIENTE`,`CODTIPO`),
  CONSTRAINT `FK_TRSUCATA_TRDOACAO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODDOACAO`) REFERENCES `trdoacao` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODDOACAO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRSUCATA_TRPRODUTO` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRSUCATA_TRPRODUTOPAI` FOREIGN KEY (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTOPAI`) REFERENCES `trproduto` (`CODCLIENTE`, `CODEMPRESA`, `CODFILIAL`, `CODPRODUTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRSUCATA_TRSTATUS` FOREIGN KEY (`CODTRSTATUS`) REFERENCES `trstatus` (`CODTRSTATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TRSUCATA_TRTIPO` FOREIGN KEY (`CODCLIENTE`, `CODTIPO`) REFERENCES `trtipo` (`CODCLIENTE`, `CODTIPO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trsucata`
--

LOCK TABLES `trsucata` WRITE;
/*!40000 ALTER TABLE `trsucata` DISABLE KEYS */;
/*!40000 ALTER TABLE `trsucata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trtipo`
--

DROP TABLE IF EXISTS `trtipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trtipo` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODTIPO` int(11) NOT NULL,
  `TIPOENTRADA` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Componente\n2 = Equipamento',
  `COMPONENTEPRODUTO` int(11) NOT NULL DEFAULT '0' COMMENT '1 = é um componentes que forma um produto\n0 = não',
  `COMPONENTELOCACAO` int(11) NOT NULL DEFAULT '0' COMMENT '1 = é um componente que forma uma locação\n0 = não',
  `PRODUTO` int(11) NOT NULL COMMENT '1 = Vira o pai de um produto\n0 = Não',
  `DESCRICAO` varchar(100) NOT NULL,
  `ATIVO` int(11) NOT NULL DEFAULT '1',
  `REGCRIADOPOR` varchar(80) NOT NULL,
  `REGCRIADOEM` datetime NOT NULL,
  `REGMODIFPOR` varchar(80) NOT NULL,
  `REGMODIFEM` datetime NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODTIPO`),
  CONSTRAINT `FK_TRTIPO_GLCLIENTE` FOREIGN KEY (`CODCLIENTE`) REFERENCES `glcliente` (`CODCLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trtipo`
--

LOCK TABLES `trtipo` WRITE;
/*!40000 ALTER TABLE `trtipo` DISABLE KEYS */;
INSERT INTO `trtipo` VALUES (1,1,2,0,0,1,'Gabinete',1,'1','2016-05-12 21:39:58','1','2016-05-26 18:49:55'),(1,2,1,1,0,0,'HD',1,'1','2016-05-12 21:50:17','1','2016-05-12 21:50:17'),(1,3,1,1,0,0,'Memória',1,'1','2016-05-12 22:17:58','1','2016-05-12 22:17:58'),(1,4,1,1,0,0,'Fonte',1,'1','2016-05-12 22:18:10','10','2016-07-02 00:49:02'),(1,5,1,1,0,0,'Placa-Mãe',1,'1','2016-05-12 22:18:19','1','2016-05-12 22:18:19'),(1,6,2,0,0,1,'Impressora',1,'1','2016-05-12 22:22:22','1','2016-05-12 22:22:22'),(1,7,2,0,1,0,'Monitor',1,'1','2016-05-12 22:22:34','1','2016-05-12 22:22:34'),(1,8,1,0,1,0,'Mouse',1,'1','2016-05-12 22:24:47','1','2016-05-12 22:24:47');
/*!40000 ALTER TABLE `trtipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trtipoentrada`
--

DROP TABLE IF EXISTS `trtipoentrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trtipoentrada` (
  `CODCLIENTE` int(11) NOT NULL,
  `CODTIPOENTRADA` int(11) NOT NULL,
  `DESCRICAO` varchar(45) NOT NULL,
  PRIMARY KEY (`CODCLIENTE`,`CODTIPOENTRADA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trtipoentrada`
--

LOCK TABLES `trtipoentrada` WRITE;
/*!40000 ALTER TABLE `trtipoentrada` DISABLE KEYS */;
INSERT INTO `trtipoentrada` VALUES (1,1,'Componente'),(1,2,'Equipamento');
/*!40000 ALTER TABLE `trtipoentrada` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-07 16:21:39
