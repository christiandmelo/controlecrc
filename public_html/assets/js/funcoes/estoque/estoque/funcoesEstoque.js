function verlogComponente($codproduto){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/verLogComponente',
				  'codproduto='+$codproduto,
				  'corpoModalCestoque');
}

function verlogEquipamento($codproduto){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/verLogEquipamento',
				  'codproduto='+$codproduto,
				  'corpoModalCestoque');
}

function modalvendaComponente($codproduto,$coddoacao){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/modalvendaComponente',
				  'codproduto='+$codproduto
				  +'&coddoacao='+$coddoacao
				  +'&tipoentrada=2',
				  'corpoModalCestoque');
}

function sucataComponente($codproduto,$codtipo,$coddoacao){
	var r = confirm("Atenção !\n Confirma enviar o componente para sucata?.");
    if (r == true) {
		AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/SucataComponente/4',
					  'codproduto='+$codproduto
					  	+'&codtipo='+$codtipo
					  	+'&coddoacao='+$coddoacao,
					  'divRetornoComp');
	}
	
}



function modalLocacaoEquipamento($codproduto,$codtrstatus){
	AJajaxCarrega('estoque/estoque/Cestoque/modalLocacao',
				  'codproduto='+$codproduto
				  +'&codtrstatus='+$codtrstatus,
				  'corpoDivCestoque');
}

function modalComponenteExternoEquipamento($codtipo,$codprodutopai,$codcomponenteslocacao,$codantigoprodutofilho,$codcomponentesdalocacao){
	AJajaxCarrega('estoque/estoque/Cestoque/modalComponenteExternoEquipamento',
				  'codtipo='+$codtipo
				  +'&codprodutopai='+$codprodutopai
				  +'&codcomponenteslocacao='+$codcomponenteslocacao
				  +'&codantigoprodutofilho='+$codantigoprodutofilho
				  +'&codcomponentesdalocacao='+$codcomponentesdalocacao,
				  'corpoModalCestoque');
}

function salvarComponenteLocacaoExterna($codprodutopai,$codprodutofilho,$codcomponenteslocacao,$codantigoprodutofilho,$codcomponentesdalocacao){
	AJajaxCarrega('estoque/estoque/Cestoque/salvarComponenteExternoEquipamento',
				  'codprodutopai='+$codprodutopai
				  +'&codprodutofilho='+$codprodutofilho
				  +'&codcomponenteslocacao='+$codcomponenteslocacao
				  +'&codantigoprodutofilho='+$codantigoprodutofilho
				  +'&codcomponentesdalocacao='+$codcomponentesdalocacao,
				  'divRetSalvCompEquipExt');
}

/*function salvarLocacaoEquipamento($tipolocacao,$codproduto){
	AJajaxCarrega('estoque/estoque/Cestoque/salvarLocacaoEquipamento',
				  'codproduto='+$codproduto
				  +'&tipolocacao='+$tipolocacao
				  +'&fornecedor='+$("#txtClienteCtriagemComponenteLocacao").val(),
				  'corpoModalCestoque');
}*/


function listarFornecedores(){
	if($("#txtTipoDoacaoCtriagemComponenteLocacao").val() == "0"){
		AJajaxCarrega('financeiro/cadastros/fornecedor/Cfornecedor/getFornecedoresClientesAtivosOptions',
					  '',
					  'txtClienteCtriagemComponenteLocacao');
		
		$("#divTeleCentroCtriagemComponenteLocacao").attr("style","display:none");
		$("#txtTeleCentroCtriagemComponenteLocacao").removeAttr("data-parsley-required","true");
		$("#txtTeleCentroCtriagemComponenteLocacao").attr("value","");
					  
		$("#divClienteCtriagemComponenteLocacao").removeAttr("style");
		$("#txtClienteCtriagemComponenteLocacao").attr("data-parsley-required","true");
	}else{
		$("#divClienteCtriagemComponenteLocacao").attr("style","display:none");
		$("#txtClienteCtriagemComponenteLocacao").removeAttr("data-parsley-required","true");
		
		$("#divTeleCentroCtriagemComponenteLocacao").removeAttr("style");
		$("#txtTeleCentroCtriagemComponenteLocacao").attr("data-parsley-required","true");
	}
}







