/*função para carregar a tela de novo/editar componente 
 *$etique => só vai estar preenchida quando for uma edição do componente
 *$codproduto => só vai estar preenchida quando for uma edição do componente 
 *$codcomponentestipo => só vai estar preenchida quando for uma inserção do componente pelo equipamento
 *$codcomponentesproduto => só vai estar preenchido se existir o registro na tabela trcomponentesproduto
 * */
function novoEditarComponente($coddoacao,$codtipo,$etiqueta,$codproduto,$codcomponentestipo){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/novoComponente',
				  'coddoacao='+$coddoacao
				  	+'&codproduto='+$codproduto
				  	+'&etiqueta='+$etiqueta
				  	+'&codtipo='+$codtipo
				  	+'&codcomponentestipo='+$codcomponentestipo
					+'&quantidade='+$("#iptQuantidade").val()
					+'&codtipoentrada='+$("#iptCodTipoEntrada").val()
					+'&codprodutopai='+$("#iptCodCodprodutoPai").val()
					+'&quantidadetriada='+$("#iptQuantidadeTriadaVTriagemComponente").val(),
				  'corpoModalCtriagem');
	
}



function sucataComponente($coddoacao,$codtipo){
	$(function() {
		var r = confirm("Atenção !\n Confirma enviar o componente para sucata?.");
	    if (r == true) {
	        AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/SucataComponente',
						  'coddoacao='+$coddoacao
						  	+'&codtipo='+$codtipo
							+'&quantidade='+$("#iptQuantidade").val()
							+'&quantidadetriada='+$("#iptQuantidadeTriadaVTriagemComponente").val(),
						  'divRetornoComp');
	    } else {
			
	    }
	});
	
	
}

function sucataComponenteEquipamento($codproduto,$codtipo,$coddoacao,$codcomponentesproduto){
	var r = confirm("Atenção !\n Confirma enviar o componente para sucata?.");
    if (r == true) {
		AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/SucataComponente/2',
					  'codproduto='+$codproduto
					  	+'&codtipo='+$codtipo
					  	+'&coddoacao='+$coddoacao
					  	+'&codcomponentesproduto='+$codcomponentesproduto,
					  'divRetornoComp');
	}
	
}

function verlogComponente($codproduto){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/verLogComponente',
				  'codproduto='+$codproduto,
				  'corpoModalCtriagem');
}

function verlogEquipamento($codproduto){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/verLogEquipamento',
				  'codproduto='+$codproduto,
				  'corpoModalCtriagem');
}


function verlogComponentesEquipamento($codproduto,$codcomponetesproduto){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/verLogComponentesEquipamento',
				  'codcomponetesproduto='+$codcomponetesproduto+'&codproduto='+$codproduto,
				  'corpoModalCtriagem');
}



function modalTrocaComponenteEquipamento($codcomponentesproduto,$coddoacao,$codtipo,$codprodutofilho,$etiquetaantiga){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/modalComponenteEquipamento',
				  'coddoacao='+$coddoacao
				  	+'&codtipo='+$codtipo
					+'&codprodutofilho='+$codprodutofilho
					+'&codcomponentesproduto='+$codcomponentesproduto
					+'&codantigoetiqueta='+$etiquetaantiga,
				  'corpoModalCtriagem');
}

function trocaComponenteEquipamentoEtiqueta(){
	$varetiquetaAntiga = "";
	if($("#iptCodAntigoEtiqueta").val() != ""){
		$varetiquetaAntiga = $("#iptCodAntigoEtiqueta").val();
	}
	if($("#selComponenteAntigoCtriagemComponente").val() === "1"){
		$("#divRetornoTrocaComponenteEquipamento").html("<br /><div class='form-group'><label class='col-md-3 control-label'>Etiqueta*:</label><div class='col-md-9'><input type='text' class='form-control' value='"+$varetiquetaAntiga+"' name='trocaEtiquetaCtriagemComponente' id='trocaEtiquetaCtriagemComponente' data-parsley-type='number' data-parsley-required='true' ></div></div>");
	}else{
		$("#divRetornoTrocaComponenteEquipamento").html("");
	}
}


function trocarComponenteEquipamento($codnovoproduto,$codprodutofilho,$codcomponentesproduto,$coddoacao,$codtipo){
	var $continuar = true;
	if($("#selComponenteAntigoCtriagemComponente").val() == ""){
		$.gritter.add({
				title : "Alerta!",
				text : "Erro trocar componente. <br /><br />Gentileza escolher um destino para o componente antigo!",
				class_name: "gritter-error",
			});
			$continuar = false;
	}else if($("#selComponenteAntigoCtriagemComponente").val() == 1){
		if($("#trocaEtiquetaCtriagemComponente").val() == ""){
			$.gritter.add({
				title : "Alerta!",
				text : "Erro enviar componente antigo para estoque. <br /><br />Gentileza informar um número de etiqueta!",
				class_name: "gritter-error",
			});
			$continuar = false;
		}
	}
	
	if($continuar){
		AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/trocarComponenteEquipamento',
					  'novocodproduto='+$codnovoproduto
					  	+'&codcomponentesproduto='+$codcomponentesproduto
						+'&codprodutofilho='+$codprodutofilho
						+'&coddoacao='+$coddoacao
						+'&codtipo='+$codtipo
						+'&codexecucaoantigo='+$("#selComponenteAntigoCtriagemComponente").val()
						+'&txtEtiquetaEstoqueCtriagemComponente='+$("#trocaEtiquetaCtriagemComponente").val(),
					  'divRetSalvCompEquip');
	}
}

function modalEstoqueComponentePorEquipamento ($codproduto,$codcomponentesproduto,$etiqueta){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/modalEstoqueComponentePorEquipamento',
				  'codproduto='+$codproduto
				  	+'&codcomponentesproduto='+$codcomponentesproduto
				  	+'&etiqueta='+$etiqueta,
				  'corpoModalCtriagem');
	
}

function trocarEstoqueComponentePorEquipamento($codproduto,$codcomponentesproduto){
	if($("#txtEtiquetaEstoqueCtriagemComponente").val() == ""){
		$.gritter.add({
			title : "Alerta!",
			text : "Erro Enviar componente para estoque. <br /><br />Gentileza informar um número de etiqueta!",
			class_name: "gritter-error",
		});
	}else{
		AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/EstoqueComponentePorEquipamento',
					  'codproduto='+$codproduto
					  	+'&codcomponentesproduto='+$codcomponentesproduto
						+'&etiqueta='+$("#txtEtiquetaEstoqueCtriagemComponente").val(),
					  'divRetSalvEstoqueCompEquip');
	}
}


function sucataGeralEquipamento($codproduto,$coddoacao){
	var $continuar = true;
	$('#formTriagemEquipamento').find(":input[data-componente-equipamento=true]").each(function(){
		if($(this).val() != ""){
			$continuar = false;
		}
	});
	if($continuar){
		var r = confirm("Atenção !\n Confirma enviar o equipamento para sucata?.");
    	if (r == true) {
			AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/SucataGeralEquipamento',
					  'codproduto='+$codproduto
					  +'&coddoacao='+$coddoacao,
					  'divRetornoComp');
		}
	}else{
		$.gritter.add({
			title : "Alerta!",
			text : "Existe componentes vinculados ao equipamento. <br /><br />Gentileza retirar os componentes para gerar a sucata do equipamento!",
			class_name: "gritter-error",
		});
	}
	
}


function modalvendaComponente($codproduto,$coddoacao){
	AJajaxCarrega('estoque/estoque/triagem/CtriagemComponente/modalvendaComponente',
				  'codproduto='+$codproduto
				  +'&coddoacao='+$coddoacao,
				  'corpoModalCtriagem');
}

